//
//  TUFTController.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 17/2/1.
//  Copyright © 2017年 czwen. All rights reserved.
//

#import "TUFTController.h"
#import "TempPermissionUnlockViewController.h"
#import "ZNSFixedViewController.h"

@interface TUFTController (){

    TempPermissionUnlockViewController *tem;
    ZNSFixedViewController *fixedTask;
}

@end

@implementation TUFTController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UISegmentedControl *seg = [[UISegmentedControl alloc] initWithItems:@[@"临时权限开锁",@"固定任务"]];
    seg.frame = CGRectMake(0, 0, YYScreenSize().width * 0.5, 30);
    [seg addTarget:self action:@selector(segmentAction:)forControlEvents:UIControlEventValueChanged];
    [seg setSelectedSegmentIndex:0];
    self.navigationItem.titleView = seg;
    
    tem = [TempPermissionUnlockViewController create];
    [self addChildViewController:tem];
    [self.view addSubview:tem.view];
    
    fixedTask = [ZNSFixedViewController create];
    [self addChildViewController:fixedTask];
    [self.view addSubview:fixedTask.view];
    [self.view sendSubviewToBack:fixedTask.view];
   
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)segmentAction:(UISegmentedControl *)Seg{
    
    if (Seg.selectedSegmentIndex) {
        [self.view bringSubviewToFront:fixedTask.view];
    }else{
        [self.view bringSubviewToFront:tem.view];
    }
}

@end
