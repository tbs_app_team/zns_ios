//
//  HistoryViewController.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 16/1/19.
//  Copyright © 2016 czwen. All rights reserved.
//

#import "HistoryViewController.h"
#import "HistoryTableViewCell.h"

@interface HistoryViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,weak) IBOutlet UITableView *tableView;
- (IBAction)ChoseBeginTime:(id)sender;
- (IBAction)ChoseEndTime:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *BeginTimeBtn;

@property (weak, nonatomic) IBOutlet UIButton *EndTimeBtn;

@property (nonatomic,strong) NSArray *datas;
@property (nonatomic,strong) NSNumber *page;
@property (nonatomic,strong) NSDate * beginTime;
@property (nonatomic,strong) NSDate * endTime;
@end

@implementation HistoryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.tableView registerNib:[HistoryTableViewCell nib] forCellReuseIdentifier:[HistoryTableViewCell reuseIdentifier]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    @weakify(self);
//    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        @strongify(self);
//        [self loadDataWithPage:@0];
//    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self loadDataWithPage:@(self.page.integerValue+1)];
    }];
//    [self.tableView.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadDataWithPage:(NSNumber *)page{
    
    if (!self.beginTime || !self.endTime) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        return;
    }
    
    @weakify(self);
    [APIClient POST:API_HISTORY withParameters:@{@"unlock_at_from":[self.beginTime stringWithFormat:@"yyyyMMddHHmmss"],@"unlock_at_to":[self.endTime stringWithFormat:@"yyyyMMddHHmmss"],@"page":page} successWithBlcok:^(id response) {
        @strongify(self);
        self.page = page;
        if ([page isEqual:@0]) {
            self.datas = [NSArray yy_modelArrayWithClass:[ZNSHistory class] json:[response valueForKey:@"items"]];
        }else{
            self.datas = [self.datas arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSHistory class] json:[response valueForKey:@"items"]]];
        }
        
        if (self.datas.count == 0) {
            [SVProgressHUD showInfoWithStatus:@"没有操作记录"];
        }
        
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        
        if (([[response valueForKey:@"page"] integerValue] + 1) == [[response valueForKey:@"pages"]integerValue]) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }
        [self.tableView reloadData];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
}

#pragma mark
#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[HistoryTableViewCell reuseIdentifier]];
    [cell setHistory:[self.datas objectAtIndex:indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    @weakify(self);
    return [tableView fd_heightForCellWithIdentifier:[HistoryTableViewCell reuseIdentifier] configuration:^(HistoryTableViewCell *cell) {
        @strongify(self);
        [cell setHistory:[self.datas objectAtIndex:indexPath.row]];
    }];
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (NSDate *)extractDate:(NSDate *)date {

    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CH"];
    fmt.dateFormat = @"yyyy.MM.dd";

     NSString *temp = [fmt stringFromDate:date];
    return [fmt dateFromString:temp];
}
- (IBAction)ChoseBeginTime:(id)sender {

    [ActionSheetDatePicker showPickerWithTitle:@"选择开始时间" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date]  minimumDate:nil  maximumDate:[NSDate date] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        

       self.beginTime =[self extractDate:selectedDate];
        
       [self.BeginTimeBtn setTitle:[NSString stringWithFormat:@"操作开始时间:%@",[NSString formatTime:self.beginTime]] forState:UIControlStateNormal];
            
            if (![self.endTime stringWithFormat:@"yyyyMMddHHmmss"] ) {
//                [SVProgressHUD showInfoWithStatus:@"资料尚未齐全"];
            }else{
                NSTimeInterval time = [self.endTime timeIntervalSinceDate:self.beginTime];
                if (time < 0) {
                    [SVProgressHUD showInfoWithStatus:@"开始时间不能大于结束时间"];
                }else{
                    [self loadDataWithPage:@0];
//                    @weakify(self);
//                    [APIClient POST:API_HISTORY withParameters:@{@"unlock_at_from":[self.beginTime stringWithFormat:@"yyyyMMddHHmmss"],@"unlock_at_to":[self.endTime stringWithFormat:@"yyyyMMddHHmmss"],@"page":@0} successWithBlcok:^(id response) {
//                        @strongify(self);
//                        self.page = @0;
//                        self.datas = [NSArray yy_modelArrayWithClass:[ZNSHistory class] json:[response valueForKey:@"items"]];
//                        if (self.datas.count == 0) {
//                            [SVProgressHUD showInfoWithStatus:@"没有操作记录"];
//                        }else{
//                            [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
//                            [self.tableView reloadData];
//                        }
//
//                    } errorWithBlock:^(ZNSError *error) {
//                        [SVProgressHUD showErrorWithStatus:error.errorMessage];
//                    }];
                }
            }
        } cancelBlock:^(ActionSheetDatePicker *picker) {
            
        } origin:self.view];
//        self.beginTime  = [selectedDate stringWithFormat:@"yyyyMMddHHmmss"];
        
 
}

- (IBAction)ChoseEndTime:(id)sender {
    
   [ActionSheetDatePicker showPickerWithTitle:@"选择结束时间" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date]  minimumDate:nil  maximumDate:[NSDate date] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        
//        self.endTime = [selectedDate stringWithFormat:@"yyyyMMddHHmmss"];
       self.endTime =[self extractDate:selectedDate];
      self.endTime = [self.endTime dateByAddingSeconds:86399];
       
        [self.EndTimeBtn setTitle:[NSString stringWithFormat:@"操作结束时间:%@",[NSString formatTime:self.endTime]] forState:UIControlStateNormal];
        
        if (![self.beginTime stringWithFormat:@"yyyyMMddHHmmss"] ) {
            [SVProgressHUD showInfoWithStatus:@"资料尚未齐全"];
        }else{
            NSTimeInterval time = [self.endTime timeIntervalSinceDate:self.beginTime];
            if (time < 0) {
                [SVProgressHUD showInfoWithStatus:@"开始时间不能大于结束时间"];
            }else{
                [self loadDataWithPage:@0];
//                @weakify(self);
//                [APIClient POST:API_HISTORY withParameters:@{@"unlock_at_from":[self.beginTime stringWithFormat:@"yyyyMMddHHmmss"],@"unlock_at_to":[self.endTime stringWithFormat:@"yyyyMMddHHmmss"],@"page":@0} successWithBlcok:^(id response) {
//                    @strongify(self);
//                    self.page = @0;
//                    self.datas = [NSArray yy_modelArrayWithClass:[ZNSHistory class] json:[response valueForKey:@"items"]];
//                    if (self.datas.count == 0) {
//                        [SVProgressHUD showInfoWithStatus:@"没有操作记录"];
//                    }else{
//                        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
//                        [self.tableView reloadData];
//                    }
//
//                } errorWithBlock:^(ZNSError *error) {
//                    [SVProgressHUD showErrorWithStatus:error.errorMessage];
//                }];
            }
            
            
        }
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}
@end
