//
//  WrokBillApplyController.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 17/1/10.
//  Copyright © 2017年 czwen. All rights reserved.
//

#import "WrokBillApplyController.h"
#import "ZNSLabelTextFieldTableViewCell.h"

#import "ObjectChooseViewController.h"

#import "ZNSSection.h"
#import "ZNSDict.h"


@interface WrokBillApplyController (){

    IBOutlet UITableView *tbvApply;
    
    ZNSLabelTextFieldTableViewCell *contentCell;
//    ZNSLabelTextFieldTableViewCell *startTimeCell;
//    ZNSLabelTextFieldTableViewCell *endTimeCell;
//    ZNSLabelTextFieldTableViewCell *timeOutCell;
//    ZNSLabelTextFieldTableViewCell *excuteCell;
//    ZNSLabelTextFieldTableViewCell *baseStopCell;
    ZNSLabelTextFieldTableViewCell *noteCell;
    
    UISwitch *switchIsToGether;
    UISwitch *swtichIsLatAndLng;
    
    
    NSString *limited;
    NSString *oid;
    NSString *oName;
    
    NSString *section_id;
    NSString *section;
    
    NSString *in_time;
    NSString *leave_time;
    
}

@property (nonatomic, strong) NSArray *dicts;
//@property (nonatomic, strong) NSArray *workTypes;//工作类型
@property (nonatomic, strong) NSArray *workLimits;//超长时间
//@property (nonatomic, strong) NSArray *section;
@property (nonatomic, strong) NSMutableArray *sname; //执行单位
@property (nonatomic,strong) NSDate *beginTime;
@property (nonatomic,strong) NSDate *endTime;

@end

@implementation WrokBillApplyController

//- (void)initData{
//    @weakify(self)
//    
//    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
//    
//    
//    [APIClient POST:@"section/" withParameters:@{} successWithBlcok:^(id response) {
//        @strongify(self);
//        self.section = [NSArray yy_modelArrayWithClass:[ZNSSection class] json:[response valueForKey:@"items"]];
//        self.sname = [NSMutableArray array];
//        [self.section  enumerateObjectsUsingBlock:^(ZNSSection* obj, NSUInteger idx, BOOL * _Nonnull stop){
//            //            if ([obj.type isEqualToString:@"区域"]) {
//            //                self.oname = [self.oname arrayByAddingObject:obj];
//            //            }
//            if ([obj.type isEqualToString:@"机构"]) {
//                [self.sname addObject:obj];
//            }
//        }];
//        [self.sname removeObjectAtIndex:0];
//        [SVProgressHUD dismiss];
//    } errorWithBlock:^(ZNSError *error) {
//        [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
//    }];
//    
//    [APIClient POST:@"/dict/" withParameters:@{@"page":@-1} successWithBlcok:^(id response) {
//        @strongify(self)
//        self.dicts = @[];
//        self.dicts = [NSArray yy_modelArrayWithClass:[ZNSDict class] json:[response valueForKey:@"items"]];
//        self.workLimits = @[];
//        self.workTypes = @[];
//        [self.dicts enumerateObjectsUsingBlock:^(ZNSDict *obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            if ([obj.type isEqualToString:@"work_limited"]) {
//                self.workLimits = [self.workLimits arrayByAddingObject:obj];
//            }
//            if ([obj.type isEqualToString:@"work_type"]) {
//                self.workTypes = [self.workTypes arrayByAddingObject:obj];
//            }
//        }];
//        
//    } errorWithBlock:^(ZNSError *error) {
//        [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
//    }];
//    
//}

- (void)doSaveApply{
    
    if (![contentCell.textField.text length]) {
        [SVProgressHUD showErrorWithStatus:@"请输入任务内容"];
        return;
    }
    
    if ([contentCell.textField.text length] > 255) {
        [SVProgressHUD showErrorWithStatus:@"任务内容不得超过255位"];
        return;
    }
    
    if (!self.beginTime) {
        [SVProgressHUD showErrorWithStatus:@"请选择进站时间"];
        return;
    }
    if (!self.endTime) {
        [SVProgressHUD showErrorWithStatus:@"请选择离站时间"];
        return;
    }
    
    if ([noteCell.textField.text length] > 255) {
        [SVProgressHUD showErrorWithStatus:@"备注最多为255位"];
        return;
    }
    
    if (![oName length]) {
        [SVProgressHUD showErrorWithStatus:@"请选择基站"];
        return;
    }

    if (![section length]) {
        [SVProgressHUD showErrorWithStatus:@"请选择执行单位"];
        return;
    }

    [SVProgressHUD show];
    
    ZNSUser *user = [ZNSUser currentUser];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:contentCell.textField.text forKey:@"workdesc"];
    [dic setObject:user.name forKey:@"in_by"];
    [dic setObject:[self.beginTime stringWithFormat:@"yyyyMMddHHmmss"] forKey:@"applyin_at"];
    [dic setObject:[self.endTime stringWithFormat:@"yyyyMMddHHmmss"] forKey:@"applyleave_at"];
    [dic setObject:@(switchIsToGether.on?1:0).stringValue forKey:@"need_accompany"];
    [dic setObject:@(swtichIsLatAndLng.on?1:0).stringValue forKey:@"validate_lng_lat"];
    [dic setObject:limited forKey:@"limited"];
    [dic setObject:user.username forKey:@"mobile"];
    [dic setObject:noteCell.textField.text forKey:@"memo"];
    [dic setObject:section_id forKey:@"sid"];
    [dic setObject:oid forKey:@"oid"];
    
    [APIClient POST:!_isAddWork?API_WORKBILL_APPLY:API_WORKBILL_NEW withParameters:dic successWithBlcok:^(id response) {
         
        [SVProgressHUD showSuccessWithStatus:@"保存成功"];
        [self.navigationController popViewControllerAnimated:YES];
        
    } errorWithBlock:^(ZNSError *error) {
         [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    limited = @"1";
    [tbvApply registerNib:[ZNSLabelTextFieldTableViewCell nib] forCellReuseIdentifier:[ZNSLabelTextFieldTableViewCell reuseIdentifier]];
    
    [self setupCells];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveAction)];
    
//    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)saveAction{

    [self doSaveApply];
    
}

- (void)showTimeSelectedWith:(NSInteger)type{

    @weakify(self);
    
    NSDate *minDate = type?[NSDate date]:(self.beginTime?self.beginTime:[NSDate new]);
    NSDate *maxDate = type?(self.endTime?self.endTime:nil):nil ;
    
    [ActionSheetDatePicker showPickerWithTitle:type?@"选择开始时间":@"选择结束时间" datePickerMode:UIDatePickerModeDateAndTime selectedDate:self.endTime?self.endTime:[NSDate new] minimumDate:minDate maximumDate:maxDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        @strongify(self);
        
        NSDateFormatter * df = [[NSDateFormatter alloc]init];
        df.dateFormat = @"yyyy-MM-dd HH:mm";
        if (type) {
            in_time = [df stringFromDate:selectedDate];
            self.beginTime = selectedDate;
        }else{
            leave_time = [df stringFromDate:selectedDate];
            self.endTime = selectedDate;
        }
        [tbvApply reloadData];
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}

- (void)showLimitedPicker{
//    @weakify(self)
    [ActionSheetStringPicker showPickerWithTitle:@"选择超时时长" rows:@[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
//        @strongify(self);
        limited = selectedValue;
        [tbvApply reloadData];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

- (void)showSnamePicker{
    @weakify(self)
    
    if (self.sname.count < 1) {
        [SVProgressHUD showErrorWithStatus:@"请先选择基站"];
        return;
    }
    
    [ ActionSheetStringPicker showPickerWithTitle:@"选择执行单位" rows:[self.sname valueForKeyPath:@"name"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        section = selectedValue;
        section_id = self.sname[selectedIndex][@"sid"];
        [tbvApply reloadData];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

- (void)showOnamePicker{
    
    ObjectChooseViewController * vc = [ObjectChooseViewController create];
    vc.selectedData = [NSMutableArray arrayWithObject:@""];
    vc.selectedName = [NSMutableArray arrayWithObject:@""];
    vc.isOnlyOneChoice = YES;
    
    @weakify(self);
    vc.selectedBlock = ^(NSArray *objs){
        @strongify(self)
        if (objs.count > 0) {
            ZNSObject *obj = objs[0];
            oid = obj.oid;
            oName = obj.name;
            [tbvApply reloadData];
            
            if (!self.sname) {
                self.sname = [NSMutableArray array];
            }
            [self.sname removeAllObjects];
            [self.sname addObject:@{@"name":obj.area,@"sid":@(obj.sid).stringValue}];
            if ([obj.agent_section length]) {
                [self.sname addObject:@{@"name":obj.agent_section,@"sid":obj.agent_sid}];
            }
            
            if ([section length] || [section_id length]) {
                section = obj.area;
                section_id = @(obj.sid).stringValue;
            }
        }
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)setupCells{

    contentCell = [self createCellWith:@"任务内容" placeholder:@"请输入任务内容"];
    noteCell = [self createCellWith:@"备注" placeholder:@"请输入备注信息"];
    
    switchIsToGether = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 70, 31)];
    swtichIsLatAndLng = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 70, 31)];
    
}

- (ZNSLabelTextFieldTableViewCell *)createCellWith:(NSString *)title placeholder:(NSString *)placeholder{

    ZNSLabelTextFieldTableViewCell *cell = [ZNSLabelTextFieldTableViewCell loadFromNib];
    [cell.label setText:title];
    cell.textField.placeholder = placeholder;
    return cell;
}

- (UITableViewCell *)createNolCellWith:(NSString *)title identifiler:(NSString *)identifier detailText:(NSString *)detailText isIndicator:(BOOL)isIndicator{

    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = title;
    cell.detailTextLabel.text = detailText;
    cell.accessoryType = isIndicator?UITableViewCellAccessoryDisclosureIndicator:UITableViewCellAccessoryNone;
    cell.textLabel.font = contentCell.label.font;
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            return contentCell;
        case 1:{
           return [self createNolCellWith:@"进站时间" identifiler:@"BeginTimeCell" detailText:in_time isIndicator:YES];
        }
        case 2:{
            return [self createNolCellWith:@"离站时间" identifiler:@"EndTimeCell" detailText:leave_time isIndicator:YES];
        }
        case 3:
            return noteCell;
        case 4:{
            return [self createNolCellWith:@"超时时长" identifiler:@"TimeOutCell" detailText:limited isIndicator:YES];
        }
        case 5:{
             return [self createNolCellWith:@"基站" identifiler:@"BaseStopCell" detailText:oName isIndicator:YES];
        }
        case 6:{
              return [self createNolCellWith:@"执行单位" identifiler:@"ExuteCell" detailText:section isIndicator:YES];
        }
        case 7:{
            UITableViewCell *cell = [self createNolCellWith:@"是否需要随工" identifiler:@"isNeedTogetherCell" detailText:@"" isIndicator:NO];;
            cell.accessoryView = switchIsToGether;
            return cell;
        }
        case 8:{
            UITableViewCell *cell = [self createNolCellWith:@"是否需要判断经纬度" identifiler:@"isNeedLatAndLngCell" detailText:@"" isIndicator:NO];
            cell.accessoryView = swtichIsLatAndLng;
            return cell;
        }
        default:
            return nil;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
            case 1:
            case 2:
            [self showTimeSelectedWith:indexPath.row == 1?1:0];
            break;
        case 4:
            [self showLimitedPicker];
            break;
        case 5:
            [self showOnamePicker];
            break;
        case 6:
            [self showSnamePicker];
            break;
        default:
            break;
    }
}

@end
