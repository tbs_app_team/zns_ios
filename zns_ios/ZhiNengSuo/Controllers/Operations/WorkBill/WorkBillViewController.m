//
//  WorkBillViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/19.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "WorkBillViewController.h"
#import "WorkBillTableViewCell.h"
#import "WorkBillAddViewController.h"

#import "WrokBillApplyController.h"

@interface WorkBillViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, assign) NSNumber *page;
@property (nonatomic, strong) NSArray *datas;

@end

@implementation WorkBillViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"新增" style:UIBarButtonItemStylePlain target:self action:@selector(WorkBillAdd)];
    
    [self.tableView registerNib:[WorkBillTableViewCell nib] forCellReuseIdentifier:[WorkBillTableViewCell reuseIdentifier]];
    self.tableView.tableFooterView = [UIView new];
    
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
         @strongify(self);
        [self loadDataWithPage:@0];
    }];
   
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self loadDataWithPage:@(self.page.integerValue+1)];
    }];
}

- (void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    [self.tableView.mj_header beginRefreshing];
}


#pragma mark -- Private
- (void)WorkBillAdd{
    WrokBillApplyController *vc = [WrokBillApplyController create];
    vc.title = @"新增派工";
    vc.isAddWork = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)loadDataWithPage:(NSNumber *)page{
    @weakify(self);
    [APIClient POST:API_WORKBILL_GET withParameters:@{@"page":page} successWithBlcok:^(id response) {
        @strongify(self);
        self.page = page;
        if ([page isEqual:@0]) {
            self.datas = [NSArray yy_modelArrayWithClass:[ZNSWorkBill class] json:[response valueForKey:@"items"]];
        }else{
            self.datas = [self.datas arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSWorkBill class] json:[response valueForKey:@"items"]]];
        }
        
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        
        if (([[response valueForKey:@"page"] integerValue] + 1) == [[response valueForKey:@"pages"]integerValue]) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }
        [self.tableView reloadData];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        
    }];
}

#pragma mark -- Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [tableView fd_heightForCellWithIdentifier:[WorkBillTableViewCell reuseIdentifier] cacheByIndexPath:indexPath configuration:^(WorkBillTableViewCell* cell) {
        cell.workBill = self.datas[indexPath.row];
        cell.excuteBtnHeight.constant = 0;
        cell.returnBtnHeight.constant = 0;
        cell.navigationBtnHeight.constant = 0;
        cell.workBillExcuteBtn.hidden = YES;
        cell.workBilReturnBtn.hidden = YES;
        cell.workBillNavigationBtn.hidden = YES;
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WorkBillTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[WorkBillTableViewCell reuseIdentifier]];
    cell.workBill = self.datas[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.excuteBtnHeight.constant = 0;
    cell.returnBtnHeight.constant = 0;
    cell.navigationBtnHeight.constant = 0;
    cell.workBillExcuteBtn.hidden = YES;
    cell.workBilReturnBtn.hidden = YES;
    cell.workBillNavigationBtn.hidden = YES;
    return cell;
}
@end
