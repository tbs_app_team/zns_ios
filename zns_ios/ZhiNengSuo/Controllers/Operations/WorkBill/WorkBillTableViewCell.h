//
//  WorkBillTableViewCell.h
//  ZhiNengSuo
//
//  Created by ekey on 16/9/19.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSWorkBill.h"

@interface WorkBillTableViewCell : UITableViewCell

@property (nonatomic, strong) ZNSWorkBill *workBill;
@property (nonatomic, copy) voidBlock excuteBlock;
@property (nonatomic, copy) voidBlock returnBlock;
@property (nonatomic, copy) voidBlock navigationBlock;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *excuteBtnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *returnBtnHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *navigationBtnHeight;

@property (weak, nonatomic) IBOutlet UIButton *workBillExcuteBtn;
@property (weak, nonatomic) IBOutlet UIButton *workBilReturnBtn;
@property (strong, nonatomic) IBOutlet UIButton *workBillNavigationBtn;

@end
