//
//  WorkBillTableViewCell.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/19.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "WorkBillTableViewCell.h"
#import "ZNSTimeTools.h"

@interface WorkBillTableViewCell()
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;

@end


@implementation WorkBillTableViewCell



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.workBillExcuteBtn bs_configureAsPrimaryStyle];
    [self.workBilReturnBtn bs_configureAsWarningStyle];
    [self.workBillNavigationBtn bs_configureAsInfoStyle];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setWorkBill:(ZNSWorkBill *)workBill{
    
    [_labels[0] setText: [NSString stringWithFormat:@"%@: %@", @"派工内容",workBill.workdesc]];
    [_labels[1] setText: [NSString stringWithFormat:@"%@: %@", @"派工类型",workBill.worktype]];
    [_labels[2] setText: [NSString stringWithFormat:@"%@: %@小时", @"超时时长",[workBill.limited length]?workBill.limited:@"0"]];
    [_labels[3] setText: [NSString stringWithFormat:@"%@: %@", @"基   站    ",workBill.oname]];
    [_labels[4] setText: [NSString stringWithFormat:@"%@: %@", @"执行单位",workBill.sname]];
    [_labels[5] setText: [NSString stringWithFormat:@"%@: %@", @"进  站 人",workBill.in_by]];
    [_labels[6] setText: [NSString stringWithFormat:@"%@: %@", @"工单状态",workBill.status]];
    [_labels[7] setText: [NSString stringWithFormat:@"%@: %@", @"派工时间",[[ZNSTimeTools shared] timeFomrmatterWith:workBill.apply_at]]];
    
    
}
- (IBAction)excuteBtnOnClick:(UIButton *)sender {
    if (self.excuteBlock) {
        self.excuteBlock();
    }
    
}
- (IBAction)returnBtnOnClick:(id)sender {
    if (self.returnBlock) {
        self.returnBlock();
    }
}
- (IBAction)navigationBtnOnClick:(UIButton *)sender {
    if (self.navigationBlock) {
        self.navigationBlock();
    }
}

@end
