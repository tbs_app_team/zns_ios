//
//  WorkBillAddViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/19.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "WorkBillAddViewController.h"
#import "ZNSLabelTextFieldTableViewCell.h"
#import "ZNSWorkBill.h"
#import "ZNSSection.h"
#import "ZNSDict.h"
#import "ObjectChooseViewController.h"

@interface WorkBillAddViewController ()<UITextViewDelegate>
@property (nonatomic, strong) ZNSWorkBill *workBill;
@property (nonatomic, strong) NSArray *dicts;
@property (nonatomic, strong) NSArray *workTypes;//工作类型
@property (nonatomic, strong) NSArray *workLimits;//超长时间
@property (nonatomic, strong) NSArray *section;
@property (nonatomic, strong) NSMutableArray *sname; //执行单位

@property (weak, nonatomic) IBOutlet UITextView *taskContentTextView;

@property (weak, nonatomic) IBOutlet UITextView *memoTextView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *taskContentTextViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *memoTextViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *workTypeBtn;

@property (weak, nonatomic) IBOutlet UIButton *limitedBtn;

@property (weak, nonatomic) IBOutlet UIButton *snameBtn;

@property (weak, nonatomic) IBOutlet UIButton *onameBtn;

@end

@implementation WorkBillAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _workBill = [ZNSWorkBill new];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(save)];
    self.taskContentTextView.delegate = self;
    self.memoTextView.delegate = self;
    
    [self.workTypeBtn bs_configureAsSuccessStyle];
    [self.limitedBtn bs_configureAsWarningStyle];
    [self.snameBtn bs_configureAsPrimaryStyle];
    [self.onameBtn bs_configureAsInfoStyle];
    [self initData];
}


#pragma mark - TextView delegate

- (void)textViewDidChange:(UITextView *)textView{
    switch (textView.tag) {
        case 201:{
            if (self.taskContentTextView.contentSize.height < 31) {
                self.taskContentTextViewHeight.constant = 31;
            }else if(self.taskContentTextView.contentSize.height <61){
                self.taskContentTextViewHeight.constant = self.taskContentTextView.contentSize.height;
            }else {
                self.taskContentTextViewHeight.constant = 61;
            }
        }
            
            break;
      
        case 202:{
            if (self.memoTextView.contentSize.height < 31) {
                self.memoTextViewHeight.constant = 31;
            }else if(self.memoTextView.contentSize.height <61){
                self.memoTextViewHeight.constant = self.memoTextView.contentSize.height;
            }else {
                self.memoTextViewHeight.constant = 61;
            }
        }
            
            break;
            
        default:
            break;
    }

    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    switch (textView.tag) {
        case 201:{
            if ([text isEqualToString:@"\n"]) {
                [_taskContentTextView resignFirstResponder];
                return NO;
            }
        }
            break;
        case 202:{
            if ([text isEqualToString:@"\n"]) {
                [_memoTextView resignFirstResponder];
                return NO;
            }
            
        }
            break;
            
        default:
            break;
    }
    return YES;
}


#pragma mark -Action
- (IBAction)workTypeBtnClick:(UIButton *)sender {
    [self showWorkTypePicker];
}

- (IBAction)limitedBtnClick:(UIButton *)sender {
    [self showLimitedPicker];
}

- (IBAction)snameBtnClick:(UIButton *)sender {
    [self showSnamePicker];
}

- (IBAction)onameBtnClick:(UIButton *)sender {
    [self showOnamePicker];
}

#pragma mark - Private

- (void)showWorkTypePicker{
    @weakify(self)
    [ ActionSheetStringPicker showPickerWithTitle:@"选择工作类型" rows:[self.workTypes valueForKeyPath:@"name"]initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        self.workBill.worktype = selectedValue;
        self.workBill.worktypeValue = [self.workTypes[selectedIndex] valueForKeyPath:@"value"];
        [self.workTypeBtn setTitle:selectedValue forState:UIControlStateNormal];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
    
}

- (void)showLimitedPicker{
    @weakify(self)
    [ ActionSheetStringPicker showPickerWithTitle:@"选择超时时长" rows:[self.workLimits valueForKeyPath:@"name"]initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        self.workBill.limited = selectedValue;
        self.workBill.limitedValue = [self.workLimits[selectedIndex] valueForKeyPath:@"value"];
        [self.limitedBtn setTitle:selectedValue forState:UIControlStateNormal];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

- (void)showSnamePicker{
    @weakify(self)
    [ ActionSheetStringPicker showPickerWithTitle:@"选择执行单位" rows:[self.sname valueForKeyPath:@"name"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        self.workBill.sname = selectedValue;
        self.workBill.sid = [self.sname[selectedIndex] valueForKeyPath:@"sid"];
        [self.snameBtn setTitle:selectedValue forState:UIControlStateNormal];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

- (void)showOnamePicker{
    
    ObjectChooseViewController * vc = [ObjectChooseViewController create];
    vc.selectedData = [NSMutableArray arrayWithObject:self.workBill.oid==nil? @"":self.workBill.oid];
    vc.selectedName = [NSMutableArray arrayWithObject:self.workBill.sname==nil? @"":self.workBill.sname];
//    vc.isOnlyOneChoice = YES;
    @weakify(self);
    vc.objsBlock = ^(NSArray *arrObjs) {
        @strongify(self);
        self.workBill.oid = [arrObjs objectAtIndex:0];
    };
    vc.nameBlock = ^(NSArray *nameArr){
        @strongify(self)
        if (nameArr.count != 0) {
            self.workBill.oname = [nameArr objectAtIndex:0];
             [self.onameBtn setTitle:[nameArr objectAtIndex:0] forState:UIControlStateNormal];
        }
        
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)initData{
    @weakify(self)
    
    [SVProgressHUD show];
   
    
    [APIClient POST:@"section/" withParameters:@{} successWithBlcok:^(id response) {
        @strongify(self);
        self.section = [NSArray yy_modelArrayWithClass:[ZNSSection class] json:[response valueForKey:@"items"]];
        self.sname = [NSMutableArray array];
        [self.section  enumerateObjectsUsingBlock:^(ZNSSection* obj, NSUInteger idx, BOOL * _Nonnull stop){
//            if ([obj.type isEqualToString:@"区域"]) {
//                self.oname = [self.oname arrayByAddingObject:obj];
//            }
            if ([obj.type isEqualToString:@"机构"]) {
                [self.sname addObject:obj];
            }
        }];
        [self.sname removeObjectAtIndex:0];
        [SVProgressHUD dismiss];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
    
    [APIClient POST:@"/dict/" withParameters:@{@"page":@-1} successWithBlcok:^(id response) {
        @strongify(self)
        self.dicts = @[];
        self.dicts = [NSArray yy_modelArrayWithClass:[ZNSDict class] json:[response valueForKey:@"items"]];
        self.workLimits = @[];
        self.workTypes = @[];
        [self.dicts enumerateObjectsUsingBlock:^(ZNSDict *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.type isEqualToString:@"work_limited"]) {
                self.workLimits = [self.workLimits arrayByAddingObject:obj];
            }
            if ([obj.type isEqualToString:@"work_type"]) {
                self.workTypes = [self.workTypes arrayByAddingObject:obj];
            }
        }];
        
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];

}

- (void)save{
//    if (self.workBill.workdesc==nil) {
//        [SVProgressHUD showErrorWithStatus:@"派工内容不能为空"];
//        return;
//    }
    if ([self.taskContentTextView.text isEqualToString:@""]) {
        [SVProgressHUD showErrorWithStatus:@"派工内容不能为空"];
        return;
    }
    if (self.workBill.worktype==nil) {
        [SVProgressHUD showErrorWithStatus:@"请选择工作类型"];
        return;
    }
    if (self.workBill.limited==nil) {
        [SVProgressHUD showErrorWithStatus:@"请选择超长时间"];
        return;
    }
    if (self.workBill.sname==nil) {
        [SVProgressHUD showErrorWithStatus:@"请选择执行机构"];
        return;
    }
    if (self.workBill.oname==nil) {
        [SVProgressHUD showErrorWithStatus:@"请选择基站"];
        return;
    }
    
    [APIClient POST:API_WORKBILL_NEW withParameters:@{@"worktype":self.workBill.worktypeValue,
                                                      @"workdesc":self.taskContentTextView.text,
                                                      @"oid":self.workBill.oid,
                                                      @"sid":self.workBill.sid,
                                                      @"limited":self.workBill.limitedValue,
                                                      @"memo":self.memoTextView.text == nil? @"":self.memoTextView.text} successWithBlcok:^(id response) {
                                                          [SVProgressHUD showInfoWithStatus:@"新增派工单成功"];
                                                          [self.navigationController popViewControllerAnimated:YES];
        
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
    
}
@end
