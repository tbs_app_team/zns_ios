//
//  WorkBillApprovalController.m
//  ZhiNengSuo
//
//  Created by Chuenlu Kuo on 17/1/12.
//  Copyright © 2017年 czwen. All rights reserved.
//

#import "WorkBillApprovalController.h"
#import "WorkBillTableViewCell.h"

@interface WorkBillApprovalController (){

    IBOutlet UITableView *tbvApproval;
    
    NSNumber *_page;
    NSArray *datas;
}

@end

@implementation WorkBillApprovalController

- (void)loadDataWithPage:(NSNumber *)page{
//    @weakify(self);
    [APIClient POST:API_WORKBILL_GET withParameters:@{@"page":page,@"status":@[@"待审批"]} successWithBlcok:^(id response) {
//        @strongify(self);
        _page = page;
        if ([page isEqual:@0]) {
            datas = [NSArray yy_modelArrayWithClass:[ZNSWorkBill class] json:[response valueForKey:@"items"]];
        }else{
            datas = [datas arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSWorkBill class] json:[response valueForKey:@"items"]]];
        }
        
        [tbvApproval.mj_header endRefreshing];
        [tbvApproval.mj_footer endRefreshing];
        
        if (([[response valueForKey:@"page"] integerValue] + 1) == [[response valueForKey:@"pages"]integerValue]) {
            [tbvApproval.mj_footer endRefreshingWithNoMoreData];
        }
        [tbvApproval reloadData];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
        [tbvApproval.mj_header endRefreshing];
        [tbvApproval.mj_footer endRefreshing];
        
    }];
}

- (void)doVerifyWith:(NSString *)sid limited:(NSString *)limited note:(NSString *)note isVerify:(BOOL)isVerify{

    [SVProgressHUD show];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:isVerify?@"批准":@"拒绝" forKey:@"action"];
    
    if (!isVerify) {
        [dic setObject:note forKey:@"memo"];
    }else{
        [dic setObject:limited forKey:@"limited"];
    }
    [APIClient POST:API_WORKBILL_VERIFY(sid) withParameters:dic successWithBlcok:^(id response) {
        //        @strongify(self);
        NSLog(@"===>%@",response);
        [SVProgressHUD dismiss];
        [tbvApproval.mj_header beginRefreshing];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
        
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [tbvApproval registerNib:[WorkBillTableViewCell nib] forCellReuseIdentifier:[WorkBillTableViewCell reuseIdentifier]];
    tbvApproval.tableFooterView = [UIView new];
    
    @weakify(self);
    tbvApproval.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self loadDataWithPage:@0];
    }];
    
    tbvApproval.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self loadDataWithPage:@(_page.integerValue+1)];
    }];
    
    [tbvApproval.mj_header beginRefreshing];
    
    tbvApproval.rowHeight = UITableViewAutomaticDimension;
    tbvApproval.estimatedRowHeight = 300;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)rejectWorkBillWith:(NSString *)skid{

    UIAlertController * ac = [UIAlertController alertControllerWithTitle:@"拒绝理由" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [ac addTextFieldWithConfigurationHandler:^(UITextField *textField){
        textField.placeholder = @"请填写拒绝理由";
        
    }];
    [ac addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    }]];
    
    @weakify(self)
    [ac addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UITextField *tf = ac.textFields.firstObject;
        if (tf.text.length == 0) {
            [SVProgressHUD  showErrorWithStatus:@"输入内容不能为空"];
            return ;
        }
        if ([[NSString isEmpty:tf.text]isEqualToString:@"yes"]){
            [SVProgressHUD showInfoWithStatus:@"输入内容不能为空"];
        }
        if (tf.text.length > 32) {
            [SVProgressHUD  showErrorWithStatus:@"输入长度应不超过32位"];
            return ;
        }
        @strongify(self)
        [self doVerifyWith:skid limited:@"0" note:tf.text isVerify:NO];
    }]];
    [self presentViewController:ac animated:YES completion:nil];
}


- (void)showLimitedPickerWith:(NSString *)skid{
        @weakify(self)
    [ActionSheetStringPicker showPickerWithTitle:@"选择超时时长" rows:@[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                @strongify(self);
        [self doVerifyWith:skid limited:selectedValue note:@"" isVerify:YES];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

#pragma mark -- Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return datas.count;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return [tableView fd_heightForCellWithIdentifier:[WorkBillTableViewCell reuseIdentifier] cacheByIndexPath:indexPath configuration:^(WorkBillTableViewCell* cell) {
//        cell.workBill = datas[indexPath.row];
//        cell.excuteBtnHeight.constant = 30;
//        cell.returnBtnHeight.constant = 30;
//        cell.navigationBtnHeight.constant = 0;
//        cell.workBillExcuteBtn.hidden = NO;
//        cell.workBilReturnBtn.hidden = NO;
//        cell.workBillNavigationBtn.hidden = YES;
//    }];
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WorkBillTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[WorkBillTableViewCell reuseIdentifier] forIndexPath:indexPath];
    ZNSWorkBill *model = datas[indexPath.row];
    cell.workBill = model;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.excuteBtnHeight.constant = 30;
    cell.returnBtnHeight.constant = 30;
    cell.navigationBtnHeight.constant = 0;
    cell.workBillExcuteBtn.hidden = NO;
    cell.workBilReturnBtn.hidden = NO;
    cell.workBillNavigationBtn.hidden = YES;
    
    [cell.workBillExcuteBtn setTitle:@"批准" forState:UIControlStateNormal];
    [cell.workBilReturnBtn setTitle:@"拒绝" forState:UIControlStateNormal];
    
    @weakify(self)
    cell.excuteBlock = ^{
         @strongify(self)
        NSLog(@"excuteBlock");
        [self showLimitedPickerWith:model.skid];
    };
  
    cell.returnBlock = ^{
        @strongify(self)
        [self rejectWorkBillWith:model.skid];
    };
    
    return cell;
}


@end
