//
//  MyWorkViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/21.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "MyWorkViewController.h"
#import "WorkBillTableViewCell.h"
#import "APIClient.h"
#import "OnlineAuthenticationOpenBtLockViewController.h"

#import <MapKit/MapKit.h>

@interface MyWorkViewController ()<UITableViewDelegate, UITableViewDataSource>{

    NSString *status;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray * delayDataSource;
@property (nonatomic, strong) NSArray * excuteDataSource;
@property (nonatomic, strong) NSArray * finishDataSource;
@property (nonatomic, assign) NSInteger segmentIndex;
@property (nonatomic, strong) NSNumber* page;


@end

@implementation MyWorkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.delayDataSource = @[];
    self.excuteDataSource = @[];
    self.finishDataSource = @[];
    self.segmentIndex = 0;
    status = @"新单";
    [self.tableView registerNib:[WorkBillTableViewCell nib] forCellReuseIdentifier:[WorkBillTableViewCell reuseIdentifier]];
//    self.tableView.tableFooterView = [UIView new];
    
     self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
         [self loadDataWithPage:@0];
     }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [self loadDataWithPage:@(self.page.integerValue+1)];
    }];
    [self.tableView.mj_header beginRefreshing]; 
}

#pragma mark--Action

- (IBAction)segmentValueChanged:(UISegmentedControl *)sender {
    self.segmentIndex = sender.selectedSegmentIndex;
//    NSLog(@"%ld",sender.selectedSegmentIndex);
    [self.tableView reloadData];
    [self.tableView.mj_header beginRefreshing];
}



#pragma mark - tableView Delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    WorkBillTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[WorkBillTableViewCell reuseIdentifier]];
    
    if (self.segmentIndex == 0) {
        
        cell.workBill = self.delayDataSource[indexPath.row];
        [cell.workBillExcuteBtn setTitle:@"接单" forState:UIControlStateNormal];
        cell.workBilReturnBtn.hidden = YES;
        cell.workBillExcuteBtn.hidden = NO;
        cell.workBillNavigationBtn.hidden = YES;
        cell.excuteBtnHeight.constant = 30;
        cell.returnBtnHeight.constant = 30;
        cell.navigationBtnHeight.constant = 30;
        
        @weakify(self)
        cell.excuteBlock = ^{
            @strongify(self)
            [self delayWorkExcute:self.delayDataSource[indexPath.row] index:indexPath];
        };

    }else if(self.segmentIndex == 1){
       
        ZNSWorkBill *bill = self.excuteDataSource[indexPath.row];
        cell.workBill = bill;//self.excuteDataSource[indexPath.row];

        [cell.workBillExcuteBtn setTitle:@"执行" forState:UIControlStateNormal];
        [cell.workBilReturnBtn setTitle:@"回单" forState:UIControlStateNormal];
        [cell.workBillNavigationBtn setTitle:@"导航" forState:UIControlStateNormal];
        cell.workBilReturnBtn.hidden = NO;
        cell.workBillExcuteBtn.hidden = NO;
        cell.workBillNavigationBtn.hidden = NO;
        cell.excuteBtnHeight.constant = 30;
        cell.returnBtnHeight.constant = 30;
        cell.navigationBtnHeight.constant = 30;
        cell.excuteBlock = ^{
            NSLog(@"excuteBlock");
            [self excuteWorkExcute:bill];
        };
        @weakify(self)
        cell.returnBlock = ^{
            @strongify(self)
            [self backWork:bill];
        };
        
        cell.navigationBlock = ^{
            @strongify(self)
            [self navigationBaseStationWith:bill];
        };

    }else if (self.segmentIndex == 2){ 
        
        cell.workBill = self.finishDataSource[indexPath.row];
        cell.workBilReturnBtn.hidden = YES;
        cell.workBillExcuteBtn.hidden = YES;
        cell.workBillNavigationBtn.hidden = YES;
        cell.navigationBtnHeight.constant = 0;
        cell.excuteBtnHeight.constant = 0;
        cell.returnBtnHeight.constant = 0;
        [self.view layoutIfNeeded];
    }
       return cell;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (self.segmentIndex == 0) {
        return self.delayDataSource.count;
    }else if (self.segmentIndex == 1){
        return self.excuteDataSource.count;
    }else {
        return self.finishDataSource.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [tableView fd_heightForCellWithIdentifier:[WorkBillTableViewCell reuseIdentifier] cacheByIndexPath:indexPath configuration:^(WorkBillTableViewCell* cell) {
        if (self.segmentIndex == 0) {
            [cell.workBillExcuteBtn setTitle:@"接单" forState:UIControlStateNormal];
            cell.workBilReturnBtn.hidden = YES;
            cell.workBillExcuteBtn.hidden = NO;
            cell.workBillNavigationBtn.hidden = YES;
            cell.excuteBtnHeight.constant = 30;
            cell.returnBtnHeight.constant = 30;
            cell.navigationBtnHeight.constant = 30;
        }else if(self.segmentIndex == 1){
            [cell.workBillExcuteBtn setTitle:@"执行" forState:UIControlStateNormal];
            [cell.workBilReturnBtn setTitle:@"回单" forState:UIControlStateNormal];
             [cell.workBillNavigationBtn setTitle:@"导航" forState:UIControlStateNormal];
            cell.workBilReturnBtn.hidden = NO;
            cell.workBillExcuteBtn.hidden = NO;
            cell.workBillNavigationBtn.hidden = NO;
            cell.navigationBtnHeight.constant = 30;
            cell.excuteBtnHeight.constant = 30;
            cell.returnBtnHeight.constant = 30;
        }else if (self.segmentIndex == 2){
            cell.workBilReturnBtn.hidden = YES;
            cell.workBillExcuteBtn.hidden = YES;
            cell.workBillNavigationBtn.hidden = YES;
            cell.excuteBtnHeight.constant = 0;
            cell.returnBtnHeight.constant = 0;
             cell.navigationBtnHeight.constant = 0;
            [self.view layoutIfNeeded];
        }
    }];
}
#pragma mark -- Private

- (void)loadDataWithPage:(NSNumber *)page{
    if (self.segmentIndex == 0) {
        @weakify(self);
        [APIClient POST:API_WORKBILL_GET withParameters:@{@"page":page,@"status":@[@"新单"],@"sid":[ZNSUser currentUser].sid} successWithBlcok:^(id response) {
            @strongify(self);
            self.page = page;
            if ([page isEqual:@0]) {
                self.delayDataSource = [NSArray yy_modelArrayWithClass:[ZNSWorkBill class] json:[response valueForKey:@"items"]];
                if (self.delayDataSource.count == 0) {
                    [SVProgressHUD showInfoWithStatus:@"暂无数据"];
                }
            }else{
                self.delayDataSource = [self.delayDataSource arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSWorkBill class] json:[response valueForKey:@"items"]]];
            }
            
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
            
            if (([[response valueForKey:@"page"] integerValue] + 1) == [[response valueForKey:@"pages"]integerValue]) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            [self.tableView reloadData];
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:error.errorMessage];
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
            
        }];

    }else if(self.segmentIndex == 1){
        @weakify(self);
        [APIClient POST:API_WORKBILL_GET withParameters:@{@"page":page,@"status":@[@"接单"],@"rec_username":[ZNSUser currentUser].username} successWithBlcok:^(id response) {
            @strongify(self);
            self.page = page;
            if ([page isEqual:@0]) {
                self.excuteDataSource = [NSArray yy_modelArrayWithClass:[ZNSWorkBill class] json:[response valueForKey:@"items"]];
                if (self.excuteDataSource.count == 0) {
                    [SVProgressHUD showInfoWithStatus:@"暂无数据"];
                }
            }else{
                self.excuteDataSource = [self.excuteDataSource arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSWorkBill class] json:[response valueForKey:@"items"]]];
            }
            
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
            
            if (([[response valueForKey:@"page"] integerValue] + 1) == [[response valueForKey:@"pages"]integerValue]) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            [self.tableView reloadData];
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:error.errorMessage];
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
            
        }];

    }else{
        @weakify(self);
        [APIClient POST:API_WORKBILL_GET withParameters:@{@"page":page,@"status":@[@"回单"],@"rec_username":[ZNSUser currentUser].username} successWithBlcok:^(id response) {
            @strongify(self);
            self.page = page;
            if ([page isEqual:@0]) {
                self.finishDataSource = [NSArray yy_modelArrayWithClass:[ZNSWorkBill class] json:[response valueForKey:@"items"]];
                if (self.finishDataSource.count == 0) {
                    [SVProgressHUD showInfoWithStatus:@"暂无数据"];
                }
            }else{
                self.finishDataSource = [self.finishDataSource arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSWorkBill class] json:[response valueForKey:@"items"]]];
            }
            
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
            
            if (([[response valueForKey:@"page"] integerValue] + 1) == [[response valueForKey:@"pages"]integerValue]) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            [self.tableView reloadData];
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:error.errorMessage];
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
            
        }];
    }

}

- (void)delayWorkExcute:(ZNSWorkBill*)workBill index:(NSIndexPath*)index{
    [SVProgressHUD show];
    [APIClient POST:API_WORKBILL_UPDATE(workBill.skid) withParameters:@{@"status":@[@"接单"]} successWithBlcok:^(id response) {
        [SVProgressHUD showSuccessWithStatus:@"接单成功"];
        NSMutableArray *temArr = [NSMutableArray arrayWithArray:self.delayDataSource];
        [temArr removeObject:workBill];
        self.delayDataSource = [NSArray arrayWithArray:temArr];
        [self.tableView reloadData];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
}

- (void)backWork:(ZNSWorkBill*)workBill{
    UIAlertController * ac = [UIAlertController alertControllerWithTitle:@"回单" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [ac addTextFieldWithConfigurationHandler:^(UITextField *textField){
        textField.placeholder = @"工作已完成";
        
    }];
    [ac addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    }]];
    [ac addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UITextField *tf = ac.textFields.firstObject;
        if (tf.text.length == 0) {
            [SVProgressHUD  showErrorWithStatus:@"输入内容不能为空"];
            return ;
        }
        if ([[NSString isEmpty:tf.text]isEqualToString:@"yes"]){
            [SVProgressHUD showInfoWithStatus:@"输入内容不能为空"];
        }
        if (tf.text.length > 32) {
            [SVProgressHUD  showErrorWithStatus:@"输入长度应不超过32位"];
            return ;
        }
        
        [SVProgressHUD show];
        @weakify(self);
        [APIClient POST:API_WORKBILL_RETURN(workBill.skid) withParameters:@{@"status":@[@"回单"],@"backmemo":tf.text} successWithBlcok:^(id response) {
            @strongify(self);
            [SVProgressHUD showSuccessWithStatus:@"回单成功"];
            NSMutableArray *temArr = [NSMutableArray arrayWithArray:self.excuteDataSource];
            [temArr removeObject:workBill];
            self.excuteDataSource = [NSArray arrayWithArray:temArr];
            [self.tableView reloadData];
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:error.errorMessage];
        }];
    }]];
    [self presentViewController:ac animated:YES completion:nil];

}

- (void)excuteWorkExcute:(ZNSWorkBill*)workBill{
    OnlineAuthenticationOpenBtLockViewController *vc =[OnlineAuthenticationOpenBtLockViewController create];
    vc.mode = AuthenticationControllerModeWorkBill;
    vc.workBillLocks = (workBill.lockset == nil || workBill.lockset == NULL || [workBill isEqual:[NSNull null]]) ? workBill.object.lockset : workBill.lockset;
    vc.skid = workBill.skid;
    [self.navigationController pushViewController:vc animated:YES];
}


//导航到基站
- (void)navigationBaseStationWith:(ZNSWorkBill *)bill{

    NSLog(@"导航。。。。");
    
    CLLocationCoordinate2D loc = CLLocationCoordinate2DMake([bill.object.lat floatValue], [bill.object.lng floatValue]);
    MKMapItem *currentLocation = [MKMapItem mapItemForCurrentLocation];
    MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:loc addressDictionary:nil]];
    [MKMapItem openMapsWithItems:@[currentLocation, toLocation]
                   launchOptions:@{MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving,
                                   MKLaunchOptionsShowsTrafficKey: [NSNumber numberWithBool:YES]}];
    
}

@end
