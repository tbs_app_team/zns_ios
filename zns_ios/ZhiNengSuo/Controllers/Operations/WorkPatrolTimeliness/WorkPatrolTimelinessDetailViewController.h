//
//  WorkPatrolTimelinessDetailViewController.h
//  ZhiNengSuo
//
//  Created by ekey on 16/9/26.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSWorkPatrolTimeliness.h"

@interface WorkPatrolTimelinessDetailViewController : UIViewController
@property (nonatomic, strong) ZNSWorkPatrolTimeliness *myWorkPatrolTimeliness;
@end
