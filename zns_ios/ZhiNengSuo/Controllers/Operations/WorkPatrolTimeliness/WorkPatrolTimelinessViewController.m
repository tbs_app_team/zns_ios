//
//  WorkPatrolTimelinessViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/22.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "WorkPatrolTimelinessViewController.h"
#import "ZNSWorkPatrolTimeliness.h"
#import "WorkPatrolTimelinessTableViewCell.h"
#import "WorkPatrolTimelinessDetailViewController.h"
#import "JKAlertDialog.h"
#import "ActionSheetDatePicker.h"
#import "WorkQueryTableViewController.h"

@interface WorkPatrolTimelinessViewController ()<UITableViewDelegate,UITableViewDataSource>{
    
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, assign) NSNumber *page;
@property (nonatomic, strong) NSArray *sourceDatas;
@property (nonatomic, strong)  JKAlertDialog *dialog;
@property (nonatomic, strong) NSDictionary *dicPara;



@end

@implementation WorkPatrolTimelinessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[WorkPatrolTimelinessTableViewCell nib] forCellReuseIdentifier:[WorkPatrolTimelinessTableViewCell reuseIdentifier]];
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"筛选" style:UIBarButtonItemStylePlain target:self action:@selector(query)];
    
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self loadDataWithPage:@0 withDicPara:self.dicPara];
    }];
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self loadDataWithPage:@(self.page.integerValue+1) withDicPara:self.dicPara];
    }];
    
    [self.tableView.mj_header beginRefreshing];

}

#pragma mark -- scrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGSize temp = self.tableView.contentSize;
    // 2. 给这个变量赋值。因为变量都是L-Value，可以被赋值
    temp.width =634.0;
    // 3. 修改frame的值
    self.tableView.contentSize = temp;
}
#pragma mark - tableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.sourceDatas.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 61.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    UIView *view = [[NSBundle mainBundle] loadNibNamed:@"WorkPatrolTimelinessTitleView" owner:self options:nil].firstObject;
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WorkPatrolTimelinessTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[WorkPatrolTimelinessTableViewCell reuseIdentifier]];
    cell.workPatrolTimeliness = self.sourceDatas[indexPath.row];
    cell.detailBlock = ^{
//        NSLog(@"detail");
        [self detailController:self.sourceDatas[indexPath.row]];
    };
    return cell;
   
}

#pragma mark - Private

- (void)detailController:(ZNSWorkPatrolTimeliness *)workPatrol{
    WorkPatrolTimelinessDetailViewController *vc = [[WorkPatrolTimelinessDetailViewController alloc] init];
    vc.myWorkPatrolTimeliness = workPatrol;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)loadDataWithPage:(NSNumber *)page withDicPara:(NSDictionary *)dic{
    NSDictionary *para = [NSDictionary dictionary];
    @weakify(self);
    if (dic == nil) {
        para = @{@"page":page,
                 @"year":[NSString formatTimeWithFormat:[NSDate date] format:@"yyyy"]};
    }else{
        para = dic;
    }
    [APIClient POST:API_WORKPATROL_FINISH withParameters:para
   successWithBlcok:^(id response) {
       @strongify(self);
       self.page = page;
       if ([page isEqual:@0]) {
           self.sourceDatas = [NSArray yy_modelArrayWithClass:[ZNSWorkPatrolTimeliness class] json:[response valueForKey:@"items"]];
       }else{
           self.sourceDatas = [self.sourceDatas arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSWorkPatrolTimeliness class] json:[response valueForKey:@"items"]]];
       }
       
       [self.tableView.mj_header endRefreshing];
       [self.tableView.mj_footer endRefreshing];
       
       if (([[response valueForKey:@"page"] integerValue] + 1) == [[response valueForKey:@"pages"]integerValue]) {
           [self.tableView.mj_footer endRefreshingWithNoMoreData];
       }
       [self.tableView reloadData];
   } errorWithBlock:^(ZNSError *error) {
       [SVProgressHUD showErrorWithStatus:error.errorMessage];
       [self.tableView.mj_header endRefreshing];
       [self.tableView.mj_footer endRefreshing];
       
   }];
}

- (void)query{

    WorkQueryTableViewController *vc = [[WorkQueryTableViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
    @weakify(self)
    [vc setDicblock:^(NSDictionary *dic){
        NSLog(@"%@",dic);
        @strongify(self)
        self.dicPara = dic;
        [self.tableView.mj_header beginRefreshing];
    }];
}
@end
