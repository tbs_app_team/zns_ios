//
//  WorkPatrolQueryTableViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/27.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "WorkQueryTableViewController.h"
#import "ZNSLabelTextFieldTableViewCell.h"
#import "ZNSSection.h"

@interface WorkQueryTableViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSArray *sections;
    NSArray *agencys;
    NSInteger selectedAgencyIndex;
   
}
@property (nonatomic, strong)  UITableView *tableView;
@end

@implementation WorkQueryTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRGB:0xefeff4 alpha:1.0];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 340) style:UITableViewStyleGrouped];
    [self.tableView registerNib:[ZNSLabelTextFieldTableViewCell nib] forCellReuseIdentifier:[ZNSLabelTextFieldTableViewCell reuseIdentifier]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(20,360, self.view.frame.size.width-40, 44)];
    [btn bs_configureAsPrimaryStyle];
    [btn setTitle:@"查询" forState:UIControlStateNormal];
    [self.view addSubview:btn];
    [btn setTarget:self action:@selector(query) forControlEvents:UIControlEventTouchUpInside];
    
    self.queryDictionary = [NSMutableDictionary dictionary];
    [self initData];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZNSLabelTextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ZNSLabelTextFieldTableViewCell reuseIdentifier]];
    @weakify(self);
    switch (indexPath.row) {
        case 0:{
            [cell.label setText:@"年    份"];
            [cell.textField setPlaceholder:@"请选择"];
            cell.textField.text = [self.queryDictionary valueForKey:@"year"];
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.showToolBar = NO;
            
            [cell setCanEditBlock:^BOOL(){
                @strongify(self);
                [self showYearPicker];
                return NO;
            }];
        }
            break;
        case 1:{
            [cell.label setText:@"代维公司"];
            [cell.textField setPlaceholder:@"请选择"];
            if (self.queryDictionary[@"sid"] != nil) {
                 cell.textField.text = [agencys[selectedAgencyIndex] valueForKey:@"name"];
            }
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.showToolBar = NO;
            
            [cell setCanEditBlock:^BOOL(){
                @strongify(self);
                [self showSnamePicker];
                
                return NO;
            }];
        }
            break;

        case 2:{
            [cell.label setText:@"巡检周期"];
            [cell.textField setPlaceholder:@"请选择巡检周期"];
            cell.textField.text = [self.queryDictionary valueForKey:@"cycle"];
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.showToolBar = NO;
            
            [cell setCanEditBlock:^BOOL(){
                @strongify(self);
                [self showWorkCyclePicker];
                return NO;
            }];
        }
            break;
        case 3:{
            [cell.label setText:@"周期数"];
            [cell.textField setPlaceholder:@"请输入周期数"];
            [cell.textField setText:self.queryDictionary[@"cycleNum"]== nil?@"":self.queryDictionary[@"cycleNum"]];
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.showToolBar = NO;
            
            [cell setCanEditBlock:^BOOL(){
                return YES;
            }];
            [cell setTextBlcok:^(NSString *text){
                @strongify(self);
                self.queryDictionary[@"cycleNum"] = text;
            }];
        }
            
            break;
        case 4:{
            [cell.label setText:@"基    站"];
            [cell.textField setPlaceholder:@"请输入基站"];
            [cell.textField setText:self.queryDictionary[@"oname"]== nil?@"":self.queryDictionary[@"oname"]];
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.showToolBar = NO;
            
            [cell setCanEditBlock:^BOOL(){
                return YES;
            }];
            [cell setTextBlcok:^(NSString *text){
                @strongify(self);
                 self.queryDictionary[@"oname"] = text;
            }];
        }
            break;

        default:
            break;
    }
    return cell;
}


#pragma mark -Private
- (void)initData{

    [SVProgressHUD show];
    
    [APIClient POST:@"section/" withParameters:@{} successWithBlcok:^(id response) {
        sections = [NSArray yy_modelArrayWithClass:[ZNSSection class] json:[response valueForKey:@"items"]];
        
        agencys = @[];
        [sections  enumerateObjectsUsingBlock:^(ZNSSection* obj, NSUInteger idx, BOOL * _Nonnull stop){
            if ([obj.type isEqualToString:@"机构"]) {
                agencys = [agencys arrayByAddingObject:obj];
            }
        }];
        agencys = [agencys subarrayWithRange:NSMakeRange(1, agencys.count-1)];
        [SVProgressHUD dismiss];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
    
    
}

- (void)showYearPicker{
    NSMutableArray *years = [NSMutableArray array];
    for (int i=0; i<100; i++) {
        [years addObject:[NSString stringWithFormat:@"%d",2016+i]];
    }
    @weakify(self)
    [ ActionSheetStringPicker showPickerWithTitle:@"选择年份" rows:years initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        self.queryDictionary[@"year"] = selectedValue;
       [self.tableView reloadData];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}
- (void)showWorkCyclePicker{
    
    @weakify(self)
    [ ActionSheetStringPicker showPickerWithTitle:@"选择巡检周期" rows:@[@"周度",@"月度",@"季度",@"半年",@"年度"]  initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        self.queryDictionary[@"cycle"] = selectedValue;
        [self.tableView reloadData];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
    
}
- (void)showSnamePicker{
    if (agencys.count == 0) {
        [self initData];
        return;
    }
    @weakify(self)
    [ ActionSheetStringPicker showPickerWithTitle:@"选择代维公司" rows:[agencys valueForKeyPath:@"name"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        self.queryDictionary[@"sid"] = [[agencys[selectedIndex] valueForKey:@"sid"] numberValue];
        selectedAgencyIndex = selectedIndex;
        [self.tableView reloadData];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

- (void)query{
    if (self.dicblock) {
        self.dicblock(self.queryDictionary);
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
