//
//  WorkPatrolTimelinessTableViewCell.h
//  ZhiNengSuo
//
//  Created by ekey on 16/9/22.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSWorkPatrolTimeliness.h"

@interface WorkPatrolTimelinessTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;
@property (weak, nonatomic) IBOutlet UIButton *detailBtn;
@property (strong, nonatomic) ZNSWorkPatrolTimeliness *workPatrolTimeliness;

@property (nonatomic, copy) voidBlock detailBlock;


@end
