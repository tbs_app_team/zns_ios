//
//  WorkPatrolTimelinessDetailTableViewCell.h
//  ZhiNengSuo
//
//  Created by ekey on 16/9/26.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSWorkPatrolTimelinessDetail.h"

@interface WorkPatrolTimelinessDetailTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;
@property (strong, nonatomic) ZNSWorkPatrolTimelinessDetail *workPatrolTimeliness;

@property (strong, nonatomic) NSString *patrolcyclenum;

@end
