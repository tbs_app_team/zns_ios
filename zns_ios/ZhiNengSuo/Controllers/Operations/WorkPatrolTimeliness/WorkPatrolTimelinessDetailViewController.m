//
//  WorkPatrolTimelinessDetailViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/26.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "WorkPatrolTimelinessDetailViewController.h"
#import "WorkPatrolTimelinessDetailTableViewCell.h"
#import "ZNSWorkPatrolTimelinessDetail.h"

@interface WorkPatrolTimelinessDetailViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSArray *sourceDatas;
@property (nonatomic,strong) NSNumber *page;
@end

@implementation WorkPatrolTimelinessDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initViewLayout];
    
    [self.tableView registerNib:[WorkPatrolTimelinessDetailTableViewCell nib] forCellReuseIdentifier:[WorkPatrolTimelinessDetailTableViewCell reuseIdentifier]];
    
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
         [self loadDataWithPage:@0];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [self loadDataWithPage:@(self.page.integerValue+1)];
    }];
    [self.tableView.mj_header beginRefreshing];
}

#pragma mark - Private
- (void)initViewLayout{
    self.tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;;
    [self.view addSubview:self.tableView];
}
- (void)loadDataWithPage:(NSNumber *)page{
    @weakify(self);
//    [APIClient POST:API_WORKPATROL_FINISH_DETAIL withParameters:@{@"page":page,
//                                                                  @"pid":[_myWorkPatrolTimeliness.pid numberValue],
//                                                                  @"cycle":_myWorkPatrolTimeliness.patrolcycle,
//                                                                  @"cyclenum":[_myWorkPatrolTimeliness.patrolcyclenum numberValue]}
    [APIClient POST:API_WORKPATROL_FINISH_DETAIL([_myWorkPatrolTimeliness.pid numberValue]) withParameters:@{@"page":page}
   successWithBlcok:^(id response) {
       @strongify(self);
       self.page = page;
       if ([page isEqual:@0]) {
           self.sourceDatas = [NSArray yy_modelArrayWithClass:[ZNSWorkPatrolTimelinessDetail class] json:[response valueForKey:@"items"]];
       }else{
           self.sourceDatas = [self.sourceDatas arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSWorkPatrolTimelinessDetail class] json:[response valueForKey:@"items"]]];
       }
       
       [self.tableView.mj_header endRefreshing];
       [self.tableView.mj_footer endRefreshing];
       
       if (([[response valueForKey:@"page"] integerValue] + 1) == [[response valueForKey:@"pages"]integerValue]) {
           [self.tableView.mj_footer endRefreshingWithNoMoreData];
       }
       [self.tableView reloadData];
   } errorWithBlock:^(ZNSError *error) {
       [SVProgressHUD showErrorWithStatus:error.errorMessage];
       [self.tableView.mj_header endRefreshing];
       [self.tableView.mj_footer endRefreshing];
       
   }];
}


#pragma mark - tabelView delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.sourceDatas.count;
//    return 4;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    WorkPatrolTimelinessDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[WorkPatrolTimelinessDetailTableViewCell reuseIdentifier]];
    cell.patrolcyclenum = _myWorkPatrolTimeliness.time;
    cell.workPatrolTimeliness = self.sourceDatas[indexPath.row];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}
- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    NSArray *arr = @[@"基    站",@"年    份",@"周期数",@"进站时间",@"进站人"];
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    view.backgroundColor = [UIColor colorWithRed:52.0/255.0 green:147.0/255.0 blue:237.0/255.0 alpha:1];
    CGFloat width = self.view.frame.size.width/5.0;
    for (int i=0; i<5; i++) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(i*width, 0, width, 60)];
        label.text = arr[i];
        label.textColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:14];
        [view addSubview:label];
    }
    return view;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 60.0;
}
@end
