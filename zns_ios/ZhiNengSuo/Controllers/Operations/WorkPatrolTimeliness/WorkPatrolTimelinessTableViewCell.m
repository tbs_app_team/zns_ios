//
//  WorkPatrolTimelinessTableViewCell.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/22.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "WorkPatrolTimelinessTableViewCell.h"

@implementation WorkPatrolTimelinessTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setFrame:(CGRect)frame{
    // 更改x、宽度
    frame.size.width = 634;
    
    [super setFrame:frame];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)detailBtnClicked:(UIButton *)sender {
    if (self.detailBlock) {
        self.detailBlock();
    }
    
}
-(void)setWorkPatrolTimeliness:(ZNSWorkPatrolTimeliness *)workPatrolTimeliness{
    [_labels[0] setText:workPatrolTimeliness.patrol_content];
    [_labels[1] setText:workPatrolTimeliness.sname];
    [_labels[2] setText:workPatrolTimeliness.patrol_cycle];
    [_labels[3] setText:workPatrolTimeliness.time];
    [_labels[4] setText:[NSString stringWithFormat:@"%.2f%%",[workPatrolTimeliness.patrol_rate floatValue] * 100]];
    [_labels[5] setText:workPatrolTimeliness.patrol_object_count];
    [_labels[6] setText:workPatrolTimeliness.patrol_exec_count];
    [_labels[7] setText:workPatrolTimeliness.unfinished];
    
    [self.detailBtn setTitle:@"详情" forState:UIControlStateNormal];

}

@end
