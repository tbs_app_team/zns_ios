//
//  WorkPatrolTimelinessDetailTableViewCell.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/26.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "WorkPatrolTimelinessDetailTableViewCell.h"
#import "ZNSTimeTools.h"

@implementation WorkPatrolTimelinessDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setWorkPatrolTimeliness:(ZNSWorkPatrolTimelinessDetail *)workPatrolTimeliness{
    
    NSString *at = [[ZNSTimeTools shared] timeFomrmatterWith:workPatrolTimeliness.patrol_at];
    
    [_labels[0] setText:workPatrolTimeliness.object.name];
    [_labels[1] setText:[at substringToIndex:4]];
    [_labels[2] setText:_patrolcyclenum];
    [_labels[3] setText:at];
    [_labels[4] setText:workPatrolTimeliness.patrol_by];
    
}

@end
