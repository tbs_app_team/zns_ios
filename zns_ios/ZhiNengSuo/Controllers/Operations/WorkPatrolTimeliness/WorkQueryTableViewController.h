//
//  WorkPatrolQueryTableViewController.h
//  ZhiNengSuo
//
//  Created by ekey on 16/9/27.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface WorkQueryTableViewController : UIViewController

@property (nonatomic, strong) NSMutableDictionary *queryDictionary;
@property (nonatomic, assign) WorkQueryMode mode;
@property (nonatomic, copy) dicBlock dicblock;


@end
