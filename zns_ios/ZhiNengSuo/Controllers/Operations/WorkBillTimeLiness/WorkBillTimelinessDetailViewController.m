//
//  WorkBillTimelinessDetailViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/26.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "WorkBillTimelinessDetailViewController.h"
#import "ZNSWorkBillTimeliness.h"
#import "WorkBillTimelinessDetailTableViewCell.h"


@interface WorkBillTimelinessDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) NSArray *sourceDatas;
@property (nonatomic,strong) NSNumber *page;
@end

@implementation WorkBillTimelinessDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initViewLayout];
    
    [self.tableView registerNib:[WorkBillTimelinessDetailTableViewCell nib] forCellReuseIdentifier:[WorkBillTimelinessDetailTableViewCell reuseIdentifier]];
    self.tableView.tableFooterView = [UIView new];
    
//    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        [self loadDataWithPage:@0];
//    }];
//    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        [self loadDataWithPage:@(self.page.integerValue+1)];
//    }];
//    [self.tableView.mj_header beginRefreshing];
    
    self.sourceDatas = _myWorkBillTimeliness.details;
}

#pragma mark - Private
- (void)initViewLayout{
    self.tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
}
- (void)loadDataWithPage:(NSNumber *)page{
    @weakify(self);
    [APIClient POST:API_WORKBILL_INTIME_DETAIL withParameters:@{@"page":page,
                                                                  @"pid":_queryobj}
   successWithBlcok:^(id response) {
       @strongify(self);
       self.page = page;
       if ([page isEqual:@0]) {
           self.sourceDatas = [NSArray yy_modelArrayWithClass:[ZNSWorkBillTimeliness class] json:[response valueForKey:@"items"]];
       }else{
           self.sourceDatas = [self.sourceDatas arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSWorkBillTimeliness class] json:[response valueForKey:@"items"]]];
       }
       
       [self.tableView.mj_header endRefreshing];
       [self.tableView.mj_footer endRefreshing];
       
       if (([[response valueForKey:@"page"] integerValue] + 1) == [[response valueForKey:@"pages"]integerValue]) {
           [self.tableView.mj_footer endRefreshingWithNoMoreData];
       }
       [self.tableView reloadData];
   } errorWithBlock:^(ZNSError *error) {
       [SVProgressHUD showErrorWithStatus:error.errorMessage];
       [self.tableView.mj_header endRefreshing];
       [self.tableView.mj_footer endRefreshing];
       
   }];
}
#pragma mark -- scrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGSize temp = self.tableView.contentSize;
    // 2. 给这个变量赋值。因为变量都是L-Value，可以被赋值
    temp.width =560.0;
    // 3. 修改frame的值
    self.tableView.contentSize = temp;
}
#pragma mark - tabelView delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.sourceDatas.count;
//        return 4;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    WorkBillTimelinessDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[WorkBillTimelinessDetailTableViewCell reuseIdentifier]];
    cell.workBill = self.sourceDatas[indexPath.row];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}
- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [[NSBundle mainBundle] loadNibNamed:@"WorkBillTimelinessDetailTitleView" owner:self options:nil].firstObject;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 60.0;
}


@end
