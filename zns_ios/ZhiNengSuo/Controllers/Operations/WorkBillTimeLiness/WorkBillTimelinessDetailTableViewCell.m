//
//  WorkBillTimelinessDetailTableViewCell.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/26.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "WorkBillTimelinessDetailTableViewCell.h"
#import "ZNSTimeTools.h"

@implementation WorkBillTimelinessDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame{
    // 更改x、宽度
    frame.size.width = 560;
    
    [super setFrame:frame];
}
- (void)setWorkBill:(ZNSWorkBill *)workBill{
    [_labels[0] setText:workBill.workdesc];
    [_labels[1] setText:workBill.oname];
    [_labels[2] setText:[[ZNSTimeTools shared] timeFomrmatterWith:workBill.apply_at]];
    [_labels[3] setText:workBill.sname];
    [_labels[4] setText:[[ZNSTimeTools shared] timeFomrmatterWith:workBill.rec_at]];
    [_labels[5] setText:workBill.rec_by];
    [_labels[6] setText:[[ZNSTimeTools shared] timeFomrmatterWith:workBill.in_at]];
    [_labels[7] setText:[[ZNSTimeTools shared] timeFomrmatterWith:workBill.back_at]];
  
}
@end
