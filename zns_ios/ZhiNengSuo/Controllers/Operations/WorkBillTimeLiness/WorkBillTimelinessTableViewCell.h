//
//  WorkBillTimelinessTableViewCell.h
//  ZhiNengSuo
//
//  Created by ekey on 16/9/20.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSWorkBillTimeliness.h"

@interface WorkBillTimelinessTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;
@property (weak, nonatomic) IBOutlet UIButton *detailBtn;
@property (nonatomic, strong) ZNSWorkBillTimeliness* workBillTimeliness;
@property (nonatomic, copy) voidBlock detailBlock;


@end
