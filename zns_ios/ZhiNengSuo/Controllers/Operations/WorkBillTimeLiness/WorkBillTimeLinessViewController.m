//
//  WorkBillTimeLinessViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/20.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "WorkBillTimeLinessViewController.h"
#import "WorkBillTimelinessTableViewCell.h"
#import "ZNSWorkBillTimeliness.h"
#import "ZNSDate.h"
#import "WorkBillTimelinessDetailViewController.h"
#import "WorkBillQueryViewController.h"

@interface WorkBillTimeLinessViewController ()<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, assign) NSNumber *page;
@property (nonatomic, strong) NSArray *sourceDatas;
@property (nonatomic, strong) NSDictionary* queryDic;

@end

@implementation WorkBillTimeLinessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//     self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"筛选" style:UIBarButtonItemStyleDone target:self action:@selector(queryWorkBill)];
    
    [self.tableView registerNib:[WorkBillTimelinessTableViewCell nib] forCellReuseIdentifier:[WorkBillTimelinessTableViewCell reuseIdentifier]];
    
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self loadDataWithPage:@0 withDic:self.queryDic];
    }];
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self loadDataWithPage:@(self.page.integerValue+1) withDic:self.queryDic];
    }];
    
    [self.tableView.mj_header beginRefreshing];
}
#pragma mark -- scrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    [self.tableView setContentSize:CGSizeMake(470.0, 1000.0)];
    CGSize temp = self.tableView.contentSize;
    
    // 2. 给这个变量赋值。因为变量都是L-Value，可以被赋值
    temp.width = 455.0;
    
    // 3. 修改frame的值
    self.tableView.contentSize = temp;
}
#pragma mark - tableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.sourceDatas.count;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return  [[NSBundle mainBundle] loadNibNamed:@"WorkBillTimelinessTitleView" owner:self options:nil].firstObject;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WorkBillTimelinessTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[WorkBillTimelinessTableViewCell reuseIdentifier]];
    cell.workBillTimeliness = self.sourceDatas[indexPath.row];
    cell.detailBlock = ^{
//            NSLog(@"detail");
        [self detailController:self.sourceDatas[indexPath.row]];
        };
    return cell;
}

#pragma mark - Private
- (void)detailController:(ZNSWorkBillTimeliness*)workBill{
    WorkBillTimelinessDetailViewController *vc = [[WorkBillTimelinessDetailViewController alloc] init];
    vc.queryobj = workBill.name;
    vc.myWorkBillTimeliness = workBill;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)loadDataWithPage:(NSNumber *)page withDic:(NSDictionary *)dic{

    NSDictionary *dicPara = [NSDictionary dictionary];
    if (dic == nil) {
        dicPara = @{@"page":page,
                    @"type":@1,
                    @"createfrom":[[NSString formatTimeWithFormat:[NSDate date] format:@"yyyyMMdd"] stringByAppendingString:@"000000"],
                    @"createto":[[NSString formatTimeWithFormat:[NSDate date] format:@"yyyyMMdd"] stringByAppendingString:@"235959"]};
    }else{
        dicPara = dic;
    }
    @weakify(self);
    [APIClient POST:API_WORKBILL_INTIME withParameters:dicPara
   successWithBlcok:^(id response) {
        @strongify(self);
        self.page = page;
        if ([page isEqual:@0]) {
            self.sourceDatas = [NSArray yy_modelArrayWithClass:[ZNSWorkBillTimeliness class] json:[response valueForKey:@"items"]];
        }else{
            self.sourceDatas = [self.sourceDatas arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSWorkBillTimeliness class] json:[response valueForKey:@"items"]]];
        }
        
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        
        if (([[response valueForKey:@"page"] integerValue] + 1) == [[response valueForKey:@"pages"]integerValue]) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }
        [self.tableView reloadData];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        
    }];
}

#pragma Private
- (void)queryWorkBill{
    
//    WorkBillQueryViewController *vc = [[WorkBillQueryViewController alloc] init];
//    @weakify(self)
//    [vc setDicblock:^(NSDictionary *dic){
//        @strongify(self)
//        NSLog(@"%@",dic);
//        self.queryDic = dic;
//        [self.tableView.mj_header beginRefreshing];
//    }];
//    [self.navigationController pushViewController:vc animated:YES];
}

@end
