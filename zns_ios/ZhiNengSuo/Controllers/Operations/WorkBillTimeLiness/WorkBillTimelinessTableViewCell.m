//
//  WorkBillTimelinessTableViewCell.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/20.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "WorkBillTimelinessTableViewCell.h"

@implementation WorkBillTimelinessTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setFrame:(CGRect)frame{
    // 更改x、宽度
    frame.size.width = 470;
    
    [super setFrame:frame];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)detailAction:(id)sender {
//    NSLog(@"detail");
    if (self.detailBlock) {
        self.detailBlock();
    }
}


- (void)setWorkBillTimeliness:(ZNSWorkBillTimeliness *)workBillTimeliness{
    [_labels[0] setText:workBillTimeliness.name];
    CGFloat incomeRate = [workBillTimeliness.incomeRate floatValue] * 100;
    [_labels[1] setText:[NSString stringWithFormat:@"%.2f%%",incomeRate]];
    [_labels[2] setText:workBillTimeliness.should];
    [_labels[3] setText:workBillTimeliness.actual];
    [_labels[4] setText:workBillTimeliness.income];
    [_labels[5] setText:workBillTimeliness.unfinished];

    [self.detailBtn setTitle:@"详情" forState:UIControlStateNormal];
    [self.detailBtn setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
//    [self.detailBtn setImage:[UIImage imageNamed:@"details"] forState:UIControlStateNormal];
    
}


@end
