//
//  WorkBillQueryViewController.h
//  ZhiNengSuo
//
//  Created by ekey on 16/10/17.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorkBillQueryViewController : UIViewController
@property (nonatomic, strong) NSMutableDictionary *queryDictionary;
@property (nonatomic, assign) WorkQueryMode mode;
@property (nonatomic, copy) dicBlock dicblock;
@end
