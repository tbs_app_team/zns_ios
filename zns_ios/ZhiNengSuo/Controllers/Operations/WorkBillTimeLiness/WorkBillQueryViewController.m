//
//  WorkBillQueryViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/10/17.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "WorkBillQueryViewController.h"
#import "ZNSLabelTextFieldTableViewCell.h"
#import "ZNSSection.h"
#import "ZNSDict.h"

@interface WorkBillQueryViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSArray *sections;//all
    NSArray *agencys;//机构
    NSArray *areas;//区域
    NSArray *constants;//常量
    NSInteger selectedAgencyIndex;
    NSInteger selectedDictIndex;
    NSInteger selectedConstantIndex;
    NSString *beginTimeString;
    NSString *endTimeString;
    
}
@property (nonatomic, strong)  UITableView *tableView;
@end
@implementation WorkBillQueryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRGB:0xefeff4 alpha:1.0];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 420) style:UITableViewStyleGrouped];
    [self.tableView registerNib:[ZNSLabelTextFieldTableViewCell nib] forCellReuseIdentifier:[ZNSLabelTextFieldTableViewCell reuseIdentifier]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(20,440, self.view.frame.size.width-40, 44)];
    [btn bs_configureAsPrimaryStyle];
    [btn setTitle:@"查询" forState:UIControlStateNormal];
    [self.view addSubview:btn];
    [btn setTarget:self action:@selector(query) forControlEvents:UIControlEventTouchUpInside];
    
    self.queryDictionary = [NSMutableDictionary dictionary];
    [self initData];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 7;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZNSLabelTextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ZNSLabelTextFieldTableViewCell reuseIdentifier]];
    @weakify(self);
    switch (indexPath.row) {
        case 0:{
            [cell.label setText:@"查看方式"];
            [cell.textField setPlaceholder:@"请选择"];
            cell.textField.text = [constants[selectedConstantIndex] valueForKey:@"name"];
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.showToolBar = NO;
            
            [cell setCanEditBlock:^BOOL(){
                @strongify(self);
                [self showConstantsPicker];
                return NO;
            }];
        }
            break;
        case 1:{
            [cell.label setText:@"代维公司"];
            [cell.textField setPlaceholder:@"请选择"];
            if (self.queryDictionary[@"sid"] != nil) {
                cell.textField.text = [agencys[selectedAgencyIndex] valueForKey:@"name"];
            }
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.showToolBar = NO;
            
            [cell setCanEditBlock:^BOOL(){
                @strongify(self);
                [self showSnamePicker];
                
                return NO;
            }];
        }
            break;
        case 2:{
            [cell.label setText:@"地    点"];
            [cell.textField setPlaceholder:@"请选择"];
            if (self.queryDictionary[@"area"] != nil) {
                cell.textField.text = [areas[selectedDictIndex] valueForKey:@"name"];
            }
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.showToolBar = NO;
            
            [cell setCanEditBlock:^BOOL(){
                @strongify(self);
                [self showOnamePicker];
                
                return NO;
            }];
        }
            break;
            
        case 3:{
            [cell.label setText:@"开始时间"];
            [cell.textField setPlaceholder:@"请选择开始时间"];
            cell.textField.text = beginTimeString == nil? @"":beginTimeString;
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.showToolBar = NO;
            
            [cell setCanEditBlock:^BOOL(){
                @strongify(self);
                [self showBeginAtPicker];
                return NO;
            }];
        }
            break;
        case 4:{
            [cell.label setText:@"结束时间"];
            [cell.textField setPlaceholder:@"请选择结束时间"];
            cell.textField.text = endTimeString == nil? @"":endTimeString;
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.showToolBar = NO;
            
            [cell setCanEditBlock:^BOOL(){
                @strongify(self);
                [self showEndAtPicker];
                return NO;
            }];
        }
            break;
        case 5:{
            [cell.label setText:@"基    站"];
            [cell.textField setPlaceholder:@"请输入基站"];
            [cell.textField setText:self.queryDictionary[@"oname"]== nil?@"":self.queryDictionary[@"oname"]];
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.showToolBar = NO;

            [cell setCanEditBlock:^BOOL(){
                return YES;
            }];
            [cell setTextBlcok:^(NSString *text){
                @strongify(self);
                self.queryDictionary[@"oname"] = text;
            }];
        }
            break;
        case 6:{
            [cell.label setText:@"进站人"];
            [cell.textField setPlaceholder:@"请输入进站人"];
            [cell.textField setText:self.queryDictionary[@"createby"]== nil?@"":self.queryDictionary[@"createby"]];
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.showToolBar = NO;
            
            [cell setCanEditBlock:^BOOL(){
                return YES;
            }];
            [cell setTextBlcok:^(NSString *text){
                @strongify(self);
                self.queryDictionary[@"createby"] = text;
            }];
        }
            
            break;

        default:
            break;
    }
    return cell;
}


#pragma mark -Private
- (void)initData{
    
    [SVProgressHUD show];
    
    [APIClient POST:@"section/" withParameters:@{} successWithBlcok:^(id response) {
        sections = [NSArray yy_modelArrayWithClass:[ZNSSection class] json:[response valueForKey:@"items"]];
        
        agencys = @[];
        areas = @[];
        [sections  enumerateObjectsUsingBlock:^(ZNSSection* obj, NSUInteger idx, BOOL * _Nonnull stop){
            if ([obj.type isEqualToString:@"机构"]) {
                agencys = [agencys arrayByAddingObject:obj];
            }
            if ([obj.type isEqualToString:@"区域"]) {
                areas = [areas arrayByAddingObject:obj];
            }
        }];
        agencys = [agencys subarrayWithRange:NSMakeRange(1, agencys.count-1)];
        areas = [areas subarrayWithRange:NSMakeRange(1, areas.count-1)];
       
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
    [APIClient POST:API_DICT_LIST withParameters:@{@"page":@-1} successWithBlcok:^(id response) {
        
        NSArray *temContants = [NSArray yy_modelArrayWithClass:[ZNSDict class] json:response[@"items"]];
        constants = @[];
        [temContants enumerateObjectsUsingBlock:^(ZNSDict* obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.type isEqualToString:@"intime_view"]) {
              constants = [constants arrayByAddingObject:obj];
            }
        }];
         [SVProgressHUD dismiss];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
}

- (void)showConstantsPicker{
   
    @weakify(self)
    [ ActionSheetStringPicker showPickerWithTitle:@"选择查看方式" rows:[constants valueForKeyPath:@"name"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        self.queryDictionary[@"type"] = @(selectedIndex+1);
        selectedConstantIndex = selectedIndex;
        [self.tableView reloadData];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}
- (void)showBeginAtPicker{
    
    @weakify(self)
    [ActionSheetDatePicker showPickerWithTitle:@"选择开始时间" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate new] minimumDate:nil maximumDate:[NSDate new] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        @strongify(self)
        self.queryDictionary[@"createfrom"] = [[NSString formatTimeWithFormat:selectedDate format:@"yyyyMMdd"] stringByAppendingString:@"000000"];
        beginTimeString = [NSString formatTimeWithFormat:selectedDate format:@"yyyy-MM-dd"];
        [self.tableView reloadData];
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}
- (void)showEndAtPicker{
    
    @weakify(self)
    [ActionSheetDatePicker showPickerWithTitle:@"选择结束时间" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate new] minimumDate:nil maximumDate:[NSDate new] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        @strongify(self)
        self.queryDictionary[@"createto"] = [[NSString formatTimeWithFormat:selectedDate format:@"yyyyMMdd"] stringByAppendingString:@"235959"];
        endTimeString = [NSString formatTimeWithFormat:selectedDate format:@"yyyy-MM-dd"];
        [self.tableView reloadData];
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}
- (void)showSnamePicker{
    if (agencys.count == 0) {
        [self initData];
        return;
    }
    @weakify(self)
    [ ActionSheetStringPicker showPickerWithTitle:@"选择代维公司" rows:[agencys valueForKeyPath:@"name"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        self.queryDictionary[@"sid"] = [[agencys[selectedIndex] valueForKey:@"sid"] numberValue];
        selectedAgencyIndex = selectedIndex;
        [self.tableView reloadData];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}
- (void)showOnamePicker{
    if (areas.count == 0) {
        [self initData];
        return;
    }
    @weakify(self)
    [ ActionSheetStringPicker showPickerWithTitle:@"选择地点" rows:[areas valueForKeyPath:@"name"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        self.queryDictionary[@"area"] = [[areas[selectedIndex] valueForKey:@"sid"] numberValue];
        selectedDictIndex = selectedIndex;
        [self.tableView reloadData];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

- (void)query{
    if (self.dicblock) {
        self.dicblock(self.queryDictionary);
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
