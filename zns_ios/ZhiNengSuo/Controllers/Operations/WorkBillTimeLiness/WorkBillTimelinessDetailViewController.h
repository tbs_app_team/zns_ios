//
//  WorkBillTimelinessDetailViewController.h
//  ZhiNengSuo
//
//  Created by ekey on 16/9/26.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSWorkBillTimeliness.h"

@interface WorkBillTimelinessDetailViewController : UIViewController

@property (nonatomic, strong) ZNSWorkBillTimeliness * myWorkBillTimeliness;
@property (nonatomic, strong) NSString *queryobj;
@end
