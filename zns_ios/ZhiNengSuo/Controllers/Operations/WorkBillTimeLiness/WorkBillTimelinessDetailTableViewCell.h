//
//  WorkBillTimelinessDetailTableViewCell.h
//  ZhiNengSuo
//
//  Created by ekey on 16/9/26.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSWorkBill.h"

@interface WorkBillTimelinessDetailTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;
@property (strong, nonatomic)ZNSWorkBill *workBill;


@end
