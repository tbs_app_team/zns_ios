//
//  WorkPatrolAddViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/20.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "WorkPatrolAddViewController.h"
#import "ZNSLabelTextFieldTableViewCell.h"
#import "ZNSWorkPatrol.h"
#import "ZNSSection.h"
#import "ObjectChooseViewController.h"


@interface WorkPatrolAddViewController ()<UITableViewDataSource, UITableViewDelegate,UITextViewDelegate>
@property (strong, nonatomic) UITableView *tableView;
@property (nonatomic, strong) ZNSWorkPatrol *workPatrol;
@property (nonatomic, strong) NSArray *section;
@property (nonatomic, strong) NSArray *onames; //选择基站的名称s
@property (nonatomic, strong) NSArray *oids; //选择基站的ids
@property (nonatomic, strong) NSArray *sname; //执行单位
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentTextViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *demandTextViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *memoTextViewHeight;
@property (weak, nonatomic) IBOutlet UITextView *contentViewText;
@property (weak, nonatomic) IBOutlet UITextView *demandTextView;
@property (weak, nonatomic) IBOutlet UITextView *memoTextView;
@property (weak, nonatomic) IBOutlet UIButton *snameBtn;
@property (weak, nonatomic) IBOutlet UIButton *onameBtn;
@property (weak, nonatomic) IBOutlet UIButton *cycleBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *onameBtnHeight;

@end

@implementation WorkPatrolAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.workPatrol = [ZNSWorkPatrol new];
//    [self initTableViewLayout];
   
     self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveWorkPatrol)];
    _contentViewText.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _contentViewText.layer.borderWidth = 1;
    
    _demandTextView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _demandTextView.layer.borderWidth = 1;
    
    _memoTextView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _memoTextView.layer.borderWidth = 1;
    
    self.onames = @[];
    self.oids   = @[];
    self.contentViewText.delegate = self;
    self.demandTextView.delegate = self;
    self.memoTextView.delegate = self;
    [self.cycleBtn bs_configureAsWarningStyle];
    [self.snameBtn bs_configureAsSuccessStyle];
    [self.onameBtn bs_configureAsPrimaryStyle];

    [self initData];
}
// MARK: -- Delegate
- (void)textViewDidChange:(UITextView *)textView{
    switch (textView.tag) {
        case 101:{
            if (self.contentViewText.contentSize.height < 31) {
                self.contentTextViewHeight.constant = 31;
            }else if(self.contentViewText.contentSize.height <61){
                self.contentTextViewHeight.constant = self.contentViewText.contentSize.height;
            }else {
                self.contentTextViewHeight.constant = 61;
            }
        }
            
            break;
        case 102:{
            if (self.demandTextView.contentSize.height < 31) {
                self.demandTextViewHeight.constant = 31;
            }else if(self.demandTextView.contentSize.height <61){
                self.demandTextViewHeight.constant = self.demandTextView.contentSize.height;
            }else {
                self.demandTextViewHeight.constant = 61;
            }
        }
            
            break;
        case 103:{
            if (self.memoTextView.contentSize.height < 31) {
                self.memoTextViewHeight.constant = 31;
            }else if(self.memoTextView.contentSize.height <61){
                self.memoTextViewHeight.constant = self.memoTextView.contentSize.height;
            }else {
                self.memoTextViewHeight.constant = 61;
            }
        }
            
            break;
            
        default:
            break;
    }
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    switch (textView.tag) {
        case 101:{
            if ([text isEqualToString:@"\n"]) {
                [_contentViewText resignFirstResponder];
                return NO;
            }
        }
            break;
        case 102:{
            if ([text isEqualToString:@"\n"]) {
                [_demandTextView resignFirstResponder];
                return NO;
            }
        }
            break;
        case 103:{
            if ([text isEqualToString:@"\n"]) {
                [_memoTextView resignFirstResponder];
                return NO;
            }

        }
            break;
            
        default:
            break;
    }
   return YES;
}

#pragma mark -- Action
- (IBAction)cycleBtnClick:(UIButton *)sender {
    [self showWorkCyclePicker];

}
- (IBAction)snameBtnClick:(UIButton *)sender {
    [self showSnamePicker];
}
- (IBAction)onameBtnClick:(UIButton *)sender {
    [self showOnamePicker];
}


#pragma mark -- tableView delagate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZNSLabelTextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ZNSLabelTextFieldTableViewCell reuseIdentifier]];
    @weakify(self);
    switch (indexPath.row) {
        case 0:{
            [cell.label setText:@"巡检内容"];
            [cell.textField setPlaceholder:@"请输入巡检内容"];
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.showToolBar = NO;
            
            [cell setCanEditBlock:^BOOL(){
                return YES;
            }];
            [cell setTextBlcok:^(NSString *text){
                @strongify(self);
                self.workPatrol.content = text;
            }];
        }
            break;
        case 1:{
            [cell.label setText:@"巡检周期"];
            [cell.textField setPlaceholder:@"请选择巡检周期"];
            cell.textField.text = self.workPatrol.cycle;
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.showToolBar = NO;
            
            [cell setCanEditBlock:^BOOL(){
                @strongify(self);
               [self showWorkCyclePicker];
                return NO;
            }];
        }
            break;
        case 2:{
            [cell.label setText:@"执行单位"];
            [cell.textField setPlaceholder:@"请选择执行单位"];
            cell.textField.text = self.workPatrol.sname;
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.showToolBar = NO;
            
            [cell setCanEditBlock:^BOOL(){
                @strongify(self);
                 [self showSnamePicker];

                return NO;
            }];
        }
            break;
        case 3:{
            [cell.label setText:@"基站"];
            [cell.textField setPlaceholder:@"请选择基站"];
            cell.textField.text = self.workPatrol.oname;
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.showToolBar = NO;
            
            [cell setCanEditBlock:^BOOL(){
                @strongify(self);
            [self showOnamePicker];
                return NO;
            }];
        }
            break;
        case 4:{
            [cell.label setText:@"巡检要求"];
            [cell.textField setPlaceholder:@"巡检要求"];
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.showToolBar = NO;
            
            [cell setCanEditBlock:^BOOL(){
                return YES;
            }];
            [cell setTextBlcok:^(NSString *text){
                @strongify(self);
                self.workPatrol.demand = text;
            }];
        }

            break;
        case 5:{
            [cell.label setText:@"备    注"];
            [cell.textField setPlaceholder:@"备注信息"];
            cell.textField.keyboardType = UIKeyboardTypeDefault;
            cell.showToolBar = NO;
            
            [cell setCanEditBlock:^BOOL(){
                return YES;
            }];
            [cell setTextBlcok:^(NSString *text){
                @strongify(self);
                self.workPatrol.memo = text;
            }];
        }
            break;
        default:
            break;
    }
    return cell;
}


#pragma mark - Private
- (void)initTableViewLayout{
    _tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
    [self.view addSubview:_tableView];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView registerNib:[ZNSLabelTextFieldTableViewCell nib] forCellReuseIdentifier:[ZNSLabelTextFieldTableViewCell reuseIdentifier]];
}

- (void)initData{
    
    @weakify(self)
    
    [SVProgressHUD show];
    
    
    [APIClient POST:@"section/" withParameters:@{} successWithBlcok:^(id response) {
        @strongify(self);
        self.section = [NSArray yy_modelArrayWithClass:[ZNSSection class] json:[response valueForKey:@"items"]];
       
        self.sname = @[];
        [self.section  enumerateObjectsUsingBlock:^(ZNSSection* obj, NSUInteger idx, BOOL * _Nonnull stop){
//            if ([obj.type isEqualToString:@"区域"]) {
//                self.oname = [self.oname arrayByAddingObject:obj];
//            }
            if ([obj.type isEqualToString:@"机构"]) {
                self.sname = [self.sname arrayByAddingObject:obj];
            }
        }];
        self.sname = [self.sname subarrayWithRange:NSMakeRange(1, self.sname.count-1)];
        [SVProgressHUD dismiss];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
    
}

- (void)showSnamePicker{
    @weakify(self)
    [ ActionSheetStringPicker showPickerWithTitle:@"选择执行单位" rows:[self.sname valueForKeyPath:@"name"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        self.workPatrol.sname = selectedValue;
        self.workPatrol.sid = [self.sname[selectedIndex] valueForKeyPath:@"sid"];
        [self.snameBtn setTitle:selectedValue forState:UIControlStateNormal];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

- (void)showOnamePicker{
    
    ObjectChooseViewController * vc = [ObjectChooseViewController create];
    vc.selectedData = [NSMutableArray arrayWithArray:self.oids];
    vc.selectedName = [NSMutableArray arrayWithArray:self.onames];
    vc.isOnlyOneChoice = YES;
    @weakify(self);
    vc.objsBlock = ^(NSArray *arrObjs) {
        @strongify(self);
        self.oids = arrObjs;
    };
    vc.nameBlock = ^(NSArray *nameArr){
        @strongify(self)
        if (nameArr.count != 0) {
            self.onames = nameArr;
            __block NSString   *str= @"";
            [nameArr enumerateObjectsUsingBlock:^(NSString*obj, NSUInteger idx, BOOL * _Nonnull stop) {
                str = [str stringByAppendingString:[NSString stringWithFormat:@"%@   ",obj]];
            }];
            self.workPatrol.oname = str;
            [self.onameBtn setTitle:str forState:UIControlStateNormal];
            CGFloat btnHeight = [str boundingRectWithSize:CGSizeMake(self.view.frame.size.width-20, 0) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size.height;
            if (btnHeight > 100) {
                self.onameBtnHeight.constant = 100;
            }else{
                self.onameBtnHeight.constant = btnHeight+8 < 40 ? 40 :btnHeight+8;
            }
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
        
    };
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)showWorkCyclePicker{
    
        @weakify(self)
        [ ActionSheetStringPicker showPickerWithTitle:@"选择巡检周期" rows:@[@"周度",@"月度",@"季度",@"半年",@"年度"]  initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            @strongify(self);
            self.workPatrol.cycle = selectedValue;
            [self.cycleBtn setTitle:selectedValue forState:UIControlStateNormal];
        } cancelBlock:^(ActionSheetStringPicker *picker) {
    
        } origin:self.view];
    
}

- (void)saveWorkPatrol{
    if ([self.contentViewText.text  isEqualToString: @""]) {
        [SVProgressHUD showErrorWithStatus:@"巡检内容不能为空"];
        return;
    }
    if (self.workPatrol.cycle==nil) {
        [SVProgressHUD showErrorWithStatus:@"请选择巡检周期"];
        return;
    }
    if (self.workPatrol.sname==nil) {
        [SVProgressHUD showErrorWithStatus:@"请选择执行单位"];
        return;
    }
    if (self.workPatrol.oname==nil) {
        [SVProgressHUD showErrorWithStatus:@"请选择基站"];
        return;
    }

    if ([self.demandTextView.text isEqualToString:@""]) {
        [SVProgressHUD showErrorWithStatus:@"巡检要求不能为空"];
        return;
    }
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{@"cycle":self.workPatrol.cycle,
                                                                               @"content":self.contentViewText.text,
                                                                               @"sid":self.workPatrol.sid,
                                                                               @"demand":self.demandTextView.text,
                                                                               @"memo":self.memoTextView.text == nil? @"":self.memoTextView.text}];
    [dic setValue:self.oids forKey:@"objects"];
    
    [APIClient POST:API_WORKPATROL_NEW withParameters:dic successWithBlcok:^(id response) {
                                                          [SVProgressHUD showInfoWithStatus:@"新增巡检任务成功"];
                                                          [self.navigationController popViewControllerAnimated:YES];
                                                          
                                                      } errorWithBlock:^(ZNSError *error) {
                                                          [SVProgressHUD showErrorWithStatus:error.errorMessage];
                                                      }];
}
@end
