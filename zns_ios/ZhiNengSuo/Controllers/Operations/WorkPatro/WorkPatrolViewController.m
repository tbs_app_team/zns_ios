//
//  WorkPatrolExecuteViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/20.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "WorkPatrolViewController.h"
#import "WorkPatrolTableViewCell.h"
#import "ZNSWorkPatrol.h"
#import "WorkPatrolAddViewController.h"

#import "WorkPatrolAddController.h"

@interface WorkPatrolViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, assign) NSNumber *page;
@property (nonatomic, strong) NSArray *dataSource;


@end

@implementation WorkPatrolViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"新增" style:UIBarButtonItemStylePlain target:self action:@selector(WorkPatrolAdd)];
    
    [self.tableView registerNib:[WorkPatrolTableViewCell nib] forCellReuseIdentifier:[WorkPatrolTableViewCell reuseIdentifier]];
    self.tableView.tableFooterView = [UIView new];
    
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self loadDataWithPage:@0];
    }];
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self loadDataWithPage:@(self.page.integerValue+1)];
    }];
    
    [self.tableView.mj_header beginRefreshing];

}
#pragma mark - Private

- (void)WorkPatrolAdd{
    
    WorkPatrolAddController *vc = [WorkPatrolAddController create];
    vc.title = @"新增巡检任务";
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)loadDataWithPage:(NSNumber *)page{
    @weakify(self);
    [APIClient POST:API_WORKPATROL_GET withParameters:@{@"page":page} successWithBlcok:^(id response) {
        @strongify(self);
        self.page = page;
        if ([page isEqual:@0]) {
            self.dataSource = [NSArray yy_modelArrayWithClass:[ZNSWorkPatrol class] json:[response valueForKey:@"items"]];
        }else{
            self.dataSource = [self.dataSource arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSWorkPatrol class] json:[response valueForKey:@"items"]]];
        }
        
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        
        if (([[response valueForKey:@"page"] integerValue] + 1) == [[response valueForKey:@"pages"]integerValue]) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }
        [self.tableView reloadData];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        
    }];
}

#pragma mark -- tableview delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WorkPatrolTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[WorkPatrolTableViewCell reuseIdentifier]];
    cell.workPatrol = self.dataSource[indexPath.row];
    cell.excuteBtn.hidden = YES;
    cell.excuteBtnHeight.constant = 0;
    [self.view layoutIfNeeded];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    @weakify(self)
    CGFloat height = [tableView fd_heightForCellWithIdentifier:[WorkPatrolTableViewCell reuseIdentifier]
                                   cacheByIndexPath:indexPath    configuration:^(WorkPatrolTableViewCell* cell) {
         @strongify(self);
         cell.workPatrol = self.dataSource[indexPath.row];
         cell.excuteBtn.hidden = YES;
         cell.excuteBtnHeight.constant = 0;
         [self.view layoutIfNeeded];
    }];
    return height;
}
@end
