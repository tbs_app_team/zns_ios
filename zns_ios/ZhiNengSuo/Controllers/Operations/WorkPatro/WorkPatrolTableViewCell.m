//
//  WorkPatrolExcuteTableViewCell.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/20.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "WorkPatrolTableViewCell.h"
#import "ZNSTimeTools.h"

@implementation WorkPatrolTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.excuteBtn bs_configureAsPrimaryStyle];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setWorkPatrol:(ZNSWorkPatrol *)workPatrol{
    
    [_labels[0] setText:[NSString stringWithFormat:@"巡检内容:%@",workPatrol.patrol_content]];
    [_labels[1] setText:[NSString stringWithFormat:@"执行单位:%@",workPatrol.sname]];
    [_labels[2] setText:[NSString stringWithFormat:@"巡检周期:%@",workPatrol.patrol_cycle]];
    [_labels[3] setText:[NSString stringWithFormat:@"创 建 人  :%@",workPatrol.create_by]];
    [_labels[4] setText:[NSString stringWithFormat:@"派工时间:%@",[[ZNSTimeTools shared]timeFomrmatterWith:workPatrol.begin_at]]];
    [_labels[5] setText:[NSString stringWithFormat:@"巡检要求:%@",workPatrol.patrol_demand]];
    NSMutableString * res = [NSMutableString string];
    
    [workPatrol.objects enumerateObjectsUsingBlock:^(ZNSObject *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if(idx == (workPatrol.objects.count-1)){
          [res appendString:[NSString stringWithFormat:@"%@",obj.name]];
        }else{
           [res appendString:[NSString stringWithFormat:@"%@ 、",obj.name]];
        }
    }];
    
    [_labels[6] setText:[NSString stringWithFormat:@"基    站   :%@",res]];

}
- (IBAction)excuteBtnOnClick:(UIButton *)sender {
    if (self.clickBlock) {
        self.clickBlock();
    }
}

@end
