//
//  WorkPatrolExcuteTableViewCell.h
//  ZhiNengSuo
//
//  Created by ekey on 16/9/20.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSWorkPatrol.h"

@interface WorkPatrolTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *labels;

@property (strong, nonatomic) ZNSWorkPatrol *workPatrol;
@property (weak, nonatomic) IBOutlet UIButton *excuteBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *excuteBtnHeight;

@property (nonatomic, copy) voidBlock clickBlock;


@end
