//
//  WorkPatrolAddController.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 17/1/12.
//  Copyright © 2017年 czwen. All rights reserved.
//

#import "WorkPatrolAddController.h"
#import "ZNSLabelTextFieldTableViewCell.h"
#import "ObjectChooseViewController.h"

@interface WorkPatrolAddController (){

    IBOutlet UITableView *tbvPatrolAdd;
    
    ZNSLabelTextFieldTableViewCell *contentCell;
    ZNSLabelTextFieldTableViewCell *demandCell;
    ZNSLabelTextFieldTableViewCell *memoCell;
    
    NSString *patrol_cycle;
    NSString *begin_time;
    NSString *end_time;
    NSString *o_name;
    NSString *s_name;
    
    NSString *o_id;
    NSString *s_id;
}

@property (nonatomic,strong) NSDate *beginTime;
@property (nonatomic,strong) NSDate *endTime;
@property (nonatomic, strong) NSMutableArray *sname; //执行单位

@end

@implementation WorkPatrolAddController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [tbvPatrolAdd registerNib:[ZNSLabelTextFieldTableViewCell nib] forCellReuseIdentifier:[ZNSLabelTextFieldTableViewCell reuseIdentifier]];
    
    [self setupCells];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveAction)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)saveAction{
    
    if ([contentCell.textField.text  isEqualToString: @""]) {
        [SVProgressHUD showErrorWithStatus:@"巡检内容不能为空"];
        return;
    }
    
    if ([contentCell.textField.text length] >255) {
         [SVProgressHUD showErrorWithStatus:@"巡检内容不得超过255位"];
        return;
    }
    
    if (![patrol_cycle length]) {
        [SVProgressHUD showErrorWithStatus:@"请选择巡检周期"];
        return;
    }
    if (![s_name length]) {
        [SVProgressHUD showErrorWithStatus:@"请选择执行单位"];
        return;
    }
    if (![o_name length]) {
        [SVProgressHUD showErrorWithStatus:@"请选择基站"];
        return;
    }
    
    if ([demandCell.textField.text isEqualToString:@""]) {
        [SVProgressHUD showErrorWithStatus:@"巡检要求不能为空"];
        return;
    }
    
    if (!self.beginTime) {
        [SVProgressHUD showErrorWithStatus:@"请选择开始时间"];
        return;
    }
    if (!self.endTime) {
        [SVProgressHUD showErrorWithStatus:@"请选择结束时间"];
        return;
    }
    
    if ([memoCell.textField.text length] > 255) {
        [SVProgressHUD showErrorWithStatus:@"备注最多为255位"];
        return;
    }
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{@"patrol_cycle":patrol_cycle,
                                                                               @"patrol_content":contentCell.textField.text,
                                                                               @"patrol_demand":demandCell.textField.text,
                                                                               @"begin_at":[self.beginTime stringWithFormat:@"yyyyMMddHHmmss"],
                                                                               @"end_at":[self.endTime stringWithFormat:@"yyyyMMddHHmmss"],
                                                                               @"memo":memoCell.textField.text}];
    [dic setObject:@[o_id] forKey:@"oid"];
    [dic setObject:s_id forKey:@"sid"];
    [dic setObject:@"" forKey:@"patrol_item"];
    
    [SVProgressHUD show];
    [APIClient POST:API_WORKPATROL_NEW withParameters:dic successWithBlcok:^(id response) {
        [SVProgressHUD showSuccessWithStatus:@"新增巡检任务成功"];
        [self.navigationController popViewControllerAnimated:YES];
        
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];

}

- (void)showWorkCyclePicker{
    
    [ ActionSheetStringPicker showPickerWithTitle:@"选择巡检周期" rows:@[@"周度",@"月度",@"季度",@"年度"]  initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        patrol_cycle = selectedValue;
        [tbvPatrolAdd reloadData];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
    
}


- (void)showTimeSelectedWith:(NSInteger)type{
    
    @weakify(self);
    [ActionSheetDatePicker showPickerWithTitle:type?@"选择开始时间":@"选择结束时间" datePickerMode:UIDatePickerModeDateAndTime selectedDate:self.endTime?self.endTime:[NSDate new] minimumDate:type?[NSDate date]:(self.beginTime?self.beginTime:[NSDate new]) maximumDate:type?(self.endTime?self.endTime:nil):nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        @strongify(self);
        
        NSDateFormatter * df = [[NSDateFormatter alloc]init];
        df.dateFormat = @"yyyy-MM-dd HH:mm";
        if (type) {
            begin_time = [df stringFromDate:selectedDate];
            self.beginTime = selectedDate;
        }else{
            end_time = [df stringFromDate:selectedDate];
            self.endTime = selectedDate;
        }
        [tbvPatrolAdd reloadData];
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}

- (void)showSnamePicker{
    @weakify(self)
    
    if (self.sname.count < 1) {
        [SVProgressHUD showErrorWithStatus:@"请先选择基站"];
        return;
    }
    
    [ActionSheetStringPicker showPickerWithTitle:@"选择执行单位" rows:[self.sname valueForKeyPath:@"name"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        s_name = selectedValue;
        s_id = self.sname[selectedIndex][@"sid"];
        [tbvPatrolAdd reloadData];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

- (void)showOnamePicker{
    
    ObjectChooseViewController * vc = [ObjectChooseViewController create];
    vc.selectedData = [NSMutableArray arrayWithObject:@""];
    vc.selectedName = [NSMutableArray arrayWithObject:@""];
    vc.isOnlyOneChoice = YES;
    vc.isAgt = YES;
    
    @weakify(self);
    vc.selectedBlock = ^(NSArray *objs){
        @strongify(self)
        if (objs.count > 0) {
            ZNSObject *obj = objs[0];
            o_id = obj.oid;
            o_name = obj.name;
          
            if (!self.sname) {
                self.sname = [NSMutableArray array];
            }
            [self.sname removeAllObjects];
//            [self.sname addObject:@{@"name":obj.area,@"sid":@(obj.sid).stringValue}];
//            if ([obj.agent_section length]) {
                [self.sname addObject:@{@"name":obj.agent_section,@"sid":obj.agent_sid}];
//            }
            s_name = obj.agent_section;
            s_id = obj.agent_sid;//@(obj.sid).stringValue;
            
            [tbvPatrolAdd reloadData];
        }
    };
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)setupCells{
    
    contentCell = [self createCellWith:@"巡检内容" placeholder:@"请输入巡检内容"];
    demandCell = [self createCellWith:@"巡检要求" placeholder:@"请输入巡检要求"];
    memoCell = [self createCellWith:@"备注" placeholder:@"请输入备注信息"]; 
    
}

- (ZNSLabelTextFieldTableViewCell *)createCellWith:(NSString *)title placeholder:(NSString *)placeholder{
    
    ZNSLabelTextFieldTableViewCell *cell = [ZNSLabelTextFieldTableViewCell loadFromNib];
    [cell.label setText:title];
    cell.textField.placeholder = placeholder;
    return cell;
}

- (UITableViewCell *)createNolCellWith:(NSString *)title identifiler:(NSString *)identifier detailText:(NSString *)detailText isIndicator:(BOOL)isIndicator{
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = title;
    cell.detailTextLabel.text = detailText;
    cell.accessoryType = isIndicator?UITableViewCellAccessoryDisclosureIndicator:UITableViewCellAccessoryNone;
    cell.textLabel.font = contentCell.label.font;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            return contentCell;
        case 1:
            return demandCell;
        case 2:
            return memoCell;
        case 3:{
            return [self createNolCellWith:@"巡检周期" identifiler:@"CycleCell" detailText:patrol_cycle isIndicator:YES];
        }
        case 4:{
            return [self createNolCellWith:@"开始时间" identifiler:@"BeginTimeCell" detailText:begin_time isIndicator:YES];
        }
        case 5:{
            return [self createNolCellWith:@"离开时间" identifiler:@"EndTimeCell" detailText:end_time isIndicator:YES];
        }
        case 6:{
            return [self createNolCellWith:@"基站" identifiler:@"BaseStopCell" detailText:o_name isIndicator:YES];
        }
        case 7:{
            return [self createNolCellWith:@"执行单位" identifiler:@"ExcuteCell" detailText:s_name isIndicator:YES];
        }
        default:
            return nil;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 3:
            [self showWorkCyclePicker];
            break;
        case 4:
        case 5:
            [self showTimeSelectedWith:indexPath.row == 4?1:0];
            break;
        case 6:
            [self showOnamePicker];
            break;
        case 7:
            [self showSnamePicker];
            break;
        default:
            break;
    }
}


@end
