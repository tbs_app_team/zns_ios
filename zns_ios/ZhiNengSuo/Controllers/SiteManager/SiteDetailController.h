//
//  SiteDetailController.h
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/7.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SiteDetailController : UIViewController

@property (nonatomic,strong) ZNSObject *object;

@property (nonatomic,assign) BOOL isAdd;

@property (nonatomic,strong) dispatch_block_t updateCallback;

@end
