//
//  SiteDetailSiteInfoCell.h
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/8.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    SiteInfoActionTypeSiteName,
    SiteInfoActionTypeSiteType,
    SiteInfoActionTypeSiteSectionAgent,
    SiteInfoActionTypeSiteLocation,
} SiteInfoActionTtyp;

@interface SiteDetailSiteInfoCell : UITableViewCell

@property (nonatomic,strong) ZNSObject *obj;

@property (nonatomic,copy) stringBlock siteNameBlcok;
@property (nonatomic,copy) indexBlock actionBlock;
@property (nonatomic,copy) voidBlock saveBlock;

+ (CGFloat)rowHeight;

@end
