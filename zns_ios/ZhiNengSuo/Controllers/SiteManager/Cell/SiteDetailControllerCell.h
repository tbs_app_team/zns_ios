//
//  SiteDetailControllerCell.h
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/8.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    ControllerActionID,
    ControllerActionType,
    ControllerActionWorkMode,
    ControllerActionRemark,
} ControllerAction;

@interface SiteDetailControllerCell : UITableViewCell

@property (nonatomic,strong) ZNSObject *obj;

@property (nonatomic,copy) dicBlock textBlcok;
@property (nonatomic,copy) indexBlock actionBlock;
@property (nonatomic,copy) voidBlock saveBlock;
@property (nonatomic,copy) voidBlock deleteBlock;

+ (CGFloat)rowHeightWithIsAdd:(BOOL)isAdd;
- (void)hiddenSaveBtn;

@end
