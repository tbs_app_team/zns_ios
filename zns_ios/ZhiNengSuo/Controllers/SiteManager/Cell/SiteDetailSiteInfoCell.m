//
//  SiteDetailSiteInfoCell.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/8.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import "SiteDetailSiteInfoCell.h"

@interface SiteDetailSiteInfoCell()<UITextFieldDelegate>{
    
    IBOutlet UITextField *siteName;
    IBOutlet UITextField *siteType;
    IBOutlet UITextField *siteSectionAgent;
    IBOutlet UITextField *siteLocation;
    
}

@end

@implementation SiteDetailSiteInfoCell

+ (CGFloat)rowHeight{
    return 225;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    siteName.delegate = self;
    siteType.delegate = self;
    siteSectionAgent.delegate = self;
    siteLocation.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setObj:(ZNSObject *)obj{
    if (!obj) {
        return;
    }
    siteName.text = obj.name;
    siteType.text = obj.type;
    siteSectionAgent.text = obj.company_name;
    siteLocation.text = obj.lat==nil ? @"":[NSString stringWithFormat:@"经度：%@ 纬度：%@",obj.lng,obj.lat];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if ([textField isEqual:siteName]) {
        return YES;
    }
    if (self.actionBlock) {
        self.actionBlock(textField.tag-100);
    }
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField isEqual:siteName]) {
        if (self.siteNameBlcok) {
            self.siteNameBlcok([textField.text stringByReplacingCharactersInRange:range withString:string]);
        }
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if ([textField isEqual:siteName]) {
        if (self.siteNameBlcok) {
            self.siteNameBlcok(textField.text);
        }
    }
}

- (IBAction)saveAction:(UIButton *)sender {
    
    if (self.saveBlock) {
        self.saveBlock();
    }
}

@end
