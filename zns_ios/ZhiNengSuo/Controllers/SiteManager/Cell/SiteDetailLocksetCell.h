//
//  SiteDetailLocksetCell.h
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/8.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    LocksetActionName,
    LocksetActionDoorType,
    LocksetActionDoorCode,
    LocksetActionLockType,
    LocksetActionLockRfid,
    LocksetActionLockBtAdr,
} LocksetAction;

@interface SiteDetailLocksetCell : UITableViewCell

@property (nonatomic,strong) ZNSLock *lock;

@property (nonatomic,copy) dicBlock textBlcok;
@property (nonatomic,copy) indexBlock actionBlock;
@property (nonatomic,copy) voidBlock saveBlock;
@property (nonatomic,copy) voidBlock deleteBlock;
@property (nonatomic,copy) indexBlock toolBarActionBlock;

+ (CGFloat)rowHeight;
- (void)hiddenSaveBtn;

@end
