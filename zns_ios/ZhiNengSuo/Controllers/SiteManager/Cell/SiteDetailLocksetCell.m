//
//  SiteDetailLocksetCell.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/8.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import "SiteDetailLocksetCell.h"

@interface SiteDetailLocksetCell()<UITextFieldDelegate>{
    
    IBOutlet UITextField *lockName;
    IBOutlet UITextField *doorType;
    IBOutlet UITextField *doorCode;
    IBOutlet UITextField *lockType;
    IBOutlet UITextField *lockRfid;
    IBOutlet UITextField *lockBtadr;
    IBOutlet NSLayoutConstraint *save_w;
    
}

@end

@implementation SiteDetailLocksetCell

+ (CGFloat)rowHeight{
    return 315;
}

- (void)hiddenSaveBtn{
    
    save_w.constant = 0;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    lockName.delegate = self;
    doorCode.delegate = self;
    doorType.delegate = self;
    lockType.delegate = self;
    lockRfid.delegate = self;
    lockBtadr.delegate = self;
    
    lockRfid.inputAccessoryView = [self setupToolBarWithSEL:@selector(toolBarActionForRfid)];
    lockBtadr.inputAccessoryView = [self setupToolBarWithSEL:@selector(toolBarActionForBtddr)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (UIToolbar *)setupToolBarWithSEL:(SEL)sel{
    UIToolbar *numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"采码" style:UIBarButtonItemStyleDone target:self action:sel],
                           nil];
    [numberToolbar sizeToFit];
    return numberToolbar;
}

- (void)setLock:(ZNSLock *)lock{
    if (!lock) {
        return;
    }
    lockName.text = lock.name;
    doorType.text = lock.doortype;
    doorCode.text = lock.doorcode;
    lockType.text = lock.type;
    lockRfid.text = lock.rfid;
    lockBtadr.text = lock.btaddr;
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if ([textField isEqual:lockName] || [textField isEqual:lockRfid] || [textField isEqual:lockBtadr]) {
        return YES;
    }
    if (self.actionBlock) {
        self.actionBlock(textField.tag-100);
    }
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField isEqual:lockName] || [textField isEqual:lockRfid] || [textField isEqual:lockBtadr]) {
        if (self.textBlcok) {
            NSInteger txtID = LocksetActionName;
            if ([textField isEqual:lockRfid]) {
                txtID = LocksetActionLockRfid;
            }else if ([textField isEqual:lockBtadr]){
                txtID = LocksetActionLockBtAdr;
            }
            self.textBlcok(@{@"text":[textField.text stringByReplacingCharactersInRange:range withString:string],@"id":[NSNumber numberWithInteger:txtID]});
        }
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if ([textField isEqual:lockName] || [textField isEqual:lockRfid] || [textField isEqual:lockBtadr]) {
        if (self.textBlcok) {
            NSInteger txtID = LocksetActionName;
            if ([textField isEqual:lockRfid]) {
                txtID = LocksetActionLockRfid;
            }else if ([textField isEqual:lockBtadr]){
                txtID = LocksetActionLockBtAdr;
            }
            self.textBlcok(@{@"text":textField.text,@"id":[NSNumber numberWithInteger:txtID]});
        }
    }
}

- (IBAction)saveAction:(UIButton *)sender {
    if (self.saveBlock) {
        self.saveBlock();
    }
}


- (IBAction)deleteAction:(UIButton *)sender {
    if (self.deleteBlock) {
        self.deleteBlock();
    }
}

- (void)toolBarActionForRfid{
    if (self.toolBarActionBlock) {
        self.toolBarActionBlock(LocksetActionLockRfid);
    }
}

- (void)toolBarActionForBtddr{
    if (self.toolBarActionBlock) {
        self.toolBarActionBlock(LocksetActionLockBtAdr);
    }
}

@end
