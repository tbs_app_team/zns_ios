//
//  SiteManagerListCell.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/6.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import "SiteManagerListCell.h"

@interface SiteManagerListCell(){
    
    IBOutlet UILabel *lblName;
    IBOutlet UILabel *lblArea;
    IBOutlet UILabel *lblDeviceID;
    IBOutlet UILabel *lblControlID;
    IBOutlet UILabel *lblSection;
    IBOutlet UILabel *lblModel;
    
}

@end

@implementation SiteManagerListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setObj:(ZNSObject *)obj{
    
    if (!obj) {
        return;
    }
    lblName.text = obj.name;
    lblArea.text = obj.area;
    lblDeviceID.text = obj.sitecode;
    lblSection.text = obj.company_name;
    
    if (!obj.controller.count) {
        return;
    }
    ZNSController *ctrl = obj.controller.firstObject;
    lblControlID.text = ctrl.code == 0?@"":@(ctrl.code).stringValue;
    lblModel.text = ctrl.work_mode;
    
}

@end
