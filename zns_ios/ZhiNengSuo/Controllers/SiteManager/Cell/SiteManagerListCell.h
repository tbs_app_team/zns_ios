//
//  SiteManagerListCell.h
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/6.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSObject.h"

@interface SiteManagerListCell : UITableViewCell

@property (nonatomic,strong) ZNSObject *obj;

@end
