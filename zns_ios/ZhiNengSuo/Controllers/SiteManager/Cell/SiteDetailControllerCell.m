//
//  SiteDetailControllerCell.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/8.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import "SiteDetailControllerCell.h"

@interface SiteDetailControllerCell()<UITextFieldDelegate>{
    
    IBOutlet UITextField *controllerID;
    IBOutlet UITextField *controllerType;
    IBOutlet UITextField *controllerWorkMode;
    IBOutlet UITextField *controllerRemark;
    IBOutlet NSLayoutConstraint *save_w;
    
}

@end

@implementation SiteDetailControllerCell

+ (CGFloat)rowHeightWithIsAdd:(BOOL)isAdd;{
    return isAdd?180:225;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    controllerID.delegate = self;
    controllerType.delegate = self;
    controllerWorkMode.delegate = self;
    controllerRemark.delegate = self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)hiddenSaveBtn{
    
    save_w.constant = 0;
}

- (void)setObj:(ZNSObject *)obj{
    if (!obj.controller.count) {
        return;
    }
    ZNSController *ctrl = obj.controller.firstObject;
    controllerID.text = ctrl.code == 0?@"":@(ctrl.code).stringValue;
    controllerType.text = ctrl.type;
    controllerWorkMode.text = ctrl.work_mode;
    controllerRemark.text = ctrl.remark;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if ([textField isEqual:controllerID] || [textField isEqual:controllerRemark]) {
        return YES;
    }
    if (self.actionBlock) {
        self.actionBlock(textField.tag-100);
    }
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
     if ([textField isEqual:controllerID] || [textField isEqual:controllerRemark]) {
        if (self.textBlcok) {
            NSInteger txtID = ControllerActionRemark;
            if ([textField isEqual:controllerID]) {
                txtID = ControllerActionID;
            }
            self.textBlcok(@{@"text":[textField.text stringByReplacingCharactersInRange:range withString:string],@"id":[NSNumber numberWithInteger:txtID]});
        }
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if ([textField isEqual:controllerID] || [textField isEqual:controllerRemark]) {
        if (self.textBlcok) {
            NSInteger txtID = ControllerActionRemark;
            if ([textField isEqual:controllerID]) {
                txtID = ControllerActionID;
            }
            self.textBlcok(@{@"text":textField.text,@"id":[NSNumber numberWithInteger:txtID]});
        }
    }
}

- (IBAction)saveAction:(UIButton *)sender {
    
    if (self.saveBlock) {
        self.saveBlock();
    }
}
- (IBAction)deleteAction:(UIButton *)sender {
    
    if (self.deleteBlock) {
        self.deleteBlock();
    }
}

@end
