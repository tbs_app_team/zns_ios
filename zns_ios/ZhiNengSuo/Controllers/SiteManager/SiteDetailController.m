//
//  SiteDetailController.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/7.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import "SiteDetailController.h"
#import "GetLocationCommonViewController.h"
#import "CommonTableViewController.h"
#import "BlueToothViewController.h"

#import "SiteDetailSiteInfoCell.h"
#import "SiteDetailControllerCell.h"
#import "SiteDetailLocksetCell.h"
#import "SiteAddSiteInfoCell.h"

#import "ZNSZone.h"
#import "ZNSSection.h"


@interface SiteDetailController ()<UITableViewDelegate,UITableViewDataSource>{
     
    IBOutlet UITableView *tbvSite;
}
// 用作获取蓝牙地址
@property (nonatomic,strong) NSMutableArray *bts;

@property (nonatomic,strong) NSArray *sections;          //所属区域数组
@property (nonatomic ,strong) ZNSSection *selcectSection;

@property (nonatomic,strong) NSArray *companys;          //代维单位数组

@property (nonatomic,strong) NSArray *lockTypes;         //锁类型数组
@property (nonatomic,strong) NSArray *objectTypes;       //站点（设备）类型数组
@property (nonatomic,strong) NSArray *rules;             //开锁范围数组
@property (nonatomic,strong) NSArray *doorTypes;         //门类型数组
@property (nonatomic,strong) NSArray *doorCodes;         //门编号数组
@property (nonatomic,strong) NSArray *controllerTypes;   //控制器的类型数组
@property (nonatomic,strong) NSArray *workModes;         //控制器的工作模式数组

@property (nonatomic, strong) JKAlertDialog *dialog;

@end

@implementation SiteDetailController

#pragma mark - API

- (void)addSite{
    
    if (self.object.area.length<=0) {
        [SVProgressHUD showErrorWithStatus:@"请选择区域"];
        return;
    }
    
    if (self.object.zname.length<=0) {
        [SVProgressHUD showErrorWithStatus:@"请选择开锁范围"];
        return;
    }
    
    if (self.object.name.length<=0 || [[NSString isEmpty:self.object.name] isEqualToString:@"yes"]) {
        [SVProgressHUD showErrorWithStatus:@"站点名称不能为空"];
        return;
    }
    
    if (self.object.name.length>32) {
        [SVProgressHUD showErrorWithStatus:@"设备名称不能超过32位"];
        return;
    }
    
    if (self.object.type.length<=0) {
        [SVProgressHUD showErrorWithStatus:@"请选择站点类型"];
        return;
    }
    
    if (self.object.lockset.count>20) {
        [SVProgressHUD showErrorWithStatus:@"一个站点最多只能安装20把锁"];
        return;
    }
    
    //遍历检查锁具是否符合输入规则
    [self checkLockIsOk];
    
    [SVProgressHUD show];
//    @weakify(self);
    [APIClient POST:[NSString stringWithFormat:@"section/%@/object/new/lockset/zone",self.selcectSection.sid]
         withParameters:[self.object generteNewObjectParameters] successWithBlcok:^(id response) {
//             @strongify(self);
             [SVProgressHUD showSuccessWithStatus:@"上传成功"];
             if (self.updateCallback) {
                 self.updateCallback();
             }
         } errorWithBlock:^(ZNSError *error) {
             [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
}

- (void)updateDevece{
    
    if (self.object.name.length<=0) {
        [SVProgressHUD showErrorWithStatus:@"站点名称不能为空"];
        return;
    }
    
    if (self.object.name.length>32) {
        [SVProgressHUD showErrorWithStatus:@"站点名称不能超过32位"];
        return;
    }
    
    if (self.object.type.length<=0) {
        [SVProgressHUD showErrorWithStatus:@"请选择站点类型"];
        return;
    }
    
    if (self.object.controller.count) {
        ZNSController *ctrl = self.object.controller.firstObject;
        if (ctrl.code == 0) {
            [SVProgressHUD showErrorWithStatus:@"请输入控制器ID"];
            return;
        }
        if (![ctrl.type length]) {
            [SVProgressHUD showErrorWithStatus:@"请选择控制器类型"];
            return;
        }
        if (![ctrl.work_mode length]) {
            [SVProgressHUD showErrorWithStatus:@"请选择控制器工作模式"];
            return;
        }
    }
    
    [SVProgressHUD show];
    [APIClient POST:[NSString stringWithFormat:@"object/%@/update2",self.object.oid]
     withParameters:[self.object modifyObjectParameters] successWithBlcok:^(id response) {
         [SVProgressHUD showSuccessWithStatus:@"修改成功"];
         if (self.updateCallback) {
             self.updateCallback();
         }
     } errorWithBlock:^(ZNSError *error) {
         [SVProgressHUD showErrorWithStatus:error.errorMessage];
     }];
}

//删除锁具
- (void)deleLockset:(ZNSLock *)lock  position:(NSInteger)position{
    
//    if (lock.oid == nil) {
        NSMutableArray *locks = [NSMutableArray arrayWithArray:self.object.lockset];
        [locks removeObject:lock];
        self.object.lockset = locks;
        [tbvSite deleteSections:[NSIndexSet indexSetWithIndex:position] withRowAnimation:UITableViewRowAnimationFade];
        return;
//    }
//    @weakify(self)
//    [APIClient POST:API_LOCKSET_DEL_WITH_LID(lock.lid) withParameters:@{@"type":lock.type,
//                                                                        @"rfid":lock.rfid==nil?@"":lock.rfid,
//                                                                        @"btaddr":lock.btaddr==nil?@"":lock.btaddr,
//                                                                        @"name":lock.name}
//   successWithBlcok:^(id response) {
//       @strongify(self)
//       //           ZNSLock *updateLock = self.object.locks[position];
//       NSMutableArray *locks = [NSMutableArray arrayWithArray:self.object.lockset];
//       [locks removeObject:lock];
//       self.object.lockset = locks;
//       [SVProgressHUD showSuccessWithStatus:@"删除成功"];
//       [tbvSite deleteSections:[NSIndexSet indexSetWithIndex:position] withRowAnimation:UITableViewRowAnimationFade];
//
//   } errorWithBlock:^(ZNSError *error) {
//       [SVProgressHUD showErrorWithStatus:error.errorMessage];
//   }] ;
}

- (void)saveLockset:(ZNSLock *)lock position:(NSInteger)position{
    if (lock.type == nil || lock.type == 0) {
        [SVProgressHUD showErrorWithStatus:@"请选择锁类型"];
        return;
    }
    if (lock.name == nil || lock.name.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"请填写锁具名称"];
        return;
    } 
    //蓝牙地址／锁码值可以为空，只有在不为空的情况下去判断
    if (lock.btaddr.length>0) {
        
        if (lock.btaddr.length != 12) {
            [SVProgressHUD showErrorWithStatus:@"蓝牙地址的长度应为12位"];
            return;
        }
        if (![self isHexString:lock.btaddr]) {
            [SVProgressHUD showErrorWithStatus:@"请输入十六进制的蓝牙地址"];
            return;
        }
        
        lock.btaddr = lock.btaddr.uppercaseString;
    }
    
    if (lock.rfid.length>0) {
        
        if (lock.rfid.length != 10) {
            [SVProgressHUD showErrorWithStatus:@"锁码的长度应为10位"];
            return;
        }
        if (![self isHexString:lock.rfid]) {
            [SVProgressHUD showErrorWithStatus:@"请输入十六进制的锁码值"];
            return;
        }
        lock.rfid = lock.rfid.uppercaseString;
    }
    
    if (lock.oid == nil) {//新建保存
        @weakify(self)
        [APIClient POST:API_LOCKSET_NEW_WITH_OID(self.object.oid) withParameters:@{@"type":lock.type,
                                                                                   @"rfid":lock.rfid==nil?@"":lock.rfid,
                                                                                   @"btaddr":lock.btaddr==nil?@"":lock.btaddr,
                                                                                   @"name":lock.name,
                                                                                   @"doortype":lock.doortype,
                                                                                   @"doorcode":lock.doorcode}
       successWithBlcok:^(id response) {
           @strongify(self)
           ZNSLock *updateLock = self.object.lockset[position - 2];
           updateLock.lid = response[@"id"];
           NSMutableArray *locks = [NSMutableArray arrayWithArray:self.object.lockset];
           [locks replaceObjectAtIndex:position-2 withObject:updateLock];
           self.object.lockset = locks;
           [SVProgressHUD showSuccessWithStatus:@"保存成功"];
       } errorWithBlock:^(ZNSError *error) {
           [SVProgressHUD showErrorWithStatus:error.errorMessage];
       }] ;
        
    }else{//修改保存
        //        @weakify(self)
        [APIClient POST:API_LOCKSET_MODIFY_WITH_OID(lock.lid) withParameters:@{@"type":lock.type,
                                                                               @"rfid":lock.rfid==nil?@"":lock.rfid,
                                                                               @"btaddr":lock.btaddr==nil?@"":lock.btaddr,
                                                                               @"name":lock.name,
                                                                               @"doortype":lock.doortype,
                                                                               @"doorcode":lock.doorcode
                                                                               }
       successWithBlcok:^(id response) {
           [SVProgressHUD showSuccessWithStatus:@"修改成功"];
           
       } errorWithBlock:^(ZNSError *error) {
           [SVProgressHUD showErrorWithStatus:error.errorMessage];
       }] ; 
    }
}

#pragma mark - 系统方法


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = self.isAdd?@"新建站点":@"站点详情";
    
//    if (self.isAdd) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(doSaveSite)];
//    }
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    tbvSite.editing = YES;
     [tbvSite registerNib:[SiteAddSiteInfoCell nib] forCellReuseIdentifier:[SiteAddSiteInfoCell reuseIdentifier]];
    [tbvSite registerNib:[SiteDetailControllerCell nib] forCellReuseIdentifier:[SiteDetailControllerCell reuseIdentifier]];
     [tbvSite registerNib:[SiteDetailLocksetCell nib] forCellReuseIdentifier:[SiteDetailLocksetCell reuseIdentifier]];
    [tbvSite registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
     
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Other

- (void)doSaveSite{
    if (self.isAdd) {
        [self addSite];
    }else{
        [self updateDevece];
    }
}

- (BOOL)isHexString:(NSString*)str{
    
    int i;
    for (i=0; i<[str length]; i++) {
        if (([str characterAtIndex:i]>='0' && [str characterAtIndex:i]<='9')||
            ([str characterAtIndex:i]>='a' && [str characterAtIndex:i]<='f') ||
            ([str characterAtIndex:i]>='A' && [str characterAtIndex:i]<='F' ) ){
            //            return YES;
        }else{
            return NO;
        }
    }
    return YES;
}

//获取当前站点的控制器
- (ZNSController *)getCurrentObjectController{
    ZNSController *ctrl;
    if (!self.object.controller.count) {
       ctrl = [ZNSController new];
       self.object.controller = @[ctrl];
    }
    ctrl = self.object.controller.firstObject;
    return ctrl;
}

- (void)checkLockIsOk{
    
    NSInteger i = 1;
    for (ZNSLock *lock in self.object.lockset) {
        
        if (lock.name.length<=0) {
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"锁具%@名称不能为空",@(i).stringValue]];
            return;
        }
        if (lock.name.length>32) {
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"锁具%@名称不能超过32位",@(i).stringValue]];
            return;
        }
        
        if (![lock.doortype length]) {
             [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"请选择锁具%@的门类型",@(i).stringValue]];
            return;
        }
        
        //门类型为光交箱门时门编码为必填，其他情况为选填
        if ([lock.doortype isEqualToString:@"光交箱门"] && ![lock.doorcode length]) {
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"请选择锁具%@的门编码",@(i).stringValue]];
            return;
        }
        if (lock.type.length<=0) {
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"请选择锁具%@的锁类型",@(i).stringValue]];
            return;
        }
        //蓝牙地址／锁码值可以为空，只有在不为空的情况下去判断
        if (lock.btaddr.length>0) {
            
            if (lock.btaddr.length != 12) {
                [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"锁具%@蓝牙地址的长度应为12位",@(i).stringValue]];
                return;
            }
            if (![self isHexString:lock.btaddr]) {
                [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"锁具%@请输入十六进制的蓝牙地址",@(i).stringValue]];
                return;
            }
            lock.btaddr = lock.btaddr.uppercaseString;
        }
        
        if (lock.rfid.length>0) {
            if (lock.rfid.length != 10) {
                 [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"锁具%@的锁码长度应为10位",@(i).stringValue]];
                return;
            }
            if (![self isHexString:lock.rfid]) {
                 [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"锁具%@请输入十六进制的锁码值",@(i).stringValue]];
                return;
            }
            
            lock.rfid = lock.rfid.uppercaseString;
        }
    }
}

#pragma mark - 点击选择类方法

- (void)showZonePicker{
    if (!self.rules.count) {
        [SVProgressHUD showWithStatus:@"正在获取数据"];
        @weakify(self)
        [ZNSAPITool getAllZonesSuccessWithBlcok:^(id response) {
            @strongify(self);
            self.rules = response;
            if (self.rules.count) { 
                [SVProgressHUD dismiss];
                [self showZonePicker];
            }else{
                [SVProgressHUD showErrorWithStatus:@"数据为空"];
            }
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:@"获取数据失败，请重新点击获取"];
        }];
        return;
    }
    @weakify(self);
    [ActionSheetStringPicker showPickerWithTitle:@"选择开锁范围" rows:[self.rules valueForKey:@"name"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        self.object.zid = [[self.rules objectAtIndex:selectedIndex] valueForKey:@"id"];
        self.object.zname = selectedValue;
        [tbvSite reloadSection:0 withRowAnimation:UITableViewRowAnimationFade];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

//获取区域
- (void)showSectionPicker{
    @weakify(self);
    if (!self.sections.count) {
        [SVProgressHUD showWithStatus:@"正在获取数据"];
        [ZNSAPITool getSectionWithUserName:[ZNSUser currentUser].username success:^(id response) {
            self.sections = response;
            if (self.sections.count) {
                [SVProgressHUD dismiss];
                [self showSectionPicker];
            }else{
                [SVProgressHUD showErrorWithStatus:@"数据为空"];
            }
        } errorWithBlock:^(ZNSError *error) {
             [SVProgressHUD showErrorWithStatus:@"获取数据失败，请重新点击获取"];
        }];
//        [ZNSAPITool getAllAreaSuccessWithBlcok:^(NSArray *response) {
//            @strongify(self);
//            self.sections = @[];
//            [response enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                ZNSSection *sec = obj;
//                if ([sec.type isEqualToString:@"区域"]) {
//                    self.sections = [self.sections arrayByAddingObject:sec];
//                }
//            }];
//            if (self.sections.count) {
//                [SVProgressHUD dismiss];
//                [self showSectionPicker];
//            }else{
//                [SVProgressHUD showErrorWithStatus:@"数据为空"];
//            }
//        } errorWithBlock:^(ZNSError *error) {
//            [SVProgressHUD showErrorWithStatus:@"获取数据失败，请重新点击获取"];
//        }];
        return;
    }
    [self.view endEditing:YES];
    [ActionSheetStringPicker showPickerWithTitle:@"选择区域" rows:[self.sections valueForKeyPath:@"name"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        self.object.area = selectedValue;
        self.selcectSection = [self.sections objectAtIndex:selectedIndex];
        self.companys = self.selcectSection.companys;
        
        if ([self.object.companyid length]) {
            self.object.companyid = @"";
            self.object.company_name = @"";
        } 
        [tbvSite reloadSection:0 withRowAnimation:UITableViewRowAnimationFade];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
    } origin:self.view];
}

- (void)showCompanysPicker{
    
    if (![self.object.area length]) {
        [SVProgressHUD showErrorWithStatus:@"请先选择区域"];
        return;
    }
    
    @weakify(self);
    if (!self.companys.count && !self.isAdd) {
        [SVProgressHUD showWithStatus:@"正在获取数据"];
        [ZNSAPITool getAgentSectionSuccessWithSID:self.isAdd?self.selcectSection.sid:@(self.object.sid).stringValue succesBlcok:^(id response) {
            @strongify(self);
            self.companys = response;
            if (self.companys.count) {
                [SVProgressHUD dismiss];
                [self showCompanysPicker];
            }else{
                [SVProgressHUD showErrorWithStatus:@"数据为空"];
            }
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:@"获取数据失败，请重新点击获取"];
        }];
        return;
    }
    [self.view endEditing:YES];
    [ActionSheetStringPicker showPickerWithTitle:@"选择代维单位" rows:[self.companys valueForKeyPath:@"name"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        if (self.companys.count) {
            self.object.company_name = selectedValue;
            self.object.companyid = [NSString stringWithFormat:@"%@",[self.companys valueForKey:@"id"][selectedIndex]];
            [tbvSite reloadSection:0 withRowAnimation:UITableViewRowAnimationFade];
        }
    } cancelBlock:^(ActionSheetStringPicker *picker) {
    } origin:self.view];
}

- (void)showObjectTypePicker{
     @weakify(self);
    if (!self.objectTypes.count) {
        [ZNSAPITool getAllObjectTypeSuccessWithBlcok:^(id response) {
            @strongify(self);
            self.objectTypes = response;
            if (self.objectTypes.count) {
                [SVProgressHUD dismiss];
                [self showObjectTypePicker];
            }else{
                [SVProgressHUD showErrorWithStatus:@"数据为空"];
            }
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:@"获取数据失败，请重新点击获取"];
        }];
        return;
    }
    [self.view endEditing:YES]; 
    [ActionSheetStringPicker showPickerWithTitle:@"选择设备类型" rows:self.objectTypes initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        self.object.type = selectedValue;
        [tbvSite reloadSection:0 withRowAnimation:UITableViewRowAnimationNone];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

- (void)goGetLocationPage{
    
    @weakify(self);
    GetLocationCommonViewController *vvc = [GetLocationCommonViewController create];
    vvc.mode = AddLockMode;
    [vvc  setLocationBlock:^(NSArray *location){
        @strongify(self);
        self.object.lat = location[0];
        self.object.lng = location[1];
        [tbvSite reloadSection:0 withRowAnimation:UITableViewRowAnimationNone];
    }];
    
    [self.navigationController pushViewController:vvc animated:YES];
}

- (void)showLockTypePickerAtIndexPath:(NSIndexPath *)indexPath{
    
    @weakify(self);
    if (!self.lockTypes.count) {
        [SVProgressHUD showWithStatus:@"正在获取数据"];
        [ZNSAPITool getAllLockTypeSuccessWithBlcok:^(id response) {
            @strongify(self);
            self.lockTypes = response;
            if (self.lockTypes.count) {
                [SVProgressHUD dismiss];
                [self showLockTypePickerAtIndexPath:indexPath];
            }else{
                [SVProgressHUD showErrorWithStatus:@"数据为空"];
            }
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:@"获取数据失败，请重新点击获取"];
        }];
        return;
    }
    [self.view endEditing:YES];
    [ActionSheetStringPicker showPickerWithTitle:@"选择锁具类型" rows:self.lockTypes initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        ZNSLock *lock = [self.object.lockset objectAtIndex:indexPath.section - 2];
        lock.type = selectedValue;
        [tbvSite reloadSection:indexPath.section withRowAnimation:UITableViewRowAnimationNone];;
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

- (void)showDoorTypePickerAtIndexPath:(NSIndexPath *)indexPath{
    
    @weakify(self);
    if (!self.doorTypes.count) {
        [SVProgressHUD showWithStatus:@"正在获取数据"];
        [ZNSAPITool getAllDoorTypeSuccessWithBlock:^(id response) {
            @strongify(self);
            self.doorTypes = response;
            if (self.doorTypes.count) {
                [SVProgressHUD dismiss];
                [self showDoorTypePickerAtIndexPath:indexPath];
            }else{
                [SVProgressHUD showErrorWithStatus:@"数据为空"];
            }
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:@"获取数据失败，请重新点击获取"];
        }];
        return;
    }
    [self.view endEditing:YES];
    [ActionSheetStringPicker showPickerWithTitle:@"选择门类型" rows:self.doorTypes initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        ZNSLock *lock = [self.object.lockset objectAtIndex:indexPath.section - 2];
        lock.doortype = selectedValue;
        [tbvSite reloadSection:indexPath.section withRowAnimation:UITableViewRowAnimationNone];;
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

- (void)showDoorCodePickerAtIndexPath:(NSIndexPath *)indexPath{
    ZNSLock *lock = [self.object.lockset objectAtIndex:indexPath.section - 2];
    if (![lock.doortype isEqualToString:@"光交箱门"]) {
        [SVProgressHUD showErrorWithStatus:@"只有光交箱门才可以选择门编号"];
        return;
    }
    
    @weakify(self);
    if (!self.doorCodes.count) {
        [SVProgressHUD dismiss];
        [ZNSAPITool getDoorcodeSuccessWithBlock:^(id response) {
            @strongify(self);
            [SVProgressHUD dismiss];
            self.doorCodes = response;
            if (!self.doorCodes.count) {
                [SVProgressHUD showErrorWithStatus:@"数据为空"];
            }else{
                [self showDoorCodePickerAtIndexPath:indexPath];
            }
        } errorWithBlock:^(ZNSError *error) {
             [SVProgressHUD showErrorWithStatus:@"获取数据失败，请重新点击获取"];
        }]; 
        return;
    }
    
//    @weakify(self);
    [self.view endEditing:YES];
    [ActionSheetStringPicker showPickerWithTitle:@"选择门编号" rows:self.doorCodes initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
//        @strongify(self);
        lock.doorcode = selectedValue;
        [tbvSite reloadSection:indexPath.section withRowAnimation:UITableViewRowAnimationNone];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
    } origin:self.view];
}

- (void)showControllerTypesPicker{
    
    @weakify(self);
    if (!self.controllerTypes.count) {
        [SVProgressHUD showWithStatus:@"正在获取数据"];
        [ZNSAPITool getControllerTypeSuccessWithBlock:^(id response) {
            @strongify(self);
            self.controllerTypes = response;
            if (self.controllerTypes.count) {
                [SVProgressHUD dismiss];
                [self showControllerTypesPicker];
            }else{
                [SVProgressHUD showErrorWithStatus:@"数据为空"];
            }
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:@"获取数据失败，请重新点击获取"];
        }];
        return;
    }
    [self.view endEditing:YES];
    [ActionSheetStringPicker showPickerWithTitle:@"选择控制器类型" rows:self.controllerTypes initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        [self getCurrentObjectController].type = selectedValue;
        [tbvSite reloadSection:1 withRowAnimation:UITableViewRowAnimationNone];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
    } origin:self.view];
}

- (void)showControllerWorkModesPicker{
    
    @weakify(self);
    if (!self.workModes.count) {
        [SVProgressHUD showWithStatus:@"正在获取数据"];
        [ZNSAPITool getControllerWorkModeSuccessWithBlock:^(id response) {
            @strongify(self);
            self.workModes = response;
            if (self.workModes.count) {
                [SVProgressHUD dismiss];
                [self showControllerWorkModesPicker];
            }else{
                [SVProgressHUD showErrorWithStatus:@"数据为空"];
            }
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:@"获取数据失败，请重新点击获取"];
        }];
        return;
    }
    [self.view endEditing:YES];
    [ActionSheetStringPicker showPickerWithTitle:@"选择控制器工作模式" rows:self.workModes initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        [self getCurrentObjectController].work_mode = selectedValue;
        [tbvSite reloadSection:1 withRowAnimation:UITableViewRowAnimationNone];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
    } origin:self.view];
}

- (void)showBTPickerAtIndexPath:(NSIndexPath *)index{
    [self.view endEditing:YES];
    [self.bts removeAllObjects];
    CommonTableViewController *tvc= [[CommonTableViewController alloc]initWithStyle:UITableViewStylePlain];
    
    _dialog = [[JKAlertDialog alloc]initWithTitle:@"选择蓝牙" message:@""];
    _dialog.contentView = tvc.view;
    
    
    @weakify(self);
    [[GGBluetooth sharedManager]setDiscoverPeripheralBlock:^(CBPeripheral *discoverPeripheral,NSDictionary *advertisementData,NSNumber *RSSI){
        @strongify(self);
        if (!self.bts) {
            self.bts = [NSMutableArray array];
        }
        
        if (![[self.bts valueForKey:@"identifier"] containsObject:discoverPeripheral.identifier]) {
            [self.bts addObject:discoverPeripheral];
            [tvc setDatas:self.bts];
        }
    }];
    
    [_dialog addButton:Button_CANCEL withTitle:@"取消" handler:^(JKAlertDialogItem *item) {
        
    }];;
    
    [tvc setSelectedBlock:^(CBPeripheral *discoverPeripheral){
        ZNSLock *lock = [self.object.lockset objectAtIndex:index.section - 2];
        lock.btaddr = discoverPeripheral.macAddress;
        [tbvSite reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
        [_dialog dismiss];
    }];
    
    [_dialog show];
    [[GGBluetooth sharedManager]startScan];
}

- (void)showRfidPickerWith:(NSIndexPath *)idxPath{
    
    if (![[GGBluetooth sharedManager] connectedPheripheral]) {
        [BlueToothViewController presentInViewContoller:self];
    }else{
        
        @weakify(self);
        [[GGBluetooth sharedManager] setNotifyCharacteristicBlock:^(CBCharacteristic *characteristic,NSData *recvData,NSError *error){
            @strongify(self);
            ZNSBluetoothInstruction *b = [[ZNSBluetoothInstruction alloc]initWithByteData:characteristic.value];
            
            ZNSLock *lock = [self.object.lockset objectAtIndex:idxPath.section - 2];
            lock.rfid = [b rfid];
            [tbvSite reloadRowsAtIndexPaths:@[idxPath] withRowAnimation:UITableViewRowAnimationNone];
        }];
        
        [SVProgressHUD showInfoWithStatus:@"请采集锁码"];
    }
}

#pragma mark - tableview 方法相关

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return [SiteAddSiteInfoCell rowHeightWithIsAdd:self.isAdd];
    }else if (indexPath.section == 1){
        return [SiteDetailControllerCell rowHeightWithIsAdd:YES];
    }else if (indexPath.section <= self.object.lockset.count + 1){
        return [SiteDetailLocksetCell rowHeight];
    }
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3+self.object.lockset.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    @weakify(self);
    
    //站点信息
    if (indexPath.section == 0) {
        SiteAddSiteInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:[SiteAddSiteInfoCell reuseIdentifier] forIndexPath:indexPath];
        [cell currentModeWith:self.isAdd];
        cell.obj = self.object;
        cell.siteNameBlcok = ^(NSString *result) {
            @strongify(self);
            self.object.name = result;
        };
        
        cell.actionBlock = ^(NSInteger index) {
            @strongify(self);
            switch (index) {
                case AddSiteInfoActionArea:
                    [self showSectionPicker];
                    break;
                case AddSiteInfoActionRange:
                    [self showZonePicker];
                    break;
                case AddSiteInfoActionType://站点类型
                    [self showObjectTypePicker];
                    break;
                case AddSiteInfoActionSectionAgent://代维单位
                    [self showCompanysPicker];
                    break;
                case AddSiteInfoActionLocation://位置点
                    [self goGetLocationPage];
                    break;
                default:
                    break;
            }
        };
        cell.saveBlock = ^{
            @strongify(self);
            [self updateDevece];
        };
        return cell;
    }else if (indexPath.section == 1){
        
        SiteDetailControllerCell *cell = [tableView dequeueReusableCellWithIdentifier:[SiteDetailControllerCell reuseIdentifier] forIndexPath:indexPath];
//        if (self.isAdd) {
            [cell hiddenSaveBtn];
//        }
        cell.obj = self.object;
        cell.textBlcok = ^(NSDictionary *dic) {
            @strongify(self);
            ControllerAction txtID = [dic[@"id"] integerValue];
            if (txtID == ControllerActionID) {//控制器ID
                [self getCurrentObjectController].code = [dic[@"text"] integerValue];
            }else{
                //控制器备注
                [self getCurrentObjectController].remark = dic[@"text"];
            }
        };
        
        cell.actionBlock = ^(NSInteger index) {
            @strongify(self);
            switch (index) {
                case ControllerActionType://控制器类型
                    [self showControllerTypesPicker];
                    break;
                case ControllerActionWorkMode://工作模式
                    [self showControllerWorkModesPicker];
                    break;
                default:
                    break;
            }
        };
        cell.saveBlock = ^{
            @strongify(self);
            [self updateDevece];
        };
        return cell;
    }else if (indexPath.section <= self.object.lockset.count+1){
        
        SiteDetailLocksetCell *cell = [tableView dequeueReusableCellWithIdentifier:[SiteDetailLocksetCell reuseIdentifier] forIndexPath:indexPath];
//        if (self.isAdd) {
            [cell hiddenSaveBtn];
//        }
        ZNSLock *lock = self.object.lockset[indexPath.section - 2];
        cell.lock = lock;
        cell.textBlcok = ^(NSDictionary *dic) {
//            @strongify(self);
            LocksetAction txtID = [dic[@"id"] integerValue];
            if (txtID == LocksetActionLockRfid) {
                lock.rfid  = dic[@"text"];
            }else if (txtID == LocksetActionLockBtAdr){
                lock.btaddr = dic[@"text"];
            }else{
               lock.name = dic[@"text"];
            }
        };
        
        cell.actionBlock = ^(NSInteger index) {
             @strongify(self);
            switch (index) {
                case LocksetActionDoorType://门类型
                    [self showDoorTypePickerAtIndexPath:indexPath];
                    break;
                case LocksetActionDoorCode://门编号
                    [self showDoorCodePickerAtIndexPath:indexPath];
                    break;
                case LocksetActionLockType://锁类型
                    [self showLockTypePickerAtIndexPath:indexPath];
                    break;
                default:
                    break;
            }
        };
        
        @weakify(self);
        cell.toolBarActionBlock = ^(NSInteger index) {
            @strongify(self);
            if (index == LocksetActionLockRfid) {
                [self showRfidPickerWith:indexPath];
            }else if (index == LocksetActionLockBtAdr){
                [self showBTPickerAtIndexPath:indexPath];
            }
         };
        
        cell.saveBlock = ^{
            @strongify(self);
            [self saveLockset:lock position:indexPath.section];
        };
        cell.deleteBlock = ^{
            @strongify(self);
            [self deleLockset:lock position:indexPath.section];
        };
        return cell;
        
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.text = @"新增锁具"; 
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return @"站点信息";
    }else if (section == 1){
        return @"控制器";
    }else if (section > 1 && section <= self.object.lockset.count + 1){
        return [NSString stringWithFormat:@"锁具%@",@(section - 1).stringValue];
    }
    return @"";
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 3 + self.object.lockset.count - 1) {
        return YES;
    }
    return NO;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 3 + self.object.lockset.count - 1) {
        return UITableViewCellEditingStyleInsert;
    }
    return UITableViewCellEditingStyleNone;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleInsert) {

        if (!self.object.lockset) {
            self.object.lockset = @[];
        }
        self.object.lockset = [self.object.lockset arrayByAddingObject:[ZNSLock new]];
        [tableView insertSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
    }else if (editingStyle == UITableViewCellEditingStyleDelete){
    }
}

@end
