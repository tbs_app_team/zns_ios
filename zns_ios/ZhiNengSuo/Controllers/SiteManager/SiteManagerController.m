//
//  SiteManagerController.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/6.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import "SiteManagerController.h"
#import "SiteManagerListCell.h"
#import "SiteDetailController.h" 

@interface SiteManagerController (){
    
    NSInteger pageIndex;
    NSArray *sites;
}

@end

@implementation SiteManagerController

- (void)doGetSiteInfoWithSearchText:(NSString *)text{
   
    @weakify(self)
    [APIClient POST:[NSString stringWithFormat:@"%@downward",API_SECTION_OBJECT_WITH_SID(@"0")] withParameters:@{@"search":text,@"page":[NSNumber numberWithInteger:pageIndex],@"pagesize":[NSNumber numberWithInteger:20]} successWithBlcok:^(id response) {
        @strongify(self)
        if (pageIndex == 0) {
            sites = [NSArray yy_modelArrayWithClass:[ZNSObject class] json:response[@"items"]];
        }else{
            sites = [sites arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSObject class] json:response[@"items"]]];
        }
        [SVProgressHUD dismiss];
        
        [self reloadData];
        
        [self endRefreshing];
        
        if (([[response valueForKey:@"page"] integerValue] + 1) == [[response valueForKey:@"pages"]integerValue]) {
            [self endRefreshingWithNoMoreData];
        }
        
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
        [self endRefreshing];
        if (pageIndex > 0) {
            pageIndex--;
        }
    }] ;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"站点信息管理";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"新增" style:UIBarButtonItemStylePlain target:self action:@selector(goAddSitePage)];
    [SVProgressHUD show];
    [self doGetSiteInfoWithSearchText:@""];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goAddSitePage{
    
    SiteDetailController *add = [SiteDetailController create];
    @weakify(self);
    add.updateCallback = ^{
        @strongify(self);
        [self headerWithRefreshing];
    };
    add.object = [ZNSObject new];
    add.isAdd = YES;
    [self.navigationController pushViewController:add animated:YES];
}

#pragma mark - 父类方法
//下拉刷新
- (void)headerWithRefreshing{
    pageIndex = 0;
    [self doGetSiteInfoWithSearchText:@""];
}
//上拉加载
- (void)footerWithRefreshing{
    pageIndex++;
    [self doGetSiteInfoWithSearchText:@""];
}
//搜索
- (void)searchWithController:(UISearchController *)ctrl{
    
    if (ctrl.active) {
        if ([ctrl.searchBar.text length]) {
            [SVProgressHUD show];
            [self doGetSiteInfoWithSearchText:ctrl.searchBar.text];
        }
    }else{
        [SVProgressHUD show];
        [self doGetSiteInfoWithSearchText:@""];
    }
}

/*
 * tableview 相关
 */

//cell的高度
- (CGFloat)rowHeight{
    return 100;
} 

//行数
- (NSInteger)row{
    return sites.count;
}

//cell的标识
- (NSString *)cellIdentifier{
   return [SiteManagerListCell reuseIdentifier];
}

- (UINib *)cellNib{
   return [SiteManagerListCell nib];
}

- (UITableViewCell *)cellWithTableView:(UITableView *)tbv indexPath:(NSIndexPath *)idxPath{
    
    SiteManagerListCell *cell = [tbv dequeueReusableCellWithIdentifier:[SiteManagerListCell reuseIdentifier] forIndexPath:idxPath];
    cell.obj = sites[idxPath.row];
    return cell;
}
 
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    SiteDetailController *siteDetail = [SiteDetailController create];
    @weakify(self);
    siteDetail.updateCallback = ^{
        @strongify(self);
        [self headerWithRefreshing];
    };
    
    siteDetail.object = [sites[indexPath.row] copy];
    [self.navigationController pushViewController:siteDetail animated:YES];
}

@end
