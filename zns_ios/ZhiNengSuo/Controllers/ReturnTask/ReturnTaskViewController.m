//
//  ReturnTaskViewController.cpp
//  ZhiNengSuo
//
//  Created by liuguanhong on 17/5/16.
//  Copyright © 2017年 czwen. All rights reserved.
//

#include "ReturnTaskViewController.h"
//
#import "BlueToothViewController.h"
//#import "ZNSTempTask.h"
//#import "JKAlertDialog.h"
#import "ZNSTools.h"

@interface ReturnTaskViewController (){
    JKAlertDialog *dialog;
}

//@property (nonatomic,strong) JKAlertDialog *dialog;
//@property (nonatomic,strong) NSMutableData *firstFrame;
//@property (nonatomic,strong) NSMutableData *lockIdFrames;
@property (nonatomic,strong) NSMutableData *recordFrames;
//@property (nonatomic,strong) NSArray * arrData;
//@property (nonatomic,assign) CBCentralManagerState blueState;
@property (nonatomic,strong) NSMutableArray * tickets;
@property (nonatomic) int ticketIndex;

@end

@implementation ReturnTaskViewController

- (NSString *)getTicketAction:(Byte)ticketType {
    switch (ticketType) {
        case TransTemp:
            return @"TransTemp";
            break;
        case TransTask:
            return @"TransTask";
            break;
        case TransFixed:
            return @"TransFixed";
            break;
        case TransAllUnlock:
            return @"TransAllUnlock";
            break;
        case TransGuardTour:
            return @"TransGuardTour";
            break;
        case TransPermanent:
            return @"TransPermanent";
            break;
        case TransCollectRFID:
            return @"TransCollectRFID";
            break;
        case TransSectionalLock:
            return @"TransSectionalLock";
            break;
        case TransSectionalUnlock:
            return @"TransSectionalUnlock";
            break;
        default:
            break;
    }
    return @"";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tickets = [NSMutableArray array];
    
    self.recordFrames = [NSMutableData data];
}


- (void)sendReturnTaskCommand:(NSData *)ticketId {
    //任务回传
    [SVProgressHUD show];
    ZNSBluetoothInstruction *da =[[ZNSBluetoothInstruction alloc] initWithInstruction:Cmd_Code_04 function:Func_Code_00 body:nil rfid:ticketId];
    NSLog(@"send return ticket command: %@",da.data);
    [[GGBluetooth sharedManager] writeValueData:da.data];
}

- (void)deleteTask:(NSData *)ticketId {
    
    //删除任务
    ZNSBluetoothInstruction *da =[[ZNSBluetoothInstruction alloc] initWithInstruction:Cmd_Code_06 function:Func_Code_00 body:nil rfid:ticketId];
    NSLog(@"send delete ticket command: %@",da.data);
    [[GGBluetooth sharedManager] writeValueData:da.data];
}

- (IBAction)buttonPressed:(UIButton *)sender {
    NSLog(@"return task button pressed");
    [[GGBluetooth sharedManager] disconnectCurrentDevice];
    static BlueToothViewController *btVC;
    if (!btVC) {
        btVC = [BlueToothViewController create];
    }
    
    @weakify(self)
    [btVC setConnectedBlock:^{
        //            @strongify(self)
        
        __block int16_t frameCount = 0;
        __block int16_t user_id = 0;
        __block int16_t j = 0;
        [[GGBluetooth sharedManager] setNotifyCharacteristicBlock:^(CBCharacteristic *characteristic,NSData *recvData,NSError *error){
            @strongify(self)
            ZNSBluetoothInstruction *b = [[ZNSBluetoothInstruction alloc]initWithByteData:recvData];
            
            
            if(b.function == Func_Code_00 && b.cmd == Cmd_Code_05){
                int taskCount = 0;
                
                [[b.bodyData subdataWithRange:NSMakeRange(0, 2)] getBytes:&taskCount length:2];
                if (taskCount == 0) {
                    [SVProgressHUD showInfoWithStatus:@"钥匙中没有可回传的任务"];
                    [[GGBluetooth sharedManager] disconnectCurrentDevice];
                }else{
                    self.ticketIndex = 0;
                    [self.tickets removeAllObjects];
                    int i;
                    for (i=0; i<taskCount; i++){
                        NSData *id = [b.bodyData subdataWithRange:NSMakeRange(2+i*16, 16)];
                        NSLog(@"ticketId:%@", id);
                        [self.tickets addObject:id];
                    }
                    
                    NSData *ticketId = [self.tickets objectAtIndex:self.ticketIndex];
                    
                    [self sendReturnTaskCommand:ticketId];
                }
            }
            
            if (b.function == Func_Code_00 && b.cmd == Cmd_Code_04) {//回传总帧数
                
                [[b.bodyData subdataWithRange:NSMakeRange(0, 2)]  getBytes:&frameCount length:2];
                [[b.bodyData subdataWithRange:NSMakeRange(2, 2)] getBytes:&user_id length:2];
                if(j!=frameCount) {
                    j++;
                    NSData *frame = [NSData dataWithBytes:&j length:2];
                    
                    [[GGBluetooth sharedManager] writeValueData:[[ZNSBluetoothInstruction alloc] initWithMoreData:Cmd_Code_04 function:Func_Code_01 body:nil rfid:frame].data];
                    
                    
                }else{
                    [SVProgressHUD showInfoWithStatus:@"钥匙中无当前任务操作记录"];
//                    [[GGBluetooth sharedManager] disconnectCurrentDevice];
                    //20170524, lgh, 删除后再回传下一操作记录
                    [self deleteTask:[self.tickets objectAtIndex:self.ticketIndex]];
                }
            }
            if (b.function == Func_Code_01 && b.cmd == Cmd_Code_04) {
                
                [self.recordFrames appendData:[b.bodyData subdataWithRange:NSMakeRange(4, [b.bodyData length]-4)]];
                
                if(j!=frameCount) {
                    j++;
                    NSData *frame = [NSData dataWithBytes:&j length:2];
                    [[GGBluetooth sharedManager] writeValueData:[[ZNSBluetoothInstruction alloc] initWithMoreData:Cmd_Code_04 function:Func_Code_01 body:nil rfid:frame].data];
                    
                    
                }else{
                    j = 0;
                    frameCount = 0;
                    NSData * test = [NSData dataWithData:self.recordFrames];
                    //                        NSLog(@"%@",test);//test
                    NSMutableArray *logs = [NSMutableArray array];
                    NSData *bit0;
                    for (int i=0; i<([test length]/17); i++) {
                        
                        NSMutableData *tmp=[NSMutableData data];
                        bit0 = [test subdataWithRange:NSMakeRange(1+i*17, 1)];
                        [tmp appendData:bit0];
                        bit0 = [test subdataWithRange:NSMakeRange(0+i*17, 1)];
                        [tmp appendData:bit0];
                        
                        if (![[tmp hexString] isEqualToString:@"FFFF"]) {
                            [logs addObject:@{
                                              
                                              @"lid": [NSString stringWithFormat:@"%ld",strtoul([[tmp hexString] UTF8String], 0, 16)],
                                              @"unlock_at": [NSString timeStringFormHexString:[[test subdataWithRange:NSMakeRange(9+i*17, 7)] hexString]],
                                              @"result": [NSString stringWithFormat:@"%ld",strtoul([[[test subdataWithRange:NSMakeRange(16+i*17, 1)] hexString] UTF8String], 0, 16)],
                                              }];
                        }
                        
                    }
                    
                    if(self.ticketIndex<[self.tickets count]) {
                        NSData *ticketId = [self.tickets objectAtIndex:self.ticketIndex];
                        Byte *byteData = (Byte *)[ticketId bytes];
                        Byte ticketType = *byteData;
                        UInt32 tid = ((*(byteData+12))<<24)+((*(byteData+13))<<16)+((*(byteData+14))<<8)+(*(byteData+12));
                        
                        NSString *ticketAction = [self getTicketAction:ticketType];
                        NSString *uid = [NSString stringWithFormat:@"%d", user_id];
                        
                        NSLog(@"up to server:tid-%d, type-%@, uid-%@, %@", tid, ticketAction, uid, ticketId);
                        
                        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{@"tid":[NSNumber numberWithInt:tid],
                                                                                                   @"action":ticketAction,
                                                                                                   @"uid":[ZNSUser currentUser].idid}];
                        [dic setObject:logs forKey:@"taskHistory"];
                        [APIClient POST:API_TASK_HISTORY withParameters:dic successWithBlcok:^(id response) {
                            @strongify(self)
                            
                            [self.recordFrames resetBytesInRange:NSMakeRange(0, [self.recordFrames length])];
                            [self.recordFrames setLength:0];
                            [SVProgressHUD showSuccessWithStatus:@"回传成功"];
                            //20170524, lgh, 删除后再回传下一操作记录
                            [self deleteTask:ticketId];
                            
                        } errorWithBlock:^(ZNSError *error) {
                            @strongify(self)
                            [self.recordFrames resetBytesInRange:NSMakeRange(0, [self.recordFrames length])];
                            [self.recordFrames setLength:0];
                            self.ticketIndex = 0;
                            [self.tickets removeAllObjects];
                            [SVProgressHUD showErrorWithStatus:error.errorMessage];
                            [[GGBluetooth sharedManager] disconnectCurrentDevice];
                        }];
                    }
                }
                
            }
            if (b.cmd == Cmd_Code_06 && b.function == Func_Code_00) {
                
                NSLog(@"任务删除成功");
                self.ticketIndex = self.ticketIndex+1;
                if(self.ticketIndex<[self.tickets count]) {
                    //20170524, lgh, 回传下一操作记录
                    NSData *ticketId = [self.tickets objectAtIndex:self.ticketIndex];
                    
                    [self sendReturnTaskCommand:ticketId];
                    
                }
                else {
                    [self.recordFrames resetBytesInRange:NSMakeRange(0, [self.recordFrames length])];
                    [self.recordFrames setLength:0];
                    self.ticketIndex = 0;
                    [self.tickets removeAllObjects];
                    [SVProgressHUD showSuccessWithStatus:@"钥匙中操作记录回传完成"];
                    [[GGBluetooth sharedManager] disconnectCurrentDevice];
                }
            }
        }];
        [[GGBluetooth sharedManager] setSendFailureBlock:^{
            [SVProgressHUD showErrorWithStatus:@"钥匙无回应，请重试"];
            [[GGBluetooth sharedManager] disconnectCurrentDevice];
            NSLog(@"钥匙无回应，请重试......");
        }];
        [[GGBluetooth sharedManager] setCanWriteValueBlock:^{//任务查询
            [[GGBluetooth sharedManager] writeValueData:[[ZNSBluetoothInstruction alloc] initWithMoreData:Cmd_Code_05 function:Func_Code_00 body:nil rfid:nil].data];
            
        }];
        
    }];
    dialog = [[JKAlertDialog alloc]initWithTitle:@"选择智能钥匙" message:@""];
    dialog.contentView = btVC.view;
    
    [dialog addButton:Button_CANCEL withTitle:@"取消" handler:^(JKAlertDialogItem *item) {
        
    }];
    
    [btVC setConnectedSmartKeyBlock:^(ZNSSmartKey *smartKey){
        [dialog dismiss];
        
    }];
    
    [dialog show];
    [btVC setupBT];
    [btVC loadSmartKeys];
}
@end
