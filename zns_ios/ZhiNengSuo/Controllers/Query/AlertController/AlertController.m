//
//  AlertController.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 17/1/24.
//  Copyright © 2017年 czwen. All rights reserved.
//

#import "AlertController.h"
#import "ZNSAlert.h"
#import "AlertMessageTableViewCell.h"
#import "ZNSTimeTools.h"

@interface AlertController (){

    IBOutlet UITableView *tbvAlert;
    IBOutlet UIButton *btnBeginTime;
    IBOutlet UIButton *btnEndTime;
    
//    NSArray *alerts;
    NSNumber *page;
    
}

@property (nonatomic,strong) NSDate * beginTime;
@property (nonatomic,strong) NSDate * endTime;

@end

@implementation AlertController

- (void)loadDataWithPage:(NSNumber *)index{
    
    if (!self.beginTime || !self.endTime) {
        [tbvAlert.mj_header endRefreshing];
        [tbvAlert.mj_footer endRefreshing];
        return;
    }
    
    @weakify(self); 
    [APIClient POST:API_ALERT withParameters:@{@"page":index,@"search":@"",@"create_at":@{@"from":[self.beginTime stringWithFormat:@"yyyyMMddHHmmss"],@"to":[self.endTime stringWithFormat:@"yyyyMMddHHmmss"]}} successWithBlcok:^(id response) {
        @strongify(self);
        page = index;
        
        NSArray *alerts = [NSArray yy_modelArrayWithClass:[ZNSAlert class] json:[response valueForKey:@"items"]];
        if ([page isEqual:@0]) {
            [self.alts removeAllObjects];
            self.alts = [NSMutableArray arrayWithArray:alerts];
        }else{  
            [self.alts addObjectsFromArray:alerts];
        }
        [tbvAlert.mj_header endRefreshing];
        [tbvAlert.mj_footer endRefreshing];
        
//        if ([[response valueForKey:@"page"] isEqual:[response valueForKey:@"pages"]]) {
//            [tbvAlert.mj_footer endRefreshingWithNoMoreData];
//        }
        
        if (self.alts.count == [response[@"total"] integerValue]) {
             [tbvAlert.mj_footer endRefreshingWithNoMoreData];
        }
        
        [tbvAlert reloadData];
        [SVProgressHUD dismiss];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
        [tbvAlert.mj_header endRefreshing];
        [tbvAlert.mj_footer endRefreshing];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    [tbvAlert registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    tbvAlert.tableFooterView = [UIView new];
    @weakify(self);
    tbvAlert.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self loadDataWithPage:@0];
    }];
    tbvAlert.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self loadDataWithPage:@(page.integerValue+1)];
    }];
//    [tbvAlert.mj_header beginRefreshing];
    
    [tbvAlert registerNib:[AlertMessageTableViewCell nib] forCellReuseIdentifier:[AlertMessageTableViewCell reuseIdentifier]];
    
    tbvAlert.rowHeight = UITableViewAutomaticDimension;
    tbvAlert.estimatedRowHeight = 100;
}

- (NSDate *)extractDate:(NSDate *)date {
    
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CH"];
    fmt.dateFormat = @"yyyy.MM.dd";
    
    NSString *temp = [fmt stringFromDate:date];
    return [fmt dateFromString:temp];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
//    [self.alts removeAllObjects];
}
 
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
} 
- (IBAction)chooseBeginTime:(id)sender {
    
    @weakify(self);
    [ActionSheetDatePicker showPickerWithTitle:@"选择开始时间" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date]  minimumDate:nil  maximumDate:[NSDate date] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        @strongify(self);
        
        self.beginTime =[self extractDate:selectedDate];
        
        [btnBeginTime setTitle:[NSString formatTime:self.beginTime] forState:UIControlStateNormal];
        
        if (![self.endTime stringWithFormat:@"yyyyMMddHHmmss"] ) {
            //                [SVProgressHUD showInfoWithStatus:@"资料尚未齐全"];
        }else{
            NSTimeInterval time = [self.endTime timeIntervalSinceDate:self.beginTime];
            if (time < 0) {
                [SVProgressHUD showInfoWithStatus:@"开始时间不能大于结束时间"];
            }else{
                 [SVProgressHUD show];
                [self loadDataWithPage:@0];
            }
        }
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}

- (IBAction)chooseEndTime:(id)sender {
    @weakify(self);
    [ActionSheetDatePicker showPickerWithTitle:@"选择结束时间" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date]  minimumDate:nil  maximumDate:[NSDate date] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        @strongify(self);
        //        self.endTime = [selectedDate stringWithFormat:@"yyyyMMddHHmmss"];
        self.endTime =[self extractDate:selectedDate];
        self.endTime = [self.endTime dateByAddingSeconds:86399];
        
        [btnEndTime setTitle:[NSString formatTime:self.endTime] forState:UIControlStateNormal];
        
        if (![self.beginTime stringWithFormat:@"yyyyMMddHHmmss"] ) {
            [SVProgressHUD showInfoWithStatus:@"资料尚未齐全"];
        }else{
            NSTimeInterval time = [self.endTime timeIntervalSinceDate:self.beginTime];
            if (time < 0) {
                [SVProgressHUD showInfoWithStatus:@"开始时间不能大于结束时间"];
            }else{
                [SVProgressHUD show];
                [self loadDataWithPage:@0];
            } 
        }
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:self.view];
}

#pragma mark - tableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AlertMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[AlertMessageTableViewCell reuseIdentifier] forIndexPath:indexPath];
    ZNSAlert *alert = [self.alts objectAtIndex:indexPath.row];
    
    [cell.dateLabel setText:[[ZNSTimeTools shared] timeFomrmatterWith:alert.create_at]];
    [cell.messageLabel setText:alert.msg];
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.alts.count;
}

@end
