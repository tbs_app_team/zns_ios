//
//  ScanQRCodeController.h
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/3.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ScanResultBlock)(NSString *value);

@interface ScanQRCodeController : UIViewController

- (void)setResultBlock:(ScanResultBlock)block;

@end
