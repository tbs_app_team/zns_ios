//
//  CollectLocksViewController.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/28/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "CollectLocksViewController.h"
#import "CollectLockTableViewCell.h"
#import "ZNSLock.h"
#import "AddLockViewController.h"
#import "BlueToothViewController.h"

@interface CollectLocksViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSArray *datas;
@property (nonatomic,strong) NSNumber *page;
@end

@implementation CollectLocksViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self configUI];
    [self.tableView.mj_header beginRefreshing];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[GGBluetooth sharedManager]disconnectCurrentDevice];
}

- (void)configUI{
    self.title = @"采码";
    
    [self.tableView registerNib:[CollectLockTableViewCell nib] forCellReuseIdentifier:[CollectLockTableViewCell reuseIdentifier]];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"添加" style:UIBarButtonItemStylePlain target:self action:@selector(addLock)];
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self loadDatasWithPage:@1];
    }];
    
//    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//        @strongify(self);
//        [self loadDatasWithPage:@(self.page.integerValue+1)];
//    }];

}

- (void)loadDatasWithPage:(NSNumber *)page{
    @weakify(self);
    [ZNSLock getLocksetWithPage:page success:^(id response) {
        @strongify(self);
        NSArray *locks = [NSArray yy_modelArrayWithClass:[ZNSLock class] json:[response valueForKey:@"items"]];

        if ([page isEqual:@1]) {
            self.datas = locks;
        }else{
            self.datas = [self.datas arrayByAddingObjectsFromArray:locks];
        }
        self.page = [response valueForKey:@"page"];
        if ([[response valueForKey:@"page"] isEqual:[response valueForKey:@"pages"]]) {
            [self.tableView.mj_footer resetNoMoreData];
        }
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    } error:^(ZNSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
    }];
}

- (void)addLock{
    [AddLockViewController showInViewContoller:self.navigationController addedWithBlock:^{
        
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark - UITalbeView Delegate Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [tableView fd_heightForCellWithIdentifier:[CollectLockTableViewCell reuseIdentifier] cacheByIndexPath:indexPath configuration:^(id cell) {
        [cell setLock:[self.datas objectAtIndex:indexPath.row]];
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CollectLockTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[CollectLockTableViewCell reuseIdentifier]];
    ZNSLock *lock = [self.datas objectAtIndex:indexPath.row];
    [cell setLock:lock];
    @weakify(self);
    @weakify(lock);
    [cell setAddBlock:^{
        @strongify(self);
        @strongify(lock);
        if (![[GGBluetooth sharedManager] connectedPheripheral]) {
            [SVProgressHUD showInfoWithStatus:@"请先连接智能钥匙" maskType:SVProgressHUDMaskTypeBlack];
            [BlueToothViewController presentInViewContoller:self.navigationController];
            return ;
        }
        UIAlertController * ac = [UIAlertController alertControllerWithTitle:@"读取RFID" message:@"请使用智能钥匙读取相应RFID" preferredStyle:UIAlertControllerStyleAlert];
        [ac addTextFieldWithConfigurationHandler:^(UITextField *textField){
            textField.placeholder = @"RFID";
            textField.enabled = NO;
        }];
        [ac addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        }]];
        [ac addAction:[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            @strongify(self);
            @strongify(lock);

            UITextField *tf = ac.textFields.firstObject;
            
            if (tf.text.length == 0) {
                [SVProgressHUD showInfoWithStatus:@"请读取RFID" maskType:SVProgressHUDMaskTypeBlack];
            } else {
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
                [APIClient POST:[NSString stringWithFormat:@"lockset/%@/code/%@",lock.lid,tf.text] withParameters:@{} successWithBlcok:^(id response) {
                    [SVProgressHUD dismiss];
                    [self.tableView.mj_header beginRefreshing];
                } errorWithBlock:^(ZNSError *error) {
                    [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
                }];
            }
        }]];
        [self presentViewController:ac animated:YES completion:nil];
        [[GGBluetooth sharedManager] setNotifyCharacteristicBlock:^(CBCharacteristic *characteristic,NSData *recvData,NSError *error){
            ZNSBluetoothInstruction *b = [[ZNSBluetoothInstruction alloc]initWithByteData:characteristic.value];
            UITextField *tf = ac.textFields.firstObject;
            tf.text = [b rfid];
        }];
    }];
    return cell;
}
@end
