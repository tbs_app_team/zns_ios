//
//  AddLockViewController.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/5/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddLockViewController : UIViewController
+ (void)showInViewContoller:(UIViewController*)vc addedWithBlock:(voidBlock)block;
@end
