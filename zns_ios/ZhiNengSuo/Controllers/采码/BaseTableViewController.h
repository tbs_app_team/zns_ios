//
//  BaseTableViewController.h
//  ZhiNengSuo
//
//  Created by ekey on 16/11/8.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, copy)  idBlock objectBlock;
@end
