//
//  BaseTableViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/11/8.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "BaseTableViewController.h"

@interface BaseTableViewController ()

@end

@implementation BaseTableViewController

- (void)viewDidLoad{
    [super viewDidLoad];
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//               [self.tableView reloadData];
//               NSLog(@"crazy");
//    });
}

//- (void)viewDidAppear:(BOOL)animated{
//    [super viewDidAppear:animated];
////    [self.tableView reloadData];
//}
//
//- (void)viewWillAppear:(BOOL)animated{
//    [super viewWillAppear:animated];
//    [self.tableView reloadData];
//}

- (void)setDataSource:(NSArray *)dataSource{
    _dataSource = dataSource;
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
////
//           NSLog(@"crazy");
//    });
//    dispatch_async(dispatch_get_main_queue(), ^{
         [self.tableView reloadData];
//    });
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tbleView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UITableViewCell"];
    }
    ZNSObject *object = self.dataSource[indexPath.row];
    cell.textLabel.text = object.name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.objectBlock) {
        self.objectBlock(self.dataSource[indexPath.row]);
    }
}


@end
