//
//  AddLockViewController.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/5/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "AddLockViewController.h"
#import "ZNSLabelTextFieldTableViewCell.h"
#import "ZNSPickerViewController.h"
#import "ZNSZone.h"
#import "ZNSSection.h"
#import "BlueToothViewController.h"
#import "JKAlertDialog.h"
#import "CommonTableViewController.h"
#import "GetLocationCommonViewController.h"
#import "BaseTableViewController.h"
#import "ZNSButtonTableViewCell.h"

@interface AddLockViewController ()<UITableViewDelegate,UITableViewDataSource>{
//    JKAlertDialog *_dialog;
//    BaseTableViewController *vc;
   
}
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (weak, nonatomic) IBOutlet UITextField *deveceNameField;

@property (nonatomic,strong) NSArray *sections;
@property (nonatomic ,strong) ZNSSection *selcectSection;

@property (nonatomic,strong) NSArray *carrieies;
@property (nonatomic,strong) NSArray *lockTypes;
@property (nonatomic,strong) NSArray *objectTypes;
@property (nonatomic,strong) NSArray *rules;

@property (nonatomic,copy) voidBlock finishBlock;

@property (nonatomic,strong) ZNSObject *object;

@property (nonatomic,strong) UITextField *editingTextField;

// 用作获取蓝牙地址
@property (nonatomic,strong) NSMutableArray *bts;

@property (nonatomic,assign) BOOL isStop ;
@property (nonatomic,assign) BOOL isModify ;

@property (nonatomic, strong) NSArray *searchResults;

@property (nonatomic, strong) BaseTableViewController *vc;
@property (nonatomic, strong) JKAlertDialog *dialog;
@end

@implementation AddLockViewController
+ (void)showInViewContoller:(UIViewController *)vc addedWithBlock:(voidBlock)block{
    AddLockViewController *addVC = [AddLockViewController create];
    addVC.finishBlock = block;
    [vc presentViewController:[[UINavigationController alloc] initWithRootViewController:addVC] animated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"取消" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(add)];
    
    self.tableView.editing = YES;
    
    [self.tableView registerNib:[ZNSLabelTextFieldTableViewCell nib] forCellReuseIdentifier:[ZNSLabelTextFieldTableViewCell reuseIdentifier]];
    

    
    self.view.backgroundColor = [UIColor colorWithRGB:0xf0f0f0];
    [self.searchBtn bs_configureAsPrimaryStyle];
    self.isModify = NO;
    
    self.object = [ZNSObject new];
    
    [self loadData];
}

- (void)loadData{
    [SVProgressHUD showWithStatus:@"正在获取数据"];
    @weakify(self);
    [ZNSAPITool getAllZonesSuccessWithBlcok:^(id response) {
        @strongify(self);
        self.rules = response;
        [SVProgressHUD dismiss];
    } errorWithBlock:nil];
    
    [ZNSAPITool getAllAreaSuccessWithBlcok:^(NSArray *response) {
        @strongify(self);
        self.sections = @[];
        [response enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ZNSSection *sec = obj;
            if ([sec.type isEqualToString:@"区域"]) {
                self.sections = [self.sections arrayByAddingObject:sec];
            }
        }];
    } errorWithBlock:nil];
    
    
    [ZNSAPITool getAllCarrierSuccessWithBlcok:^(id response) {
        @strongify(self);
        self.carrieies = response;
    } errorWithBlock:nil];
  

    
    [ZNSAPITool getAllObjectTypeSuccessWithBlcok:^(id response) {
        @strongify(self);
        self.objectTypes = response;
    } errorWithBlock:nil];
    
    [ZNSAPITool getAllLockTypeSuccessWithBlcok:^(id response) {
        @strongify(self);
        self.lockTypes = response;
    } errorWithBlock:nil];
    

    
    
  
    
}

- (void)dismiss{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)add{
    
    if (!self.isModify) {
        if (self.object.zname.length<=0) {
            [SVProgressHUD showErrorWithStatus:@"请选择开锁范围" maskType:SVProgressHUDMaskTypeBlack];
            return;
        }
      
    }
    if (![self.object.carrier length]) {
        [SVProgressHUD showErrorWithStatus:@"请选择运营商" maskType:SVProgressHUDMaskTypeBlack];
        return;
    }
    if (self.object.area.length<=0) {
        [SVProgressHUD showErrorWithStatus:@"请选择区域" maskType:SVProgressHUDMaskTypeBlack];
        return;
    }
   
    if (self.object.type.length<=0) {
        [SVProgressHUD showErrorWithStatus:@"请选择设备类型" maskType:SVProgressHUDMaskTypeBlack];
        return;
    }
    
    
    if (self.object.name.length<=0 || [[NSString isEmpty:self.object.name] isEqualToString:@"yes"]) {
        [SVProgressHUD showErrorWithStatus:@"设备名称不能为空" maskType:SVProgressHUDMaskTypeBlack];
        return;
    }
    if (self.object.name.length>32) {
        [SVProgressHUD showErrorWithStatus:@"设备名称不能超过32位" maskType:SVProgressHUDMaskTypeBlack];
        return;
    }
    if (self.object.lockset.count>20) {
        [SVProgressHUD showErrorWithStatus:@"一个设备最多只能安装20把锁" maskType:SVProgressHUDMaskTypeBlack];
        return;
    }
    self.isStop = NO;
    [self.object.lockset enumerateObjectsUsingBlock:^(ZNSLock *lock, NSUInteger idx, BOOL * _Nonnull stop) {
            if (lock.name.length<=0) {
                [SVProgressHUD showErrorWithStatus:@"锁具名称不能为空" maskType:SVProgressHUDMaskTypeBlack];
                *stop = YES;
                self.isStop = YES;
            }
            if (lock.name.length>32) {
                [SVProgressHUD showErrorWithStatus:@"锁具名称不能超过32位" maskType:SVProgressHUDMaskTypeBlack];
                *stop = YES;
                self.isStop = YES;
            }
            if (lock.type.length<=0) {
                [SVProgressHUD showErrorWithStatus:@"请选择锁具类型" maskType:SVProgressHUDMaskTypeBlack];
                *stop = YES;
                self.isStop = YES;
            }
            //蓝牙地址／锁码值可以为空，只有在不为空的情况下去判断
            if (lock.btaddr.length>0) {
                
               
                if (lock.btaddr.length != 12) {
                    [SVProgressHUD showErrorWithStatus:@"蓝牙地址的长度应为12位" maskType:SVProgressHUDMaskTypeBlack];
                    *stop = YES;
                    self.isStop = YES;
                }
                if (![self isHexString:lock.btaddr]) {
                    [SVProgressHUD showErrorWithStatus:@"请输入十六进制的蓝牙地址" maskType:SVProgressHUDMaskTypeBlack];
                    *stop = YES;
                    self.isStop = YES;
                }
              
                lock.btaddr = lock.btaddr.uppercaseString;
            }
            
            if (lock.rfid.length>0) {
                
                if (lock.rfid.length != 10) {
                    [SVProgressHUD showErrorWithStatus:@"锁码的长度应为10位" maskType:SVProgressHUDMaskTypeBlack];
                    *stop = YES;
                    self.isStop = YES;
                }
                if (![self isHexString:lock.rfid]) {
                    [SVProgressHUD showErrorWithStatus:@"请输入十六进制的锁码值" maskType:SVProgressHUDMaskTypeBlack];
                    *stop = YES;
                    self.isStop = YES;
                }
               
                lock.rfid = lock.rfid.uppercaseString;
            }
            
            
    }];
    
    if (self.isStop) {
        return;
    }
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    @weakify(self);

    if (self.isModify) {
        [APIClient POST:[NSString stringWithFormat:@"object/%@/update",self.object.oid]
         withParameters:[self.object modifyObjectParameters] successWithBlcok:^(id response) {
             @strongify(self);
             [SVProgressHUD showSuccessWithStatus:@"上传成功" maskType:SVProgressHUDMaskTypeBlack];
             self.object = [ZNSObject new];
             self.isModify = NO;
             self.deveceNameField.text = @"";
             [self.tableView reloadData];
         } errorWithBlock:^(ZNSError *error) {
             [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
         }];
        
    }else{
        [APIClient POST:[NSString stringWithFormat:@"section/%@/object/new/lockset",self.selcectSection.sid]
         withParameters:[self.object generteNewObjectParameters] successWithBlcok:^(id response) {
            @strongify(self);
            [SVProgressHUD showSuccessWithStatus:@"上传成功" maskType:SVProgressHUDMaskTypeBlack];
             self.object = [ZNSObject new];
             self.isModify = NO;
             self.deveceNameField.text = @"";
             [self.tableView reloadData];
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
        }];
    }
}
- (void)updateDevece{
    
    if (!self.isModify) {
        if (self.object.zname.length<=0) {
            [SVProgressHUD showErrorWithStatus:@"请选择开锁范围" maskType:SVProgressHUDMaskTypeBlack];
            return;
        }
        
    }
    if (self.object.area.length<=0) {
        [SVProgressHUD showErrorWithStatus:@"请选择区域" maskType:SVProgressHUDMaskTypeBlack];
        return;
    }
    
    if (self.object.type.length<=0) {
        [SVProgressHUD showErrorWithStatus:@"请选择设备类型" maskType:SVProgressHUDMaskTypeBlack];
        return;
    }
    
    
    if (self.object.name.length<=0) {
        [SVProgressHUD showErrorWithStatus:@"设备名称不能为空" maskType:SVProgressHUDMaskTypeBlack];
        return;
    }
    if (self.object.name.length>32) {
        [SVProgressHUD showErrorWithStatus:@"设备名称不能超过32位" maskType:SVProgressHUDMaskTypeBlack];
        return;
    }
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    
    if (self.isModify) {
        [APIClient POST:[NSString stringWithFormat:@"object/%@/update",self.object.oid]
         withParameters:[self.object modifyObjectParameters] successWithBlcok:^(id response) {
             [SVProgressHUD showSuccessWithStatus:@"上传成功" maskType:SVProgressHUDMaskTypeBlack];
         } errorWithBlock:^(ZNSError *error) {
             [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
         }];
        
    }
}

- (BOOL)isHexString:(NSString*)str{
    
    int i;
    for (i=0; i<[str length]; i++) {
        if (([str characterAtIndex:i]>='0' && [str characterAtIndex:i]<='9')||
            ([str characterAtIndex:i]>='a' && [str characterAtIndex:i]<='f') ||
            ([str characterAtIndex:i]>='A' && [str characterAtIndex:i]<='F' ) ){
            //            return YES;
        }else{
            return NO;
        }
        
    }
    return YES;
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2+self.object.lockset.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        if(self.isModify){
            return 7;
        }else{
             return 6;
        }
    }else if (section<=self.object.lockset.count){
        return 5;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    @weakify(self);
    if (indexPath.section==0) {
        ZNSLabelTextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ZNSLabelTextFieldTableViewCell reuseIdentifier]];
        switch (indexPath.row) {
            case 0:{
                [cell.label setText:@"开锁范围"];
                [cell.textField setPlaceholder:@"请选择开锁范围"];
                cell.textField.text = self.object.zname;
                
                if (self.isModify) {
                    cell.textField.enabled = NO;
                }else{
                    cell.textField.enabled = YES;
                }
                
                cell.textField.keyboardType = UIKeyboardTypeDefault;
                cell.showToolBar = NO;
                
                [cell setCanEditBlock:^BOOL(){
                    
                    @strongify(self);
                    [self showZonePicker];
                    
                    return NO;
                }];
//                [cell setTextBlcok:^(NSString *text){
//                    @strongify(self);
//                    self.object.zname = text;
//                }];
               }
                break;
            case 1:{
                [cell.label setText:@"运营商"];
                [cell.textField setPlaceholder:@"请选择运营商"];
                cell.textField.text = self.object.carrier;
                cell.textField.enabled = YES;
                cell.textField.keyboardType = UIKeyboardTypeDefault;
                cell.showToolBar = NO;
                
                [cell setCanEditBlock:^BOOL(){
                    
                    @strongify(self);
                    [self showCarrierPicker];
                    
                    return NO;
                }];
        
            }
                break;
            case 2:{
                [cell.label setText:@"区域"];
                [cell.textField setPlaceholder:@"请选择区域"];
                cell.textField.text = self.object.area;
                
                if (self.isModify) {
                    cell.textField.enabled = NO;
                }else{
                    cell.textField.enabled = YES;
                }
                
                cell.textField.keyboardType = UIKeyboardTypeDefault;
                cell.showToolBar = NO;

                [cell setCanEditBlock:^BOOL(){
                    
                    @strongify(self);
                    [self showSectionPicker];

                    return NO;
                }];
//                [cell setTextBlcok:^(NSString *text){
//                    @strongify(self);
//                    self.object.section = text;
//                }];
            }
                break;
            case 3:{
                [cell.label setText:@"设备类型"];
                [cell.textField setPlaceholder:@"请选择设备类型"];
                cell.textField.text = self.object.type;
                cell.showToolBar = NO;

                cell.textField.keyboardType = UIKeyboardTypeDefault;
                [cell setCanEditBlock:^BOOL(){
                    @strongify(self);
                    [self showObjectTypePicker];
                    return NO;
                }];
            }
                break;
            case 4:{
                [cell.label setText:@"设备名称"];
                [cell.textField setPlaceholder:@"请输入设备名称"];
                
                cell.textField.text = self.object.name;
                cell.showToolBar = NO;

                cell.textField.keyboardType = UIKeyboardTypeDefault;
                [cell setCanEditBlock:^BOOL(){
                    return YES;
                }];
                [cell setTextBlcok:^(NSString *text){
                    @strongify(self);
                    self.object.name = text;
                }];
            }
                break;
            case 5:{
                [cell.label setText:@"设备位置"];
                [cell.textField setPlaceholder:@"请采集设备的位置"];
                cell.textField.text = self.object.lat==nil ? @"":[NSString stringWithFormat:@"经度：%@ 纬度：%@",self.object.lng,self.object.lat];
                cell.textField.keyboardType = UIKeyboardTypeDefault;
                cell.showToolBar = NO;
                
                [cell setCanEditBlock:^BOOL(){
                    
                    @strongify(self);
                    GetLocationCommonViewController *vvc = [GetLocationCommonViewController create];
                    vvc.mode = AddLockMode;
                    [vvc  setLocationBlock:^(NSArray *location){
                        
                        self.object.lat = location[0];
                        self.object.lng = location[1];
                        [self.tableView reloadData];
//                        [self.tableView reloadRow:5 inSection:0 withRowAnimation:UITableViewRowAnimationFade];
                        
                    }];
                    
                    [self.navigationController pushViewController:vvc animated:YES];
                    
                    return NO;
                }];
           
            }
                break;
            default:{
                ZNSButtonTableViewCell *btnCell = [tableView dequeueReusableCellWithIdentifier:@"ZNSButtonTableViewCell"];
                if (btnCell == nil) {
                    btnCell = [[NSBundle mainBundle] loadNibNamed:@"ZNSButtonTableViewCell" owner:self options:nil].firstObject;
                }
                if (!self.isModify) {
                    btnCell.saveBtn.hidden = YES;
                    btnCell.deleteBtn.hidden = YES;
                
                }else{

                    btnCell.saveBtn.hidden = YES;
                    btnCell.deleteBtn.hidden = NO;
                    [btnCell.deleteBtn setTitle:@"保存" forState:UIControlStateNormal];
                    btnCell.deleBlock = ^(void){
                        [self updateDevece];
                    };
                }
               
                return btnCell;
                
            }
                break;

        }
        return cell;
    }else if(indexPath.section <= self.object.lockset.count){
        ZNSLabelTextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ZNSLabelTextFieldTableViewCell reuseIdentifier]];
        @strongify(self);
        ZNSLock *lock = [self.object.lockset objectAtIndex:indexPath.section-1];
        switch (indexPath.row) {
            case 0:{
                [cell.label setText:@"锁具类型"];
                [cell.textField setPlaceholder:@"请选择锁具类型"];
                cell.textField.text = lock.type;
                cell.showToolBar = NO;

                cell.textField.keyboardType = UIKeyboardTypeDefault;
                [cell setCanEditBlock:^BOOL(){
                    @strongify(self);
                    [self showLockTypePickerAtIndexPath:indexPath];
                    return NO;
                }];
                
                [cell setTextBlcok:^(NSString *text){
                    lock.type = text;
                }];
            }
                break;
            case 1:{
                [cell.label setText:@"锁具名称"];
                [cell.textField setPlaceholder:@"请输入锁具名称"];
                cell.textField.text = lock.name;
                cell.showToolBar = NO;

                cell.textField.keyboardType = UIKeyboardTypeDefault;
                [cell setCanEditBlock:^BOOL(){
                    return YES;
                }];
                [cell setTextBlcok:^(NSString *text){
                    lock.name = text;
                }];
            }
                break;
            case 2:{
                [cell.label setText:@"蓝牙地址"];
                [cell.textField setPlaceholder:@"点击采码或手动输入十二位"];
                cell.textField.text = lock.btaddr;
                cell.showToolBar = YES;
                [cell setToolBarActionBlock:^{
                    [self showBTPickerAtIndexPath:indexPath];
                }];
                cell.textField.keyboardType = UIKeyboardTypeASCIICapable;
                [cell setCanEditBlock:^BOOL(){
                    return YES;
                }];
                [cell setTextBlcok:^(NSString *text){

                        lock.btaddr = text;
             
                }];
            }
                break;
            case 3:{
                [cell.label setText:@"锁码"];
                [cell.textField setPlaceholder:@"点击采码或手动输入十位"];
                cell.textField.keyboardType = UIKeyboardTypeASCIICapable;
                cell.textField.text = lock.rfid;
                cell.showToolBar = YES;
                @weakify(cell);
                [cell setToolBarActionBlock:^{
                    @strongify(self);
                    if (![[GGBluetooth sharedManager] connectedPheripheral]) {
                        [BlueToothViewController presentInViewContoller:self];
                    }else{
                        
                        [[GGBluetooth sharedManager] setNotifyCharacteristicBlock:^(CBCharacteristic *characteristic,NSData *recvData,NSError *error){
                            @strongify(cell);
                            ZNSBluetoothInstruction *b = [[ZNSBluetoothInstruction alloc]initWithByteData:characteristic.value];
                            cell.textField.text = [b rfid];
                            lock.rfid = [b rfid];
                        }];
                        
                        [SVProgressHUD showInfoWithStatus:@"请采集锁码"];
                    }
                }];
                
                [cell setCanEditBlock:^BOOL(){
                    return YES;
                }];
                
                [cell setTextBlcok:^(NSString *text){
                    lock.rfid = text;
                }];
            }
                break;
            default:{
                ZNSButtonTableViewCell *btnCell = [tableView dequeueReusableCellWithIdentifier:@"ZNSButtonTableViewCell"];
                if (btnCell == nil) {
                    btnCell = [[NSBundle mainBundle] loadNibNamed:@"ZNSButtonTableViewCell" owner:self options:nil].firstObject;
                }
                if (!self.isModify) {
                    btnCell.saveBtn.hidden = YES;
                    btnCell.deleteBtn.hidden = NO;
                    [btnCell.deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
                }else{
                     btnCell.saveBtn.hidden = NO;
                     btnCell.deleteBtn.hidden = NO;
                     [btnCell.deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
                     [btnCell.saveBtn setTitle:@"保存" forState:UIControlStateNormal];
                     btnCell.saveBlock = ^(void){
                        [self saveLockset:lock position:indexPath.section-1];
                    };

                }
                btnCell.deleBlock = ^(void){
                    [self deleLockset:lock position:indexPath.section-1];
                };
                return btnCell;
//                [cell.label setText:@"删除"];
//                [cell.textField setPlaceholder:@""];
//                cell.textField.text = @"";
//                cell.showToolBar = NO;
//                [cell setCanEditBlock:^BOOL(){
//                    return NO;
//                }];
            }
                break;
        }
        
        return cell;

    }else{
        
        ZNSLabelTextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ZNSLabelTextFieldTableViewCell reuseIdentifier]];
        [cell.label setText:@"增加锁具"];
        [cell.textField setPlaceholder:@""];
        cell.textField.text = @"";
        [cell setCanEditBlock:^BOOL(){
            return NO;
        }];
        return cell;
    }
    return nil;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return NO;
    }else{
//        if (indexPath.row==4) {
//            return YES;
//        }
        if (indexPath.section==(2+self.object.lockset.count-1)) {
            return YES;
        }
    }
    return NO;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return UITableViewCellEditingStyleNone;
    }else{
//        if (indexPath.row==4) {
//            return UITableViewCellEditingStyleDelete;
//        }
        if (indexPath.section==(2+self.object.lockset.count-1)) {
            return UITableViewCellEditingStyleInsert;
        }
    }
    return UITableViewCellEditingStyleNone;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleInsert) {
        
        if (!self.isModify) {
            if (self.object.zname.length<=0) {
                [SVProgressHUD showErrorWithStatus:@"请选择开锁范围" maskType:SVProgressHUDMaskTypeBlack];
                return;
            }
            if (self.object.area.length<=0) {
                [SVProgressHUD showErrorWithStatus:@"请选择区域" maskType:SVProgressHUDMaskTypeBlack];
                return;
            }
        }
        if (self.object.type.length<=0) {
            [SVProgressHUD showErrorWithStatus:@"请选择设备类型" maskType:SVProgressHUDMaskTypeBlack];
            return;
        }
        
        
        if (self.object.name.length<=0) {
            [SVProgressHUD showErrorWithStatus:@"设备名称不能为空" maskType:SVProgressHUDMaskTypeBlack];
            return;
        }
        if (self.object.name.length>32) {
            [SVProgressHUD showErrorWithStatus:@"设备名称不能超过32位" maskType:SVProgressHUDMaskTypeBlack];
            return;
        }
    
      
        
        if (!self.object.lockset) {
            self.object.lockset = @[];
        }
        self.object.lockset = [self.object.lockset arrayByAddingObject:[ZNSLock new]];
        [tableView insertSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
    }else if (editingStyle == UITableViewCellEditingStyleDelete){
        NSMutableArray *array = [NSMutableArray arrayWithArray:self.object.lockset];
        [array removeObjectAtIndex:indexPath.section-1];
        self.object.lockset = array;
        [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section>0 && section<=self.object.lockset.count) {
        return [NSString stringWithFormat:@"新增锁%@",@(section)];
    }else{
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

- (void)saveLockset:(ZNSLock *)lock position:(NSInteger)position{
    if (lock.type == nil || lock.type == 0) {
        [SVProgressHUD showErrorWithStatus:@"请选择锁类型"];
        return;
    }
    if (lock.name == nil || lock.name.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"请填写锁具名称"];
        return;
    }
    
    //蓝牙地址／锁码值可以为空，只有在不为空的情况下去判断
    if (lock.btaddr.length>0) {
        
        if (lock.btaddr.length != 12) {
            [SVProgressHUD showErrorWithStatus:@"蓝牙地址的长度应为12位"];
            return;
        }
        if (![self isHexString:lock.btaddr]) {
            [SVProgressHUD showErrorWithStatus:@"请输入十六进制的蓝牙地址"];
            return;
        }
        
        lock.btaddr = lock.btaddr.uppercaseString;
    }
    
    if (lock.rfid.length>0) {
        
        if (lock.rfid.length != 10) {
            [SVProgressHUD showErrorWithStatus:@"锁码的长度应为10位"];
            return;
        }
        if (![self isHexString:lock.rfid]) {
            [SVProgressHUD showErrorWithStatus:@"请输入十六进制的锁码值"];
            return;
        }
        
        lock.rfid = lock.rfid.uppercaseString;
    }
   
    if (lock.oid == nil) {//新建保存
        @weakify(self)
        [APIClient POST:API_LOCKSET_NEW_WITH_OID(self.object.oid) withParameters:@{@"type":lock.type,
                                                                                   @"rfid":lock.rfid==nil?@"":lock.rfid,
                                                                                   @"btaddr":lock.btaddr==nil?@"":lock.btaddr,
                                                                                   @"name":lock.name}
       successWithBlcok:^(id response) {
           @strongify(self)
           ZNSLock *updateLock = self.object.lockset[position];
           updateLock.lid = response[@"id"];
           NSMutableArray *locks = [NSMutableArray arrayWithArray:self.object.lockset];
           [locks replaceObjectAtIndex:position withObject:updateLock];
           self.object.lockset = locks;
           [SVProgressHUD showSuccessWithStatus:@"保存成功"];
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
        }] ;
        
    }else{//修改保存
//        @weakify(self)
        [APIClient POST:API_LOCKSET_MODIFY_WITH_OID(lock.lid) withParameters:@{@"type":lock.type,
                                                                               @"rfid":lock.rfid==nil?@"":lock.rfid,
                                                                               @"btaddr":lock.btaddr==nil?@"":lock.btaddr,
                                                                               @"name":lock.name}
       successWithBlcok:^(id response) {
//           @strongify(self)
//           ZNSLock *updateLock = self.object.locks[position];
//           NSMutableArray *locks = [NSMutableArray arrayWithArray:self.object.locks];
//           [locks replaceObjectAtIndex:position withObject:updateLock];
//           self.object.locks = locks;
           [SVProgressHUD showSuccessWithStatus:@"修改成功"];
           
       } errorWithBlock:^(ZNSError *error) {
           [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
       }] ;
        
    }
}

- (void)deleLockset:(ZNSLock *)lock  position:(NSInteger)position{
    
    if (lock.oid == nil) {
        NSMutableArray *locks = [NSMutableArray arrayWithArray:self.object.lockset];
        [locks removeObjectAtIndex:position];
        self.object.lockset = locks;
        [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:position+1] withRowAnimation:UITableViewRowAnimationFade];
        return;
    }
    @weakify(self)
    [APIClient POST:API_LOCKSET_DEL_WITH_LID(lock.lid) withParameters:@{@"type":lock.type,
                                                                        @"rfid":lock.rfid==nil?@"":lock.rfid,
                                                                        @"btaddr":lock.btaddr==nil?@"":lock.btaddr,
                                                                           @"name":lock.name}
   successWithBlcok:^(id response) {
       @strongify(self)
//           ZNSLock *updateLock = self.object.locks[position];
       NSMutableArray *locks = [NSMutableArray arrayWithArray:self.object.lockset];
       [locks removeObjectAtIndex:position];
       self.object.lockset = locks;
       [SVProgressHUD showSuccessWithStatus:@"删除成功"];
       [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:position+1] withRowAnimation:UITableViewRowAnimationFade];
       
   } errorWithBlock:^(ZNSError *error) {
       [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
   }] ;
    
}
- (void)showSectionPicker{
    if (!self.sections.count) {
        [self loadData];
//        [SVProgressHUD showWithStatus:@"正在获取数据"];
        return;
    }
    [self.view endEditing:YES];
    @weakify(self);
    if (self.sections) {
        [ActionSheetStringPicker showPickerWithTitle:@"选择区域" rows:[self.sections valueForKeyPath:@"name"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            @strongify(self);
            self.object.area = selectedValue;
            self.selcectSection = [self.sections objectAtIndex:selectedIndex];
//            [self.tableView reloadData];
            [self.tableView reloadRow:2 inSection:0 withRowAnimation:UITableViewRowAnimationFade];
        } cancelBlock:^(ActionSheetStringPicker *picker) {
            
        } origin:self.view];
    }else{
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        [ZNSAPITool getAllAreaSuccessWithBlcok:^(id response) {
            @strongify(self);
            [SVProgressHUD dismiss];
            self.sections = response;
            [self showSectionPicker];
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
        }];
    }
}

- (void)showObjectTypePicker{
    if (!self.objectTypes.count) {
        [self loadData];
//        [SVProgressHUD showWithStatus:@"正在获取数据"];
        return;
    }
    [self.view endEditing:YES];
    @weakify(self);
    [ActionSheetStringPicker showPickerWithTitle:@"选择设备类型" rows:self.objectTypes initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        self.object.type = selectedValue;
//        [self.tableView reloadData];
        [self.tableView reloadRow:3 inSection:0 withRowAnimation:UITableViewRowAnimationFade];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

//- (void)showObjectPicker{
//    if (!self.selectedSection) {
//        [SVProgressHUD showErrorWithStatus:@"请先选择单位"];
//        return;
//    }
//    
//    ZNSPickerViewController *picker = [ZNSPickerViewController create];
//    picker.api = [NSString stringWithFormat:@"section/%@/object/",self.selectedSection];
//    picker.parameter = @{};
//    picker.modelClass = [ZNSObject class];
//    [picker setConfigCell:^UITableViewCell*(UITableViewCell *cell, ZNSObject *object){
//        [cell.textLabel setText:object.name];
//        return cell;
//    }];
//    @weakify(self);
//    [picker setSelectedBlock:^(ZNSObject *object){
//        @strongify(self);
//        self.selectedObject = object;
//        self.objectCell.textField.text = object.name;
//    }];
//    [picker showInController:self.navigationController];
//}

- (void)showLockTypePickerAtIndexPath:(NSIndexPath*)index{
    
    [self.view endEditing:YES];
    
    if (!self.lockTypes.count) {
        [self loadData];
//        [SVProgressHUD showWithStatus:@"正在获取数据"];
        return;
    }
    
    @weakify(self);
    [ActionSheetStringPicker showPickerWithTitle:@"选择锁具类型" rows:self.lockTypes initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        ZNSLock *lock = [self.object.lockset objectAtIndex:index.section-1];
        lock.type = selectedValue;
        [self.tableView reloadData];

    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}


- (void)showBTPickerAtIndexPath:(NSIndexPath*)index{
    [self.view endEditing:YES];
    [self.bts removeAllObjects];
    CommonTableViewController *tvc= [[CommonTableViewController alloc]initWithStyle:UITableViewStylePlain];
    
    _dialog = [[JKAlertDialog alloc]initWithTitle:@"选择蓝牙" message:@""];
    _dialog.contentView = tvc.view;
    
    
    @weakify(self);
    [[GGBluetooth sharedManager]setDiscoverPeripheralBlock:^(CBPeripheral *discoverPeripheral,NSDictionary *advertisementData,NSNumber *RSSI){
        @strongify(self);
        if (!self.bts) {
            self.bts = [NSMutableArray array];
        }
        
        if (![[self.bts valueForKey:@"identifier"] containsObject:discoverPeripheral.identifier]) {
            [self.bts addObject:discoverPeripheral];
            [tvc setDatas:self.bts];
        }
    }];
    
    [_dialog addButton:Button_CANCEL withTitle:@"取消" handler:^(JKAlertDialogItem *item) {

    }];;
    
    [tvc setSelectedBlock:^(CBPeripheral *discoverPeripheral){
        ZNSLock *lock = [self.object.lockset objectAtIndex:index.section-1];
        lock.btaddr = discoverPeripheral.macAddress;
        [self.tableView reloadData];
        [_dialog dismiss];
    }];
    
    [_dialog show];
    [[GGBluetooth sharedManager]startScan];
}

- (void)showCarrierPicker{
    @weakify(self);
    [ActionSheetStringPicker showPickerWithTitle:@"选择运营商" rows:self.carrieies initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        self.object.carrier = selectedValue;
        [self.tableView reloadRow:1 inSection:0 withRowAnimation:UITableViewRowAnimationFade];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}


- (void)showZonePicker{
    if (!self.rules.count) {
        [SVProgressHUD showWithStatus:@"正在获取数据"];
        @weakify(self)
        [ZNSAPITool getAllZonesSuccessWithBlcok:^(id response) {
            @strongify(self);
            self.rules = response;
            [SVProgressHUD dismiss];
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:@"获取数据失败，请重新点击获取"];
        }];
        return;
    }
    @weakify(self);
    [ActionSheetStringPicker showPickerWithTitle:@"选择开锁范围" rows:[self.rules valueForKey:@"name"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        self.object.zid = [[self.rules objectAtIndex:selectedIndex]valueForKey:@"id"];
        self.object.zname = selectedValue;
//        [self.tableView reloadData];
          [self.tableView reloadRow:0 inSection:0 withRowAnimation:UITableViewRowAnimationFade];

    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.view];
}

//- (void)getDeviceLocation{
//    [self.navigationController pushViewController:[GetLocationViewController create] animated:YES];
//}
- (IBAction)searchBtnOnClick:(UIButton *)sender {
    
    if ([self.deveceNameField.text  isEqual: @""]) {
        [SVProgressHUD showInfoWithStatus:@"请输入设备名称"];
        return;
    }
    [SVProgressHUD showProgress:1.0 status:@"正在搜索"];
    @weakify(self)
    [APIClient POST:[NSString stringWithFormat:@"%@downward",API_SECTION_OBJECT_WITH_SID(@"0")] withParameters:@{@"search":self.deveceNameField.text} successWithBlcok:^(id response) {
        @strongify(self)
        self.searchResults = [NSArray yy_modelArrayWithClass:[ZNSObject class] json:response[@"items"]];
        [SVProgressHUD dismiss];
        [self showDialog];
        
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
    }] ;
}
- (IBAction)fieldDidEndOnExit:(UITextField *)sender {
    [sender resignFirstResponder];
}

#pragma Private

- (void)showDialog{
    [self.view endEditing:YES];
    _dialog = [[JKAlertDialog alloc] initWithTitle:@"请选择要修改的设备" message:nil];
    _vc = [[BaseTableViewController alloc] initWithStyle:UITableViewStylePlain];
    _dialog.contentView = _vc.view; 
    [_dialog addButton:Button_CANCEL withTitle:@"取消" handler:^(JKAlertDialogItem *item) {
        
    }];
    @weakify(self)
    _vc.objectBlock = ^(ZNSObject *object){
        @strongify(self)
        self.isModify = YES;
        self.object = object;
        [self.tableView reloadData];
        [self.dialog dismiss];
    };
    _vc.dataSource = self.searchResults;
    [_dialog show];
   
}



@end
