//
//  ContactController.m
//  ZhiNengSuo
//
//  Created by Chuenlu Kuo on 17/1/19.
//  Copyright © 2017年 czwen. All rights reserved.
//

#import "ContactController.h"
#import "ZNSContact.h"

@interface ContactController ()<UISearchResultsUpdating>{

    IBOutlet UITableView *tbvContact;
    
    UISearchController *searchCtrl;
    
    NSArray *items;
    NSMutableArray *results;
}

@end

@implementation ContactController

- (void)doGetContactList{

    [SVProgressHUD show];
    [APIClient POST:API_CONTACT withParameters:@{@"search":@"",@"page":@"-1"} successWithBlcok:^(id response) {
        NSLog(@"%@",response);
        items = [NSArray yy_modelArrayWithClass:[ZNSContact class] json:[response valueForKey:@"items"]];
        [tbvContact reloadData];
        [SVProgressHUD dismiss];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"通讯录";
    [self doGetContactList];
    
//    [tbvContact registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    tbvContact.tableFooterView = [UIView new];
    [self setSearchControllerView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setSearchControllerView{
    searchCtrl = [[UISearchController alloc]initWithSearchResultsController:nil];
    searchCtrl.searchBar.frame = CGRectMake(0, 0, 0, 44);
    searchCtrl.dimsBackgroundDuringPresentation = false;
    //搜索栏表头视图
    tbvContact.tableHeaderView = searchCtrl.searchBar;
    [searchCtrl.searchBar sizeToFit];
    //背景颜色
//    searchCtrl.searchBar.backgroundColor = [UIColor orangeColor];
    searchCtrl.searchBar.searchBarStyle = UISearchBarStyleProminent;
    searchCtrl.searchResultsUpdater = self;
    
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    NSLog(@"%@",searchController.searchBar.text);
    if (!results) {
        results = [NSMutableArray array];
    } 
    [results removeAllObjects];
    for (ZNSContact *contact in items) {
        NSString *key = searchController.searchBar.text;
        NSRange nameRange = [contact.name rangeOfString:key options:NSCaseInsensitiveSearch];
        NSRange phoneRange = [contact.phone rangeOfString:key options:NSCaseInsensitiveSearch];
        NSRange sectionRange = [contact.section rangeOfString:key options:NSCaseInsensitiveSearch];
        
        if (nameRange.length || phoneRange.length || sectionRange.length) {
            [results addObject:contact];
        }
    }
    //NSPredicate 谓词
//    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS %@",searchController.searchBar.text]; ;
//    results = [[items filteredArrayUsingPredicate:searchPredicate] mutableCopy];
    //刷新表格
    [tbvContact reloadData];
}


#pragma mark - tableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    ZNSContact *contact = (!searchCtrl.active)?items[indexPath.row]:results[indexPath.row];
    cell.textLabel.text = contact.phone;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@，%@",contact.name,contact.section];
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return (!searchCtrl.active)?items.count:results.count;
}

@end
