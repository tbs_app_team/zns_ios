//
//  NoticeDetailController.m
//  ZhiNengSuo
//
//  Created by Chuenlu Kuo on 16/12/29.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "NoticeDetailController.h"

@interface NoticeDetailController (){

    IBOutlet UITextView *txvNotice;
    
}

@end

@implementation NoticeDetailController

//- (void)doGetNoticeDetail{
//    
//    [SVProgressHUD show];
//    [APIClient POST:API_NOTICE_DETAIL(@(_notice.nid).stringValue) withParameters:nil successWithBlcok:^(id response) {
//       
//        ZNSNotice *n = (ZNSNotice *)[NSObject yy_modelWithJSON:response];
//        txvNotice.text = n.content;
//        [SVProgressHUD dismiss];
//    } errorWithBlock:^(ZNSError *error) {
//        [SVProgressHUD showErrorWithStatus:error.errorMessage];
//    }];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = _notice.title;
    
    txvNotice.text = [NSString stringWithFormat:@"创建人：%@\n创建时间：%@\n\n%@",_notice.createBy,_notice.createAt,_notice.content];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
 

@end
