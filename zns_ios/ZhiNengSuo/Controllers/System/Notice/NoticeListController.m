//
//  NoticeListController.m
//  ZhiNengSuo
//
//  Created by Chuenlu Kuo on 16/12/28.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "NoticeListController.h"
#import "NoticeDetailController.h"
#import <MJRefresh.h>

#import "ZNSNotice.h"

@interface NoticeListController ()<UITableViewDelegate,UISearchBarDelegate>{

    IBOutlet UITableView *tbvNoticeList;
    IBOutlet UISearchBar *searchbar;
    
    NSMutableArray *datas;
    NSInteger page;
}

@end

@implementation NoticeListController

- (void)doGetNoticeList{
 
    [APIClient POST:API_NOTICE withParameters:@{@"search":searchbar.text,@"page":@(page).stringValue} successWithBlcok:^(id response) {
        NSDictionary *dic = response;
        NSArray *items = dic[@"items"];
        if (!datas) {
            datas = [NSMutableArray array];
        }
        
        if (page == 0) {
            [datas removeAllObjects];
        }
        
        if (items && [items isKindOfClass:[NSArray class]]) {
            for (NSDictionary *d in items) {
                ZNSNotice *n = [[ZNSNotice alloc] init];
                [n setupDataWith:d];
                [datas addObject:n];
            }
        }
        
        [tbvNoticeList reloadData]; 
        [SVProgressHUD dismiss];
        
         [tbvNoticeList.mj_header endRefreshing];
        if ([dic[@"total"] integerValue] == datas.count) {
            [tbvNoticeList.mj_footer endRefreshingWithNoMoreData];
        }else{
            [tbvNoticeList.mj_footer endRefreshing];
        }
    } errorWithBlock:^(ZNSError *error) {
        [tbvNoticeList.mj_header endRefreshing];
        [tbvNoticeList.mj_footer endRefreshing];
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"公告列表";
    page = 0;
//    [tbvNoticeList registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    tbvNoticeList.tableFooterView = [UIView new];
  
    
    @weakify(self);
    tbvNoticeList.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        page = 0;
        [self doGetNoticeList];
    }];
    
    tbvNoticeList.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        page++;
        [self doGetNoticeList];
    }];
    [tbvNoticeList.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - searchbar delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{

    [SVProgressHUD show];
    page = 0;
    [self doGetNoticeList];
}


#pragma mark - tableView Delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    ZNSNotice *notice = datas[indexPath.row];
    cell.textLabel.text = notice.title;
    cell.detailTextLabel.text = notice.content;
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return datas.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NoticeDetailController *detail = [NoticeDetailController create];
    detail.notice = datas[indexPath.row]; 
    [self.navigationController pushViewController:detail animated:YES];
    
}

@end
