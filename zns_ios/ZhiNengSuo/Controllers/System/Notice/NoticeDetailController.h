//
//  NoticeDetailController.h
//  ZhiNengSuo
//
//  Created by Chuenlu Kuo on 16/12/29.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSNotice.h"

@interface NoticeDetailController : UIViewController

@property (nonatomic,strong) ZNSNotice *notice;

@end
