//
//  AccountInfoController.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 16/12/16.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "AccountInfoController.h"
#import "EditPasswordViewController.h"

@interface AccountInfoController ()<UITableViewDelegate,UITableViewDataSource>{

    IBOutlet UITableView *tbvAccount;
    NSMutableArray *infos;
}

@end

@implementation AccountInfoController

- (void)requestLogoutApi{

    [SVProgressHUD show];
    [APIClient POST:API_USER_LOGOUT withParameters:nil successWithBlcok:^(id response) {
        [SVProgressHUD dismiss];
        [[ZNSUser currentUser] logout];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD dismiss];
        [[ZNSUser currentUser] logout];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"账号信息";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"注销" style:UIBarButtonItemStylePlain target:self action:@selector(doLogout)];
    
    [self setupDataSource];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupDataSource{

    if (!infos) {
        infos = [NSMutableArray array];
    }
     ZNSUser *user = [ZNSUser currentUser];
    [infos addObject:@{@"title":@"姓名",@"detail":user.name}];
    [infos addObject:@{@"title":@"账号",@"detail":user.username}];
    [infos addObject:@{@"title":@"组织机构",@"detail":user.section}];
    [infos addObject:@{@"title":@"最后登陆时间",@"detail":user.lastlogin_at}];
    [infos addObject:@{@"title":@"修改密码",@"action":@"pushEditPasswordPage"}];
    
    [tbvAccount reloadData];
    
}

- (void)doLogout{
    
    @weakify(self);
    [UIAlertController showAlertInViewController:self withTitle:nil message:@"确定要注销该账号吗？" cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@[@"确定"] tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
        if (buttonIndex == 2) {
            @strongify(self);
            [self requestLogoutApi];
        }
    }];
    
}

- (void)pushEditPasswordPage{
    
    [self.navigationController pushViewController:[EditPasswordViewController create] animated:YES];
}

#pragma mark - TableView delegate & datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return infos.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *dic = infos[indexPath.row];
    cell.textLabel.text = dic[@"title"];
    cell.detailTextLabel.text = dic[@"detail"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dic = infos[indexPath.row]; 
    //没有页面的责获取点击事件
    SEL action =  NSSelectorFromString(dic[@"action"]);
    if ([self respondsToSelector:action] && action) {
        IMP imp = [self methodForSelector:action];
        void (*func)(id, SEL) = (void *)imp;
        func(self, action);
    }
}


@end
