//
//  BaseSearchTableViewController.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/6.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import "BaseSearchTableViewController.h"

@interface BaseSearchTableViewController ()<UISearchResultsUpdating,UITableViewDelegate,UITableViewDataSource>{
    
    UITableView *tableView;
    UISearchController *searchCtrl;
    
    NSMutableArray *sources;   //数据源
    NSMutableArray *results;   //搜索结果
    
}

@end

@implementation BaseSearchTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    tableView = [[UITableView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self headerWithRefreshing];
    }];
    tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [self footerWithRefreshing];
    }];
    
    [tableView registerNib:[self cellNib] forCellReuseIdentifier:[self cellIdentifier]];
    
    //初始化搜索
    searchCtrl = [[UISearchController alloc]initWithSearchResultsController:nil];
    searchCtrl.searchBar.frame = CGRectMake(0, 0, 0, 44);
    searchCtrl.dimsBackgroundDuringPresentation = false;
    //搜索栏表头视图
    tableView.tableHeaderView = searchCtrl.searchBar;
    [searchCtrl.searchBar sizeToFit];
    //背景颜色
    //    searchCtrl.searchBar.backgroundColor = [UIColor orangeColor];
    searchCtrl.searchBar.searchBarStyle = UISearchBarStyleProminent;
    searchCtrl.searchResultsUpdater = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UISearchController *)searchController{
    return searchCtrl;
}

- (void)headerWithRefreshing{
    
}

- (void)footerWithRefreshing{
    
}

- (void)endRefreshing{
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
}

- (void)endRefreshingWithNoMoreData{
    [tableView.mj_footer endRefreshingWithNoMoreData];
}

- (void)reloadData{
    [tableView reloadData];
}

- (void)searchWithController:(UISearchController *)ctrl{
    
}

- (UINib *)cellNib{
    return nil;
}

- (NSString *)cellIdentifier{
    return @"";
}

- (UITableViewCell *)cellWithTableView:(UITableView *)tbv indexPath:(NSIndexPath *)idxPath{
    return nil;
}

- (NSInteger)section{
    return 1;
}

- (NSInteger)row{
    return 0;
}

- (CGFloat)rowHeight{
    return 44.f;
}

#pragma mark - serach


- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    [self searchWithController:searchController];
}


#pragma mark - tableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self respondsToSelector:@selector(rowHeight)]) {
        return [self rowHeight];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([self respondsToSelector:@selector(cellWithTableView:indexPath:)]) {
        return [self cellWithTableView:tableView indexPath:indexPath];
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([self respondsToSelector:@selector(row)]) {
        return [self row];
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if ([self respondsToSelector:@selector(section)]) {
        return [self section];
    }
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)]) {
        [self tableView:tableView didSelectRowAtIndexPath:indexPath];
    }
}

@end
