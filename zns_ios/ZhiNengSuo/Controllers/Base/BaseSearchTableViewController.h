//
//  BaseSearchTableViewController.h
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/6.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseSearchTableViewController : UIViewController

- (UINib *)cellNib;
 
- (NSString *)cellIdentifier;

- (UITableViewCell *)cellWithTableView:(UITableView *)tbv indexPath:(NSIndexPath *)idxPath;

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;

- (NSInteger)section;

- (NSInteger)row;

- (CGFloat)rowHeight;

- (void)reloadData;

- (UISearchController *)searchController;

- (void)headerWithRefreshing;

- (void)footerWithRefreshing;

- (void)endRefreshing;
- (void)endRefreshingWithNoMoreData;

- (void)searchWithController:(UISearchController *)ctrl;
@end
