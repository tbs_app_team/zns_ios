//
//  SmartKeyDetailController.h
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/10.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SmartKeyDetailController : UIViewController

@property (nonatomic,strong) ZNSSmartKey *smartKey;
@property (nonatomic,assign) BOOL isAdd;

@property (nonatomic,strong) dispatch_block_t updateCallback;

@end
