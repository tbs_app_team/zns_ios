//
//  SmartKeyListCell.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/10.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import "SmartKeyListCell.h"

@interface SmartKeyListCell(){
    
    IBOutlet UILabel *keyName;
    IBOutlet UILabel *area;
    IBOutlet UILabel *btaddr;
    IBOutlet UILabel *keyType;
    IBOutlet UILabel *owner;
    
    
}

@end

@implementation SmartKeyListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code 
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setKey:(ZNSSmartKey *)key{
    
    if (!key) {
        return;
    }
    keyName.text = key.name;
    area.text = key.section;
    btaddr.text = key.btAddr;
    keyType.text = key.type;
    owner.text = key.user;
}

@end
