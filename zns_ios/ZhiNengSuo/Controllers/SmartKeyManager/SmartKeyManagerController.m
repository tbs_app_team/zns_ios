//
//  SmartKeyManagerController.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/10.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import "SmartKeyManagerController.h"
#import "SmartKeyDetailController.h"
#import "SmartKeyListCell.h"
#import "ZNSInitialViewController.h"

@interface SmartKeyManagerController (){
    
    NSInteger pageIndex;
    NSArray *smartKeys;
}

@end

@implementation SmartKeyManagerController

- (void)doGetSmartkeyWithSearchText:(NSString *)text{
    
    @weakify(self)
    [APIClient POST:API_SECTION_SMART_KEY_WITH_SID(@"0") withParameters:@{@"search":text,@"page":[NSNumber numberWithInteger:pageIndex],@"pagesize":[NSNumber numberWithInteger:20]} successWithBlcok:^(id response) {
        @strongify(self)
        if (pageIndex == 0) {
            smartKeys = [NSArray yy_modelArrayWithClass:[ZNSSmartKey class] json:response[@"items"]];
        }else{
            smartKeys = [smartKeys arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSSmartKey class] json:response[@"items"]]];
        }
        [SVProgressHUD dismiss];
        [self reloadData];
        [self endRefreshing];
        if (([[response valueForKey:@"page"] integerValue] + 1) == [[response valueForKey:@"pages"]integerValue]) {
            [self endRefreshingWithNoMoreData];
        }
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
        [self endRefreshing];
        if (pageIndex > 0) {
            pageIndex--;
        }
    }] ;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"智能钥匙管理";
    UIBarButtonItem *initItem = [[UIBarButtonItem alloc] initWithTitle:@"初始化" style:UIBarButtonItemStylePlain target:self action:@selector(goInitSmartKeyPage)];
    UIBarButtonItem *addItem = [[UIBarButtonItem alloc] initWithTitle:@"新增" style:UIBarButtonItemStylePlain target:self action:@selector(goAddSmartKeyPage)];
    self.navigationItem.rightBarButtonItems = @[addItem,initItem];
    
    [SVProgressHUD show];
    [self doGetSmartkeyWithSearchText:@""];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goInitSmartKeyPage{
    ZNSInitialViewController *initial = [ZNSInitialViewController create];
    initial.title = @"初始化钥匙";
    [self.navigationController pushViewController:initial animated:YES];
}

- (void)goAddSmartKeyPage{
    SmartKeyDetailController *detail = [SmartKeyDetailController create];
    @weakify(self);
    detail.updateCallback = ^{
        @strongify(self);
        [self headerWithRefreshing];
    };
    detail.isAdd = YES;
    detail.smartKey = [ZNSSmartKey new];
    [self.navigationController pushViewController:detail animated:YES];
}

#pragma mark - 父类方法
//下拉刷新
- (void)headerWithRefreshing{
    pageIndex = 0;
    [self doGetSmartkeyWithSearchText:@""];
}
//上拉加载
- (void)footerWithRefreshing{
    pageIndex++;
    [self doGetSmartkeyWithSearchText:@""];
}
//搜索
- (void)searchWithController:(UISearchController *)ctrl{
    
    if (ctrl.active) {
        if ([ctrl.searchBar.text length]) {
            [SVProgressHUD show];
            [self doGetSmartkeyWithSearchText:ctrl.searchBar.text];
        }
    }else{
        [SVProgressHUD show];
        [self doGetSmartkeyWithSearchText:@""];
    }
}

/*
 * tableview 相关
 */

//cell的高度
- (CGFloat)rowHeight{
    return 75;
}

//行数
- (NSInteger)row{
    return smartKeys.count;
}

//cell的标识
- (NSString *)cellIdentifier{
    return [SmartKeyListCell reuseIdentifier];
}

- (UINib *)cellNib{
    return [SmartKeyListCell nib];
}

- (UITableViewCell *)cellWithTableView:(UITableView *)tbv indexPath:(NSIndexPath *)idxPath{
    
    SmartKeyListCell *cell = [tbv dequeueReusableCellWithIdentifier:[SmartKeyListCell reuseIdentifier] forIndexPath:idxPath];
    cell.key = smartKeys[idxPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SmartKeyDetailController *detail = [SmartKeyDetailController create];
    @weakify(self);
    detail.updateCallback = ^{
        @strongify(self);
        [self headerWithRefreshing];
    };
    detail.isAdd = NO;
    detail.smartKey = smartKeys[indexPath.row];
    [self.navigationController pushViewController:detail animated:YES];
}

@end
