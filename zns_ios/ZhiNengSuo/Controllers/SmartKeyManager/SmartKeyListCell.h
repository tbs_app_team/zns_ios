//
//  SmartKeyListCell.h
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/10.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SmartKeyListCell : UITableViewCell

@property (nonatomic,strong) ZNSSmartKey *key;

@end
