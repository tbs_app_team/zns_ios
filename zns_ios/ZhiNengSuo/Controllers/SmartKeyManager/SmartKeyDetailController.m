//
//  SmartKeyDetailController.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/10.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import "SmartKeyDetailController.h"
#import "ZNSLabelTextFieldTableViewCell.h"
#import "CommonTableViewController.h"

#import "ZNSSection.h"

@interface SmartKeyDetailController (){
    
    IBOutlet UITableView *tbvSmartKey;
    
}

// 用作获取蓝牙地址
@property (nonatomic,strong) NSMutableArray *bts;

@property (nonatomic,strong) NSArray *sections;               //所属区域
@property (nonatomic ,strong) ZNSSection *selcectSection;

@property (nonatomic,strong) NSArray *smartKeyTypes;          //智能钥匙类型
@property (nonatomic,strong) NSMutableArray *keyUsers;          //智能钥匙类型

@property (nonatomic, strong) JKAlertDialog *dialog;

@end

@implementation SmartKeyDetailController

- (void)doSaveSmartKey{
    
    if (![self.smartKey.name length]) {
        [SVProgressHUD showErrorWithStatus:@"请输入钥匙名称"];
        return;
    }
    
    if (![self.smartKey.type length]) {
        [SVProgressHUD showErrorWithStatus:@"请选择钥匙类型"];
        return;
    }
    
    if (![self.smartKey.section length]) {
        [SVProgressHUD showErrorWithStatus:@"请所属区域"];
        return;
    }
    
    if (![self.smartKey.btAddr length]) {
        [SVProgressHUD showErrorWithStatus:@"请输入钥匙蓝牙地址"];
        return;
    }
    
//    if (![self.smartKey.user length]) {
//        [SVProgressHUD showErrorWithStatus:@"请选择拥有者"];
//        return;
//    }
    @weakify(self);
    [SVProgressHUD show];
    
    NSString *api = [NSString stringWithFormat:@"/smartkey/%@/update/",self.smartKey.skid];
    if (self.isAdd) {
        api = [NSString stringWithFormat:@"section/%@/smartkey/new",self.selcectSection.sid];
    }
    
    [APIClient POST:api withParameters:[self.smartKey generteSmartKeyParametersWith:self.isAdd] successWithBlcok:^(id response) {
        @strongify(self);
        [SVProgressHUD showSuccessWithStatus:@"保存成功"];
        if (self.updateCallback) {
            self.updateCallback();
        }
        [self.navigationController popViewControllerAnimated:YES];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.title = self.isAdd?@"新增智能钥匙":@"智能钥匙详情";
     self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(doSaveSmartKey)];
    [tbvSmartKey registerNib:[ZNSLabelTextFieldTableViewCell nib] forCellReuseIdentifier:[ZNSLabelTextFieldTableViewCell reuseIdentifier]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void)showSectionPicker{
    @weakify(self);
    if (!self.sections.count) {
        [SVProgressHUD showWithStatus:@"正在获取数据"];
        [ZNSAPITool getAllAreaSuccessWithBlcok:^(NSArray *response) {
            @strongify(self);
            [SVProgressHUD dismiss];
            self.sections = @[];
            [response enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ZNSSection *sec = obj;
                if ([sec.type isEqualToString:@"区域"]) {
                    self.sections = [self.sections arrayByAddingObject:sec];
                }
            }];
            [self showSectionPicker];
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:@"获取数据失败，请重新点击获取"];
        }];
        return;
    }
    [self.view endEditing:YES];
    [ActionSheetStringPicker showPickerWithTitle:@"选择区域" rows:[self.sections valueForKeyPath:@"name"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        self.smartKey.section = selectedValue;
        self.selcectSection = [self.sections objectAtIndex:selectedIndex];
        [tbvSmartKey reloadRow:2 inSection:0 withRowAnimation:UITableViewRowAnimationFade];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
    } origin:self.view];
}

- (void)showKeyTypePicker{
    @weakify(self);
    if (!self.smartKeyTypes.count) {
        [SVProgressHUD showWithStatus:@"正在获取数据"];
        [ZNSAPITool getSmartKeyTypeSuccessWithBlcok:^(NSArray *response) {
            @strongify(self);
            [SVProgressHUD dismiss];
            self.smartKeyTypes = response;
            [self showKeyTypePicker];
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:@"获取数据失败，请重新点击获取"];
        }];
        return;
    }
    [self.view endEditing:YES];
    [ActionSheetStringPicker showPickerWithTitle:@"选择钥匙类型" rows:self.smartKeyTypes initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        self.smartKey.type = selectedValue;
        [tbvSmartKey reloadRow:1 inSection:0 withRowAnimation:UITableViewRowAnimationFade];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
    } origin:self.view];
}

- (void)showKeyUsersPicker{
    @weakify(self);
    
    if (!self.keyUsers) {
        self.keyUsers = [NSMutableArray array];
    }
    
    if (!self.keyUsers.count) {
        [SVProgressHUD showWithStatus:@"正在获取数据"];
        [APIClient POST:@"/user/getUser" withParameters:nil successWithBlcok:^(id response) {
            @strongify(self);
            [SVProgressHUD dismiss];
            NSArray *arr = response[@"items"];
            for (NSDictionary *d in arr) {
                [self.keyUsers addObject:[NSString stringWithFormat:@"%@(%@)",d[@"name"],d[@"username"]]];
            }
            [self showKeyUsersPicker];
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:@"获取数据失败，请重新点击获取"];
        }];
        return;
    }
    [self.view endEditing:YES];
    [ActionSheetStringPicker showPickerWithTitle:@"选择拥有者" rows:self.keyUsers initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        self.smartKey.user = selectedValue;
        [tbvSmartKey reloadRow:4 inSection:0 withRowAnimation:UITableViewRowAnimationFade];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
    } origin:self.view];
}

- (void)showBTPicker{
    [self.view endEditing:YES];
    [self.bts removeAllObjects];
    CommonTableViewController *tvc= [[CommonTableViewController alloc]initWithStyle:UITableViewStylePlain];
    
    _dialog = [[JKAlertDialog alloc]initWithTitle:@"选择蓝牙" message:@""];
    _dialog.contentView = tvc.view;
    
    
    @weakify(self);
    [[GGBluetooth sharedManager]setDiscoverPeripheralBlock:^(CBPeripheral *discoverPeripheral,NSDictionary *advertisementData,NSNumber *RSSI){
        @strongify(self);
        if (!self.bts) {
            self.bts = [NSMutableArray array];
        }
        
        if (![[self.bts valueForKey:@"identifier"] containsObject:discoverPeripheral.identifier]) {
            [self.bts addObject:discoverPeripheral];
            [tvc setDatas:self.bts];
        }
    }];
    
    [_dialog addButton:Button_CANCEL withTitle:@"取消" handler:^(JKAlertDialogItem *item) {
        
    }];;
    
    [tvc setSelectedBlock:^(CBPeripheral *discoverPeripheral){
        self.smartKey.btAddr = discoverPeripheral.macAddress;
        [tbvSmartKey reloadRow:3 inSection:0 withRowAnimation:UITableViewRowAnimationFade];
        [_dialog dismiss];
    }];
    
    [_dialog show];
    [[GGBluetooth sharedManager]startScan];
}


#pragma mark - TableView 类方法

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{ 
    ZNSLabelTextFieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ZNSLabelTextFieldTableViewCell reuseIdentifier] forIndexPath:indexPath];
    NSString *title = @"";
    NSString *value = @"";
    NSString *placeholder = @"";
    BOOL isCanEdit = NO;
    NSString *strAction = @"";
    switch (indexPath.row) {
        case 0:{
            title = @"钥匙名称:";
            value = self.smartKey.name;
            placeholder = @"请输入钥匙名称";
            isCanEdit = YES;
            cell.textBlcok = ^(NSString *result) {
                self.smartKey.name = result;
            };
        }
            break;
        case 1:
            title = @"钥匙类型:";
            value = self.smartKey.type;
            placeholder = @"请选择钥匙类型";
            strAction = @"showKeyTypePicker";
            break;
        case 2:
            title = @"所属区域:";
            value = self.smartKey.section;
            placeholder = @"请选择所属区域";
            strAction = @"showSectionPicker";
            break;
        case 3:{
            title = @"蓝牙地址:";
            value = self.smartKey.btAddr;
            placeholder = @"请输入蓝牙地址或者点击采码";
            isCanEdit = YES;
            cell.showToolBar = YES;
            @weakify(self);
            cell.toolBarActionBlock = ^{
                @strongify(self);
                [self showBTPicker];
            };
            cell.textBlcok = ^(NSString *result) {
                self.smartKey.btAddr = result;
            };
        }
            break;
        case 4:
            title = @"拥有者:";
            value = self.smartKey.user;
            placeholder = @"请选择拥有者";
            strAction = @"showKeyUsersPicker";
            break;
        default:
            break;
    }
    cell.label.text = title;
    cell.textField.text = value;
    cell.textField.placeholder = placeholder;
    cell.canEditBlock = ^BOOL{
        
        //获取点击事件
        SEL action =  NSSelectorFromString(strAction);
        if ([self respondsToSelector:action] && action) {
            IMP imp = [self methodForSelector:action];
            void (*func)(id, SEL) = (void *)imp;
            func(self, action);
        } 
        return isCanEdit;
    };
    return cell;
}

@end
