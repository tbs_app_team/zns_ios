//
//  TempPermissionApprovalViewController.m
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/28.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import "TempPermissionApprovalViewController.h"
#import "TempPermissionApprovalTableViewCell.h"
#import "TempPermissionApprovalHistoryCell.h"
#import "ZNSTempTask.h"
#import "MJRefresh.h"

@interface TempPermissionApprovalViewController () <UITableViewDataSource, UITableViewDelegate> {
    IBOutlet UIButton *disagreeButton;
    IBOutlet UIButton *agreeButton;
    
}
@property (strong, nonatomic) IBOutlet UITableView *mainTableView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeight;
@property (strong, nonatomic) NSMutableArray * arrData;
@property (strong, nonatomic) NSMutableArray * historyDatas;
@property (assign, nonatomic) NSInteger page;

@end

@implementation TempPermissionApprovalViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initializeUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)initializeUI {
    [disagreeButton bs_configureAsDangerStyle];
    [agreeButton bs_configureAsSuccessStyle];
    
    
    @weakify(self);
    self.mainTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        self.page = 0;
        if (self.segmentedControl.selectedSegmentIndex == 0) {
            [self loadDataFromInternet];
        }else{
            [self loadHistoyWithPage:self.page];
        }
    }];
    
    self.mainTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.page ++;
        if (self.segmentedControl.selectedSegmentIndex == 0) {
            [self loadDataFromInternet];
        }else{
            [self loadHistoyWithPage:self.page];
        }
    }];
    [self.mainTableView.mj_header beginRefreshing];
    
    
    [self.segmentedControl addTarget:self
                              action:@selector(segmentedControlChanged)
                    forControlEvents:UIControlEventValueChanged];
    
    self.mainTableView.rowHeight = UITableViewAutomaticDimension;
    self.mainTableView.estimatedRowHeight = 300;
    

}

- (void)loadDataFromInternet {
    @weakify(self);
    [ZNSTempTask getVerifyTempTasksWithPage:[NSNumber numberWithInteger:self.page] success:^(id response) {
        @strongify(self);
        if (self.page == 0) {
            [self.arrData removeAllObjects];
            self.arrData = [response[@"items"] mutableCopy];
            if (self.arrData.count == 0) {
                [SVProgressHUD showInfoWithStatus:@"暂时没有待审核的临时鉴权"];
            }
            [self.mainTableView.mj_header endRefreshing];
            [self.mainTableView.mj_footer resetNoMoreData];
        } else {
            [self.arrData addObjectsFromArray:response[@"items"]];
            [self.mainTableView.mj_footer endRefreshing];
        }
        if ([response[@"page"]intValue] + 1 == [response[@"pages"]intValue]) {
            [self.mainTableView.mj_footer endRefreshingWithNoMoreData];
        }
        [self.mainTableView reloadData];
    } error:^(ZNSError *error) {
        [self.mainTableView.mj_header endRefreshing];
        [self.mainTableView.mj_footer endRefreshing];
        [SVProgressHUD showInfoWithStatus:error.errorMessage];
    }];
}

- (void)loadHistoyWithPage:(NSInteger)page{
    [APIClient POST:[NSString stringWithFormat:@"user/%@/temp_task/",[ZNSUser currentUser].username] withParameters:@{@"page":[NSNumber numberWithInteger:page],@"status": @[@"待审批", @"待下载", @"已拒绝", @"已过期"]} successWithBlcok:^(id response) {
        [self.mainTableView.mj_header endRefreshing];
        [self.mainTableView.mj_footer endRefreshing];
        
        if (!self.historyDatas) {
            self.historyDatas = [NSMutableArray array];
        }
        if (page == 0) {
            [self.historyDatas removeAllObjects];
            NSArray *arr = [NSArray yy_modelArrayWithClass:[ZNSTempTask class] json:[response valueForKey:@"items"]];
            [self.historyDatas addObjectsFromArray:arr];
        }else{
            [self.historyDatas addObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSTempTask class] json:[response valueForKey:@"items"]]];
        }
        if ([response[@"page"]intValue] + 1 == [response[@"pages"]intValue]) {
            [self.mainTableView.mj_footer endRefreshingWithNoMoreData];
        }
       
        [self.mainTableView reloadData];
    } errorWithBlock:^(ZNSError *error) {
        [self.mainTableView.mj_header endRefreshing];
        [self.mainTableView.mj_footer endRefreshing];
        
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
}


#pragma mark - Action

- (IBAction)didTapButton:(UIButton *)sender {
    
    if (sender.tag == 1) {
        [UIAlertController showAlertInViewController:self.navigationController withTitle:@"是否全部同意？" message:@"" cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@[@"是"] tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
            [SVProgressHUD show];
            @weakify(self);
            [ZNSAPITool batchHandel:API_TEMP_TASK_VERIFYALL agree:YES reason:@"" successWithBlcok:^(id response) {
                @strongify(self);
                [SVProgressHUD showSuccessWithStatus:@"已批量同意"];
                
                [self.mainTableView.mj_header beginRefreshing];
            } errorWithBlock:^(ZNSError *error) {
                [SVProgressHUD showErrorWithStatus:error.errorMessage];
            }];
        }];
    }else{
        UIAlertController * ac = [UIAlertController alertControllerWithTitle:@"拒绝这个用户的临时请求" message:nil preferredStyle:UIAlertControllerStyleAlert];
        [ac addTextFieldWithConfigurationHandler:^(UITextField *textField){
            textField.placeholder = @"请输入理由";
            
        }];
        [ac addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        }]];
        [ac addAction:[UIAlertAction actionWithTitle:@"拒绝" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            UITextField *tf = ac.textFields.firstObject;
            if ( tf.text.length == 0) {
                [SVProgressHUD showInfoWithStatus:@"必须输入拒绝理由"];
                
            } else if([[NSString isEmpty:tf.text] isEqualToString:@"yes"]){
                [SVProgressHUD showInfoWithStatus:@"必须输入拒绝理由"];
                
            }else  if (tf.text.length > 32) {
                [SVProgressHUD  showInfoWithStatus:@"输入长度应不超过32位"];
            }else{
                [SVProgressHUD show];
                @weakify(self);
                [ZNSAPITool batchHandel:API_TEMP_TASK_VERIFYALL agree:NO reason:tf.text successWithBlcok:^(id response) {
                    @strongify(self);
                    [SVProgressHUD showSuccessWithStatus:@"已批量拒绝"];
                    
                    [self.mainTableView.mj_header beginRefreshing];
                } errorWithBlock:^(ZNSError *error) {
                    [SVProgressHUD showErrorWithStatus:error.errorMessage];
                }];
            }
            
           
        }]];
        [self presentViewController:ac animated:YES completion:nil];
    }
}

- (void)segmentedControlChanged{
    [self.mainTableView reloadData];
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        self.bottomViewHeight.constant = 44;
//        if (self.arrData.count<=0) {
            [self.mainTableView.mj_header beginRefreshing];
//        }
    }else{
        self.bottomViewHeight.constant = 0;
//        if (self.historyDatas.count<=0) {
            [self.mainTableView.mj_header beginRefreshing];
//        }
    }
}

#pragma mark - UITableView

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        return self.arrData.count;
    }else{
        return self.historyDatas.count;
    }
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    @weakify(self);
//    if (self.segmentedControl.selectedSegmentIndex == 0) {
//        return [tableView fd_heightForCellWithIdentifier:@"TempPermissionApprovalCell" cacheByIndexPath:indexPath configuration:^(id cell) {
//            @strongify(self);
//            [cell setValue:[ZNSTempTask yy_modelWithDictionary:self.arrData[indexPath.row]] forKeyPath:@"tempTask"];
//        }];
//    }else{
//        return [tableView fd_heightForCellWithIdentifier:@"TempPermissionApprovalHistoryCell" cacheByIndexPath:indexPath configuration:^(id cell) {
//            @strongify(self);
//            [cell setValue:[ZNSTempTask yy_modelWithDictionary:self.historyDatas[indexPath.row]] forKeyPath:@"tempTask"];
//        }];    }
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        
        TempPermissionApprovalTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"TempPermissionApprovalCell" forIndexPath:indexPath];
        cell.tempTask = [ZNSTempTask yy_modelWithDictionary:self.arrData[indexPath.row]];
        cell.indexBlock = ^(NSInteger index) {
            if (index == 0) {
                
                UIAlertController * ac = [UIAlertController alertControllerWithTitle:index == 0?@"拒绝这个用户的临时请求":@"同意这个用户的临时请求" message:nil preferredStyle:UIAlertControllerStyleAlert];
                [ac addTextFieldWithConfigurationHandler:^(UITextField *textField){
                    textField.placeholder = @"请输入理由";
                    
                }];
                [ac addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                }]];
                [ac addAction:[UIAlertAction actionWithTitle:index == 0?@"确定":@"批准" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    
                    UITextField *tf = ac.textFields.firstObject;
                    if (index == 0 && tf.text.length == 0) {
                        [SVProgressHUD showInfoWithStatus:@"必须输入拒绝理由"];
                        
                    } else if(index==0 && [[NSString isEmpty:tf.text] isEqualToString:@"yes"]){
                          [SVProgressHUD showInfoWithStatus:@"必须输入拒绝理由"];
                        
                    }else if (index == 0 && tf.text.length > 32) {
                        [SVProgressHUD  showInfoWithStatus:@"输入长度应不超过32位"];
                    }else{
                        @weakify(self);
                        [APIClient POST:API_TEMPTASK_VERIFY_WITH_TID(self.arrData[indexPath.row][@"id"]) withParameters:@{@"action": index == 0?@"拒绝":@"批准" ,@"memo" : tf.text} successWithBlcok:^(id response) {
                            @strongify(self);
                            [self.arrData removeObjectAtIndex:indexPath.row];
                            [self.mainTableView deleteRow:indexPath.row inSection:indexPath.section withRowAnimation:UITableViewRowAnimationRight];
                            [self.mainTableView reloadData];
                        } errorWithBlock:^(ZNSError *error) {
                            [SVProgressHUD showInfoWithStatus:error.errorMessage];
                        }];
                    }
                }]];
                [self presentViewController:ac animated:YES completion:nil];
            }else{
                @weakify(self);
                [APIClient POST:API_TEMPTASK_VERIFY_WITH_TID(self.arrData[indexPath.row][@"id"]) withParameters:@{@"action": index == 0?@"拒绝":@"批准"} successWithBlcok:^(id response) {
                    @strongify(self);
                    [self.arrData removeObjectAtIndex:indexPath.row];
                    [self.mainTableView deleteRow:indexPath.row inSection:indexPath.section withRowAnimation:UITableViewRowAnimationRight];
                    [self.mainTableView reloadData];
                } errorWithBlock:^(ZNSError *error) {
                    [SVProgressHUD showInfoWithStatus:error.errorMessage];
                }];
            }
            
        };
        return cell;
    }else{
        TempPermissionApprovalHistoryCell * cell = [tableView dequeueReusableCellWithIdentifier:@"TempPermissionApprovalHistoryCell" forIndexPath:indexPath];
        [cell setTempTask:[self.historyDatas objectAtIndex:indexPath.row]];
        return cell;
    }
}

@end
