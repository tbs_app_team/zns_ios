//
//  TempPermissionUnlockViewController.m
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/29.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import "TempPermissionUnlockViewController.h"
#import "TempPermissionUnlockTableViewCell.h"
#import "ZNSTempTask.h"
#import "MJRefresh.h"
#import "TempPermissionUnlockObjectViewController.h"
#import "BlueToothViewController.h"
#import "ZNSTools.h"

@interface TempPermissionUnlockViewController ()<UITableViewDataSource, UITableViewDelegate> {
//    NSStringEncoding  gb_2312Encoding;
    JKAlertDialog *dialog;
}

@property (strong, nonatomic) IBOutlet UITableView *mainTableView;
@property (strong, nonatomic) NSArray * arrData;
@property (assign, nonatomic) NSInteger page;


@property (nonatomic,strong) NSMutableData *firstFrame;
@property (nonatomic,strong) NSMutableData *lockIdFrames;
@property (nonatomic,strong) NSMutableData *recordFrames;
@property (nonatomic,assign) CBCentralManagerState blueState;

@property (nonatomic,strong) NSString *tid;

@end

@implementation TempPermissionUnlockViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.firstFrame = [NSMutableData data];
    self.lockIdFrames = [NSMutableData data];
    self.recordFrames = [NSMutableData data];
//    gb_2312Encoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    
    [self initializeUI];
    @weakify(self)
    [[GGBluetooth sharedManager] setBluetoothStatusUpdateBlock:^(CBCentralManager *centeral){\
        @strongify(self)
        self.blueState = (CBCentralManagerState)centeral.state;
    }];
}


#pragma mark - Private

- (void)initializeUI {
    
    self.mainTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 0;
        [self loadDataFromInternet];
    }];
    self.mainTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self loadDataFromInternet];
    }];
    [self.mainTableView.mj_header beginRefreshing];
    
    self.mainTableView.rowHeight = UITableViewAutomaticDimension;
    self.mainTableView.estimatedRowHeight = 124;
}

- (void)loadDataFromInternet {
    @weakify(self);
    [ZNSTempTask getCurrentUserTempTasksWithPage:[NSNumber numberWithInteger:self.page] success:^(id response) {
        @strongify(self);
        if (self.page == 0) {
            self.arrData = @[];
            [response[@"items"]enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                @strongify(self);
//                if ([[obj valueForKey:@"status"] isEqualToString:@"待下载"]) {
                    self.arrData = [self.arrData arrayByAddingObject:[ZNSTempTask yy_modelWithJSON:obj]];
//                }
            }];
            if (self.arrData.count == 0) {
                [SVProgressHUD showInfoWithStatus:@"暂时没有临时鉴权"];
            }
            [self.mainTableView.mj_header endRefreshing];
        } else {
            [response[@"items"]enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                @strongify(self);
//                if ([[obj valueForKey:@"status"] isEqualToString:@"待下载"]) {
                    self.arrData = [self.arrData arrayByAddingObject:[ZNSTempTask yy_modelWithJSON:obj]];
//                }
            }];
            [self.mainTableView.mj_footer endRefreshing];
        }
        if ([response[@"page"]intValue] + 1 == [response[@"pages"]intValue]) {
            [self.mainTableView.mj_footer endRefreshingWithNoMoreData];
        } else {
            [self.mainTableView.mj_footer resetNoMoreData];
        }
        [self.mainTableView reloadData];
    } error:^(ZNSError *error) {
        [self.mainTableView.mj_header endRefreshing];
        [self.mainTableView.mj_footer endRefreshing];
        [SVProgressHUD showInfoWithStatus:error.errorMessage];
    }];
    
}

#pragma mark - Action

#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrData.count;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    @weakify(self);
//    return [tableView fd_heightForCellWithIdentifier:@"TempPermissionUnlockCell" cacheByIndexPath:indexPath configuration:^(TempPermissionUnlockTableViewCell *cell) {
//        @strongify(self);
//        cell.tempTask = [self.arrData objectAtIndex:indexPath.row];
//    }];
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TempPermissionUnlockTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"TempPermissionUnlockCell" forIndexPath:indexPath];
    cell.tempTask = [self.arrData objectAtIndex:indexPath.row];
    [cell setGetLocationBlock:^(void){
        
        NSMutableArray *item = [NSMutableArray array];
        
        MKMapItem *currentLocation = [MKMapItem mapItemForCurrentLocation];
        [item addObject:currentLocation];
        
        ZNSTempTask *temp=[self.arrData objectAtIndex:indexPath.row];
         [temp.objects enumerateObjectsUsingBlock:^(ZNSObject* obj, NSUInteger idx, BOOL * _Nonnull stop) {
             CLLocationCoordinate2D coordinate;
             coordinate.latitude=[obj.lat floatValue];
             coordinate.longitude=[obj.lng floatValue];
             MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:coordinate addressDictionary:nil]];
             
             toLocation.name = obj.name;
             [item addObject:toLocation];
         }];
        
        
//        NSArray *items = [NSArray arrayWithObjects:currentLocation, nil, nil];
        NSDictionary *options = @{ MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving, MKLaunchOptionsMapTypeKey: [NSNumber numberWithInteger:MKMapTypeStandard], MKLaunchOptionsShowsTrafficKey:@YES };
        //打开苹果自身地图应用，并呈现特定的item
        [MKMapItem openMapsWithItems:item launchOptions:options];
    }];
    [cell setDownBlock:^{
        if (self.blueState == CBCentralManagerStatePoweredOff) {
            [SVProgressHUD showErrorWithStatus:@"该设备尚未打开蓝牙,请在设置中打开"];
        }else{
            [[GGBluetooth sharedManager] disconnectCurrentDevice];
            [self separateFrame:[self.arrData objectAtIndex:indexPath.row]];
        }
    }];
    [cell setReturnBlock:^{
        if (self.blueState == CBCentralManagerStatePoweredOff) {
            [SVProgressHUD showErrorWithStatus:@"该设备尚未打开蓝牙,请在设置中打开"];
        }else{
            [[GGBluetooth sharedManager] disconnectCurrentDevice];
            [self returnData:[self.arrData objectAtIndex:indexPath.row]];
        }
    }];
    if ([ZNSTools isSCLock]) {
        cell.btnDownload.hidden = YES;
        cell.btnReturn.hidden = YES;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    TempPermissionUnlockObjectViewController *vc = [TempPermissionUnlockObjectViewController create];
    vc.tempTask = [self.arrData objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}



- (void)separateFrame:(ZNSTempTask *)task{
    
    [self.firstFrame resetBytesInRange:NSMakeRange(0, [self.firstFrame length])];
    [self.firstFrame setLength:0];
    
    [self.lockIdFrames resetBytesInRange:NSMakeRange(0, [self.lockIdFrames length])];
    [self.lockIdFrames setLength:0];

    NSData *tem = [NSData data];
    
    NSInteger guid = task.tid.integerValue;
    tem = [ZNSTools getTaskIdDataFromInt:guid taskFlag:TransTask]; //16byte
    [self.firstFrame appendData:tem];
    
    tem = [ZNSTools getNameDataFromString:task.name nameLength:16];
    [self.firstFrame appendData:tem];
    
    __block NSUInteger temCount = 0;
    __block NSMutableArray *locks=[NSMutableArray array];
    [task.objects enumerateObjectsUsingBlock:^(ZNSObject *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        temCount += obj.lockset.count;
        [locks addObjectsFromArray:obj.lockset];
    }];
    tem = [NSData dataWithBytes:&temCount length:4];
    [self.firstFrame appendData:tem];
    
    __block NSMutableData *temIdData = [NSMutableData data];
    [locks enumerateObjectsUsingBlock:^(ZNSLock* obj, NSUInteger idx, BOOL * _Nonnull stop) {
        int16_t i1 = [obj.lid integerValue];
        NSData *tem1 = [NSData dataWithBytes:&i1 length:2];
        [temIdData appendData:tem1];
        
//        i1 = [obj.stationId integerValue];
        i1 = 1 ;
        tem1 = [NSData dataWithBytes:&i1 length:2];
        [temIdData appendData:tem1];
        
    }];
    [self.lockIdFrames appendData:temIdData];
    
    
    guid = task.users.count;
    tem = [NSData dataWithBytes:&guid length:2];
    [self.firstFrame appendData:tem];
    
    temIdData = [NSMutableData data];
    [task.users enumerateObjectsUsingBlock:^(ZNSUser *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        temCount = obj.sid.integerValue;
        [temIdData appendBytes:&temCount length:2];
    }];
    [self.firstFrame appendData:temIdData];
    
    tem  =  [ZNSTools getFinalDateString:[NSData dataWithHexString:[NSString dateHexStringFromDigitalString:task.begin_at]]];
    [self.firstFrame appendData:tem];
    
    tem  = [ZNSTools getFinalDateString:[NSData dataWithHexString:[NSString dateHexStringFromDigitalString:task.end_at]]];
    [self.firstFrame appendData:tem];
    
    tem = self.firstFrame;
    [UIAlertController showAlertInViewController:self.parentViewController withTitle:@"选择钥匙" message:@"数据准备完毕，是否下载到钥匙" cancelButtonTitle:@"否" destructiveButtonTitle:nil otherButtonTitles:@[@"是"] tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
        if(buttonIndex == 2){
            [self selectKey];
            _tid = task.tid;
        }
    }];
    NSLog(@"firstFrame ==== %@",self.firstFrame);
    NSLog(@"lockIdFrames ===== %@",self.lockIdFrames);
}
- (void)selectKey{
    static BlueToothViewController *btVC;
    
    if (!btVC) {
        btVC = [BlueToothViewController create];
    }
    
    @weakify(self)
    [btVC setConnectedBlock:^{
        @strongify(self)
        __block NSUInteger i=0;
        
        [[GGBluetooth sharedManager] setNotifyCharacteristicBlock:^(CBCharacteristic *characteristic,NSData *recvData,NSError *error){
            @strongify(self)
            ZNSBluetoothInstruction *b = [[ZNSBluetoothInstruction alloc]initWithByteData:characteristic.value];
            
            if (b.function == Func_Code_00 && b.cmd == Cmd_Code_03 ) {//firstFrame
                if (b.isRight == 0x80) {
                    if ([self.lockIdFrames length]) {
                        NSUInteger framesCount = [self.lockIdFrames length]%256==0? [self.lockIdFrames length]/256 : ([self.lockIdFrames length]/256 + 1);
                        NSData *framesData = [NSData dataWithBytes:&framesCount length:2];
                        ZNSBluetoothInstruction *d=[[ZNSBluetoothInstruction alloc] initWithMoreData:Cmd_Code_03 function:Func_Code_01 body:nil rfid:framesData];
                        
                        [[GGBluetooth sharedManager] writeValueData:d.data];
                    }else{
                        [self updateStatusWith:@"已下载"];
                        [SVProgressHUD showSuccessWithStatus:@"任务下载成功"];
                        [[GGBluetooth sharedManager] disconnectCurrentDevice];
                    }
                    
                }else{
                    [SVProgressHUD showErrorWithStatus:@"任务下载失败，请重试"];
                    [[GGBluetooth sharedManager] disconnectCurrentDevice];
                }
                
            }
            
            if (b.function == Func_Code_01 || b.function == Func_Code_02 ) {//lockid frames
                if (b.isRight == 0x80) {
                    if(i!=([self.lockIdFrames length]%256==0? [self.lockIdFrames length]/256 : ([self.lockIdFrames length]/256 + 1))) {
                        [[GGBluetooth sharedManager] sendFrame:(i+1) numberOfFramesPer:[self.lockIdFrames length]%256 == 0 ? 256/4: ([self.lockIdFrames length]%256)/4 frameLen:256  cmd:Cmd_Code_03 fun:Func_Code_02 data:self.lockIdFrames];
                        i++;
                        NSLog(@"lock frame ：%ld",i);
                    }else{
                        i = 0;
                       [self updateStatusWith:@"已下载"];
                        [SVProgressHUD showSuccessWithStatus:@"任务下载成功"];
                        [[GGBluetooth sharedManager] disconnectCurrentDevice];
                    }
  
                }else{
                    i = 0;
                    [SVProgressHUD showErrorWithStatus:@"任务下载失败，请重试"];
                    [[GGBluetooth sharedManager] disconnectCurrentDevice];
                }
                
            }
            
        }];
        
        [SVProgressHUD showWithStatus:@"正在下载任务到钥匙"];
        [[GGBluetooth sharedManager] setSendFailureBlock:^{
            [SVProgressHUD showErrorWithStatus:@"钥匙无回应，请重试"];
            [[GGBluetooth sharedManager] disconnectCurrentDevice];
        }];
        [self packPlanPession];
        
        
    }];
    dialog = [[JKAlertDialog alloc]initWithTitle:@"选择智能钥匙" message:@""];
    dialog.contentView = btVC.view;
    
    [dialog addButton:Button_CANCEL withTitle:@"取消" handler:^(JKAlertDialogItem *item) {
        
    }];;
    
    [btVC setConnectedSmartKeyBlock:^(ZNSSmartKey *smartKey){
        [dialog dismiss];
        
    }];
    
    [dialog show];
    [btVC setupBT];
    [btVC loadSmartKeys];
}

- (void)updateStatusWith:(NSString *)status{
    
    //    @weakify(self)
    [APIClient POST:API_USER_TEMP_TASK_STATUS(_tid) withParameters:@{@"status":status} successWithBlcok:^(id response) {
        //        @strongify(self)
        NSLog(@"response=%@", response);
    } errorWithBlock:^(ZNSError *error) {
        NSLog(@"%@",error.errorMessage);
    }];
}

- (void)deletedTask:(ZNSTempTask *)task{

    NSData *tem = [ZNSTools getTaskIdDataFromInt:task.tid.integerValue taskFlag:TransTask];
    
    //删除任务
    ZNSBluetoothInstruction *da =[[ZNSBluetoothInstruction alloc] initWithInstruction:Cmd_Code_06 function:Func_Code_00 body:nil rfid:tem];
    //    NSLog(@"%@",da.data);
    [[GGBluetooth sharedManager] writeValueData:da.data];

}
- (void)packPlanPession{
    
    
    [[GGBluetooth sharedManager] setCanWriteValueBlock:^{
        
        [[GGBluetooth sharedManager] writeValueData:[[ZNSBluetoothInstruction alloc] initWithMoreData:Cmd_Code_03 function:Func_Code_00 body:nil rfid:self.firstFrame].data ];
    }];
    
}

- (void)returnData:(ZNSTempTask *)task{
    
    static BlueToothViewController *btVC;
    if (!btVC) {
        btVC = [BlueToothViewController create];
    }
    
    @weakify(self)
    [btVC setConnectedBlock:^{
        //            @strongify(self)
        
        __block int16_t frameCount = 0;
        __block int16_t user_id = 0;
        __block int16_t j = 0;
        [[GGBluetooth sharedManager] setNotifyCharacteristicBlock:^(CBCharacteristic *characteristic,NSData *recvData,NSError *error){
            @strongify(self)
            ZNSBluetoothInstruction *b = [[ZNSBluetoothInstruction alloc]initWithByteData:recvData];
            
            if(b.function == Func_Code_00 && b.cmd == Cmd_Code_05){
                int taskCount ;
              
                [[b.bodyData subdataWithRange:NSMakeRange(0, 2)] getBytes:&taskCount length:2];
                if (taskCount == 0) {
                     [SVProgressHUD showInfoWithStatus:@"钥匙中没有可回传的任务"];
                     [[GGBluetooth sharedManager] disconnectCurrentDevice];
                }else{
                    
                    NSData *tem = [ZNSTools getTaskIdDataFromInt:task.tid.integerValue taskFlag:TransTask];
                    int i;
                    BOOL hasReturnTicket = NO;
                    for (i=0; i<taskCount; i++){
                        
                        if ([tem isEqualToData:[b.bodyData subdataWithRange:NSMakeRange(2+i*16, 16)]]) {
                            //任务回传
                            hasReturnTicket = YES;
                            ZNSBluetoothInstruction *da =[[ZNSBluetoothInstruction alloc] initWithInstruction:Cmd_Code_04 function:Func_Code_00 body:nil rfid:tem];
                            NSLog(@"%@",da.data);
                            [[GGBluetooth sharedManager] writeValueData:da.data];
                        }
                    }
                    if (!hasReturnTicket) { //钥匙中要回传的任务不在列表中
                        [SVProgressHUD showInfoWithStatus:@"钥匙中需要回传的任务不在列表中"];
                        [[GGBluetooth sharedManager] disconnectCurrentDevice];
                    }
                }
            }
            
            if (b.function == Func_Code_00 && b.cmd == Cmd_Code_04) {//回传总帧数
                
                [[b.bodyData subdataWithRange:NSMakeRange(0, 2)]  getBytes:&frameCount length:2];
                [[b.bodyData subdataWithRange:NSMakeRange(2, 2)] getBytes:&user_id length:2];
                if(j!=frameCount) {
                    j++;
                    NSData *frame = [NSData dataWithBytes:&j length:2];
                    
                    [[GGBluetooth sharedManager] writeValueData:[[ZNSBluetoothInstruction alloc] initWithMoreData:Cmd_Code_04 function:Func_Code_01 body:nil rfid:frame].data];
                    
                    
                }else{
                    [SVProgressHUD showInfoWithStatus:@"钥匙中无当前任务操作记录"];
//                    [[GGBluetooth sharedManager] disconnectCurrentDevice];
                    [self updateStatusWith:@"待下载"];
                    [self deletedTask:task];
                }
            }
            if (b.function == Func_Code_01 && b.cmd == Cmd_Code_04) {
                
                [self.recordFrames appendData:[b.bodyData subdataWithRange:NSMakeRange(4, [b.bodyData length]-4)]];
                
                if(j!=frameCount) {
                    j++;
                    NSData *frame = [NSData dataWithBytes:&j length:2];
                    [[GGBluetooth sharedManager] writeValueData:[[ZNSBluetoothInstruction alloc] initWithMoreData:Cmd_Code_04 function:Func_Code_01 body:nil rfid:frame].data];
                    
                    
                }else{
                    j = 0;
                    frameCount = 0;
                    NSData * test = [NSData dataWithData:self.recordFrames];
                    //                        NSLog(@"%@",test);//test
                    NSMutableArray *logs = [NSMutableArray array];
                    NSData *bit0;
                    for (int i=0; i<([test length]/17); i++) {
                        
                        NSMutableData *tmp=[NSMutableData data];
                        bit0 = [test subdataWithRange:NSMakeRange(1+i*17, 1)];
                        [tmp appendData:bit0];
                        bit0 = [test subdataWithRange:NSMakeRange(0+i*17, 1)];
                        [tmp appendData:bit0];
                        
                        if (![[tmp hexString] isEqualToString:@"FFFF"]) {
                            [logs addObject:@{
                                              
                                              @"lid": [NSString stringWithFormat:@"%ld",strtoul([[tmp hexString] UTF8String], 0, 16)],
                                              @"unlock_at": [NSString timeStringFormHexString:[[test subdataWithRange:NSMakeRange(9+i*17, 7)] hexString]],
                                              @"result": [NSString stringWithFormat:@"%ld",strtoul([[[test subdataWithRange:NSMakeRange(16+i*17, 1)] hexString] UTF8String], 0, 16)],
                                              }];
                        }
                        
                    }
                    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{@"tid":task.tid,
                                                                                               @"action":@"TransTask",
                                                                                               @"uid":[ZNSUser currentUser].idid}];
                    [dic setObject:logs forKey:@"taskHistory"];
                    [APIClient POST:API_TASK_HISTORY withParameters:dic successWithBlcok:^(id response) {
                        @strongify(self)
                        
                        [self.recordFrames resetBytesInRange:NSMakeRange(0, [self.recordFrames length])];
                        [self.recordFrames setLength:0];
                        //请求接口，改变状态为“已下载”
                        [self updateStatusWith:@"已执行"];
                        [SVProgressHUD showSuccessWithStatus:@"回传成功"];
                        [self deletedTask:task];
                        [self.mainTableView.mj_header beginRefreshing];
      
                    } errorWithBlock:^(ZNSError *error) {
                        @strongify(self)
                        [self.recordFrames resetBytesInRange:NSMakeRange(0, [self.recordFrames length])];
                        [self.recordFrames setLength:0];
                        [SVProgressHUD showErrorWithStatus:error.errorMessage];
                        [[GGBluetooth sharedManager] disconnectCurrentDevice];
                    }];
                    
                }
                
            }
            if (b.cmd == Cmd_Code_06 && b.function == Func_Code_00) {
                
                NSLog(@"任务删除成功");
                [[GGBluetooth sharedManager] disconnectCurrentDevice];
            }
        }];
        [[GGBluetooth sharedManager] setSendFailureBlock:^{
            [SVProgressHUD showErrorWithStatus:@"钥匙无回应，请重试"];
            [[GGBluetooth sharedManager] disconnectCurrentDevice];
        }];
        [[GGBluetooth sharedManager] setCanWriteValueBlock:^{//任务查询
            [[GGBluetooth sharedManager] writeValueData:[[ZNSBluetoothInstruction alloc] initWithMoreData:Cmd_Code_05 function:Func_Code_00 body:nil rfid:nil].data];

        }];
        
    }];
    dialog = [[JKAlertDialog alloc]initWithTitle:@"选择智能钥匙" message:@""];
    dialog.contentView = btVC.view;
    
    [dialog addButton:Button_CANCEL withTitle:@"取消" handler:^(JKAlertDialogItem *item) {
        
    }];
    
    [btVC setConnectedSmartKeyBlock:^(ZNSSmartKey *smartKey){
        [dialog dismiss];
        
    }];
    
    [dialog show];
    [btVC setupBT];
    [btVC loadSmartKeys];
    
}

@end
