//
//  TempermissionAuthorizeViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/5/11.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "TempermissionAuthorizeViewController.h"
#import "ObjectChooseViewController.h"
#import "ExecutorChooseViewController.h"

@interface TempermissionAuthorizeViewController(){
    
    __weak IBOutlet UIButton *choseExecutorButton;
    
    __weak IBOutlet UIButton *choseBeginTimeButton;
    __weak IBOutlet UIButton *choseEndTimeButton;
    __weak IBOutlet UIButton *choseDeviceButton;
    __weak IBOutlet UITextField *nameTextField;
    __weak IBOutlet UILabel *nameLabel;
    __weak IBOutlet UILabel *numberLabel;
    __weak IBOutlet UILabel *sectionLabel;
    __weak IBOutlet UILabel *missionNameLabel;
}
- (IBAction)tapButton:(id)sender;
@property (nonatomic,strong) NSDate *beginTime;
@property (nonatomic,strong) NSDate *endTime;
@property (strong, nonatomic) NSArray * arrObject;
@property (strong, nonatomic) NSArray * arrName;
@property (nonatomic,strong) ZNSUser * selectedUserInfo;
@property (nonatomic,strong) NSString *missionName;

@end

@implementation TempermissionAuthorizeViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initializeUI];
    
    
    UIAlertController * ac = [UIAlertController alertControllerWithTitle:@"临时任务授权" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [ac addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    UIAlertAction *action  = [UIAlertAction actionWithTitle:@"申请" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UITextField *tf = ac.textFields.firstObject;
        missionNameLabel.text = [NSString stringWithFormat:@"任务名称：%@",tf.text];
        self.missionName = tf.text;
        ZNSUser * user = [ZNSUser currentUser];
        nameLabel.text = [NSString stringWithFormat:@"准许人：%@",user.name];
        numberLabel.text = [NSString stringWithFormat:@"准许人电话：%@",user.username];
        sectionLabel.text = [NSString stringWithFormat:@"准许人部门：%@",user.section];
    }];
    action.enabled = NO;
    [ac addTextFieldWithConfigurationHandler:^(UITextField *textField){
        textField.placeholder = @"请输入任务名";
        [[NSNotificationCenter defaultCenter]addObserverForName:UITextFieldTextDidChangeNotification object:textField queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
            action.enabled  = textField.text.length>0;
            if (textField.text.length>32) {
                [SVProgressHUD showErrorWithStatus:@"任务名称不能超过32位"];
                action .enabled = NO;
            }
            if ([[NSString isEmpty:textField.text] isEqualToString:@"yes"]) {
                action .enabled = NO;
            };
        }];
        
    }];
    [ac addAction:action];
    [self presentViewController:ac animated:YES completion:nil];
}
- (void)initializeUI{
    [choseExecutorButton bs_configureAsSuccessStyle];
    [choseBeginTimeButton bs_configureAsInfoStyle];
    [choseEndTimeButton bs_configureAsDangerStyle];
    [choseDeviceButton bs_configureAsPrimaryStyle];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"提交" style:UIBarButtonItemStylePlain target:self action:@selector(commit)];
}

- (void)commit{

    NSLog(@"%@",self.missionName);
    if (self.selectedUserInfo == nil) {
        [SVProgressHUD showInfoWithStatus:@"请选择执行人"];
        return;
    }
    if (self.beginTime == nil) {
        [SVProgressHUD showInfoWithStatus:@"请选择开始时间"];
        return;
    }
    if (self.endTime == nil) {
        [SVProgressHUD showInfoWithStatus:@"请选择结束时间"];
        return;
    }
  
    if (self.arrObject.count  == 0) {
        [SVProgressHUD showInfoWithStatus:@"请选择设备"];
        return;
    }
    
    NSTimeInterval time = [self.endTime timeIntervalSinceDate:self.beginTime];
    if (time < 0) {
        [SVProgressHUD showInfoWithStatus:@"申请结束时间不能小于开始时间"];
        return;
    }
    [APIClient POST:[NSString stringWithFormat:@"user/%@/temp_task/new",self.selectedUserInfo.username] withParameters:@{@"name":self.missionName,@"begin_at":[self.beginTime  stringWithFormat:@"yyyyMMddHHmmss"],@"end_at":[self.endTime stringWithFormat:@"yyyyMMddHHmmss"],@"object":[self.arrObject yy_modelToJSONObject]}successWithBlcok:^(id response) {
            
            [SVProgressHUD showInfoWithStatus:@"上传成功"];
            [self.navigationController popViewControllerAnimated:YES];
        } errorWithBlock:^(ZNSError *error) {
             [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)tapButton:(UIButton *)sender {
    
   if (sender.tag == 0) {
       ExecutorChooseViewController *er = [ExecutorChooseViewController create];
       @weakify(self)
       [er setUserBlock:^(ZNSUser * user){
           @strongify(self)
           self.selectedUserInfo = user;
           [choseExecutorButton setTitle:[NSString stringWithFormat:@"执行人： %@",user.name] forState:UIControlStateNormal];
           
       }];
       [self.navigationController pushViewController:er animated:YES];

       
   }else if (sender.tag == 1){
       @weakify(self)
       NSDate *defualDate = self.beginTime?self.beginTime:[NSDate new];
       NSDate *maxDate = self.endTime?self.endTime:nil;
       [ActionSheetDatePicker showPickerWithTitle:@"选择开始时间" datePickerMode:UIDatePickerModeDateAndTime selectedDate:defualDate minimumDate:[NSDate new] maximumDate: maxDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
            @strongify(self);
           self.beginTime = selectedDate;
           NSDateFormatter * df = [[NSDateFormatter alloc]init];
           df.dateFormat = @"yyyy-MM-dd HH:mm";
           [choseBeginTimeButton setTitle:[NSString stringWithFormat:@"开始时间:%@",[df stringFromDate:selectedDate]] forState:UIControlStateNormal];
           
       } cancelBlock:^(ActionSheetDatePicker *picker) {
           
       } origin:self.view];
   }else if (sender.tag == 2){
        @weakify(self);
        [ActionSheetDatePicker showPickerWithTitle:@"选择结束时间" datePickerMode:UIDatePickerModeDateAndTime selectedDate:self.endTime?self.endTime:[NSDate new] minimumDate:self.beginTime?self.beginTime:[NSDate new] maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
            @strongify(self);
            self.endTime = selectedDate;
            NSDateFormatter * df = [[NSDateFormatter alloc]init];
            df.dateFormat = @"yyyy-MM-dd HH:mm";
            [choseEndTimeButton setTitle:[NSString stringWithFormat:@"结束时间:%@",[df stringFromDate:selectedDate]] forState:UIControlStateNormal];
        } cancelBlock:^(ActionSheetDatePicker *picker) {
            
        } origin:self.view];
   }else{
    
        ObjectChooseViewController * vc = [ObjectChooseViewController create];
        vc.selectedData = [NSMutableArray arrayWithArray:self.arrObject];
        @weakify(self);
        vc.objsBlock = ^(NSArray *arrObjs) {
            @strongify(self);
            //            self.arrObject = [arrObjs valueForKeyPath:@"oid"];
            self.arrObject = arrObjs;
        };
       vc.nameBlock = ^(NSArray *nameArr){
           
           if (nameArr.count == 0) {
               [choseDeviceButton setTitle:@"选择设备" forState:UIControlStateNormal];
           }else{
               @strongify(self)
               self.arrName = nameArr;
               __block NSString   *str= @"";
               [nameArr enumerateObjectsUsingBlock:^(NSString*obj, NSUInteger idx, BOOL * _Nonnull stop) {
                   str = [str stringByAppendingString:[NSString stringWithFormat:@"%@   ",obj]];
               }];
               [choseDeviceButton setTitle:str forState:UIControlStateNormal];
           }
           
       };
        [self.navigationController pushViewController:vc animated:YES];
  
    }
    
}
@end
