//
//  ExecutorChooseViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/5/11.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "ExecutorChooseViewController.h"
#import "APIClient.h"

@interface ExecutorChooseViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchControllerDelegate,UISearchResultsUpdating>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSArray *datas;
@property (nonatomic,strong) ZNSUser *selectedUser;
@property (nonatomic,strong) NSMutableArray *filterData;//sousuo
@property (nonatomic,strong) UISearchController *searchController;

@end

@implementation ExecutorChooseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"选择执行人";
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
//    [APIClient POST:API_USER withParameters:@{@"status":@"启用",@"page":@-1} successWithBlcok:^(id response) {
//        self.datas = [NSArray yy_modelArrayWithClass:[ZNSUser class] json:[response valueForKey:@"items"]];
//        [self.tableView reloadData];
//    } errorWithBlock:^(ZNSError *error) {
//        [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
//        
//    }];
    self.datas = [ZNSCache getUsers:[ZNSUser currentUser].username];
    [self.tableView reloadData];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStylePlain target:self action:@selector(save)];
    
    
    self.searchController = [[UISearchController alloc]initWithSearchResultsController:nil];
    
    self.searchController.searchBar.frame = CGRectMake(0, 0, self.view.frame.size.width, 44);
    self.searchController.dimsBackgroundDuringPresentation = false;
    
    //搜索栏表头视图
    self.tableView.tableHeaderView = self.searchController.searchBar;
    [self.searchController.searchBar sizeToFit];
    //背景颜色
    self.searchController.searchBar.backgroundColor = [UIColor orangeColor];
    self.searchController.searchResultsUpdater = self;
}
- (void)save{
    if (self.userBlock) {
        if (self.selectedUser) {
             self.userBlock(self.selectedUser);
        }else{
            [SVProgressHUD  showErrorWithStatus:@"请选择执行人"];
            return;
        }
        
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return self.datas.count;
    return (!self.searchController.active) ? self.datas.count : self.filterData.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    ZNSUser * user;
    if (self.searchController.active) {
        user = [self.filterData objectAtIndex:indexPath.row];
    }else{
        user = [self.datas objectAtIndex:indexPath.row];
    }
//    ZNSUser * user = [self.datas objectAtIndex:indexPath.row];
    if ([user.username isEqualToString:self.selectedUser.username]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
       cell.accessoryType = UITableViewCellAccessoryNone;
    }
    [cell.textLabel  setText:[NSString stringWithFormat:@" %@  %@   %@",user.name,user.username,user.section] ];
    
    [cell.textLabel setLineBreakMode: NSLineBreakByCharWrapping];
    cell.textLabel.numberOfLines = 0;
    [cell.textLabel sizeToFit];
//    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.searchController.active) {
        self.selectedUser = [self.filterData objectAtIndex:indexPath.row];
    }else{
        self.selectedUser = [self.datas objectAtIndex:indexPath.row];
    }
//     [tableView reloadRowAtIndexPath:indexPath withRowAnimation:UITableViewRowAnimationNone];
     [self.tableView reloadData];
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
     self.selectedUser = nil;
    [self.tableView reloadData];
//     [tableView reloadRowAtIndexPath:indexPath withRowAnimation:UITableViewRowAnimationNone];
    
}
#pragma mark 协议中的方法

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    //NSPredicate 谓词
    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"name contains [c] %@",searchController.searchBar.text];
    
    self.filterData = [[self.datas filteredArrayUsingPredicate:searchPredicate]mutableCopy];
    //刷新表格
    [self.tableView reloadData];
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
