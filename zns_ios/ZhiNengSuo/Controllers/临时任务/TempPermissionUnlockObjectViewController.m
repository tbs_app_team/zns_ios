//
//  TempPermissionUnlockObjectViewController.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/13/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "TempPermissionUnlockObjectViewController.h"
#import "OnlineAuthenticationOpenBtLockViewController.h"
#import "OnlineAuthenticationSmartKeyViewController.h"
#import "ZNSExecuteUnlockHelper.h"

@interface TempPermissionUnlockObjectViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@end

@implementation TempPermissionUnlockObjectViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self configUI];
    
    if(self.mode == AuthenticationControllerModeWorkPatrol){
        
    }else{
        if (!self.tempTask.objects || !self.tempTask.objects.count) {
            [self.tableView.mj_header beginRefreshing];
        }
    }

}
#pragma mark -- Private

- (void)configUI{
    self.title = @"选择设备";
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:[UITableViewCell reuseIdentifier]];
    self.tableView.delegate =self;
    self.tableView.dataSource = self;
    
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        if(self.mode == AuthenticationControllerModeWorkPatrol){
            
            [self.tableView.mj_header endRefreshing];
        }else{
            @weakify(self);
            [self.tempTask getObjectsSuccess:^(id response) {
                @strongify(self);
                self.tempTask = response;
                [self.tableView reloadData];
                [self.tableView.mj_header endRefreshing];
                if (!self.tempTask.objects.count) {
                    [SVProgressHUD showInfoWithStatus:@"无设备"];
                }
            } error:^(ZNSError *error) {
                @strongify(self);
                [SVProgressHUD showErrorWithStatus:error.errorMessage];
                [self.tableView.mj_header endRefreshing];
            }];
        }
       
    }];
}



#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.mode == AuthenticationControllerModeWorkPatrol){
        return self.objects.count;
    }
    return self.tempTask.objects.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[UITableViewCell reuseIdentifier]];
    if (self.mode == AuthenticationControllerModeWorkPatrol) {
        ZNSObject *object = self.objects[indexPath.row];
        [cell.textLabel setText:object.name];
    }else{
        ZNSObject *object = [self.tempTask.objects objectAtIndex:indexPath.row];
        [cell.textLabel setText:object.name];
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [UIAlertController showActionSheetInViewController:self.navigationController withTitle:@"选择开锁方式" message:nil cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@[@"蓝牙开锁",@"智能钥匙开锁"] popoverPresentationControllerBlock:^(UIPopoverPresentationController * _Nonnull popover) {
//        
//    } tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
//        if (buttonIndex == 2) {
    if(self.mode == AuthenticationControllerModeWorkPatrol){
        OnlineAuthenticationOpenBtLockViewController *vc =[OnlineAuthenticationOpenBtLockViewController create];
        vc.mode = AuthenticationControllerModeWorkPatrol; 
        vc.optionObject = [self.objects objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        
        OnlineAuthenticationOpenBtLockViewController *vc = [OnlineAuthenticationOpenBtLockViewController create];
        
        NSArray *unlocks = [[NSUserDefaults standardUserDefaults] objectForKey:FIRST_UNLOCK];
        if (unlocks && [unlocks isKindOfClass:[NSArray class]] && unlocks.count) {
            NSInteger btLockIndex = [unlocks indexOfObject:@"蓝牙"];
            NSInteger keyIndex = [unlocks indexOfObject:@"智能锁"];
            if (btLockIndex > keyIndex) {
                vc = [OnlineAuthenticationSmartKeyViewController create];
                vc.title = @"智能钥匙开锁";
            }
        }
        
        vc.mode = AuthenticationControllerModeTemptask;
        vc.optionObject = [self.tempTask.objects objectAtIndex:indexPath.row];
        vc.tempTask = self.tempTask;
        [self.navigationController pushViewController:vc animated:YES];
    }
//        }else if (buttonIndex == 3){
//            OnlineAuthenticationSmartKeyViewController *vc =[OnlineAuthenticationSmartKeyViewController create];
//            vc.mode = AuthenticationControllerModeTemptask;
//            vc.optionObject = [self.tempTask.objects objectAtIndex:indexPath.row];
//            [self.navigationController pushViewController:vc animated:YES];
//        }
//    }];
}
@end
