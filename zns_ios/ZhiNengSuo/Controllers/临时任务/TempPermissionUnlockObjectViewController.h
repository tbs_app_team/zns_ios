//
//  TempPermissionUnlockObjectViewController.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/13/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSTempTask.h"
@interface TempPermissionUnlockObjectViewController : UIViewController
@property (nonatomic,strong) ZNSTempTask *tempTask;
@property (nonatomic,assign) AuthenticationControllerMode mode;
@property (nonatomic,strong) NSArray *objects; 
@end
