//
//  ObjectChooseViewController.m
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/12/7.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import "ObjectChooseViewController.h"
#import "ZNSObject.h"
#import "MJRefresh.h"
#import "ObjectChoseTableViewCell.h"

@interface ObjectChooseViewController ()<UITableViewDataSource,UITableViewDelegate,UISearchResultsUpdating>{
    BOOL isSelected;
    
     UISearchController *searchCtrl;
    
    NSDictionary *results;
}

@property (strong, nonatomic) IBOutlet UITableView *mainTableView;
@property (strong, nonatomic) NSArray * arrData;
@property (nonatomic,strong) NSDictionary *sortedData;
@property (nonatomic,strong) NSDictionary *selectDic;
@property (assign, nonatomic) NSInteger page;

@property (nonatomic,assign) NSInteger expandedSection;

@end


@implementation ObjectChooseViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initializeUI];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (searchCtrl.active) {
        [searchCtrl setActive:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)initializeUI {
    self.title = @"设备选择";
//    self.mainTableView.editing = YES;
    isSelected = NO;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"确定" style:UIBarButtonItemStylePlain target:self action:@selector(commit)];
    [self.mainTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    self.mainTableView.rowHeight = 45;
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 0;
        [self loadDataFromInternet];
    }];
    self.expandedSection = 99999;
    if (!self.selectedData) {
        self.selectedData = [NSMutableArray array];
    }
    if (!self.selectedName) {
        self.selectedName = [NSMutableArray array];
    }
    
    if (!self.selectedObj) {
        self.selectedObj = [NSMutableArray array];
    }
    
//    self.mainTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//        self.page ++;
//        [self loadDataFromInternet];
//    }];
    [self.mainTableView.mj_header beginRefreshing];
    
    [self setSearchControllerView];
    
}

- (void)setSearchControllerView{
    searchCtrl = [[UISearchController alloc]initWithSearchResultsController:nil];
    searchCtrl.searchBar.frame = CGRectMake(0, 0, 0, 44);
    searchCtrl.dimsBackgroundDuringPresentation = false;
    searchCtrl.hidesNavigationBarDuringPresentation = NO;
    //搜索栏表头视图
    _mainTableView.tableHeaderView = searchCtrl.searchBar;
    [searchCtrl.searchBar sizeToFit];
    //背景颜色
    //    searchCtrl.searchBar.backgroundColor = [UIColor orangeColor];
    searchCtrl.searchBar.searchBarStyle = UISearchBarStyleProminent;
    searchCtrl.searchResultsUpdater = self;
    
}

- (void)loadDataFromInternet {
    @weakify(self);
    [ZNSObject getAllDevicesWithPage:[NSNumber numberWithInteger:-1] search:@"" isAgt:self.isAgt successWithBlock:^(id response) {
        @strongify(self);
//        if (self.page == 0) {
            self.arrData = [NSArray yy_modelArrayWithClass:[ZNSObject class] json:response[@"items"]];
            [self sordData:self.arrData];
            if (self.arrData.count == 0) {
                [self.navigationController popViewControllerAnimated:YES];
                [SVProgressHUD showInfoWithStatus:@"单位暂时没有设备可供选择"];
            }
            [self.mainTableView.mj_header endRefreshing];
//        } else {
//            self.arrData = [self.arrData arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSObject class] json:response[@"items"]]];
//
//            [self.mainTableView.mj_footer endRefreshing];
//        }
//        if ([response[@"page"]intValue] == [response[@"pages"]intValue]) {
//            [self.mainTableView.mj_footer endRefreshingWithNoMoreData];
//        } else {
//            [self.mainTableView.mj_footer resetNoMoreData];
//        }
        [self.mainTableView reloadData];
    } errorWithBlock:^(ZNSError *error) {
        [self.mainTableView.mj_header endRefreshing];
        [self.mainTableView.mj_footer endRefreshing];
        [SVProgressHUD showInfoWithStatus:error.errorMessage];
    }];
}

- (void)sordData:(NSArray*)data{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    NSMutableDictionary *selDic = [NSMutableDictionary dictionary];
    [data enumerateObjectsUsingBlock:^(ZNSObject *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        //        NSArray *arr = [dic valueForKey:obj.area];
        NSInteger tId = self.isAgt?[obj.agent_sid integerValue]:obj.sid;
        NSArray *arr = [dic valueForKey:@(tId).stringValue];
        if (!arr) {
            arr = @[];
        }
        arr = [arr arrayByAddingObject:obj];
        [dic setValue:arr forKey:@(tId).stringValue];
        [selDic setValue:[NSNumber numberWithBool:NO] forKey:@(tId).stringValue];
    }];
    self.sortedData = dic;
    
    self.selectDic = [self setBoolValue:selDic];
}

//- (void)sordData:(NSArray*)data{
//    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//    NSMutableDictionary *selDic = [NSMutableDictionary dictionary];
//    [data enumerateObjectsUsingBlock:^(ZNSObject *obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        NSArray *arr = [dic valueForKey:obj.area];
//        if (!arr) {
//            arr = @[];
//        }
//        arr = [arr arrayByAddingObject:obj];
//        [dic setObject:arr forKey:obj.area];
//        [selDic setValue:[NSNumber numberWithBool:NO] forKey:obj.area];
//    }];
//    self.sortedData = dic;
//   
//    self.selectDic = [self setBoolValue:selDic];
//}
- (NSDictionary *)setBoolValue:(NSDictionary *)dic{
    NSDictionary *dict = dic;
    NSDictionary *md = searchCtrl.active?results:self.sortedData;
    [md.allKeys enumerateObjectsUsingBlock:^(NSString* key, NSUInteger idx, BOOL * _Nonnull stop) {
        NSArray *arr = [md valueForKey:key];
        NSArray *arrName = [arr valueForKeyPath:@"oid"];
        if ([self containArray:self.selectedData compArr:arrName]) {
            [dict setValue:[NSNumber numberWithBool:YES] forKey:key];
        }else{
            [dict setValue:[NSNumber numberWithBool:NO] forKey:key];
        }
    }];
    return dict;
}

- (BOOL)containArray:(NSArray *)origArr compArr:(NSArray *)compArr{
   __block BOOL isCon = YES;
    [compArr enumerateObjectsUsingBlock:^(id _Nonnull str, NSUInteger idx, BOOL * _Nonnull stop) {
        if (![origArr containsObject:str]) {
            isCon = NO;
        };
    }];
    return isCon;
   
}

#pragma mark - Search Controller Delegate

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    NSLog(@"%@",searchController.searchBar.text);
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    NSMutableDictionary *selDic = [NSMutableDictionary dictionary];
    NSString *key = searchController.searchBar.text;
    
    for (NSString *str in self.sortedData) {
        NSArray *objs = self.sortedData[str];
        NSMutableArray *rlts = [NSMutableArray array];
        for (ZNSObject *obj in objs) {
            NSRange nameRange = [obj.name rangeOfString:key options:NSCaseInsensitiveSearch];
            if (nameRange.length) {
                NSLog(@"==>%@",str);
                //                NSArray *arr = self.sortedData[str];
                [rlts addObject:obj];
                [dic setObject:rlts forKey:str];
                [selDic setValue:[NSNumber numberWithBool:NO] forKey:str];
                //                break;
            }
        }
    }
    
    results = dic;
    self.selectDic = [self setBoolValue:selDic];
    //NSPredicate 谓词
    //    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS %@",searchController.searchBar.text]; ;
    //    results = [[items filteredArrayUsingPredicate:searchPredicate] mutableCopy];
    //刷新表格
    [self.mainTableView reloadData];
}

#pragma mark - Action

- (void)commit {
    if (self.objsBlock) {
        self.objsBlock(self.selectedData);
        if (self.nameBlock) {
            self.nameBlock(self.selectedName);
            [self.navigationController popViewControllerAnimated:YES];
        } 
    }
    
    if (self.selectedBlock) {
        self.selectedBlock(self.selectedObj);
         [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSInteger count = searchCtrl.active?[results allKeys].count:[[self.sortedData allKeys]count];
    return  count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.expandedSection == section) {
        NSDictionary *dic = searchCtrl.active?results:self.sortedData;
        NSArray *arr = [dic valueForKey:[[dic allKeys] objectAtIndex:section]];
        return arr.count+1;
    }else{
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (indexPath.row==0) {
//      return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
//    }
//    return  UITableViewCellEditingStyleNone;
//}
//
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.row==0) {
//        return YES;
//    }
//    return NO;
//}
//-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSArray *arr = [self.sortedData valueForKey:[[self.sortedData allKeys]objectAtIndex:indexPath.section]];
//    [arr enumerateObjectsUsingBlock:^(ZNSObject *object, NSUInteger idx, BOOL * _Nonnull stop) {
//        
//        if ([self.selectedData containsObject:object.oid]) {
//            [self.selectedData removeObject:object.oid];
//        }else{
//            [self.selectedData addObject:object.oid];
//        }
//        if ([self.selectedName containsObject:object.name]) {
//            [self.selectedName removeObject:object.name];
//        }else{
//            [self.selectedName addObject:object.name];
//        }
//    }];
//    [self.mainTableView reloadData];
//    
//}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSArray *arr = [self.sortedData valueForKey:[[self.sortedData allKeys]objectAtIndex:indexPath.section]];
    NSDictionary *dic = searchCtrl.active?results:self.sortedData;
    NSArray *arr = [dic valueForKey:[[dic allKeys] objectAtIndex:indexPath.section]];
    if (indexPath.row==0) {
//        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
//        cell.textLabel.text = [[self.sortedData allKeys]objectAtIndex:indexPath.section];
//        cell.accessoryType = UITableViewCellAccessoryNone;
//        cell.backgroundColor = [UIColor whiteColor];
//        return cell;
        ObjectChoseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ObjectChoseTableViewCell"];
        if (cell==nil) {
            cell = [[NSBundle mainBundle] loadNibNamed:@"ObjectChoseTableViewCell" owner:self options:nil].firstObject;
        }
        
//        NSString *sid = [[self.sortedData allKeys] objectAtIndex:indexPath.section];
//        NSArray *arr = self.sortedData[sid];
        ZNSObject *obj = arr.firstObject;
        
        cell.titleLab.text = self.isAgt?obj.agent_section:obj.section;//[[dic allKeys]objectAtIndex:indexPath.section];
        
        self.selectDic = [self setBoolValue:self.selectDic]; 
        isSelected = [[self.selectDic valueForKey:[self.selectDic.allKeys objectAtIndex:indexPath.section]] boolValue];
        
        if (!isSelected){
            [cell.selectedBtn setTitle:@"全选" forState:UIControlStateNormal];
           
        }else{
            [cell.selectedBtn setTitle:@"取消全选" forState:UIControlStateNormal];
            
        }
        
        cell.selectedBtn.hidden = self.isOnlyOneChoice;
        
        //设置选中的title显示为红色
        BOOL isCheck = NO;
        for (NSString *str in _selectedData) {
            for (ZNSObject *obj in arr) {
                if ([obj.oid isEqualToString:str]) {
                    isCheck = YES;
                    break;
                }
            }
        }
        if (isCheck) {
            cell.titleLab.textColor = [UIColor redColor];
        }else{
            cell.titleLab.textColor = [UIColor blackColor];
        }
        

        @weakify(cell)
        cell.seleBlock = ^(void){
            @strongify(cell)
            if ([cell.selectedBtn.titleLabel.text isEqualToString:@"全选"]) {
//                isSelected = YES;
//                [self.selectDic setValue:[NSNumber numberWithBool:YES] forKey:[self.selectDic.allKeys objectAtIndex:indexPath.section]];
//                NSArray *arr = [dic valueForKey:[[dic allKeys] objectAtIndex:indexPath.section]];
                [arr enumerateObjectsUsingBlock:^(ZNSObject *object, NSUInteger idx, BOOL * _Nonnull stop) {
                
                        if (![self.selectedData containsObject:object.oid]) {
                            [self.selectedData addObject:object.oid];
                        }
                        if (![self.selectedName containsObject:object.name]) {
                            [self.selectedName addObject:object.name];
                        }
                }];
                [self.mainTableView reloadData];

            }else  {
//                [self.selectDic setValue:[NSNumber numberWithBool:NO] forKey:[self.selectDic.allKeys objectAtIndex:indexPath.section]];
//                NSArray *arr = [dic valueForKey:[[dic allKeys]objectAtIndex:indexPath.section]];
                [arr enumerateObjectsUsingBlock:^(ZNSObject *object, NSUInteger idx, BOOL * _Nonnull stop) {
                    
                    if ([self.selectedData containsObject:object.oid]) {
                        [self.selectedData removeObject:object.oid];
                    }
                    if ([self.selectedName containsObject:object.name]) {
                        [self.selectedName removeObject:object.name];
                    }
                }];
                [self.mainTableView reloadData];

            }
        };
        return cell;
    }else{
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        ZNSObject *object = [arr objectAtIndex:indexPath.row-1];
        if ([self.selectedData containsObject:object.oid]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.textLabel.text = [@"  " stringByAppendingString:object.name];
        cell.backgroundColor = UIColorHex(0xe6f9ff);
        return cell;
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if (self.expandedSection == indexPath.section) {
            self.expandedSection = 99999;
        }else{
            self.expandedSection = indexPath.section;
        }
        [tableView reloadData];
    }else{
//        NSArray *arr = [self.sortedData valueForKey:[[self.sortedData allKeys]objectAtIndex:indexPath.section]];
        NSDictionary *dic = searchCtrl.active?results:self.sortedData;
        NSArray *arr = [dic valueForKey:[[dic allKeys] objectAtIndex:indexPath.section]];
        ZNSObject *object = [arr objectAtIndex:indexPath.row-1];
         
        if (self.isOnlyOneChoice) {
            [self.selectedData removeAllObjects];
            [self.selectedData addObject:object.oid];
            [self.selectedName removeAllObjects];
            [self.selectedName addObject:object.name];
            [self.selectedObj removeAllObjects];
            [self.selectedObj addObject:object];
        }else{
            if ([self.selectedData containsObject:object.oid]) {
                [self.selectedData removeObject:object.oid];
            }else{
                [self.selectedData addObject:object.oid];
            }
            if ([self.selectedName containsObject:object.name]) {
                [self.selectedName removeObject:object.name];
            }else{
                [self.selectedName addObject:object.name];
            }
        }
        
//        [tableView reloadRowAtIndexPath:indexPath withRowAnimation:UITableViewRowAnimationNone];
        [tableView reloadData];
    }
}


@end
