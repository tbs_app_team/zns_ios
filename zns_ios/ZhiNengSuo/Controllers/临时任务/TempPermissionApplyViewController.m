//
//  TempPermissionApplyViewController.m
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/28.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import "TempPermissionApplyViewController.h"
#import "CRMediaPickerController.h"
#import "ZNSTempTask.h"
#import "ObjectChooseViewController.h"

@interface TempPermissionApplyViewController ()<CRMediaPickerControllerDelegate> {
    
    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *phoneLabel;
    IBOutlet UILabel *companyLabel;
    IBOutlet UILabel *taskLabel;
    IBOutlet UIButton *chooseButton;
}

@property (nonatomic,weak) IBOutlet UIImageView *selfImageView;
@property (nonatomic,weak) IBOutlet UIImageView *cardImageView;


@property (nonatomic,strong) UIImage *selfImage;
@property (nonatomic,strong) UIImage *cardImage;

@property (strong, nonatomic) NSArray * arrObject;
@property (strong, nonatomic) NSArray * arrObjectNames;

@property (nonatomic,strong) IBOutlet UIButton *beginTimeButton;
@property (nonatomic,strong) IBOutlet UIButton *endTimeButton;
@property (nonatomic,strong) NSDate *beginTime;
@property (nonatomic,strong) NSDate *endTime;

@property (nonatomic,strong) CRMediaPickerController *imagePicker;
@property (nonatomic,assign) NSInteger currentPicking;//1,2

@property (nonatomic,strong) NSArray *selectedObjects;
@property (nonatomic,strong) NSString* objectNameStr;
@end

@implementation TempPermissionApplyViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initializeUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)initializeUI {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"提交" style:UIBarButtonItemStylePlain target:self action:@selector(commit)];
    [self.beginTimeButton bs_configureAsSuccessStyle];
    [self.endTimeButton bs_configureAsWarningStyle];
    [chooseButton bs_configureAsPrimaryStyle];
    
    self.objectNameStr = @"";
    
    @weakify(self);
    [self.cardImageView setTapActionWithBlock:^{
        @strongify(self);
        self.currentPicking = 2;
        [self.imagePicker show];
    }];
    
    [self.selfImageView setTapActionWithBlock:^{
        @strongify(self);
        self.currentPicking = 1;
        [self.imagePicker show];
    }];
    
    UIAlertController * ac = [UIAlertController alertControllerWithTitle:@"申请临时鉴权" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [ac addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    UIAlertAction *action  = [UIAlertAction actionWithTitle:@"申请" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UITextField *tf = ac.textFields.firstObject;
        taskLabel.text = tf.text;
        ZNSUser * user = [ZNSUser currentUser];
        nameLabel.text = user.name;
        phoneLabel.text = user.username;
        companyLabel.text = user.section;
    }];
    action.enabled = NO;
    [ac addTextFieldWithConfigurationHandler:^(UITextField *textField){
        textField.placeholder = @"请输入任务名";
        [[NSNotificationCenter defaultCenter]addObserverForName:UITextFieldTextDidChangeNotification object:textField queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
            action.enabled  = textField.text.length>0;
            
            if ([[NSString isEmpty:textField.text] isEqualToString:@"yes"]) {
                action.enabled = NO;
            };
            if (textField.text.length>32) {
                [SVProgressHUD showErrorWithStatus:@"任务名称不能超过32位"];
                action .enabled = NO;
            }
        }];
        
    }];
    [ac addAction:action];
    [self presentViewController:ac animated:YES completion:nil];
}

- (CRMediaPickerController *)imagePicker{
    if (!_imagePicker) {
        _imagePicker = [[CRMediaPickerController alloc]init];
        _imagePicker.delegate = self;
        _imagePicker.mediaType = CRMediaPickerControllerMediaTypeImage;
//        _imagePicker.sourceType = (CRMediaPickerControllerSourceTypePhotoLibrary|CRMediaPickerControllerSourceTypeCamera);
        _imagePicker.sourceType = CRMediaPickerControllerSourceTypeCamera;
        
    }
    return _imagePicker;
}

- (void)uploadImageForUid:(NSString *)uid{
    @weakify(self);
    [ZNSAPITool uploadImage:self.selfImage imageType:ZNSImageTypePortrait type:ZNSUploadTypeTempTaskApply forId:uid progressWithBlcok:^(CGFloat percentage) {
        [SVProgressHUD showProgress:percentage status:@"正在上传自拍头像"];
    } successWithBlcok:^(id response) {
        @strongify(self);
        
        [ZNSAPITool uploadImage:self.cardImage imageType:ZNSImageTypeIdCard type:ZNSUploadTypeTempTaskApply forId:uid progressWithBlcok:^(CGFloat percentage) {
            [SVProgressHUD showProgress:percentage status:@"正在上传身份证图片"];
        } successWithBlcok:^(id response) {
            @strongify(self);
            [SVProgressHUD showSuccessWithStatus:@"申请成功"];
            [self.navigationController popToRootViewControllerAnimated:YES];
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:@"上传身份证图片失败"];
        }];
        
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:@"上传自拍头像失败"];
    }];
}

#pragma mark - Action

- (IBAction)didTapButton:(UIButton *)sender {
    if (sender.tag == 0) {
        @weakify(self);
        [ActionSheetDatePicker showPickerWithTitle:@"选择开始时间" datePickerMode:UIDatePickerModeDateAndTime selectedDate:[NSDate new] minimumDate:[NSDate new] maximumDate:nil  doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
            @strongify(self);
            self.beginTime = selectedDate;
            self.endTime = [self.endTime laterDate:selectedDate];
            NSDateFormatter * df = [[NSDateFormatter alloc]init];
            df.dateFormat = @"yyyy-MM-dd HH:mm";
            [self.beginTimeButton setTitle:[NSString stringWithFormat:@"开始时间:%@",[df stringFromDate:selectedDate]] forState:UIControlStateNormal];
            if ([selectedDate isEqualToDate:self.endTime]) {
                [self.endTimeButton setTitle:[NSString stringWithFormat:@"结束时间:%@",[df stringFromDate:self.endTime]] forState:UIControlStateNormal];
            }
        } cancelBlock:^(ActionSheetDatePicker *picker) {
            
        } origin:self.view];
    } else if (sender.tag == 1) {
        @weakify(self);
        [ActionSheetDatePicker showPickerWithTitle:@"选择结束时间" datePickerMode:UIDatePickerModeDateAndTime selectedDate:self.endTime?self.endTime:[NSDate new] minimumDate:self.beginTime?self.beginTime:[NSDate new] maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
            @strongify(self);
            
            self.endTime = selectedDate;
            NSDateFormatter * df = [[NSDateFormatter alloc]init];
            df.dateFormat = @"yyyy-MM-dd HH:mm";
            [self.endTimeButton setTitle:[NSString stringWithFormat:@"结束时间:%@",[df stringFromDate:selectedDate]] forState:UIControlStateNormal];
        } cancelBlock:^(ActionSheetDatePicker *picker) {
            
        } origin:self.view];
    } else if (sender.tag == 2) {
        ObjectChooseViewController * vc = [ObjectChooseViewController create];
        vc.selectedData = [NSMutableArray arrayWithArray:self.arrObject];
        vc.selectedName = [NSMutableArray arrayWithArray:self.arrObjectNames];
        @weakify(self);
        vc.objsBlock = ^(NSArray *arrObjs) {
            @strongify(self);

            self.arrObject = arrObjs;
            

        };
        vc.nameBlock =^(NSArray *arrNames){
            @strongify(self);
            self.arrObjectNames = arrNames;
//            @weakify(self)
            self.objectNameStr = @"";
            [arrNames enumerateObjectsUsingBlock:^(NSString* obj, NSUInteger idx, BOOL * _Nonnull stop) {
                @strongify(self)
                self.objectNameStr = [self.objectNameStr stringByAppendingString:[NSString stringWithFormat:@"%@   ",obj]];
                
            }];
            if (self.objectNameStr.length) {
                 [chooseButton setTitle:self.objectNameStr forState:UIControlStateNormal];
                NSLog(@"%@",self.objectNameStr);
              
            }else{
                [chooseButton setTitle:@"选择设备"   forState:UIControlStateNormal];
//                NSLog(@"%@",self.objectNameStr);
            }
            
        };
        [self.navigationController pushViewController:vc animated:YES];
    }
}


- (void)commit {
    
    if (!self.beginTime  ) {
        [SVProgressHUD showInfoWithStatus:@"请选择开始时间"];
        return;
    }
    if(!self.endTime){
        [SVProgressHUD showInfoWithStatus:@"请选择结束时间"];
        return;
    }
    if (self.arrObject.count == 0) {
        [SVProgressHUD showInfoWithStatus:@"请选择闭锁对象"];
        return;
    }
    if (self.selfImage == nil) {
        [SVProgressHUD showInfoWithStatus:@"请拍摄头像照片"];
        return;
    }
    if (self.cardImage == nil) {
        [SVProgressHUD showInfoWithStatus:@"请拍摄身份证照片"];
        return;
    }
        
    NSTimeInterval time = [self.endTime timeIntervalSinceDate:self.beginTime];
    if (time < 0) {
        [SVProgressHUD showInfoWithStatus:@"申请结束时间不能小于开始时间"];
    }else{
   
        ZNSTempTask * tt = [[ZNSTempTask alloc]init];
        tt.name = taskLabel.text;
        
        tt.begin_at = [self.beginTime stringWithFormat:@"yyyyMMddHHmmss"];
        tt.end_at = [self.endTime stringWithFormat:@"yyyyMMddHHmmss"];
        [SVProgressHUD show];
        @weakify(self);
        [tt applyTempTaskWithObjects:self.arrObject success:^(id response) {
            @strongify(self);
            [self uploadImageForUid:[response valueForKey:@"id"]];
        } error:^(ZNSError *error) {
            [SVProgressHUD showInfoWithStatus:error.errorMessage];
        }];
    }
}

#pragma mark - CRMediaPickerController
- (void)CRMediaPickerController:(CRMediaPickerController *)mediaPickerController didFinishPickingAsset:(ALAsset *)asset error:(NSError *)error{
    if (!error) {
        if (mediaPickerController.sourceType == CRMediaPickerControllerSourceTypeCamera) {
            ALAssetsLibrary * library = [[ALAssetsLibrary alloc] init];
            [library writeImageToSavedPhotosAlbum:asset.defaultRepresentation.fullResolutionImage orientation:asset.defaultRepresentation.orientation completionBlock:nil];
        }
        if (self.currentPicking == 1) {
            [self.selfImageView setImage:[UIImage imageWithCGImage:asset.thumbnail]];
            self.selfImage = [UIImage imageWithCGImage:asset.defaultRepresentation.fullResolutionImage scale:1 orientation:(UIImageOrientation)asset.defaultRepresentation.orientation];
        } else {
            [self.cardImageView setImage:[UIImage imageWithCGImage:asset.thumbnail]];
             self.cardImage = [UIImage imageWithCGImage:asset.defaultRepresentation.fullResolutionImage scale:1 orientation:(UIImageOrientation)asset.defaultRepresentation.orientation];
        }
    }
}

#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

@end
