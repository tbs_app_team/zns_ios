//
//  ObjectChooseViewController.h
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/12/7.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSTempTask.h"

@interface ObjectChooseViewController : UIViewController

@property (nonatomic,strong) arrayBlock objsBlock;
@property (nonatomic,strong) NSMutableArray *selectedData;
@property (nonatomic,strong) arrayBlock nameBlock;
@property (nonatomic,strong) NSMutableArray *selectedName;
@property (nonatomic,strong) arrayBlock selectedBlock;
@property (nonatomic,strong) NSMutableArray *selectedObj;

@property (nonatomic,assign) BOOL isOnlyOneChoice;
@property (nonatomic,assign) BOOL isAgt;

@end
