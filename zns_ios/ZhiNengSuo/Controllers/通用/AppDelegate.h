//
//  AppDelegate.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/22/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSUser.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,strong) ZNSUser *currentUser;

@end

