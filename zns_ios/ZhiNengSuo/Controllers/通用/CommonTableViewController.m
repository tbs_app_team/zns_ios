//
//  CommonTableViewController.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 16/3/20.
//  Copyright © 2016 czwen. All rights reserved.
//

#import "CommonTableViewController.h"
#import "GGBluetooth.h"
@implementation CommonTableViewController

- (void)setDatas:(NSArray *)datas{
    _datas = datas;
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"UITableViewCell"];
    }
    CBPeripheral *c = [self.datas objectAtIndex:indexPath.row];
    [cell.textLabel setText:c.name];
    [cell.detailTextLabel setText:c.macAddress];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.selectedBlock) {
        self.selectedBlock([self.datas objectAtIndex:indexPath.row]);
    }
}
@end
