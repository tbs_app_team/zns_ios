//
//  BlueToothViewController.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/8/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlueToothViewController : UIViewController
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,copy) voidBlock connectedBlock;
@property (nonatomic,copy) idBlock connectedSmartKeyBlock;
@property (nonatomic,copy) dispatch_block_t timeOutBlock;
@property (nonatomic,assign) BOOL useInBorrowSmartKey;
+ (instancetype)shareController;
+ (void)presentInViewContoller:(UIViewController*)vc;

- (void)setupBT;
- (void)loadSmartKeys;
- (void)setSmartKeyType:(NSString *)smartKeyType;

@end
