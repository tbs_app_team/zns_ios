//
//  GetLocationCommonViewController.h
//  ZhiNengSuo
//
//  Created by ekey on 16/4/29.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKReverseGeocoder.h>
#import "ZNSObject.h"

@interface GetLocationCommonViewController : UIViewController

@property (nonatomic,copy) voidBlock getBlock;
@property (nonatomic,copy) arrayBlock locationBlock;
@property (nonatomic,strong) ZNSObject *myObject;
@property (nonatomic,assign) GetDeveceLocationMode mode;

@end
