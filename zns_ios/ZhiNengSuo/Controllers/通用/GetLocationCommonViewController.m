//
//  GetLocationCommonViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/4/29.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "GetLocationCommonViewController.h"

@interface GetLocationCommonViewController ()<CLLocationManagerDelegate,MKMapViewDelegate,UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *myMapView;
//@property (nonatomic,strong) MKMapView * myMapView;
@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic,strong) NSString *strShow;
@property (nonatomic,strong) NSString *locatedAt;
@property (nonatomic,strong) NSString *lattitude;
@property (nonatomic,strong) NSString *longitude;
@end

@implementation GetLocationCommonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//     self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"退出地图" style:UIBarButtonItemStylePlain target:self  action:@selector(quitMap)];
    
//    self.myMapView = [[MKMapView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.myMapView.delegate = self;
    //在这里先让地图视图隐藏起来，
    //等获取当前经纬度完成后在把整个地图显示出来
    self.myMapView.hidden = true;
//    [self.view addSubview:self.myMapView];
    
    //创建定位管理器
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = 10.0f;
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager startUpdatingLocation];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)quitMap{
    self.myMapView.hidden = YES;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation{
    //获取当前位置的经纬度
    CLLocationCoordinate2D loc = [newLocation coordinate];
    
    self.lattitude = [NSString stringWithFormat:@"%f",loc.latitude];
    self.longitude = [NSString stringWithFormat:@"%f",loc.longitude];

    //    float horizon = newLocation.horizontalAccuracy;
    //    float vertical = newLocation.verticalAccuracy;
    self.strShow = [[NSString alloc] initWithFormat:
                    @"经度＝%@ 维度＝%@ ",
                    self.lattitude, self.longitude];
    NSLog(@"%@",self.strShow);
    
    //让MapView使用定位功能。
    self.myMapView.showsUserLocation =YES;
//    [self.myMapView setCenterCoordinate:_myMapView.userLocation.coordinate animated:YES];
//
//     MKCoordinateRegion theRegion = MKCoordinateRegionMake(_myMapView.userLocation.coordinate, MKCoordinateSpanMake(0.01f, 0.01f));
//    [self.myMapView setRegion:theRegion animated:YES];
    
    //更新地址，
    [manager stopUpdatingLocation];
    
    //设置定位后的自定义图标。
    MKCircle* circle = [MKCircle circleWithCenterCoordinate:CLLocationCoordinate2DMake(self.myMapView.userLocation.location.coordinate.latitude, self.myMapView.userLocation.location.coordinate.longitude) radius:5000];
    
    [self.myMapView addAnnotation:circle];
    
    
    //我们需要通过当前用户的经纬度换成出它现在在地图中的地名
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation: self.locationManager.location completionHandler:
     ^(NSArray *placemarks, NSError *error) {
         
         //得到自己当前最近的地名
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         
         self.locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
         
         //locatedAt就是当前我所在的街道名称
         [self.myMapView.userLocation setTitle:self.locatedAt];
//         MKCoordinateRegion theRegion = MKCoordinateRegionMakeWithDistance(loc, 0, 0);
//         //缩放的精度。数值越小约精准
//         theRegion.span.longitudeDelta = 0.01f;
//         theRegion.span.latitudeDelta = 0.01f;
         
         MKCoordinateRegion region = MKCoordinateRegionMake(loc, MKCoordinateSpanMake(.1, .1));
         MKCoordinateRegion adjustedRegion = [self.myMapView regionThatFits:region];
         //让MapView显示缩放后的地图。
         [self.myMapView setRegion:adjustedRegion animated:YES];
//         [self.myMapView setCenterCoordinate:theRegion.center animated:YES];
         self.myMapView.hidden = NO;
     }];
    
    
}

//定位失败后将进入此方法
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    if ( [error code] == kCLErrorDenied )
    {
        
        //第一次安装含有定位功能的软件时
        //程序将自定提示用户是否让当前App打开定位功能，
        //如果这里选择不打开定位功能，
        //再次调用定位的方法将会失败，并且进到这里。
        //除非用户在设置页面中重新对该软件打开定位服务，
        //否则程序将一直进到这里。
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"定位服务已经关闭"
                                                        message:@"请您在设置页面中打开本软件的定位服务"
                                                       delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [manager stopUpdatingHeading];
    }
    else if ([error code] == kCLErrorHeadingFailure)
    {
        
    }
}

#pragma mark
#pragma mark  - MKMapViewDelegate
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    [[[UIAlertView alloc] initWithTitle:@"您所在的位置"
                                message:[NSString stringWithFormat:@"%@,经度:%@,纬度:%@",self.locatedAt,self.longitude,self.lattitude]
                               delegate:self
                      cancelButtonTitle:@"取消"
                      otherButtonTitles:@"确定定位",nil] show];
    
    
}
- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view{
    
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    //    NSLog(@"didUpdateUserLocation");
}

- (void)mapView:(MKMapView *)mapView didFailToLocateUserWithError:(NSError *)error{
    NSLog(@"didFailToLocateUserWithError");
}

#pragma mark
#pragma mark  - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"%li",(long)buttonIndex);
    if (buttonIndex == 1) {
        self.myObject.lat = self.lattitude;
        self.myObject.lng = self.longitude;
        [self upObjectWithLocation:self.myObject];
    }
}
-(void)upObjectWithLocation:(ZNSObject * )object{

    if (self.mode  == AddLockMode) {
        
        if(self.locationBlock){
            
            NSArray *arr = [NSArray arrayWithObjects:self.lattitude,self.longitude, nil];
            self.locationBlock(arr);
        }
        [self quitMap];
    }else{
        [SVProgressHUD showWithStatus:@"正在上传，请稍等。。。"];
        @weakify(self)
        
        [APIClient POST:[NSString stringWithFormat:@"/object/%@/location",object.oid] withParameters:@{@"lat":object.lat,@"lng":object.lng} successWithBlcok:^(id response) {
            @strongify(self)
            [SVProgressHUD showInfoWithStatus:@"上传成功"];
            
            if(self.locationBlock){
            
                NSArray *arr = [NSArray arrayWithObjects:self.lattitude,self.longitude, nil];
                self.locationBlock(arr);
            }
            [self quitMap];

        } errorWithBlock:^(ZNSError *error) {
            @strongify(self)
            [SVProgressHUD showErrorWithStatus:error.errorMessage];
            [self quitMap];
        }];
        
    }
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
