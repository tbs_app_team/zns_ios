//
//  GetLocationViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/4/28.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "GetLocationViewController.h"
#import "GetLocationTableViewCell.h"
#import "GetLocationCommonViewController.h"
#import "ZNSCache.h"

@interface GetLocationViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchControllerDelegate,UISearchResultsUpdating>

@property (weak, nonatomic) IBOutlet UITableView *myTable;
@property (nonatomic,strong) NSArray *datas;
@property (nonatomic,strong) NSMutableArray *filterData;//sousuo
@property (nonatomic,strong) NSNumber *page;
@property (nonatomic,strong) UISearchController *searchController;
@end

@implementation GetLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [self.myTable registerNib:[UITableViewCell nib] forCellReuseIdentifier:[UITableViewCell reuseIdentifier]];
   self.filterData = [NSMutableArray array];
    
//    @weakify(self)
//    self.myTable.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//            @strongify(self);
//            [self loadDataWithPage:@0];
//        }];
//    self.myTable.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        @strongify(self);
//        [self loadDataWithPage:@(self.page.integerValue+1)];
//    }];
//    [self.myTable.mj_header beginRefreshing];
    self.datas = [ZNSCache getAllObjects:[ZNSUser currentUser].username];
    
    // UISearchBar的常用方法 搜索框
//    UISearchBar *oneSearchBar = [[UISearchBar alloc] init];
////    oneSearchBar.frame = CGRectMake(0, 80,[UIScreen mainScreen].bounds.size.width , 50); // 设置位置和大小
//    oneSearchBar.size = CGSizeMake(300, 30);
//    oneSearchBar.keyboardType = UIKeyboardTypeEmailAddress; // 设置弹出键盘的类型
//    oneSearchBar.barStyle = UIBarStyleDefault; // 设置UISearchBar的样式
//    oneSearchBar.tintColor = [UIColor blackColor]; // 设置UISearchBar的颜色 使用clearColor就是去掉背景
//    oneSearchBar.placeholder = @"请输入：设备名称"; // 设置提示文字
////    oneSearchBar.text = @""; // 设置默认的文字
////    oneSearchBar.prompt = @"提示信息"; // 设置提示
//    oneSearchBar.delegate = self; // 设置代理
//    
//    oneSearchBar.showsCancelButton = YES; // 设置时候显示关闭按钮
////     oneSearchBar.showsScopeBar = YES; // 设置显示范围框
//     oneSearchBar.showsSearchResultsButton = YES; // 设置显示搜索结果
//    // oneSearchBar.showsBookmarkButton = YES; // 设置显示书签按钮
////    oneSearchBar.scopeButtonTitles = [NSArray arrayWithObjects:@"356",@"2", nil];
////    oneSearchBar.selectedScopeButtonIndex = 0;
////    [self.view addSubview:oneSearchBar]; // 添加到View上
//    self.navigationItem.titleView = oneSearchBar;
    
    self.searchController = [[UISearchController alloc]initWithSearchResultsController:nil];
    
    self.searchController.searchBar.frame = CGRectMake(0, 0, self.view.frame.size.width, 44);
    self.searchController.dimsBackgroundDuringPresentation = false;
    
    //搜索栏表头视图
    self.myTable.tableHeaderView = self.searchController.searchBar;
    [self.searchController.searchBar sizeToFit];
    //背景颜色
    self.searchController.searchBar.backgroundColor = [UIColor orangeColor];
    self.searchController.searchResultsUpdater = self;

    
}

- (void)loadDataWithPage:(NSNumber *)page{
    @weakify(self);
    [APIClient POST:API_OBJECT withParameters:@{@"page":page} successWithBlcok:^(id response) {
        @strongify(self);
        self.page = page;
        if ([page isEqual:@0]) {
            self.datas = [NSArray yy_modelArrayWithClass:[ZNSObject class] json:[response valueForKey:@"items"]];
        }else{
            self.datas = [self.datas arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSObject class] json:[response valueForKey:@"items"]]];
        }
        
        [self.myTable.mj_header endRefreshing];
        [self.myTable.mj_footer endRefreshing];
        
        if (([[response valueForKey:@"page"] integerValue] + 1) == [[response valueForKey:@"pages"]integerValue]) {
            [self.myTable.mj_footer endRefreshingWithNoMoreData];
        }
        [self.myTable reloadData];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
        [self.myTable.mj_header endRefreshing];
        [self.myTable.mj_footer endRefreshing];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark
#pragma mark  - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return (!self.searchController.active) ? self.datas.count : self.filterData.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    GetLocationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GetLocationTableViewCell" forIndexPath:indexPath];
//               ZNSObject *object = [self.datas objectAtIndex:indexPath.row];
    ZNSObject *object ;
    if (self.searchController.active) {
        object = [self.filterData objectAtIndex:indexPath.row];
    }else{
        object = [self.datas objectAtIndex:indexPath.row];
    }
    [cell setObject:object];
    [cell setGetBlock:^(void){
        GetLocationCommonViewController *vc = [GetLocationCommonViewController create];
        vc.myObject = [self.datas objectAtIndex:indexPath.row];
        vc.mode = SystemSettingMode;
        [vc  setLocationBlock:^(NSArray *location){
            ZNSObject *object = [self.datas objectAtIndex:indexPath.row];
            object.lat = location[0];
            object.lng = location[1];
            [ZNSCache cacheAllObjects:[self.datas yy_modelToJSONObject] withUserName:[[ZNSUser currentUser] username]];
            
            [self.myTable reloadData];
            
        }];
            
        [self.navigationController pushViewController:vc animated:YES];
    }];
    return cell;

    
}
-(void)upObjectWithLocation:(ZNSObject * )object{
//    @weakify(self)
    [APIClient POST:[NSString stringWithFormat:@"/object/%@/location",object.oid] withParameters:@{@"lat":object.lat,@"lng":object.lng} successWithBlcok:^(id response) {
//       @strongify(self)
      
        NSLog(@"test");
        
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
    
}
        



#pragma mark 协议中的方法

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    //NSPredicate 谓词
     NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"name contains [c] %@",searchController.searchBar.text];

    self.filterData = [[self.datas filteredArrayUsingPredicate:searchPredicate]mutableCopy];
    //刷新表格
    [self.myTable reloadData];
    
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
