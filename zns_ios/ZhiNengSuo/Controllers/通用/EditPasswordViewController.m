//
//  EditPasswordViewController.m
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/30.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import "EditPasswordViewController.h"
#import "ZNSLabelTextFieldTableViewCell.h"
#import "ZNSUser.h"

@interface EditPasswordViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,strong) ZNSLabelTextFieldTableViewCell *oldPasswordCell;
@property (nonatomic,strong) ZNSLabelTextFieldTableViewCell *passwordCell;
@property (nonatomic,strong) ZNSLabelTextFieldTableViewCell *surePasswordCell;
@property (nonatomic,strong) UIBarButtonItem *showButtonItem;

@end


@implementation EditPasswordViewController
- (void)viewDidLoad{
    [super viewDidLoad];
    self.title = @"修改密码";
    [self.tableView registerNib:[ZNSLabelTextFieldTableViewCell nib] forCellReuseIdentifier:[ZNSLabelTextFieldTableViewCell reuseIdentifier]];
    [self configCells];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"显示密码" style:UIBarButtonItemStylePlain target:self action:@selector(showPassword:)];
    
//    self.showButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"关闭显示" style:UIBarButtonItemStyleDone target:self action:@selector(dismissPassword)];
//    
//    self.navigationItem.rightBarButtonItems = [self.navigationItem.rightBarButtonItems arrayByAddingObjectsFromArray:@[self.showButtonItem]];
    
    
}
- (void)showPassword:(UIBarButtonItem *)sender{
    
    if ([sender.title isEqualToString:@"显示密码"]) {
        sender.title = @"隐藏密码";
        self.oldPasswordCell.textField.secureTextEntry = NO;
        self.passwordCell.textField.secureTextEntry = NO;
        self.surePasswordCell.textField.secureTextEntry = NO;
    }else{
        sender.title = @"显示密码";
        self.oldPasswordCell.textField.secureTextEntry = YES;
        self.passwordCell.textField.secureTextEntry = YES;
        self.surePasswordCell.textField.secureTextEntry = YES;
    }
    
    
}
- (void)dismissPassword{
    self.oldPasswordCell.textField.secureTextEntry = YES;
    self.passwordCell.textField.secureTextEntry = YES;
    self.surePasswordCell.textField.secureTextEntry = YES;
}

- (void)configCells{
    self.oldPasswordCell = [ZNSLabelTextFieldTableViewCell loadFromNib];
    [self.oldPasswordCell.label setText:@"旧密码"];
    [self.oldPasswordCell.textField setPlaceholder:@"请输入旧密码"];
    self.oldPasswordCell.textField.secureTextEntry = YES;
    self.passwordCell = [ZNSLabelTextFieldTableViewCell loadFromNib];
    [self.passwordCell.label setText:@"新密码"];
    [self.passwordCell.textField setPlaceholder:@"请输入密码"];
    self.passwordCell.textField.secureTextEntry = YES;
    self.surePasswordCell = [ZNSLabelTextFieldTableViewCell loadFromNib];
    [self.surePasswordCell.label setText:@"确认密码"];
    [self.surePasswordCell.textField setPlaceholder:@"请再次输入密码"];
    self.surePasswordCell.textField.secureTextEntry = YES;
}

- (IBAction)loginAction:(id)sender {
    if (self.oldPasswordCell.textField.text.length == 0 || self.passwordCell.textField.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入完整信息" maskType:SVProgressHUDMaskTypeBlack];
    }else{
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        NSString *oldPassword = [NSString getSha512String:self.oldPasswordCell.textField.text];
        NSString *password = [NSString getSha512String:self.passwordCell.textField.text];
        NSString *surePassword = [NSString getSha512String:self.surePasswordCell.textField.text];
        if (![surePassword isEqualToString:password]) {
            [SVProgressHUD showErrorWithStatus:@"两次输入密码不匹配，请重新输入新密码"];
            self.passwordCell.textField.text = @"";
            self.surePasswordCell.textField.text = @"";
            return;
        }
        
        if (self.passwordCell.textField.text.length>30) {
            [SVProgressHUD showErrorWithStatus:@"密码最多30位"];
            return;
        }
        NSString *username = [[ZNSUser currentUser] username];
        
        [APIClient POST:[NSString stringWithFormat:@"%@%@%@",API_USER,username,API_USER_PASSWORD] withParameters:@{@"old": oldPassword,@"new": password} successWithBlcok:^(id response) {
            [SVProgressHUD showSuccessWithStatus:@"修改成功"];
            [ZNSUser cacheUsername:username andPassword:password];
            [self.navigationController popViewControllerAnimated:YES];
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showSuccessWithStatus:error.errorMessage];
        }];
    }
}

#pragma mark
#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row ==0) {
        return self.oldPasswordCell;
    }else if(indexPath.row == 1){
        return self.passwordCell;
    }else{
        return self.surePasswordCell;
    }
}


@end
