//
//  BlueToothViewController.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/8/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "BlueToothViewController.h"
#import "ZNSSmartKey.h"
#import "ZNSTools.h"
#import "SCBluetoothInstruction.h"
@interface BlueToothViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) NSMutableArray *datas;
@property (nonatomic,strong) NSArray *smartKeys;
@property (nonatomic,strong) NSString *smartKeyType;
@end

@implementation BlueToothViewController
+ (instancetype)shareController{
    static dispatch_once_t onceToken;
    static BlueToothViewController *vc;
    dispatch_once(&onceToken, ^{
        vc = [BlueToothViewController create];
    });
    vc.smartKeyType = nil;
    return vc;
}

+ (void)presentInViewContoller:(UIViewController *)vc{
    [vc presentViewController:[[UINavigationController alloc] initWithRootViewController:[self shareController]]animated:YES completion:^{
        
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self configUI];
    [self setupBT];

}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setupBT];
    [self loadSmartKeys];
}

- (void)setSmartKeyType:(NSString *)smartKeyType
{
    _smartKeyType = smartKeyType;
}

- (void)loadSmartKeys{
    @weakify(self);
    if (self.smartKeys.count<=0) {
        [SVProgressHUD showWithStatus:@"正在加载智能钥匙"];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [ZNSSmartKey getAllSmartKeySuccess:^(id response) {
            @strongify(self);
            self.smartKeys = response;
            [ZNSCache cacheSmartKeys:self.smartKeys withUserName:[ZNSUser currentUser].username];
            [self scan];
            [SVProgressHUD dismiss];
        } error:^(ZNSError *error) {
            @strongify(self);
            NSArray *keys = [ZNSCache findSmartKeysCacheOfUserName:[ZNSUser currentUser].username];
            if ([keys isKindOfClass:[NSArray class]]) {
                [SVProgressHUD dismiss];
                self.smartKeys = keys;
            }else{
                [SVProgressHUD showErrorWithStatus:error.errorMessage];
            }
            [self setupBT];
            [self scan];
        }];
    });
}

- (void)configUI{
    self.title = @"连接智能钥匙";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"扫描" style:UIBarButtonItemStyleDone target:self action:@selector(scan)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.datas = [NSMutableArray array];
}

- (void)dismiss{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)scan{
    [self.datas removeAllObjects];
    [self.tableView reloadData];
    [[GGBluetooth sharedManager] stopScan];
    [[GGBluetooth sharedManager] startScan];
    
    [SVProgressHUD showWithStatus:@"正在扫描中，请稍后。。。"];
    @weakify(self); 
    [NSTimer scheduledTimerWithTimeInterval:8 block:^(NSTimer *timer) {
        @strongify(self);
        [SVProgressHUD dismiss];
        if (self!=nil && self.datas.count<=0) {
            if (self.timeOutBlock) { 
                self.timeOutBlock();
            }
        } 
    } repeats:NO];

}

- (void)setupBT{
    @weakify(self);
    __block NSArray *smartKeyAddr = [[ZNSCache findSmartKeysCacheOfUserName:[ZNSUser currentUser].username]valueForKeyPath:@"btAddr"];

    [[GGBluetooth sharedManager] setDiscoverPeripheralBlock:^(CBPeripheral *discoverPeripheral,NSDictionary *advertisementData,NSNumber *RSSI){
        @strongify(self);
        NSString *macAds = discoverPeripheral.macAddress;
        if (![[self.datas valueForKeyPath:@"identifier"]containsObject:discoverPeripheral.identifier] && [smartKeyAddr containsObject:macAds]) {
            if (_smartKeyType != nil) {
                ZNSSmartKey *key = [self.smartKeys objectAtIndex:[[self.smartKeys valueForKeyPath:@"btAddr"] indexOfObject:[macAds stringByReplacingOccurrencesOfString:@"-" withString:@""]]];
                if ([key.type isEqualToString:_smartKeyType]) {
                    [SVProgressHUD dismiss];
                    [self.datas addObject:discoverPeripheral];
                    [self.tableView reloadData];
                }
            } else {
                [SVProgressHUD dismiss];
                [self.datas addObject:discoverPeripheral];
                [self.tableView reloadData];
            }
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark - UITalbeView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[UITableViewCell reuseIdentifier]];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[UITableViewCell reuseIdentifier]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    CBPeripheral *peripheral = [self.datas objectAtIndex:indexPath.row];

    if ([[[GGBluetooth sharedManager] connectedPheripheral].macAddress isEqual: peripheral.macAddress]) {
        [cell.detailTextLabel setText:@"已连接"];
    }else{
        [cell.detailTextLabel setText:@""];
    }
    BOOL enable = [[self.smartKeys valueForKeyPath:@"btAddr"] containsObject:[peripheral.macAddress stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    if (enable) {
        ZNSSmartKey *key = [self.smartKeys objectAtIndex:[[self.smartKeys valueForKeyPath:@"btAddr"] indexOfObject:[peripheral.macAddress stringByReplacingOccurrencesOfString:@"-" withString:@""]]];
        [cell.textLabel setText:key.name];
       
    }else{
        [cell.textLabel setText:@"未知设备"];
    }
    cell.textLabel.textColor = enable?UIColorHex(0x000000):UIColorHex(0xa5a5a5);
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CBPeripheral *peripheral = [self.datas objectAtIndex:indexPath.row];
    BOOL enable = [[self.smartKeys valueForKeyPath:@"btAddr"] containsObject:[peripheral.macAddress stringByReplacingOccurrencesOfString:@"-" withString:@""]];

    if (!enable) {
        return;
    }
    
    ZNSSmartKey *key = [self.smartKeys objectAtIndex:[[self.smartKeys valueForKeyPath:@"btAddr"] indexOfObject:[peripheral.macAddress stringByReplacingOccurrencesOfString:@"-" withString:@""]]];
    if (self.connectedSmartKeyBlock) {
        self.connectedSmartKeyBlock(key);
    }

    [SVProgressHUD show];
    @weakify(self);
    [[GGBluetooth sharedManager]connectPheripheral:peripheral withSuccessBlcok:^(BOOL success, CBPeripheral *connectedPeripheral, NSError *error) {
        [SVProgressHUD dismiss];
        @strongify(self);
        [self.tableView reloadData];
        [[GGBluetooth sharedManager] discoverCurrentPheripheralServices:^(NSArray *services) {
            [[GGBluetooth sharedManager]discoverCharacteristicsFormService:services.firstObject withBlock:^(NSArray *characteristics, NSError *error) {
                
            }];
        }];
        [[GGBluetooth sharedManager] setCanWriteValueBlock:^{
            
            if ([ZNSTools isSCLock]) {
                [[GGBluetooth sharedManager] writeValueDataSC:[SCBluetoothInstruction lockInfoInstruction].data];
            }
            else {
                [[[GGBluetooth sharedManager] connectedPheripheral]writeValue:[ZNSBluetoothInstruction batteryInstruction].data forCharacteristic:[[GGBluetooth sharedManager] writeableCharacteristic] type:[[GGBluetooth sharedManager] writeableCharacteristic].properties==(CBCharacteristicPropertyRead+CBCharacteristicPropertyWriteWithoutResponse+CBCharacteristicPropertyNotify)?CBCharacteristicWriteWithoutResponse:CBCharacteristicWriteWithResponse];
            }
        }];
        if (self.connectedBlock) {
            self.connectedBlock();
        }
    }];
}
@end
