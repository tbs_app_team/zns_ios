//
//  ZNSPickerViewController.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/10/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "ZNSPickerViewController.h"

@interface ZNSPickerViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *datas;
@property (nonatomic,strong) NSNumber *page;
@end

@implementation ZNSPickerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.editing = YES;
    self.tableView.allowsMultipleSelection = self.allowMultiSelection;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:[UITableViewCell reuseIdentifier]];
    self.datas = [NSMutableArray array];
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self loadDatasAtPage:@0];
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self loadDatasAtPage:@(self.page.integerValue + 1)];
    }];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"取消" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStyleDone target:self action:@selector(finish)];

}

- (void)dismiss{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)finish{
    if (!self.tableView.indexPathsForSelectedRows.count) {
        [SVProgressHUD showInfoWithStatus:@"请选择" maskType:SVProgressHUDMaskTypeBlack];
        return;
    }
    if (self.selectedBlock) {
        if (self.allowMultiSelection) {
            __block NSMutableArray *arr = [NSMutableArray array];
            [self.tableView.indexPathsForSelectedRows enumerateObjectsUsingBlock:^(NSIndexPath * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [arr addObject:[self.datas objectAtIndex:obj.row]];
            }];
            self.selectedBlock(arr);
        }else{
            self.selectedBlock([self.datas objectAtIndex:self.tableView.indexPathForSelectedRow.row]);
        }
    }
    [self dismiss];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.tableView.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showInController:(UIViewController *)vc{
    [vc presentViewController:[[UINavigationController alloc] initWithRootViewController:self] animated:YES completion:nil];
}

- (void)loadDatasAtPage:(NSNumber *)page{
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:self.parameter];
    [dic setValue:page forKey:@"page"];
    @weakify(self);
    [APIClient POST:self.api withParameters:dic successWithBlcok:^(id response) {
        @strongify(self);
        self.page = [response valueForKey:@"page"];
        NSNumber *pages = [response valueForKey:@"pages"];
        if ([self.page isEqual:@0]) {
            [self.datas removeAllObjects];
            [self.datas addObjectsFromArray:[NSArray yy_modelArrayWithClass:self.modelClass json:[response valueForKey:@"items"]]];
        }else{
            [self.datas addObjectsFromArray:[NSArray yy_modelArrayWithClass:self.modelClass json:[response valueForKey:@"items"]]];
        }
        
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        if (![pages isEqual:@0]&&[self.page isEqual:@(pages.integerValue-1)]) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }
        [self.tableView reloadData];
    } errorWithBlock:^(ZNSError *error) {
        @strongify(self);
        [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
}
#pragma mark
#pragma mark - UITalbeView 
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[UITableViewCell reuseIdentifier]];
    if (self.configCell) {
        return self.configCell(cell,[self.datas objectAtIndex:indexPath.row]);
    }else{
        return cell;
    }
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  UITableViewCellEditingStyleDelete|UITableViewCellEditingStyleInsert;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!self.allowMultiSelection) {
        [tableView.indexPathsForSelectedRows enumerateObjectsUsingBlock:^(NSIndexPath * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (![obj isEqual:indexPath]) {
                [tableView deselectRowAtIndexPath:obj animated:NO];
            }
        }];
    }
}
@end
