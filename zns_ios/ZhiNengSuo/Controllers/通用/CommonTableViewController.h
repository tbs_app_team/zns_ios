//
//  CommonTableViewController.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 16/3/20.
//  Copyright © 2016 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonTableViewController : UITableViewController
@property (nonatomic,copy) idBlock selectedBlock;
@property (nonatomic,strong) NSArray *datas;
@end
