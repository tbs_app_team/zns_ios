//
//  AppDelegate.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/22/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "MainOnlineViewController.h"
#import <IQKeyboardManager.h>

@interface AppDelegate ()
@property (nonatomic,strong) NSDate *enterBackgroundDate;
@property (nonatomic,strong) NSDate *enterForegroundDate;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //键盘遮挡管理
    IQKeyboardManager *keyboardManager = [IQKeyboardManager sharedManager];
    [keyboardManager setEnable:YES];
    [keyboardManager setEnableAutoToolbar:NO];
//    [keyboardManager setShouldShowTextFieldPlaceholder:NO];
    keyboardManager.shouldShowToolbarPlaceholder = NO;
    [keyboardManager setShouldResignOnTouchOutside:YES];
    // Override point for customization after application launch.
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD setMinimumDismissTimeInterval:1.5f];
    
    [self startNotifierNetWork];
    [self configRootViewController];
//    [self showServerSettingAlertViewIfNeed];
    [self setupDefault];
    [[GGBluetooth sharedManager]startScan];
    return YES;
}

- (void)configRootViewController{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = [[UINavigationController alloc]initWithRootViewController:[LoginViewController create]];
    [self.window makeKeyAndVisible];
    @weakify(self);
    [[NSNotificationCenter defaultCenter]addObserverForName:NOTIFICATION_ACCOUNT_CHANGE object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        @strongify(self);
        BOOL login = [[note.userInfo valueForKey:@"login"] boolValue];
        if (login) {
            BOOL offline = [[note.userInfo valueForKey:@"isOffline"] boolValue];
            MainOnlineViewController *mainVC = [MainOnlineViewController create];
            mainVC.isOnline = !offline;
            @weakify(self);
            [UIView transitionWithView:self.window
                              duration:0.5f
                               options:UIViewAnimationOptionTransitionFlipFromLeft
                            animations:^{
                                @strongify(self);
                                self.window.rootViewController = [[UINavigationController alloc]initWithRootViewController:mainVC];
                            }
                            completion:nil];
        }else{
            [UIView transitionWithView:self.window
                              duration:0.5f
                               options:UIViewAnimationOptionTransitionFlipFromLeft
                            animations:^{
                                @strongify(self);
                                self.window.rootViewController = [[UINavigationController alloc]initWithRootViewController:[LoginViewController create]];
                            }
                            completion:nil];
        }
    }];
}

- (void)showServerSettingAlertViewIfNeed{
    if (!API_HOST) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"请设置服务器地址" message:@"" preferredStyle:UIAlertControllerStyleAlert];

        __block UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [alertController.textFields enumerateObjectsUsingBlock:^(UITextField * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [[NSNotificationCenter defaultCenter]removeObserver:obj];
            }];
            SAVE_SERVER_ADDRESS([alertController.textFields.firstObject valueForKey:@"text"])
        }];
        
        [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            
            textField.placeholder = @"请输入服务器地址";
            textField.text = @"http://112.91.145.58:33330";
            [[NSNotificationCenter defaultCenter]addObserverForName:UITextFieldTextDidChangeNotification object:textField queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
                action.enabled  = textField.text.length>0;
            }];
            action.enabled  = textField.text.length>0;
        }];
        
        [alertController addAction:action];
        [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)setupDefault{
    if (![[NSUserDefaults standardUserDefaults]valueForKey:AUTO_LOGOUT_TIME]) {
        [[NSUserDefaults standardUserDefaults]setValue:@30 forKey:AUTO_LOGOUT_TIME];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    if ((!API_ALERT_PORT) || [API_ALERT_PORT isEqualToString:@""]) {
        SAVE_ALERT_PORT(@"33331");
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
//    self.enterBackgroundDate = [NSDate date];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
//    self.enterForegroundDate = [NSDate date];
//    
//    NSInteger autoLogoutTime = [[[NSUserDefaults standardUserDefaults]valueForKey:AUTO_LOGOUT_TIME] integerValue]*60;
//    
//    NSTimeInterval time = [self.enterForegroundDate timeIntervalSinceDate:self.enterBackgroundDate];
//    
//    if (time>autoLogoutTime) {
//        [SVProgressHUD showErrorWithStatus:@"长时间无操作，已自动注销" maskType:SVProgressHUDMaskTypeBlack];
//        [[ZNSUser currentUser]logout];
//    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)startNotifierNetWork
{
    [[Reachability reachabilityForLocalWiFi] startNotifier];
    [[NSNotificationCenter defaultCenter] addObserverForName:kReachabilityChangedNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        Reachability *reachDetector = note.object;
        switch (reachDetector.currentReachabilityStatus) {
            case ReachableViaWiFi:
                NSLog(@"已连接上wifi");
                [[NSUserDefaults standardUserDefaults] setObject:@"OnNetWork" forKey:@"netWork"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                break;
            case ReachableViaWWAN:
                NSLog(@"正在使用蜂窝网络");
                [[NSUserDefaults standardUserDefaults] setObject:@"OnNetWork" forKey:@"netWork"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                break;
            case NotReachable:
                NSLog(@"网络已断开");
                [[NSUserDefaults standardUserDefaults] setObject:@"OffNetWork" forKey:@"netWork"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                break;
            default:
                break;
        }
    }];
}
@end
