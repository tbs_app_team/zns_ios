//
//  ZNSInputController.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 16/12/17.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "ZNSInputController.h"

@interface ZNSInputController (){

    IBOutlet UITextField *txtInput; 
    InputCallBackBlock _block;
    
}

@end

@implementation ZNSInputController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    txtInput.text = _defaulValue;
    txtInput.placeholder = _placholder;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [txtInput becomeFirstResponder];
}

- (void)setInputDoneCallBackBlockWith:(InputCallBackBlock)block{

    _block = block;
}


- (void)doneAction:(id)sender{

    if (![[txtInput.text stringByReplacingOccurrencesOfString:@" " withString:@""] length]) {
        [SVProgressHUD showErrorWithStatus:@"输入内容不能为空"];
        return;
    }
    
    if ([txtInput.text length] > _maxInputCount && _maxInputCount != 0) {
        [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"最多只能输入%zi个字符",_maxInputCount]];
        return;
    }
    
    if (_block) {
        [self.view endEditing:YES];
        _block(txtInput.text);
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
