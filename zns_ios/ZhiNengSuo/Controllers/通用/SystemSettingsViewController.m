//
//  SystemSettingsViewController.m
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/27.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import "SystemSettingsViewController.h"
#import "APIAddress.h"
#import "EditPasswordViewController.h"
#import "AppDelegate.h"
#import "GetLocationViewController.h"
#import "BlueToothViewController.h"
#import "ZNSBluetoothInstruction.h"
#import "ZNSInitialViewController.h"
#import "ZNSInputController.h"
#import "HelpController.h"
#import "SystemUpdateController.h"

@interface SystemSettingsViewController () <UITextFieldDelegate,UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate> {
    
    IBOutlet UITextField *serverAddressTextField;
    IBOutlet UITextField *timeOfAutoLogoutTextField;
    IBOutlet UIButton *editPasswordButton;
    IBOutlet UIButton *initializeDataButton;
     IBOutlet UITextField *alertPortTextField;
     IBOutlet UIButton *getLocationButton;
    
    IBOutlet UITableView *tbvSetting;
    
    NSMutableArray *arrConfigs;
    NSMutableArray *configVaules;
    NSString *strHost;
    NSString *strAlertProt;
    NSString *strAutoLogoutTime;
    
}
@property (nonatomic,strong) JKAlertDialog *dialog;


@end

@implementation SystemSettingsViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"设置";
    // Do any additional setup after loading the view.
    [getLocationButton setHidden:YES];
    [self initializeUI];
    
    [self setupDataSources];
    
//     self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)makeCache{
    [SVProgressHUD showWithStatus:@"正在初始化数据"];
    [ZNSAPITool cacheOfflineDataSuccessWithBlcok:^(id response) {
        // cache smart keys
        [ZNSSmartKey getAllSmartKeySuccess:^(id response) {
            [ZNSCache cacheSmartKeys:response withUserName:[ZNSUser currentUser].username];
            [SVProgressHUD dismiss];
        } error:^(ZNSError *error) {
//            [SVProgressHUD showErrorWithStatus:@"初始化数据失败" maskType:SVProgressHUDMaskTypeBlack];
            [SVProgressHUD dismiss];
            [SVProgressHUD showErrorWithStatus:@"初始化数据失败"];
        }];
    } errorWithBlock:^(ZNSError *error) {
//        [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
    } ];
    
    [APIClient POST:API_SECTION_OBJECT_WITH_SID([ZNSUser currentUser].sid) withParameters:@{} successWithBlcok:^(id response) {
        [ZNSCache cacheObjects:[response objectForKey:@"items"]withUserName:[ZNSUser currentUser].username];
        [SVProgressHUD showInfoWithStatus:@"初始化成功"];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:@"初始化数据失败"];
        
    }];
    
    [ZNSSmartKey getAllSmartKeySuccess:nil error:nil];
}

#pragma mark - Private

- (void)initializeUI {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(save)];
    
    serverAddressTextField.text = API_HOST;
    timeOfAutoLogoutTextField.text = [[[NSUserDefaults standardUserDefaults]valueForKey:AUTO_LOGOUT_TIME] stringValue];
    timeOfAutoLogoutTextField.keyboardType = UIKeyboardTypeNumberPad;
    alertPortTextField.text = API_ALERT_PORT;
    alertPortTextField.keyboardType = UIKeyboardTypeNumberPad;
    [editPasswordButton bs_configureAsPrimaryStyle];
    [initializeDataButton bs_configureAsPrimaryStyle];
    [getLocationButton bs_configureAsPrimaryStyle];
    if (_loginView) {
        _loginView = NO;
        [editPasswordButton setHidden:YES];
        [initializeDataButton setHidden:YES];
    
    }else{
        [editPasswordButton setHidden:NO];
        [initializeDataButton setHidden:NO];
    }
   

  
}

- (void)getDeviceLocation{
    [self.navigationController pushViewController:[GetLocationViewController create] animated:YES];
}

- (BOOL)smartURLForString:(NSString *)str {
//    NSString *reg = @"/^(http|https)://([\\w-]+\\.)+[\\w-]+(/[\\w-./?%&=]*)?$/";.*
    NSString *reg = @"[a-zA-z]+://.*";
    NSPredicate *urlPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", reg];
    return [urlPredicate evaluateWithObject:str];
}

//判断是否为整形：
- (BOOL)isPureInt:(NSString*)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return[scan scanInt:&val] && [scan isAtEnd];
}

#pragma mark - Action

- (void)goHelpPage{
    [self.navigationController pushViewController:[HelpController create] animated:YES];
}

- (void)goSystemUpdatePage{
    [self.navigationController pushViewController:[SystemUpdateController create] animated:YES];
}

- (void)save {
    SAVE_SERVER_ADDRESS(serverAddressTextField.text);
    
    
    if ([strAlertProt length]<=0) {
        [SVProgressHUD showErrorWithStatus:@"推送端口号不能为空，请填写完整信息"];
        return;
//    }else if([alertPortTextField.text integerValue]==0){
//        [SVProgressHUD showErrorWithStatus:@"推送端口号不能为0，请填写完整信息"];
//        return;
    }else if ([strAlertProt integerValue]>=0 && [strAlertProt integerValue]<=65535) {
        
        SAVE_ALERT_PORT(strAlertProt);
        
    }else{
        [SVProgressHUD showErrorWithStatus:@"端口号输入范围为0～65535，请重新填写"];
        return;
    }
    
    // 更改自动注销时间
    if ([strAutoLogoutTime length]<=0) {
        
//        [[NSUserDefaults standardUserDefaults]setValue:@30 forKey:AUTO_LOGOUT_TIME];
//        timeOfAutoLogoutTextField.text = @"30";
        [SVProgressHUD showErrorWithStatus:@"注销时间不能为空，请填写完整信息"];
        return;
    }else{
        if ([strAutoLogoutTime integerValue] == 0) {
            [SVProgressHUD showErrorWithStatus:@"注销时间不能为0，请重新填写"];
            return;
            
        }else  if ([strAutoLogoutTime integerValue]>0 &&[strAutoLogoutTime integerValue] < 100) {
          [[NSUserDefaults standardUserDefaults]setValue:@([strAutoLogoutTime integerValue]) forKey:AUTO_LOGOUT_TIME];
            
        }else{
            [SVProgressHUD showErrorWithStatus:@"注销时长范围为1～99，请重新填写"];
            return;
        }
        
    }
    [[NSUserDefaults standardUserDefaults]synchronize];

    
    if (![API_HOST isEqualToString:strHost]) {
        SAVE_SERVER_ADDRESS(strHost);
        [[ZNSUser currentUser]logout];
    }
    if (![API_ALERT_PORT isEqualToString:strAlertProt]) {
        SAVE_ALERT_PORT(strAlertProt);
        [[ZNSUser currentUser] logout];
    }
    
//    [[[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"是否退出程序，使保存的数据生效？" delegate:self cancelButtonTitle:@"否" otherButtonTitles:@"是", nil] show];
     [[[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"只有在后台退出程序，保存的数据才会生效!" delegate:self cancelButtonTitle:@"好" otherButtonTitles:nil, nil] show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        [self .navigationController popViewControllerAnimated:YES];
    }else if (buttonIndex == 1){
//        [self exitApplication];
    }
}

- (void)exitApplication {
    
    AppDelegate *app =  (AppDelegate *)[UIApplication sharedApplication].delegate;
    UIWindow *window = app.window;
    
    [UIView animateWithDuration:1.0f animations:^{
        window.alpha = 0;
        window.frame = CGRectMake(0, window.bounds.size.width, 0, 0);
    } completion:^(BOOL finished) {
        exit(0);
    }];
    //exit(0);
    
}

- (IBAction)didTapButtonActon:(UIButton *)sender {
    if (sender.tag == 0) {
        [self.navigationController pushViewController:[EditPasswordViewController create] animated:YES];
    }else if(sender.tag == 1){
        [self makeCache];
    }else{
//        [self getDeviceLocation];
        [self inititalSmartKey];
    }
}


- (void)inititalSmartKey{
    
    [self.navigationController pushViewController:[ZNSInitialViewController create] animated:YES];

}


#pragma mark - 第4期（2016.12.16更新逻辑）

- (void)setupDataSources{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"SystemSettingConfig" ofType:@"plist"];
    arrConfigs = [NSMutableArray arrayWithArray:[NSArray arrayWithContentsOfFile:path]];
    
    strHost = API_HOST;
    strAlertProt = API_ALERT_PORT;
    strAutoLogoutTime = [[[NSUserDefaults standardUserDefaults]valueForKey:AUTO_LOGOUT_TIME] stringValue];
    
    NSMutableArray *arrService = [NSMutableArray array];
    [arrService addObject:strHost];
    [arrService addObject:strAlertProt];
    [arrService addObject:strAutoLogoutTime];
    
//    NSArray *firstUnlocks = [[NSUserDefaults standardUserDefaults] objectForKey:FIRST_UNLOCK];
//    NSString *strFirstUnlock = (firstUnlocks.count && firstUnlocks)?firstUnlocks.firstObject:@"";
    NSArray *arrOther = @[@"",@"",@""];
    if (!configVaules) {
        configVaules = [NSMutableArray array];
    }
    [configVaules addObject:arrService];
    [configVaules addObject:arrOther];
    
    [tbvSetting reloadData];
}

- (void)excetionWith:(NSString *)strAction{
 
    SEL action =  NSSelectorFromString(strAction);
    if ([self respondsToSelector:action] && action) {
        IMP imp = [self methodForSelector:action];
        void (*func)(id, SEL) = (void *)imp;
        func(self, action);
    }
}


- (void)pushEditPasswordPage{

    [self.navigationController pushViewController:[EditPasswordViewController create] animated:YES];
}

- (void)showSelectOpenLocks{

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"请选择优先开锁方式" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *bleAction = [UIAlertAction actionWithTitle:@"蓝牙开锁" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    UIAlertAction *keyAction = [UIAlertAction actionWithTitle:@"智能钥匙开锁" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:bleAction];
    [alertController addAction:keyAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - TableView delegate & datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return arrConfigs.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSArray *arr = arrConfigs[section];
    return arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSArray *arr =  arrConfigs[indexPath.section];
    NSDictionary *d = arr[indexPath.row];
    cell.textLabel.text = d[@"title"];
    
    NSArray *values = configVaules[indexPath.section];
    cell.detailTextLabel.text = values[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    NSArray *arr =  arrConfigs[indexPath.section];
    NSDictionary *d = arr[indexPath.row];
    NSString *sel = d[@"action"];
    
    if (indexPath.section) {
        [self excetionWith:sel];
        return;
    }
    
    ZNSInputController *input = [ZNSInputController create];
    
    input.title =  d[@"title"];
    NSMutableArray *values = configVaules[indexPath.section];
    input.defaulValue = values[indexPath.row];
    
    __weak __typeof(self)weakSelf = self;
    [input setInputDoneCallBackBlockWith:^(NSString *value) {
        NSLog(@"%@",value);
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        switch (indexPath.row) {
            case 0:{
                if (![self smartURLForString:value]) {
                    [SVProgressHUD showErrorWithStatus:@"请输入正确的服务器地址"];
                    return;
                }
                strHost = value;
            }
                break;
            case 1:{
                if (![self isPureInt:value]) {
                     [SVProgressHUD showErrorWithStatus:@"请输入正确的端口号"];
                    return;
                }
            }
                strAlertProt = value;
                break;
            case 2:{
                if (![self isPureInt:value]) {
                    [SVProgressHUD showErrorWithStatus:@"请输入正确的时长"];
                    return;
                }
            }
                strAutoLogoutTime = value;
                break;
            default:
                break;
        }
        
        [values replaceObjectAtIndex:indexPath.row withObject:value];
        [tableView reloadData];
        [strongSelf excetionWith:sel];
    }];
    
    [self.navigationController pushViewController:input animated:YES];
    
}


@end
