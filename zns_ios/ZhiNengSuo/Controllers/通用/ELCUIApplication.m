//
//  ELCUIApplication.m
//
//  Created by Brandon Trebitowski on 9/19/11.
//  Copyright 2011 ELC Technologies. All rights reserved.
//

#import "ELCUIApplication.h"

@implementation ELCUIApplication

- (void)sendEvent:(UIEvent *)event {
	[super sendEvent:event];
	
	// Fire up the timer upon first event
	if(!_idleTimer) {
		[self resetIdleTimer];
	}
	
	// Check to see if there was a touch event
    NSSet *allTouches = [event allTouches];
    if ([allTouches count] > 0) {
        UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
        if (phase == UITouchPhaseBegan) {
            [self resetIdleTimer];         
		}
    }
}

- (void)resetIdleTimer 
{
    if (_idleTimer) {
        [_idleTimer invalidate];
       
    }
//    NSLog(@"time begin");
	// Schedule a timer to fire in kApplicationTimeoutInMinutes * 60
   NSInteger autoLogoutTime = [[[NSUserDefaults standardUserDefaults]valueForKey:AUTO_LOGOUT_TIME] integerValue]*60;
    _idleTimer = [NSTimer scheduledTimerWithTimeInterval:autoLogoutTime
												  target:self 
												selector:@selector(idleTimerExceeded) 
												userInfo:nil 
												 repeats:NO];
    
}

- (void)idleTimerExceeded {
	/* Post a notification so anyone who subscribes to it can be notified when
	 * the application times out */
    
    if (![ZNSUser currentUser]) {
        return;
    }
    UIAlertView *view = [[UIAlertView alloc] initWithTitle:nil message:@"长时间无操作，已自动注销"  delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
    [view  show];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [view  dismissWithClickedButtonIndex:0 animated:YES];
        [[ZNSUser currentUser]logout];
    });
}

- (void) dealloc {
//	[super dealloc];
}

@end
