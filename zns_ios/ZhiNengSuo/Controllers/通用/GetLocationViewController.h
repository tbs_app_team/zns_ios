//
//  GetLocationViewController.h
//  ZhiNengSuo
//
//  Created by ekey on 16/4/28.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface GetLocationViewController : UIViewController

@property (nonatomic,strong)MKMapView * myMapView;
@property (nonatomic,strong)CLLocationManager *locationManager;


@end
