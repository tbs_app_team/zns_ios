//
//  ZNSInputController.h
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 16/12/17.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^InputCallBackBlock)(NSString *value);

@interface ZNSInputController : UIViewController

@property (nonatomic,strong) NSString *defaulValue;
@property (nonatomic,strong) NSString *placholder;
@property (nonatomic,assign) NSInteger maxInputCount;


- (void)setInputDoneCallBackBlockWith:(InputCallBackBlock)block;


@end
