//
//  ZNSPickerViewController.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/10/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZNSPickerViewController : UIViewController
@property (nonatomic,strong) Class modelClass;
@property (nonatomic,strong) NSString *api;
@property (nonatomic,strong) NSDictionary *parameter; // 不用包括page
@property (nonatomic,copy) cellBlock configCell;
@property (nonatomic,copy) idBlock selectedBlock;
@property (nonatomic,assign) BOOL allowMultiSelection;
- (void)showInController:(UIViewController *)vc;
@end
