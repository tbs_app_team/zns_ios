//
//  SmartKeyUnlockController.h
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/3.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSObject.h"
#import "ZNSTempTask.h"

@interface SmartKeyUnlockController : UIViewController

@property (nonatomic,assign) AuthenticationControllerMode mode;
@property (nonatomic,strong) ZNSObject *optionObject;
@property (nonatomic,strong) NSArray *workBillLocks;
@property (nonatomic,strong) ZNSTempTask *tempTask;

@property (nonatomic,strong) NSString *skid;      //运维任务的id，当执行开锁的类型为工单执行的时候带上此参数

@end
