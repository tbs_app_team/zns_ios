//
//  SiteNavigationController.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/3.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import "SiteNavigationController.h"
#import "SiteManagerListCell.h"

@interface SiteNavigationController (){
    
    NSInteger pageIndex;
    NSArray *sites;
}

@end

@implementation SiteNavigationController

- (void)doGetSiteInfoWithSearchText:(NSString *)text{
    
    @weakify(self)
    [APIClient POST:[NSString stringWithFormat:@"%@downward",API_SECTION_OBJECT_WITH_SID(@"0")] withParameters:@{@"search":text,@"page":[NSNumber numberWithInteger:pageIndex],@"pagesize":[NSNumber numberWithInteger:20]} successWithBlcok:^(id response) {
        @strongify(self)
        if (pageIndex == 0) {
            sites = [NSArray yy_modelArrayWithClass:[ZNSObject class] json:response[@"items"]];
        }else{
            sites = [sites arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSObject class] json:response[@"items"]]];
        }
        [SVProgressHUD dismiss];
        
        [self reloadData];
        
        [self endRefreshing];
        
        if (([[response valueForKey:@"page"] integerValue] + 1) == [[response valueForKey:@"pages"]integerValue]) {
            [self endRefreshingWithNoMoreData];
        }
        
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
        [self endRefreshing];
        if (pageIndex > 0) {
            pageIndex--;
        }
    }] ;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib. 
    self.title = @"站址导航";
    [SVProgressHUD show];
    [self doGetSiteInfoWithSearchText:@""];
} 

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//导航到基站
- (void)navigationSiteWith:(ZNSObject *)obj{
    
    NSLog(@"导航。。。。");
    
    CLLocationCoordinate2D loc = CLLocationCoordinate2DMake([obj.lat floatValue], [obj.lng floatValue]);
    MKMapItem *currentLocation = [MKMapItem mapItemForCurrentLocation];
    MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:loc addressDictionary:nil]];
    [MKMapItem openMapsWithItems:@[currentLocation, toLocation]
                   launchOptions:@{MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving,
                                   MKLaunchOptionsShowsTrafficKey: [NSNumber numberWithBool:YES]}];
    
}

#pragma mark - 父类方法
//下拉刷新
- (void)headerWithRefreshing{
    pageIndex = 0;
    [self doGetSiteInfoWithSearchText:@""];
}
//上拉加载
- (void)footerWithRefreshing{
    pageIndex++;
    [self doGetSiteInfoWithSearchText:@""];
}
//搜索
- (void)searchWithController:(UISearchController *)ctrl{
    
    pageIndex = 0;
    if (ctrl.active) {
        if ([ctrl.searchBar.text length]) {
            [SVProgressHUD show];
            [self doGetSiteInfoWithSearchText:ctrl.searchBar.text];
        }
    }else{
        [SVProgressHUD show];
        [self doGetSiteInfoWithSearchText:@""];
    }
}

/*
 * tableview 相关
 */

//cell的高度
- (CGFloat)rowHeight{
    return 100;
}

//行数
- (NSInteger)row{
    return sites.count;
}

//cell的标识
- (NSString *)cellIdentifier{
    return [SiteManagerListCell reuseIdentifier];
}

- (UINib *)cellNib{
    return [SiteManagerListCell nib];
}

- (UITableViewCell *)cellWithTableView:(UITableView *)tbv indexPath:(NSIndexPath *)idxPath{
    
    SiteManagerListCell *cell = [tbv dequeueReusableCellWithIdentifier:[SiteManagerListCell reuseIdentifier] forIndexPath:idxPath];
    cell.obj = sites[idxPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.view endEditing:YES];
    
    UISearchController *strl = [self searchController];
    [UIAlertController showAlertInViewController:strl.active?strl:self withTitle:@"确认" message:@"开始导航？" cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@[@"确认"] tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
        if (buttonIndex == 2) {
            [self navigationSiteWith:sites[indexPath.row]];
        }
    }];
}


@end
