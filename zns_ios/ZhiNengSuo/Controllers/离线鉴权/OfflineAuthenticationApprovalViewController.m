//
//  OfflineAuthenticationApprovalViewController.m
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/28.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import "OfflineAuthenticationApprovalViewController.h"
#import "OfflineAuthenticationApproval1TableViewCell.h"
#import "OfflineAuthenticationApproval2TableViewCell.h"
#import "ZNSOffline.h"

@interface OfflineAuthenticationApprovalViewController ()<UITableViewDataSource, UITableViewDelegate>  {
    
    IBOutlet UIButton *disagreeButton;
    IBOutlet UIButton *agreeButton;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeight;

@property (nonatomic,strong) NSNumber *page1;
@property (nonatomic,strong) NSNumber *page2;
@property (nonatomic,strong) NSArray *authDatas;
@property (nonatomic,strong) NSArray *historyDatas;
@end

@implementation OfflineAuthenticationApprovalViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initializeUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)initializeUI {
    [disagreeButton bs_configureAsDangerStyle];
    [agreeButton bs_configureAsSuccessStyle];
    [self.segmentedControl addTarget:self
                         action:@selector(segmentedControlChanged)
               forControlEvents:UIControlEventValueChanged];
    
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        if (self.segmentedControl.selectedSegmentIndex == 0) {
            [self loadOfflineAuthApprovalDatasWithPage:@0];
        }else{
            [self loadHistoyWithPage:@0];
        }
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        if (self.segmentedControl.selectedSegmentIndex == 0) {
            [self loadOfflineAuthApprovalDatasWithPage:@(self.page1.integerValue+1)];
        }else{
            [self loadHistoyWithPage:@(self.page2.integerValue+1)];
        }
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)loadOfflineAuthApprovalDatasWithPage:(NSNumber*)page{
    [APIClient POST:@"offline/" withParameters:@{@"status":@[@"待审批"],@"page":page} successWithBlcok:^(id response) {
        if ([page isEqualToNumber:@0]) {
            self.authDatas = [NSArray yy_modelArrayWithClass:[ZNSOffline class] json:[response valueForKey:@"items"]];
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer resetNoMoreData];
        }else{
            self.authDatas = [self.authDatas arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSOffline class] json:[response valueForKey:@"items"]]];
            [self.tableView.mj_footer endRefreshing];
        }
        self.page1 = [response valueForKey:@"page"];
        if ([response[@"page"]intValue] + 1 == [response[@"pages"]intValue]) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }
        [self.tableView reloadData];
    } errorWithBlock:^(ZNSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
}


- (void)loadHistoyWithPage:(NSNumber*)page{
    [APIClient POST:@"offline/" withParameters:@{@"page":page} successWithBlcok:^(id response) {
        if ([page isEqualToNumber:@0]) {
            self.historyDatas = [NSArray yy_modelArrayWithClass:[ZNSOffline class] json:[response valueForKey:@"items"]];
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer resetNoMoreData];
        }else{
            self.historyDatas = [self.historyDatas arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSOffline class] json:[response valueForKey:@"items"]]];
            [self.tableView.mj_footer endRefreshing];
        }
        self.page2 = [response valueForKey:@"page"];
        if ([response[@"page"]intValue] + 1 == [response[@"pages"]intValue]) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }
        [self.tableView reloadData];
    } errorWithBlock:^(ZNSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
}



#pragma mark - Action

- (IBAction)didTapButton:(UIButton *)sender {
    if (sender.tag == 1) {
        
        [UIAlertController showAlertInViewController:self.navigationController withTitle:@"是否全部同意？" message:@"" cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@[@"是"] tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
            
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
            @weakify(self);
            [ZNSAPITool batchHandel:API_OFFLINE_VERIFYALL agree:YES reason:@"" successWithBlcok:^(id response) {
                @strongify(self);
                [SVProgressHUD showSuccessWithStatus:@"已批量同意" maskType:SVProgressHUDMaskTypeBlack];
                
                [self.tableView.mj_header beginRefreshing];
            } errorWithBlock:^(ZNSError *error) {
                [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
            }];
        }];
        
    }else{
        UIAlertController * ac = [UIAlertController alertControllerWithTitle:@"拒绝这个用户的离线鉴权申请" message:nil preferredStyle:UIAlertControllerStyleAlert];
        [ac addTextFieldWithConfigurationHandler:^(UITextField *textField){
            textField.placeholder = @"请输入理由";
            
        }];
        [ac addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        }]];
        [ac addAction:[UIAlertAction actionWithTitle:@"拒绝" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            UITextField *tf = ac.textFields.firstObject;
            
            if (tf.text.length == 0) {
                [SVProgressHUD showInfoWithStatus:@"必须输入拒绝理由" maskType:SVProgressHUDMaskTypeBlack];
                
            } else if ( [[NSString isEmpty:tf.text] isEqualToString:@"yes"]) {
                [SVProgressHUD showInfoWithStatus:@"必须输入拒绝理由" maskType:SVProgressHUDMaskTypeBlack];
                
            } else if (tf.text.length > 32) {
                [SVProgressHUD  showInfoWithStatus:@"输入长度应不超过32位" maskType:SVProgressHUDMaskTypeBlack];
            }else{
                
                [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
                @weakify(self);
                [ZNSAPITool batchHandel:API_OFFLINE_VERIFYALL agree:NO reason:tf.text successWithBlcok:^(id response) {
                    @strongify(self);
                    [SVProgressHUD showSuccessWithStatus:@"已批量拒绝" maskType:SVProgressHUDMaskTypeBlack];
                    
                    [self.tableView.mj_header beginRefreshing];
                } errorWithBlock:^(ZNSError *error) {
                    [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
                }];
            }
            
            
            
        }]];
        [self presentViewController:ac animated:YES completion:nil];
    }
}

- (void)segmentedControlChanged{
    [self.tableView reloadData];
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        self.bottomViewHeight.constant = 40;
//        if (self.authDatas.count<=0) {
            [self.tableView.mj_header beginRefreshing];
//        }
    }else{
        self.bottomViewHeight.constant = 0;
//        if (self.historyDatas.count<=0) {
            [self.tableView.mj_header beginRefreshing];
//        }
    }
}

#pragma mark - UITableView

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        return self.authDatas.count;
    }else{
        return self.historyDatas.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        return [tableView fd_heightForCellWithIdentifier:[OfflineAuthenticationApproval1TableViewCell reuseIdentifier] cacheByIndexPath:indexPath configuration:^(id cell) {
            
        }];
    }else{
        return [tableView fd_heightForCellWithIdentifier:[OfflineAuthenticationApproval2TableViewCell reuseIdentifier] cacheByIndexPath:indexPath configuration:^(id cell) {
            
        }];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        OfflineAuthenticationApproval1TableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:[OfflineAuthenticationApproval1TableViewCell reuseIdentifier] forIndexPath:indexPath];
        
        ZNSOffline *offline = [self.authDatas objectAtIndex:indexPath.row];
        
        [cell setOffline:offline];
        
        @weakify(self);
        @weakify(offline);
        
        [cell setActionBlock:^(NSInteger index){
            @strongify(self);
            @strongify(offline);
            if (index == 0) {
                
                UIAlertController * ac = [UIAlertController alertControllerWithTitle:index == 0?@"拒绝这个用户的请求":@"同意这个用户的请求" message:nil preferredStyle:UIAlertControllerStyleAlert];
                [ac addTextFieldWithConfigurationHandler:^(UITextField *textField){
                    textField.placeholder = @"请输入理由";
                    
                }];
                [ac addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                }]];
                [ac addAction:[UIAlertAction actionWithTitle:index == 0?@"确定":@"批准" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    
                    @strongify(self);
                    @strongify(offline);
                    
                    UITextField *tf = ac.textFields.firstObject;

                    if (index == 0 && tf.text.length == 0) {
                        [SVProgressHUD showInfoWithStatus:@"必须输入拒绝理由" maskType:SVProgressHUDMaskTypeBlack];
                        
                    } else if (index == 0 && [[NSString isEmpty:tf.text] isEqualToString:@"yes"]) {
                        [SVProgressHUD showInfoWithStatus:@"必须输入拒绝理由" maskType:SVProgressHUDMaskTypeBlack];
                        
                    } else if (index == 0 && tf.text.length > 32) {
                        [SVProgressHUD  showInfoWithStatus:@"输入长度应不超过32位" maskType:SVProgressHUDMaskTypeBlack];
                    } else{
                        if (index == 0) {
                            [self handleOffline:offline action:@"拒绝" memo:tf.text];
                        }else{
                            [self handleOffline:offline action:@"批准" memo:STRING_OR_EMPTY(tf.text)];
                        }
                    }
                }]];
                [self presentViewController:ac animated:YES completion:nil];
            }else{
                [self handleOffline:offline action:@"批准" memo:@""];
            }
        }];
        
        return cell;
    } else {
        OfflineAuthenticationApproval2TableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:[OfflineAuthenticationApproval2TableViewCell reuseIdentifier] forIndexPath:indexPath];
        
        ZNSOffline *offline = [self.historyDatas objectAtIndex:indexPath.row];
        
        [cell setOffline:offline];
        return cell;
    }
}

- (void)handleOffline:(ZNSOffline*)offline action:(NSString *)acion memo:(NSString *)memo {
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    [APIClient POST:API_VERIFT_OFFLINE_WITH_OID(offline.oid) withParameters:@{@"action":acion,@"memo":memo} successWithBlcok:^(id response) {
        [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"已%@",acion] maskType:SVProgressHUDMaskTypeBlack];
        [self.tableView.mj_header beginRefreshing];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
    }];
}
@end
