//
//  OfflineAuthenticationApplyViewController.m
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/28.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import "OfflineAuthenticationApplyViewController.h"
#import "CRMediaPickerController.h"

@interface OfflineAuthenticationApplyViewController ()<CRMediaPickerControllerDelegate> {
    
    IBOutlet UILabel *_applyTimeLabel;
    IBOutlet UILabel *_applyHourLabel;
    IBOutlet UIButton *_commitButton;
}

@property (nonatomic,weak) IBOutlet UIImageView *selfImageView;
@property (nonatomic,weak) IBOutlet UIImageView *cardImageView;

@property (nonatomic,strong) UIImage *selfImage;
@property (nonatomic,strong) UIImage *cardImage;

@property (nonatomic,strong) CRMediaPickerController *imagePicker;
@property (nonatomic,assign) NSInteger currentPicking;//1,2

@property (nonatomic,strong) IBOutlet UILabel *zoneLabel;
@property (nonatomic,strong) NSDictionary * zoneData;

@property (nonatomic,strong) IBOutlet UIButton *applyHourButton;
@property (nonatomic,assign) float hours;

@end

@implementation OfflineAuthenticationApplyViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initializeUI];
    [self preLoadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)initializeUI {
    [self.applyHourButton bs_configureAsSuccessStyle];
    [_commitButton bs_configureAsPrimaryStyle];
    
    
    @weakify(self);
    [self.cardImageView setTapActionWithBlock:^{
        @strongify(self);
        self.currentPicking = 2;
        [self.imagePicker show];
    }];
    
    [self.selfImageView setTapActionWithBlock:^{
        @strongify(self);
        self.currentPicking = 1;
        [self.imagePicker show];
    }];
    
}

- (void)preLoadData {
    [APIClient POST:[NSString stringWithFormat:@"user/%@/last_offline/",[ZNSUser currentUser].username] withParameters:@{} successWithBlcok:^(id response) {
        
        _applyTimeLabel.text = [NSString formatTimeString:response[@"apply_at"]];
        NSInteger duration = [response[@"duration"] integerValue];
        if (duration > 0) {
            _applyHourLabel.text = [NSString stringWithFormat:@"%@分钟",@(duration).stringValue];
        }
    } errorWithBlock:^(ZNSError *error) {
        
    }];
//    return;
//    NSString *sid = [[ZNSUser currentUser] sid];
//    __block NSArray * arr;
//    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
//    [APIClient POST:[NSString stringWithFormat:@"%@%@/",API_ZONE,sid] withParameters:@{} successWithBlcok:^(id response) {
//        [SVProgressHUD dismiss];
//        arr = response[@"items"];
//        if (arr.count == 0) {
//            [SVProgressHUD showInfoWithStatus:@"暂时没有鉴权开锁区域"];
//            [self.navigationController popViewControllerAnimated:YES];
//            return ;
//        }
//        @weakify(self);
//        [ActionSheetStringPicker showPickerWithTitle:@"请先选择鉴权开锁区域" rows:[response[@"items"] valueForKeyPath:@"name"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
//            @strongify(self);
//            [self.zoneLabel setText:selectedValue];
//            self.zoneData = arr[selectedIndex];
//            //获取上次离线记录
//            NSString * username = [ZNSUser currentUser].username;
//            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
//            [APIClient POST:[NSString stringWithFormat:@"%@%@%@%@",API_USER,username,API_USER_LASTOFFLINE,self.zoneData[@"id"]] withParameters:@{} successWithBlcok:^(id response) {
//                @weakify(self);
//                [SVProgressHUD dismiss];
////                _zoneLabel.text = response[@"zone"];
//                _applyTimeLabel.text = [NSString formatTimeString:response[@"apply_at"]];
//                _applyHourLabel.text = [NSString stringWithFormat:@"%@",@([response[@"duration"] integerValue]/60)];
//            } errorWithBlock:^(ZNSError *error) {
//                [SVProgressHUD showSuccessWithStatus:error.errorMessage];
//            }];
//        } cancelBlock:^(ActionSheetStringPicker *picker) {
//            [self.navigationController popViewControllerAnimated:YES];
//        } origin:self.view];
//    } errorWithBlock:^(ZNSError *error) {
//        [SVProgressHUD showSuccessWithStatus:error.errorMessage];
//    }];
}

- (CRMediaPickerController *)imagePicker{
    if (!_imagePicker) {
        _imagePicker = [[CRMediaPickerController alloc]init];
        _imagePicker.delegate = self;
        _imagePicker.mediaType = CRMediaPickerControllerMediaTypeImage;
//        _imagePicker.sourceType = (CRMediaPickerControllerSourceTypePhotoLibrary|CRMediaPickerControllerSourceTypeCamera);
        _imagePicker.sourceType = CRMediaPickerControllerSourceTypeCamera;
    }
    return _imagePicker;
}

#pragma mark - Action

- (IBAction)didTapButton:(UIButton *)sender {
    if (sender.tag == 0) {
        @weakify(self);
        [ActionSheetStringPicker showPickerWithTitle:@"请选择申请时长" rows:@[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",@"24"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            @strongify(self);
            [self.applyHourButton setTitle:[NSString stringWithFormat:@"申请时长(小时)：%@",selectedValue] forState:UIControlStateNormal];
            self.hours = [selectedValue integerValue];
//            self.hours = 0.1;
        } cancelBlock:^(ActionSheetStringPicker *picker) {
        } origin:self.view];
    } else {
        if (self.hours == 0) {
            [self didTapButton:self.applyHourButton];
            return;
        }
        
        if (self.selfImage==nil) {
            [SVProgressHUD showErrorWithStatus:@"请拍摄头像" maskType:SVProgressHUDMaskTypeBlack];
            return;
        }
        if (self.cardImage==nil) {
            [SVProgressHUD showErrorWithStatus:@"请拍摄身份证照片" maskType:SVProgressHUDMaskTypeBlack];
            return;
        }
        
        
        NSMutableDictionary * dicParameters = [NSMutableDictionary dictionary];
//        [dicParameters setValue:self.zoneData[@"id"] forKey:@"zid"];
        [dicParameters setValue:@(self.hours*60) forKey:@"duration"];
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
        @weakify(self);
        [APIClient POST:API_APPLYOFFLINE withParameters:dicParameters successWithBlcok:^(id response) {
            @strongify(self);
            [self uploadImageForUid:[response valueForKey:@"id"]];
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
        }];
    }
}

- (void)uploadImageForUid:(NSString *)uid{
    @weakify(self);
    [ZNSAPITool uploadImage:self.selfImage imageType:ZNSImageTypePortrait type:ZNSUploadTypeOfflineApply forId:uid progressWithBlcok:^(CGFloat percentage) {
        [SVProgressHUD showProgress:percentage status:@"正在上传自拍头像" maskType:SVProgressHUDMaskTypeBlack];
    } successWithBlcok:^(id response) {
        @strongify(self);
        
        [ZNSAPITool uploadImage:self.cardImage imageType:ZNSImageTypeIdCard type:ZNSUploadTypeOfflineApply forId:uid progressWithBlcok:^(CGFloat percentage) {
            [SVProgressHUD showProgress:percentage status:@"正在上传身份证图片" maskType:SVProgressHUDMaskTypeBlack];
        } successWithBlcok:^(id response) {
            @strongify(self);
            [SVProgressHUD showSuccessWithStatus:@"申请成功" maskType:SVProgressHUDMaskTypeBlack];
            [self.navigationController popViewControllerAnimated:YES];
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:@"上传身份证图片失败" maskType:SVProgressHUDMaskTypeBlack];
        }];
        
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:@"上传自拍头像失败" maskType:SVProgressHUDMaskTypeBlack];
    }];
}

#pragma mark - CRMediaPickerController
- (void)CRMediaPickerController:(CRMediaPickerController *)mediaPickerController didFinishPickingAsset:(ALAsset *)asset error:(NSError *)error{
    if (!error) {
        if (mediaPickerController.sourceType == CRMediaPickerControllerSourceTypeCamera) {
            ALAssetsLibrary * library = [[ALAssetsLibrary alloc] init];
            [library writeImageToSavedPhotosAlbum:asset.defaultRepresentation.fullResolutionImage orientation:asset.defaultRepresentation.orientation completionBlock:nil];
        }
        if (self.currentPicking == 1) {
            [self.selfImageView setImage:[UIImage imageWithCGImage:asset.aspectRatioThumbnail]];
            self.selfImage = [UIImage imageWithCGImage:asset.defaultRepresentation.fullResolutionImage scale:1 orientation:(UIImageOrientation)asset.defaultRepresentation.orientation];
        }else{
            [self.cardImageView setImage:[UIImage imageWithCGImage:asset.aspectRatioThumbnail]];
            self.cardImage = [UIImage imageWithCGImage:asset.defaultRepresentation.fullResolutionImage scale:1 orientation:(UIImageOrientation)asset.defaultRepresentation.orientation];
        }
    }
}

#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

@end
