//
//  OfflineAuthenticationOpenBtLockViewController.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/13/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "OfflineAuthenticationOpenBtLockViewController.h"

@interface OfflineAuthenticationOpenBtLockViewController ()

@end

@implementation OfflineAuthenticationOpenBtLockViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
