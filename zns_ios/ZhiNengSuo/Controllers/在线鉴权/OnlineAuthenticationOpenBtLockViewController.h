//
//  OnlineAuthenticationOpenBtLockViewController.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/28/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSObject.h"
#import "ZNSTempTask.h"
@interface OnlineAuthenticationOpenBtLockViewController : UIViewController
@property (nonatomic,assign) AuthenticationControllerMode mode;
@property (nonatomic,strong) ZNSObject *optionObject;
@property (nonatomic,strong) NSArray *workBillLocks;
@property (nonatomic,strong) ZNSTempTask *tempTask; 
@property (nonatomic,strong) NSString *skid;      //运维任务的id，当执行开锁的类型为工单执行的时候带上此参数
@end
