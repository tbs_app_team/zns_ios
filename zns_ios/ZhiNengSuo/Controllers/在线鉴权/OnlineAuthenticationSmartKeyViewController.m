//
//  OnlineAuthenticationSmartKeyViewController.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/11/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "OnlineAuthenticationSmartKeyViewController.h"
#import "OnlineAuthenticationOpenBtLockViewController.h"
#import "BlueToothViewController.h"
#import "ZNSZone.h"
#import "ZNSLock.h"
#import "JKAlertDialog.h"
#import "HistoryTableViewCell.h"
#import "ZNSCache.h"
#import "ZNSOfflineKeyOperation.h"
#import "ZNSDate.h"
#import "ZNSExecuteUnlockHelper.h"
#import "ZNSTools.h"
#import "SCBluetoothInstruction.h"
#import "SCBluetoothReturn.h"
#import "InstructionUtil.h"
@interface OnlineAuthenticationSmartKeyViewController ()<UITableViewDelegate,UITableViewDataSource>{
    JKAlertDialog *_dialog;
    NSNumber *mLockState;
}
@property (weak, nonatomic) IBOutlet UIButton *pairButton;
@property (nonatomic,weak) IBOutlet UILabel *nameLabel;
@property (nonatomic,weak) IBOutlet UILabel *phoneLabel;
@property (nonatomic,weak) IBOutlet UILabel *sectionLabel;
@property (nonatomic,weak) IBOutlet UIView *deviceInfoView;
@property (nonatomic,weak) IBOutlet UILabel *lockNameLabel;
@property (nonatomic,weak) IBOutlet UILabel *lockTypeLabel;
@property (nonatomic,strong) NSArray *locks;
@property (nonatomic,strong) NSArray *locksAll;
@property (nonatomic,strong) ZNSLock *lastUnlock;
@property (nonatomic,assign) NSInteger loadLocksQueueCount;
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *history;
@property (nonatomic,strong) NSString *operationID;
@property (nonatomic,strong) ZNSOfflineKeyOperation * offlineKeyOperation;
@end

@implementation OnlineAuthenticationSmartKeyViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if ([ZNSUser currentUser].isOfflineLogin) {
        self.mode = AuthenticationControllerModeOffline;
    }
    self.offlineKeyOperation = [ZNSOfflineKeyOperation new];
    [self configUI];
    [self loadData];
    [self configBT];
    self.history = [NSMutableArray array];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[GGBluetooth sharedManager]disconnectCurrentDevice];
    [_dialog dismiss];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (![[GGBluetooth sharedManager] connectedPheripheral]) {
        static BlueToothViewController *btVC;
        if (!btVC) {
            btVC = [BlueToothViewController create];
        }
        [btVC setConnectedBlock:^{
            [_dialog dismiss];
            [SVProgressHUD showSuccessWithStatus:@"已连接"];
        }];
        
       
        if (self.mode == AuthenticationControllerModeNormal || self.mode == AuthenticationControllerModeTemptask) {
            if ([[ZNSExecuteUnlockHelper unlock] isKindOfClass:[self class]]) {
                @weakify(self);
                btVC.timeOutBlock = ^{
                    [_dialog dismiss];
                    [SVProgressHUD dismiss];
                    // 显示蓝牙开锁
                    [UIAlertController showAlertInViewController:self.navigationController withTitle:@"没有扫描到设备，是否使用蓝牙开锁？" message:@"" cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@[@"是"] tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
                        @strongify(self);
                        
                        if (buttonIndex == 2) {
                            OnlineAuthenticationOpenBtLockViewController *vc = [OnlineAuthenticationOpenBtLockViewController create];
                            if (self.mode == AuthenticationControllerModeTemptask){
                                vc.mode = AuthenticationControllerModeTemptask;
                                vc.optionObject = self.optionObject;
                                vc.tempTask = self.tempTask;
                            }else{
                                vc.mode = AuthenticationControllerModeNormal;
                            } 
                            [self.navigationController pushViewController:vc animated:YES];
                        }
                    }]; 
                };
            }
        }
      
        _dialog = [[JKAlertDialog alloc]initWithTitle:@"选择智能钥匙" message:@""];
        _dialog.contentView = btVC.view;
        
        [_dialog addButton:Button_CANCEL withTitle:@"取消" handler:^(JKAlertDialogItem *item) {
            NSLog(@"click %@",item.title);
        }];;
        
        [_dialog show];
        [btVC setupBT];
        [btVC loadSmartKeys];
    }
}

- (void)configUI{
    [self.pairButton bs_configureAsPrimaryStyle];
    self.pairButton.layer.cornerRadius = 50;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[HistoryTableViewCell nib] forCellReuseIdentifier:[HistoryTableViewCell reuseIdentifier]];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"配对" style:UIBarButtonItemStylePlain target:self action:@selector(goPait)];
}

- (void)configBT{
    
    [[GGBluetooth sharedManager] setNotifyCharacteristicBlock:^(CBCharacteristic *characteristic,NSData *recvData,NSError *error){
        if (self.mode != AuthenticationControllerModeOffline) {
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"netWork"] isEqualToString:@"OffNetWork"]) {
                [SVProgressHUD showErrorWithStatus:@"网络异常，禁止开锁操作"];
                return;
            }
        }
        if (self.mode == AuthenticationControllerModeTemptask) { //判断任务是否过期
            NSDateFormatter * df = [[NSDateFormatter alloc]init];
            df.dateFormat = @"yyyyMMddHHmmss";
            NSDate * endDate = [df dateFromString:self.tempTask.end_at];
            if ([endDate timeIntervalSinceDate:[NSDate date]] < 0) {
                [SVProgressHUD showErrorWithStatus:@"任务已经过期，请重新申请"];
                return;
            }
        }else if (self.mode == AuthenticationControllerModeOffline){
            
            ZNSOffline  *offline = [ZNSCache findOfflineDataOfUserName:[ZNSUser currentUser].username];
            if((offline.beginAt.integerValue+offline.duration.integerValue*60)<[NSDate date].timeIntervalSince1970){
                [SVProgressHUD showErrorWithStatus:@"离线任务已过期，禁止操作"];
                return;
            }
            
        }
        
        if ([ZNSTools isSCLock]) {
            
            SCBluetoothReturn *bluetoothReturn = [[SCBluetoothReturn alloc]initWithByteData:recvData];
            
            switch ([bluetoothReturn bluetoothReturnType]) {
                case ReturnTypeInit:
                {
                    SCInitResult *result = [bluetoothReturn parseInitInstruction];
                    if (result.mState == 1) {
                        [[GGBluetooth sharedManager] writeValueDataSC:[SCBluetoothInstruction lockInfoInstruction].data];
                    }
                    else {
                        [SVProgressHUD showErrorWithStatus:@"初始化失败"];
                    }
                }
                    break;
                case ReturnTypeBattery:
                {
                    SCBatteryResult *result = [bluetoothReturn parseBatteryInstruction];
                    if (result.mState == 1) {
                        [[GGBluetooth sharedManager] writeValueDataSC:[SCBluetoothInstruction lockInfoInstruction].data];
                    }
                }
                    break;
                case ReturnTypeSwitch:
                {
                    SCSwitchResult *result = [bluetoothReturn parseSwitchInstruction];

                    if(result.mState == 1){
                        if (self.lastUnlock) {
                            [SVProgressHUD showSuccessWithStatus:@"已开锁"];
                            
                            if (self.mode == AuthenticationControllerModeTemptask) {
                                //                        [self.lastUnlock createUnlockHistorySuccess:YES verifyAt:nil verifyBy:nil action:@"temp"];
                                [self upUnlockHistorySuccess:self.lastUnlock isSusccess:YES verifyAt:nil verifyBy:nil action:@"temp"];
                            }else if(self.mode == AuthenticationControllerModeNormal){
                                //                        [self.lastUnlock createUnlockHistorySuccess:YES verifyAt:nil verifyBy:nil action:@"fixed"];
                                [self upUnlockHistorySuccess:self.lastUnlock isSusccess:YES verifyAt:nil verifyBy:nil action:@"fixed"];
                            }else if(self.mode == AuthenticationControllerModeWorkBill){
                                 [self upUnlockHistorySuccess:self.lastUnlock isSusccess:YES verifyAt:nil verifyBy:nil action:@"workbill"];
                            }else if(self.mode == AuthenticationControllerModeWorkPatrol){
//                                [self makeOrderWithResult:6 lock:self.lastUnlock];
                                
                                 [self upUnlockHistorySuccess:self.lastUnlock isSusccess:YES verifyAt:nil verifyBy:nil action:@"fixed"];
                                
                            }else{//offlineOperation
                                [self initOfflineKeyOperation:ZNSOfflineKeyResultOrderNormal lid:[self.lastUnlock.lid integerValue] action:@"fixed" unlock_at:[ZNSDate getCurrentTime] isSeverError:NO];
                                
                            }
                            
                            [self.history addObject:@{@"rfid":STRING_OR_EMPTY(self.lastUnlock.rfid),@"name":STRING_OR_EMPTY(self.lastUnlock.name),@"time":[[[NSDate date] dateByAddingHours:0] stringWithFormat:@"yyyy-MM-dd hh:mm:ss"],@"result":@"正常操作",@"state":mLockState}];
                        }
                        
                        [self.tableView reloadData];
                    } else {
                        self.deviceInfoView.hidden = YES;
                        [SVProgressHUD showErrorWithStatus:@"开锁失败"];
                        
                        if (self.lastUnlock) {
                            if (self.mode == AuthenticationControllerModeTemptask) {
                                //                        [self.lastUnlock createUnlockHistorySuccess:NO verifyAt:nil verifyBy:nil action:@"temp"];
                                [self upUnlockHistorySuccess:self.lastUnlock isSusccess:NO verifyAt:nil verifyBy:nil action:@"temp"];
                            }else if(self.mode == AuthenticationControllerModeNormal){
                                //                        [self.lastUnlock createUnlockHistorySuccess:NO verifyAt:nil verifyBy:nil action:@"fixed"];
                                [self upUnlockHistorySuccess:self.lastUnlock isSusccess:NO verifyAt:nil verifyBy:nil action:@"fixed"];
                            }else if(self.mode == AuthenticationControllerModeWorkBill){
                                [self upUnlockHistorySuccess:self.lastUnlock isSusccess:NO verifyAt:nil verifyBy:nil action:@"workbill"];
                            }else if(self.mode == AuthenticationControllerModeWorkPatrol){
//                                [self makeOrderWithResult:255 lock:self.lastUnlock];
                                 [self upUnlockHistorySuccess:self.lastUnlock isSusccess:NO verifyAt:nil verifyBy:nil action:@"fixed"];
                            }else{
                                //offlineOperation
                                [self initOfflineKeyOperation:ZNSOfflineKeyResultOrderWarnning lid:[self.lastUnlock.lid integerValue] action:@"fixed" unlock_at:[ZNSDate getCurrentTime] isSeverError:NO];
                                
                            }
                        }
                    }
                }
                    break;
                case ReturnTypeLockInfo:
                {
                    SCLockInfoResult *result = [bluetoothReturn parseLockInfoInstruction];
                    
                    mLockState = [NSNumber numberWithInteger:result.mLockState];
                    if ([[self.locks valueForKeyPath:@"rfid"]containsObject:result.mLockId]) { //权限范围内的锁具
                        
                        self.deviceInfoView.hidden = NO;
                        NSInteger index = [[self.locks valueForKeyPath:@"rfid"]indexOfObject:result.mLockId];
                        ZNSLock *lock = [self.locks objectAtIndex:index];
                        [self.lockNameLabel setText:lock.name];
                        [self.lockTypeLabel setText:lock.type];
                        if (lock) {
                            self.lastUnlock = lock;
                        }
                        
                        if (result.mState == 1) {
//                            if (result.mLockState == 1) {
//                                [SVProgressHUD showSuccessWithStatus:@"锁已开"];
//                                return ;
//                            }
                            if (result.mLockInstallState == 0) {
                                [[GGBluetooth sharedManager] writeValueDataSC:[SCBluetoothInstruction initInstruction].data];
                                return ;
                            }
                            if (result.mLockInstallState == 1) {
                                //                                [[GGBluetooth sharedManager] writeValueDataSC:[SCBluetoothInstruction clearInstruction].data];
                                [[GGBluetooth sharedManager] writeValueDataSC:[SCBluetoothInstruction switchLockInstruction:1].data];
                                [SVProgressHUD showWithStatus:@"正在开锁"];
                            }
                        }
                        
                    } else {
                        //                self.lastUnlock = nil;
                        
                        self.deviceInfoView.hidden = YES;
                        //                [SVProgressHUD showErrorWithStatus:@"RFID 不匹配"];
                        [SVProgressHUD dismiss];
                        
                        if ([[self.locksAll valueForKey:@"rfid"] containsObject:result.mLockId]) {
                            
                            self.lastUnlock = [self.locksAll objectAtIndex:[[self.locksAll valueForKey:@"rfid"]indexOfObject:result.mLockId]];
                            [self.history addObject:@{@"rfid":STRING_OR_EMPTY(result.mLockId),@"name":@"权限范围外设备",@"time":[[NSDate date]  stringWithFormat:@"yyyy-MM-dd hh:mm:ss"],@"result":@"越权未遂"}];
                            
                        }else{
                            
                            self.lastUnlock = nil;
                            [self.history addObject:@{@"rfid":STRING_OR_EMPTY(result.mLockId),@"name":@"未知设备",@"time":[[NSDate date]  stringWithFormat:@"yyyy-MM-dd hh:mm:ss"],@"result":@"操作失败"}];
                        }
                        [self.tableView reloadData];
                        
                        if (self.lastUnlock) {
                            if (self.mode == AuthenticationControllerModeTemptask) {
                                //                        [self.lastUnlock createUnlockHistorySuccess:NO verifyAt:nil verifyBy:nil action:@"temp"];
                                [self upUnlockHistorySuccess:self.lastUnlock isSusccess:NO verifyAt:nil verifyBy:nil action:@"temp"];
                            }else if(self.mode == AuthenticationControllerModeNormal){
                                //                        [self.lastUnlock createUnlockHistorySuccess:NO verifyAt:nil verifyBy:nil action:@"fixed"];
                                [self upUnlockHistorySuccess:self.lastUnlock isSusccess:NO verifyAt:nil verifyBy:nil action:@"fixed"];
                                
                            }else if(self.mode == AuthenticationControllerModeWorkBill){
                                [self upUnlockHistorySuccess:self.lastUnlock isSusccess:NO verifyAt:nil verifyBy:nil action:@"workbill"];
                            }else if(self.mode == AuthenticationControllerModeWorkPatrol){
//                                 [self makeOrderWithResult:255 lock:self.lastUnlock];
                                 [self upUnlockHistorySuccess:self.lastUnlock isSusccess:NO verifyAt:nil verifyBy:nil action:@"fixed"];
                            }else{//offlineOperation
                                [self initOfflineKeyOperation:ZNSOfflineKeyResultOrderWarnning lid:[self.lastUnlock.lid integerValue] action:@"fixed" unlock_at:[ZNSDate getCurrentTime] isSeverError:NO];
                                
                            }
                        }
                        //                [self.history addObject:@{@"rfid":STRING_OR_EMPTY(self.lastUnlock.rfid),@"name":self.lastUnlock.name?self.lastUnlock.name:@"未知设备",@"time":[[NSDate date]  stringWithFormat:@"yyyy-MM-dd hh:mm:ss"],@"result":@"越权未遂"}];
                    }

                }
                    break;
                case ReturnTypeClear:
                {
                    SCClearResult *result = [bluetoothReturn parseClearInstruction];
                    if (result.mState == 1) {
                        [[GGBluetooth sharedManager] writeValueDataSC:[SCBluetoothInstruction initInstruction].data];
                    }
                    else {
                        [SVProgressHUD showErrorWithStatus:@"清空失败"];
                    }
                }
                    break;
                case ReturnTypeSuperClear:
                {
                    SCSuperClearResult *result = [bluetoothReturn parseSuperClearInstruction];
                    if (result.mState == 1) {
                        [[GGBluetooth sharedManager] writeValueDataSC:[SCBluetoothInstruction initInstruction].data];
                    }
                    else {
                        [SVProgressHUD showErrorWithStatus:@"超级清空失败"];
                    }
                }
                    break;
                default:
                    break;
            }
        }
        //一般锁
        else {
            
            ZNSBluetoothInstruction *b = [[ZNSBluetoothInstruction alloc]initWithByteData:characteristic.value];
            
            if ([[self.locks valueForKeyPath:@"rfid"]containsObject:b.rfid]) { //权限范围内的锁具
                
                self.deviceInfoView.hidden = NO;
                NSInteger index = [[self.locks valueForKeyPath:@"rfid"]indexOfObject:b.rfid];
                ZNSLock *lock = [self.locks objectAtIndex:index];
                [self.lockNameLabel setText:lock.name];
                [self.lockTypeLabel setText:lock.type];
                if (lock) {
                    self.lastUnlock = lock;
                }
                
                [[[GGBluetooth sharedManager] connectedPheripheral]writeValue:[ZNSBluetoothInstruction unlockInstructionWithRFID:b.rfidData].data forCharacteristic:[[GGBluetooth sharedManager] writeableCharacteristic] type:[[GGBluetooth sharedManager] writeableCharacteristic].properties==(CBCharacteristicPropertyRead+CBCharacteristicPropertyWriteWithoutResponse+CBCharacteristicPropertyNotify)?CBCharacteristicWriteWithoutResponse:CBCharacteristicWriteWithResponse];
                
                [SVProgressHUD showWithStatus:@"正在开锁"];
                
            }else{
                if (b.rfidData) {
                    //                self.lastUnlock = nil;
                    
                    self.deviceInfoView.hidden = YES;
                    //                [SVProgressHUD showErrorWithStatus:@"RFID 不匹配"];
                    [SVProgressHUD dismiss];
                    
                    if ([[self.locksAll valueForKey:@"rfid"] containsObject:b.rfid]) {
                        
                        self.lastUnlock = [self.locksAll objectAtIndex:[[self.locksAll valueForKey:@"rfid"]indexOfObject:b.rfid]];
                        [self.history addObject:@{@"rfid":STRING_OR_EMPTY(b.rfid),@"name":@"权限范围外设备",@"time":[[NSDate date]  stringWithFormat:@"yyyy-MM-dd hh:mm:ss"],@"result":@"越权未遂"}];
                        
                    }else{
                        
                        self.lastUnlock = nil;
                        [self.history addObject:@{@"rfid":STRING_OR_EMPTY(b.rfid),@"name":@"未知设备",@"time":[[NSDate date]  stringWithFormat:@"yyyy-MM-dd hh:mm:ss"],@"result":@"操作失败"}];
                    }
                    [self.tableView reloadData];
                    
                    if (self.lastUnlock) {
                        if (self.mode == AuthenticationControllerModeTemptask) {
                            //                        [self.lastUnlock createUnlockHistorySuccess:NO verifyAt:nil verifyBy:nil action:@"temp"];
                            [self upUnlockHistorySuccess:self.lastUnlock isSusccess:NO verifyAt:nil verifyBy:nil action:@"temp"];
                        }else if(self.mode == AuthenticationControllerModeNormal){
                            //                        [self.lastUnlock createUnlockHistorySuccess:NO verifyAt:nil verifyBy:nil action:@"fixed"];
                            [self upUnlockHistorySuccess:self.lastUnlock isSusccess:NO verifyAt:nil verifyBy:nil action:@"fixed"];
                            
                        }else if(self.mode == AuthenticationControllerModeWorkBill){
                            [self upUnlockHistorySuccess:self.lastUnlock isSusccess:NO verifyAt:nil verifyBy:nil action:@"workbill"];
                        }else if(self.mode == AuthenticationControllerModeWorkPatrol){
//                             [self makeOrderWithResult:255 lock:self.lastUnlock];
                            
                             [self upUnlockHistorySuccess:self.lastUnlock isSusccess:NO verifyAt:nil verifyBy:nil action:@"fixed"];
                        }else{//offlineOperation
                            [self initOfflineKeyOperation:ZNSOfflineKeyResultOrderWarnning lid:[self.lastUnlock.lid integerValue] action:@"fixed" unlock_at:[ZNSDate getCurrentTime] isSeverError:NO];
                            
                        }
                    }
                    //                [self.history addObject:@{@"rfid":STRING_OR_EMPTY(self.lastUnlock.rfid),@"name":self.lastUnlock.name?self.lastUnlock.name:@"未知设备",@"time":[[NSDate date]  stringWithFormat:@"yyyy-MM-dd hh:mm:ss"],@"result":@"越权未遂"}];
                    
                    
                }else if(b.on){
                    
                    if (self.lastUnlock) {
                        [SVProgressHUD showSuccessWithStatus:@"已开锁"];
                        
                        if (self.mode == AuthenticationControllerModeTemptask) {
                            //                        [self.lastUnlock createUnlockHistorySuccess:YES verifyAt:nil verifyBy:nil action:@"temp"];
                            [self upUnlockHistorySuccess:self.lastUnlock isSusccess:YES verifyAt:nil verifyBy:nil action:@"temp"];
                        }else if(self.mode == AuthenticationControllerModeNormal){
                            //                        [self.lastUnlock createUnlockHistorySuccess:YES verifyAt:nil verifyBy:nil action:@"fixed"];
                            [self upUnlockHistorySuccess:self.lastUnlock isSusccess:YES verifyAt:nil verifyBy:nil action:@"fixed"];
                        }else if(self.mode == AuthenticationControllerModeWorkBill){
                            [self upUnlockHistorySuccess:self.lastUnlock isSusccess:YES verifyAt:nil verifyBy:nil action:@"workbill"];
                        }else if(self.mode == AuthenticationControllerModeWorkPatrol){
//                            [self makeOrderWithResult:6 lock:self.lastUnlock];
                             [self upUnlockHistorySuccess:self.lastUnlock isSusccess:YES verifyAt:nil verifyBy:nil action:@"fixed"];
                        }else{//offlineOperation
                            [self initOfflineKeyOperation:ZNSOfflineKeyResultOrderNormal lid:[self.lastUnlock.lid integerValue] action:@"fixed" unlock_at:[ZNSDate getCurrentTime] isSeverError:NO];
                            
                        }
                        
                        [self.history addObject:@{@"rfid":STRING_OR_EMPTY(self.lastUnlock.rfid),@"name":STRING_OR_EMPTY(self.lastUnlock.name),@"time":[[[NSDate date] dateByAddingHours:0] stringWithFormat:@"yyyy-MM-dd hh:mm:ss"],@"result":@"正常操作"}];
                    }
                    
                    [self.tableView reloadData];
                }else{
                    self.deviceInfoView.hidden = YES;
                    [SVProgressHUD showErrorWithStatus:@"开锁失败"];
                    
                    if (self.lastUnlock) {
                        if (self.mode == AuthenticationControllerModeTemptask) {
                            //                        [self.lastUnlock createUnlockHistorySuccess:NO verifyAt:nil verifyBy:nil action:@"temp"];
                            [self upUnlockHistorySuccess:self.lastUnlock isSusccess:NO verifyAt:nil verifyBy:nil action:@"temp"];
                        }else if(self.mode == AuthenticationControllerModeNormal){
                            //                        [self.lastUnlock createUnlockHistorySuccess:NO verifyAt:nil verifyBy:nil action:@"fixed"];
                            [self upUnlockHistorySuccess:self.lastUnlock isSusccess:NO verifyAt:nil verifyBy:nil action:@"fixed"];
                        }else if(self.mode == AuthenticationControllerModeWorkBill){
                            [self upUnlockHistorySuccess:self.lastUnlock isSusccess:NO verifyAt:nil verifyBy:nil action:@"workbill"];
                        }else if(self.mode == AuthenticationControllerModeWorkPatrol){
//                             [self makeOrderWithResult:255 lock:self.lastUnlock];
                             [self upUnlockHistorySuccess:self.lastUnlock isSusccess:NO verifyAt:nil verifyBy:nil action:@"fixed"];
                        }else{
                            //offlineOperation
                            [self initOfflineKeyOperation:ZNSOfflineKeyResultOrderWarnning lid:[self.lastUnlock.lid integerValue] action:@"fixed" unlock_at:[ZNSDate getCurrentTime] isSeverError:NO];
                            
                        }
                    }
                }
            }
        }
        
    }];
}
- (void)initOfflineKeyOperation:(NSInteger)result lid:(NSInteger)lid
                         action:(NSString*)action unlock_at:(NSString*)unlock_at isSeverError:(BOOL)isSeverError{
    
    self.offlineKeyOperation.result = result;
    self.offlineKeyOperation.lid = lid ;
    self.offlineKeyOperation.action = action;
    self.offlineKeyOperation.unlock_at = unlock_at;
    [self creatOfflineKeyOperation:self.offlineKeyOperation isSeverError:isSeverError];
    
    
}
- (void) creatOfflineKeyOperation:(ZNSOfflineKeyOperation *)offlineKeyOperation isSeverError:(BOOL)isSeverError{
    
    NSArray * offlineArr = [ZNSCache  getOfflineKeyOperation:[ZNSUser currentUser].username];
    offlineArr = [offlineArr arrayByAddingObject:offlineKeyOperation];
    if (isSeverError) {
       [ZNSCache cacheOnlineKeyOperation:offlineArr withUserName:[ZNSUser currentUser].username];
    }else{
       [ZNSCache cacheOfflineKeyOperation:offlineArr withUserName:[ZNSUser currentUser].username];
    }
}

- (void)upUnlockHistorySuccess:(ZNSLock*)lock isSusccess:(BOOL)susccess verifyAt:(NSString *)verifyAt verifyBy:(NSString *)verifyBy action:(NSString *)action{
    
    NSMutableDictionary *paramers = [NSMutableDictionary dictionary];
    [paramers setObject:lock.lid forKey:@"lid"];
    [paramers setObject:susccess?@6:@255 forKey:@"result"];
    [paramers setObject:action forKey:@"action"];
    [paramers setObject:self.mode==AuthenticationControllerModeTemptask? self.tempTask.tid:@"" forKey:@"tid"];
//    NSDictionary *dic = @{
//                           @"lid":lock.lid,
//                           @"result":susccess?@6:@255,
//                           @"action":action,
//                           @"tid":self.mode==AuthenticationControllerModeTemptask? self.tempTask.tid:@"",
//                          };
    
    if (self.mode == AuthenticationControllerModeWorkBill){
        [paramers setObject:[NSNumber numberWithString:self.skid] forKey:@"workBillId"];
    }
    [APIClient POST:@"history/new" withParameters:paramers
   successWithBlcok:^(id response) {
       NSLog(@"tid:%@",self.mode==AuthenticationControllerModeTemptask? self.tempTask.tid:@"nil");
       
   } errorWithBlock:^(ZNSError *error) {
       
       if([error.errorMessage isEqualToString: @"服务器出错！"]){//服务器出错，先缓存操作记录
           if (susccess == YES) {
                [self initOfflineKeyOperation:ZNSOfflineKeyResultOrderNormal lid:[self.lastUnlock.lid integerValue] action:@"fixed" unlock_at:[ZNSDate getCurrentTime] isSeverError:YES];
               
           }else{
               
               [self initOfflineKeyOperation:ZNSOfflineKeyResultOrderWarnning lid:[self.lastUnlock.lid integerValue] action:@"fixed" unlock_at:[ZNSDate getCurrentTime] isSeverError:YES];
           }
          
           
       }
   }];
}



- (NSArray *)locks{
    if (!_locks) {
        _locks = @[];
    }
    return _locks;
}

- (void)loadData{
    [SVProgressHUD showWithStatus:@"加载设备数据"];
    
    {
        ZNSUser *user = [ZNSUser currentUser];
        [self.nameLabel setText:user.name];
        [self.phoneLabel setText:user.username];
        [self.sectionLabel setText:user.section];
        
    }
    self.locksAll = [ZNSCache getAllLocksetsFromAllObjects];
    
    if (self.mode == AuthenticationControllerModeNormal) {
        @weakify(self);
       
        
        [APIClient POST:[NSString stringWithFormat:@"user/%@/object/",[ZNSUser currentUser].username] withParameters:@{@"page":@(-1)} successWithBlcok:^(id response) {
            @strongify(self);
            self.locks = @[];
            NSArray *objects = [NSArray yy_modelArrayWithClass:[ZNSObject class] json:[response valueForKey:@"items"]];
            
            [objects enumerateObjectsUsingBlock:^(ZNSObject *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                @strongify(self);
                self.locks = [self.locks arrayByAddingObjectsFromArray:obj.lockset];
            }];
            
            [SVProgressHUD dismiss];
            
        } errorWithBlock:^(ZNSError *error) {

            [SVProgressHUD showErrorWithStatus:error.errorMessage];
            @strongify(self);
            self.locks = @[];
        }];
//        self.locks = [ZNSCache findLockersCacheOfUserName:[ZNSUser currentUser].username];
//        [SVProgressHUD dismiss];

    }else if (self.mode == AuthenticationControllerModeTemptask) {
            @weakify(self);
            [APIClient POST:[NSString stringWithFormat:@"object/%@/",self.optionObject.oid] withParameters:@{} successWithBlcok:^(id locks) {
                @strongify(self);
                self.locks = [NSArray yy_modelArrayWithClass:[ZNSLock class] json:[locks valueForKey:@"items"]];
                [SVProgressHUD dismiss];
            } errorWithBlock:^(ZNSError *error) {
                [SVProgressHUD showErrorWithStatus:error.errorMessage];
            }];
    }else if (self.mode == AuthenticationControllerModeWorkBill){
//        @weakify(self);
//        [APIClient POST:[NSString stringWithFormat:@"object/%@/",self.objectOid] withParameters:@{} successWithBlcok:^(id locks) {
//            @strongify(self);
//            self.locks = [NSArray yy_modelArrayWithClass:[ZNSLock class] json:[locks valueForKey:@"items"]];
//            [SVProgressHUD dismiss];
//        } errorWithBlock:^(ZNSError *error) {
//            [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
//        }];
        self.locks = self.workBillLocks;
        [SVProgressHUD dismiss];
        
    }else if(self.mode == AuthenticationControllerModeWorkPatrol){
        self.locks = self.optionObject.lockset;
        [SVProgressHUD dismiss];

    }else{
        self.locks = [ZNSCache findOfflineDataOfUserName:[ZNSUser currentUser].username].locks;
        [SVProgressHUD dismiss];
    }
    
}
- (IBAction)pair:(id)sender{
    [BlueToothViewController presentInViewContoller:self.navigationController];
}

- (void)goPait{
    [BlueToothViewController presentInViewContoller:self.navigationController];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.history.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    HistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[HistoryTableViewCell reuseIdentifier]];
    NSDictionary *dic = [self.history objectAtIndex:self.history.count-indexPath.row-1];
    //    @"rfid":b.rfid,@"name":self.lastUnlock.name,@"time":[[[NSDate date] dateByAddingHours:8] stringWithFormat:@"yyyy-MM-dd hh:mm:ss"],@"result":@"开锁成功"
    [cell.objectNameLabel setText:[dic valueForKey:@"name"]];
    [cell.userNameLabel setText:[NSString stringWithFormat:@"锁码：%@",[[dic valueForKey:@"rfid"] uppercaseString]]];
    [cell.dateNameLabel setText:[dic valueForKey:@"time"]];
    [cell.resultLabel setText:[dic valueForKey:@"result"]];
    if ([[dic valueForKey:@"result"]isEqualToString:@"开锁成功"]||[[dic valueForKey:@"result"]isEqualToString:@"正常操作"]) {
        [cell.resultLabel setTextColor:[UIColor greenColor]];
    }else{
        [cell.resultLabel setTextColor:[UIColor redColor]];
    }
    if ([[dic allKeys] containsObject:@"state"]) {
        NSNumber *mState = [dic valueForKey:@"state"];
        if (mState != nil && [mState intValue] == 1) {
            [cell.resultLabel setTextColor:[UIColor redColor]];
        }
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    @weakify(self);
    return [tableView fd_heightForCellWithIdentifier:[HistoryTableViewCell reuseIdentifier] configuration:^(HistoryTableViewCell *cell) {
        @strongify(self);
        NSDictionary *dic = [self.history objectAtIndex:indexPath.row];
        //    @"rfid":b.rfid,@"name":self.lastUnlock.name,@"time":[[[NSDate date] dateByAddingHours:8] stringWithFormat:@"yyyy-MM-dd hh:mm:ss"],@"result":@"开锁成功"
        [cell.objectNameLabel setText:[dic valueForKey:@"name"]];
        [cell.userNameLabel setText:[NSString stringWithFormat:@"锁码：%@",[[dic valueForKey:@"rfid"] uppercaseString]]];
        [cell.dateNameLabel setText:[dic valueForKey:@"time"]];
        [cell.resultLabel setText:[dic valueForKey:@"result"]];
    }];
}

#pragma mark - 上传操作记录

@end
