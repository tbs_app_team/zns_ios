//
//  RemoteUnlockDetailController.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/12.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import "RemoteUnlockDetailController.h"
#import "RemoteUnlockDetailCell.h"

@interface RemoteUnlockDetailController ()<UITableViewDelegate,UITableViewDataSource>{
    
    IBOutlet UITableView *tbvLock;
    
//    NSMutableArray *locks;
    
}

@end

@implementation RemoteUnlockDetailController

- (void)doUnlockingWith:(ZNSLock *)lk index:(NSInteger)idx{
    @weakify(self);
    [SVProgressHUD show];
    [APIClient POST:@"controllerLog/unlock" withParameters:@{@"oid":lk.oid,@"doorcode":lk.doorcode,@"lockName":lk.name} successWithBlcok:^(id response) {
        @strongify(self);
//        lk.lock_status_desc = @"开启";
//        lk.lock_status = @"1";
//        [tbvLock reloadData];
        [SVProgressHUD showSuccessWithStatus:@"请求成功"];
        
        if (self.updateCallback) {
            self.updateCallback();
        }
        
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"远程开锁详情";
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [tbvLock registerNib:[RemoteUnlockDetailCell nib] forCellReuseIdentifier:[RemoteUnlockDetailCell reuseIdentifier]];
    
    //处理锁状态信息
//    locks = [NSMutableArray array];
//    NSDictionary *log = self.object.controllerlog.firstObject;
//    NSInteger i = 0;
//    for (ZNSLock *lock in self.object.lockset) {
//        NSString *status = @"-";
//        if ([lock.lock_status length]) {
//            Boolean sts = [log[[NSString stringWithFormat:@"lock_status%@",@(i+1).stringValue]] boolValue];
//            status = [lock.lock_status integerValue]?@"开启":@"关闭";
//        }
//        lock.lock_status_desc = status;
//        i++;
//    }
    [tbvLock reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - tableview delegate & datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.object.lockset.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RemoteUnlockDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:[RemoteUnlockDetailCell reuseIdentifier] forIndexPath:indexPath];
    ZNSLock *lock = self.object.lockset[indexPath.row];
    cell.lock = lock; 
    @weakify(self);
    cell.unlockBlock = ^{
        @strongify(self);
        [self doUnlockingWith:self.object.lockset[indexPath.row] index:indexPath.row];
    };
    return cell;
}

@end
