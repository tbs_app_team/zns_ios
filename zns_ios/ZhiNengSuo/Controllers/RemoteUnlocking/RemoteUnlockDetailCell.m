//
//  RemoteUnlockDetailCell.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/12.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import "RemoteUnlockDetailCell.h"

@interface RemoteUnlockDetailCell(){
    
    IBOutlet UILabel *lockName;
    IBOutlet UILabel *lockStatus;
    IBOutlet UIButton *btnUnlock;
    IBOutlet UIImageView *imgDoorStatus;
    IBOutlet UIImageView *imgLockStatus;
}

@end

@implementation RemoteUnlockDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setLock:(ZNSLock *)lock{
    if (!lock) {
        return;
    }
    lockName.text = lock.name;
    btnUnlock.enabled = ![lock.type isEqualToString:@"屏柜门锁"];
//    lockStatus.text = lock.lock_status_desc;
    imgDoorStatus.image = [UIImage imageNamed:[lock.gate_status integerValue]?@"door_open":@"door_close"];
    imgLockStatus.image = [UIImage imageNamed:[lock.lock_status integerValue]?@"remote_unlock_open":@"remote_unlock_close"];
}

- (IBAction)unlockAction:(UIButton *)sender {
    
    if (self.unlockBlock) {
        self.unlockBlock();
    }
} 

@end
