//
//  RemoteUnlockDetailCell.h
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/12.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RemoteUnlockDetailCell : UITableViewCell

@property (nonatomic,copy) voidBlock unlockBlock;
@property (nonatomic,strong) ZNSLock *lock; 


@end
