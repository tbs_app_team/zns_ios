//
//  RemoteUnlockDetailController.h
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/12.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RemoteUnlockDetailController : UIViewController

@property (nonatomic,strong) ZNSObject *object;

@property (nonatomic,strong) dispatch_block_t updateCallback;

@end
