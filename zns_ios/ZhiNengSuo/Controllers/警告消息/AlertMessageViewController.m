//
//  AlertMessageViewController.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 16/1/22.
//  Copyright © 2016 czwen. All rights reserved.
//

#import "AlertMessageViewController.h"
#import "AlertMessageTableViewCell.h"
#import "ZNSAlert.h"
@interface AlertMessageViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSArray *datas;
@property (nonatomic,strong) NSNumber *page;
@end

@implementation AlertMessageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"警告消息";
    self.datas = self.alert;
     /* test
    ZNSAlert *test = [[ZNSAlert alloc] init];
    test.message =@"警告消息警告消息警告消息警告消息警告消息警告消息警告消息警告消息警告消息警告消息警告消息警告消息警告消息警告消息警告消息";
    test.time =@"20160304120340";
    self.datas = [NSArray array];
    self.datas = [self.datas arrayByAddingObject:test]; 
     */
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[AlertMessageTableViewCell nib] forCellReuseIdentifier:[AlertMessageTableViewCell reuseIdentifier]];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100;
 
//    @weakify(self);
//    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        @strongify(self);
////        [self loadDataWithPage:@0];
//    }];
//    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        @strongify(self);
////        [self loadDataWithPage:@(self.page.integerValue+1)];
//    }];
//    [self.tableView.mj_header beginRefreshing];
}

- (void)loadDataWithPage:(NSNumber *)page{
    @weakify(self);
    [APIClient POST:API_ALERT withParameters:@{@"page":page} successWithBlcok:^(id response) {
        @strongify(self);
        self.page = page;
        if ([page isEqual:@0]) {
            self.datas = [NSArray yy_modelArrayWithClass:[ZNSAlert class] json:[response valueForKey:@"items"]];
            
        }else{
            
            self.datas = [self.datas arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSAlert class] json:[response valueForKey:@"items"]]];

        }
        
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        
//        if ([[response valueForKey:@"page"] isEqual:[response valueForKey:@"pages"]]) {
//            [self.tableView.mj_footer endRefreshingWithNoMoreData];
//        }
        
        if (self.datas.count == [response[@"total"] integerValue]) {
             [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }
        [self.tableView reloadData];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AlertMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[AlertMessageTableViewCell reuseIdentifier]];
    ZNSAlert *alert = [self.datas objectAtIndex:indexPath.row];
    
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init] ;
    [inputFormatter setDateFormat: @"yyyyMMddHHmmss"];
    [inputFormatter setLocale:[[NSLocale alloc]initWithLocaleIdentifier:@"zh_CN"] ];
     NSDate *inputDate =[[NSDate alloc]init];
    inputDate = [inputFormatter dateFromString:alert.time];
    
    NSDateFormatter* df2 = [[NSDateFormatter alloc]init];
    [df2 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString* str1 = [df2  stringFromDate:inputDate];
    [cell.dateLabel setText:str1];
    
//    [cell.dateLabel setText:[[NSDate dateWithTimeIntervalSince1970:(alert.time.time.integerValue/1000)] stringWithFormat:@"yyyy-MM-dd hh:mm"]];
//    [[cell messageLabel] setLineBreakMode: NSLineBreakByCharWrapping];
//    [cell messageLabel].numberOfLines = 0;
//    [[cell messageLabel] sizeToFit];
//    [cell sizeToFit];
    [cell.messageLabel setText:alert.message];
    return cell;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return [tableView fd_heightForCellWithIdentifier:[AlertMessageTableViewCell reuseIdentifier] configuration:^(AlertMessageTableViewCell *cell) {
//        ZNSAlert *alert = [self.datas objectAtIndex:indexPath.row];
//
//        NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init] ;
//        [inputFormatter setDateFormat: @"yyyyMMddHHmmss"];
//        [inputFormatter setLocale:[[NSLocale alloc]initWithLocaleIdentifier:@"zh_CN"] ];
//        NSDate *inputDate =[[NSDate alloc]init];
//        inputDate = [inputFormatter dateFromString:alert.time];
//
//        NSDateFormatter* df2 = [[NSDateFormatter alloc]init];
//        [df2 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//        NSString* str1 = [df2  stringFromDate:inputDate];
//        [cell.dateLabel setText:str1];
////        [cell.dateLabel setText:[[NSDate dateWithTimeIntervalSince1970:(alert.time.time.integerValue/1000)] stringWithFormat:@"yyyy-MM-dd hh:mm"]];
//        [[cell messageLabel] setLineBreakMode: NSLineBreakByCharWrapping];
//        [cell messageLabel].numberOfLines = 0;
//        [[cell messageLabel] sizeToFit];
//        [cell sizeToFit];
//        [cell.messageLabel setText:alert.message];
//    }];
//}
@end
