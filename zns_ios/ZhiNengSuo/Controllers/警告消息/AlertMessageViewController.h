//
//  AlertMessageViewController.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 16/1/22.
//  Copyright © 2016 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertMessageViewController : UIViewController

@property (nonatomic,strong)  NSNumber* alertCount;
@property (nonatomic,strong)  NSArray * alert;

@end
