//
//  MainMapItemCell.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/3.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import "MainMapItemCell.h"

@interface MainMapItemCell(){
    
    IBOutlet MKMapView *mapView;
    
}

@end;

@implementation MainMapItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSites:(NSArray *)sites{
    
    for (ZNSObject *obj in sites) {
        MKPointAnnotation *ann = [[MKPointAnnotation alloc] init];
        ann.coordinate = CLLocationCoordinate2DMake([obj.lat floatValue], [obj.lng floatValue]);
        [mapView addAnnotation:ann];
    } 
}

@end
