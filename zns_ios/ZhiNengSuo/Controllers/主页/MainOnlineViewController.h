//
//  MainOnlineViewController.h
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/26.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainOnlineViewController : UIViewController
@property (nonatomic,assign) BOOL isOnline;
@end
