//
//  MainOnlineViewController.m
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/26.
//  Copyright © 2015年 czwen. All rights reserved.

#import "MainOnlineViewController.h"
#import "TempPermissionUnlockViewController.h"

#import "OperationCollectionViewCell.h"
#import "OfflineAuthenticationApprovalViewController.h"
#import "RegisterUserApprovalViewController.h"
#import "SystemSettingsViewController.h"
#import "TempPermissionApprovalViewController.h"
#import "CollectLocksViewController.h"
#import "OnlineAuthenticationSmartKeyViewController.h"
#import "HistoryViewController.h"
#import "ZNSGiveBackKeyConfirmViewController.h"
#import "ZNSOffline.h"
#import "MZTimerLabel.h"
#import <SIOSocket/SIOSocket.h>
#import "ZhiNengSuo-Swift.h"
#import "GGJSONKit.h"
#import "AlertMessageViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "AddLockViewController.h"
#import "ZNSBorrowKeyViewController.h"
#import "ZNSGiveBackKeyViewController.h"
#import "ZNSAlert.h"
#import "ZNSOfflineKeyOperation.h"
#import "ZNSOfflineBtOperation.h"
#import "ZNSBorrowKeyConfirmViewController.h"
#import "MainItemCell.h"
#import "ZNSExecuteUnlockHelper.h"
#import "ZNSTools.h"
#import "MainMapItemCell.h"
#import "MyController.h"
#import "ScanQRCodeController.h"
#import "BuletoothUnlockController.h"
#import "AlertController.h"


@interface MainOnlineViewController ()<UICollectionViewDataSource, UICollectionViewDelegate,NSURLSessionDelegate,UITabBarDelegate,UIScrollViewDelegate> {
    NSArray * arrOperate;
    NSArray * arrOperateClassName;
    
    NSArray *arrItemConfig;
  
    NSInteger tabbarSelectedIdx;
    
    NSInteger offlineCount;              //离线鉴权审批未读数量
    NSInteger tempTaskCount;             //临时权限审批未读数量
    NSInteger registerCount;             //注册用户审批未读数量
    NSInteger workbillCount;             //进站审批未读数量
//    NSInteger alertCount;                //告警未读数量
    BOOL noticeIsNew;                    //是否有新的公告
    
    UIScrollView *scroll;
    
    NSMutableArray *arrColls;
    IBOutlet NSLayoutConstraint *tabbar_h;
    IBOutlet UIButton *btnScanCode;
    IBOutlet UIButton *btnMy;
    
    NSArray *mapSites;
}

@property (weak, nonatomic) IBOutlet UICollectionView *operateCollectView;
@property (weak, nonatomic) IBOutlet UILabel *offlineCountdownLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoBottom;
@property (strong, nonatomic) IBOutlet UIView *vMainBg;

@property (nonatomic,strong) ZNSOffline *offline;
//@property (nonatomic,strong) NSTimer *timer;
@property (nonatomic,strong)  MZTimerLabel *timer;

@property (nonatomic,strong) NSNumber *offlineApproveCount;
@property (nonatomic,strong) NSNumber *tempApproveCount;
@property (nonatomic,strong) NSNumber *registerApproveCount;
@property (nonatomic,assign) NSInteger alertCount;
@property (nonatomic,strong) NSMutableArray *alerts;
@property (nonatomic,assign) BOOL isFirstView;
@property (nonatomic,assign) BOOL isOffView;
@property (nonatomic,assign) BOOL isOffOver;

@property (nonatomic,strong) SocketIOClient *socket;
@property (weak, nonatomic) IBOutlet UITabBar *myTabBar;
@property (nonatomic,weak) IBOutlet UIButton *messageButton;
@end

@implementation MainOnlineViewController

- (void)doGetSiteInfoOnMap{
    [SVProgressHUD showWithStatus:@"正在加载地图站点信息"];
    [APIClient POST:@"object/map3" withParameters:nil successWithBlcok:^(id response) {
        [SVProgressHUD dismiss];
        mapSites = [NSArray yy_modelArrayWithClass:[ZNSObject class] json:response[@"items"]];;
        UICollectionView *coll = arrColls[2];
        [coll reloadData];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
}

- (void)doGetUnlockOrder{

    if (!self.isOnline) {
        return;
    }
    
    [SVProgressHUD show];
    [APIClient POST:API_UNLOCK_ORDER withParameters:@{} successWithBlcok:^(id response){
        [SVProgressHUD dismiss];
        
        NSLog(@"===>%@",response);
        NSDictionary *dic = response;
        [[NSUserDefaults standardUserDefaults] setObject:dic[@"items"] forKey:FIRST_UNLOCK];
        
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
}

- (void)doGetUnlockProtocol{
    
    if (!self.isOnline) {
        return;
    }
    
    [SVProgressHUD show];
    [APIClient POST:API_UNLOCK_PROTOCOL withParameters:@{} successWithBlcok:^(id response){
        [SVProgressHUD dismiss];
        
        NSLog(@"===>%@",response);
        NSDictionary *dic = response;
        NSArray *items = dic[@"items"];
        if (items && items != nil && items != NULL && items.count > 0) {
            NSDictionary *item = items[0];
            if ([item containsObjectForKey:@"value"]) {
                id typeValue = item[@"value"];
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt: [typeValue intValue]] forKey:LOCK_TYPE];
                 [[NSUserDefaults standardUserDefaults] setObject:@1 forKey:LOCK_TYPE];
                return ;
            }
        }
        [[NSUserDefaults standardUserDefaults] setObject:@1 forKey:LOCK_TYPE];
        
    } errorWithBlock:^(ZNSError *error) {
//        [SVProgressHUD showErrorWithStatus:error.errorMessage];
        [[NSUserDefaults standardUserDefaults] setObject:@1 forKey:LOCK_TYPE];
    }];
}

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [self makeCache];
    
    scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 54,kScreenSize.width, kScreenSize.height)];
    scroll.pagingEnabled = YES;
    scroll.showsHorizontalScrollIndicator = NO;
    scroll.delegate = self;
    [self.vMainBg addSubview:scroll];
    [self.vMainBg sendSubviewToBack:scroll];
    
    self.alerts = [NSMutableArray array];
    self.operateCollectView.backgroundColor = [UIColor clearColor];
    //设置底部tabbar相关的item
    [self setupTabbarItems];
    //初始化相关配置
    [self initializeUI];
    //加载相关缓存
    [self setupCache];
    //设置sokcet相关连接
    [self setupSocket];
    
    [self doGetUnlockOrder];
    [self doGetUnlockProtocol];
    [self doGetSiteInfoOnMap];
}
- (void)dealloc{
//    [self.socket close];
    [self.socket disconnect];
    self.socket = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.isOnline) {
        NSArray *tem1 = [ZNSCache getOnlineKeyOperation:[ZNSUser currentUser].username];
        if (tem1.count) {
            [APIClient POST:@"operation/offline/do" withParameters:@{@"operation":[tem1 yy_modelToJSONObject]} successWithBlcok:^(id response) {
                
                [ZNSCache cacheOnlineKeyOperation: nil withUserName:[ZNSUser currentUser].username];
                
            } errorWithBlock:^(ZNSError *error) {
                
                NSLog(@"%@",error.errorMessage);
            }];
        }
        if (self.socket.status == SocketIOClientStatusNotConnected) {
            [self.socket reconnect];
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Socket

- (void)setupSocket{
    
    if (self.isOnline) {
        NSURL *url = [NSURL URLWithString:API_HOST];
        NSString *alertPort = API_ALERT_PORT;
         
        NSURLComponents *urlComponents = [[NSURLComponents alloc]init];
        
        urlComponents.scheme = @"http";
        urlComponents.host = url.host;
//                urlComponents.port = @18889;
        urlComponents.port = [alertPort numberValue];
        NSLog(@"%@",[alertPort numberValue]);
        
        @weakify(self);
        
        //        SocketIOClient* socket = [[SocketIOClient alloc] initWithSocketURL:urlComponents.URL.absoluteString options:@{@"log": @YES, }];
        SocketIOClient *socket = [[SocketIOClient alloc] initWithSocketURL:urlComponents.URL config:@{@"log": @YES, }];
        self.socket = socket;
        [socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
            NSLog(@"socket connected");
            [ack with:@[@"ios"]];
            //            [socket emit:@"connectin" withItems:@[@{@"message":@"ios",@"k":[ZNSUser currentUser].token}]];
            NSString *token = [ZNSUser currentUser].token;
            if ([token length]) {
                [socket emit:@"connectin" with:@[@{@"message":@"ios",@"k":token}]];
            }
        }];
        
        [socket on:@"connectin" callback:^(NSArray *data, SocketAckEmitter *ack) {
            [ack with:@[@"ios"]];
            
        }];
        
        [socket on:@"error" callback:^(NSArray* data, SocketAckEmitter* ack) {
            [socket reconnect];
            //            [socket emit:@"connectin" withItems:@[@{@"message":@"ios",@"k":[ZNSUser currentUser].token}]];
            
        }];
        
        [socket on:@"alert" callback:^(NSArray* data, SocketAckEmitter* ack) {
            @strongify(self);
            [self playRingtone];
//            self.alertCount = @(self.alertCount.integerValue+1);
//            [self.messageButton setTitle:[NSString stringWithFormat:@"警告消息（%@）",self.alertCount] forState:UIControlStateNormal];
            
            //            self.alerts = [NSArray yy_modelArrayWithClass:[ZNSAlert class] json:[data valueForKey:@"alert"]];
            
            NSArray *temAlerts  = [NSArray yy_modelArrayWithClass:[ZNSAlert class] json:data] ;
            //           self.alerts = [self.alerts arrayByAddingObjectsFromArray:temAlerts];
            [self.alerts addObjectsFromArray:temAlerts];
            self.alertCount = self.alerts.count;
            //更新角标
            UICollectionView *coll = arrColls[2];
            [coll reloadData];
            [ack with:@[@"Got your alert"]];
        }];
        
        [socket on:@"verify" callback:^(NSArray* data, SocketAckEmitter* ack) {
            @strongify(self);
//            [self loadNumberData];
            for (NSDictionary *d in data) {
                NSDictionary *msg = [self dictionaryWithJsonString:d[@"message"]];
                if (msg) {
                    offlineCount = [msg[@"offline"] integerValue];
                    tempTaskCount = [msg[@"temp_task"] integerValue];
                    registerCount = [msg[@"register"] integerValue];
                    workbillCount = [msg[@"workbill"] integerValue];
                }
            }
            
            UICollectionView *coll = arrColls[3];
            [coll reloadData];
//            [self.operateCollectView reloadData];
            [ack with:@[@"Got your verifynews"]];
        }];
        
        [socket on:@"notice" callback:^(NSArray* data, SocketAckEmitter* ack) {
            @strongify(self);
            for (NSDictionary *d in data) {
                NSDictionary *msg = [self dictionaryWithJsonString:d[@"message"]];
                if (msg) {
                    noticeIsNew = YES;
                }
            }
//            [self.operateCollectView reloadData];
            [ack with:@[@"Got your verifynews"]];
        }];
        [socket connect];
    }
}


- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    
    if (jsonString == nil) {
        return nil;
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}

#pragma mark - Cache

- (void)setupCache{
    if (self.isOnline) {
        // cache locks
        [self makeCache];
        NSArray * tem = [ZNSCache getOfflineKeyOperation:[ZNSUser currentUser].username];
        if(tem.count){
            [APIClient POST:@"history/offline/new" withParameters:@{@"history":[tem yy_modelToJSONObject]} successWithBlcok:^(id response) {
                
                [ZNSCache cacheOfflineKeyOperation:nil withUserName:[ZNSUser currentUser].username];
                
            } errorWithBlock:^(ZNSError *error) {
                NSLog(@"%@",error.errorMessage);
            }];
        }
        ZNSOffline *offline = [ZNSCache findOfflineDataOfUserName:[ZNSUser currentUser].username];
        //有离线任务，并且过期，清票
        if ((offline.beginAt.integerValue+offline.duration.integerValue*60)<[NSDate date].timeIntervalSince1970 && offline!=nil){
            
            [APIClient POST:[NSString stringWithFormat:@"offline/%@/action_at/%@",offline.oid,[[[NSDate dateWithTimeIntervalSince1970:offline.beginAt.integerValue] dateByAddingHours:8] stringWithFormat:@"YYYYMMDDHHmmss"]] withParameters:@{} successWithBlcok:^(id response) {
                [ZNSCache cacheOffline:nil withUserName:[ZNSUser currentUser].username];
                [ZNSAPITool cacheOfflineDataSuccessWithBlcok:nil errorWithBlock:nil];
                
            } errorWithBlock:^(ZNSError *error) {
            }];
        }else{//没有离线任务 或者没有过期
            if (offline == nil) {
                [ZNSAPITool cacheOfflineDataSuccessWithBlcok:nil errorWithBlock:nil];
            }
        }
    }else{
        self.messageButton.hidden = YES;
        self.offline = [ZNSCache findOfflineDataOfUserName:[ZNSUser currentUser].username];
        self.timer = [[MZTimerLabel alloc]initWithLabel:self.offlineCountdownLabel andTimerType:MZTimerLabelTypeTimer];
        self.timer.timeFormat = @"离线鉴权剩余时间 HH:mm:ss";
        
        if (self.offline.beginAt.integerValue > 0) {
            [self.timer setCountDownTime:self.offline.duration.integerValue*60-([NSDate date].timeIntervalSince1970-self.offline.beginAt.integerValue)];
            [self.timer start];
        }else{
//            self.offline.beginAt = [NSString stringWithFormat:@"%ld", (long)[NSDate date].timeIntervalSince1970];
//            [ZNSCache cacheOffline:self.offline withUserName:[ZNSUser currentUser].username];
            [self.timer setCountDownTime:self.offline.duration.integerValue*60];
//            [self.timer start];
        }
    }
}


- (void)makeCache{
    
    //    [ZNSAPITool cacheOfflineDataSuccessWithBlcok:nil errorWithBlock:nil];
    
    // cache smart keys
    [ZNSSmartKey getAllSmartKeySuccess:^(id response) {
        
        [ZNSCache cacheSmartKeys:response withUserName:[ZNSUser currentUser].username];
    } error:nil];
    
    [APIClient POST:API_SECTION_OBJECT_WITH_SID([ZNSUser currentUser].sid) withParameters:@{@"page":@-1} successWithBlcok:^(id response) {
        [ZNSCache cacheObjects:[response objectForKey:@"items"]withUserName:[ZNSUser currentUser].username];
    } errorWithBlock:^(ZNSError *error) {
        
    }];
    
    [APIClient POST:API_OBJECT withParameters:@{@"page":@-1} successWithBlcok:^(id response) {
        [ZNSCache cacheAllObjects:[response objectForKey:@"items"]withUserName:[ZNSUser currentUser].username];
    } errorWithBlock:^(ZNSError *error) {
        //        [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
        
    }];
    
    [APIClient POST:API_USER withParameters:@{@"status":@"启用",@"page":@-1} successWithBlcok:^(id response) {
        [ZNSCache cacheUsers:[response objectForKey:@"items"] withUserName:[ZNSUser currentUser].username];
    } errorWithBlock:^(ZNSError *error) {
        //        [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
        
    }];
}

#pragma mark - Private

- (void)initializeUI {
    
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] init];
//    backButton.title = @"返回";
//    self.navigationItem.backBarButtonItem = backButton;
//    self.operateCollectView.hidden = !self.isOnline;
    
    CGRect rect = scroll.frame;
    
    if (self.isOnline) {
        self.isFirstView = YES;
        self.isOffView = NO;
        self.logoBottom.constant = 23;
        self.myTabBar.hidden = NO;
        
        rect.origin.y = 0;
        
    }else{
        arrOperate = @[@"离线鉴权智能钥匙开锁",
                       @"退出登陆"];
        arrOperateClassName = @[@"OnlineAuthenticationSmartKeyViewController"
                                ];
        self.logoBottom.constant = 50;
        self.myTabBar.hidden = YES;
        self.isOffView = YES;
         rect.origin.y = 84;
    }
    scroll.frame = rect;
    self.offlineCountdownLabel.hidden = self.isOnline;
}

- (void)setupTabbarItems{
 
    NSString *fileName = @"MainItems";
    if (!self.isOnline) {
        fileName = @"MainItemsOffline";
    }
    
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"];
    arrItemConfig = [NSArray arrayWithContentsOfFile:path];
    
    NSMutableArray *arrTabbarItems = [NSMutableArray array];
    NSInteger i = 0;
    if (!arrColls) {
        arrColls = [NSMutableArray array];
    }
    [arrColls removeAllObjects];
    for (NSDictionary *dic in arrItemConfig) {
        
        UITabBarItem *item = [[UITabBarItem alloc] initWithTitle:dic[@"title"] image:nil selectedImage:nil];
        item.image = [UIImage imageNamed:dic[@"icon"]];
        item.tag = i;
        [arrTabbarItems addObject:item];
       
        
        //确定是水平滚动，还是垂直滚动
        UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
        
        UICollectionView *coll = [[UICollectionView alloc] initWithFrame:CGRectMake(i*kScreenSize.width, 0,kScreenSize.width,CGRectGetHeight(self.vMainBg.frame)) collectionViewLayout:flowLayout];
        coll.delegate = self;
        coll.dataSource = self;
        coll.backgroundColor = [UIColor clearColor];
        coll.tag = 100+i;
        [scroll addSubview:coll];
        
        [coll registerNib:[UINib nibWithNibName:@"MainItemCell" bundle:nil] forCellWithReuseIdentifier:@"MainItemCell"];
          [coll registerNib:[UINib nibWithNibName:@"MainMapItemCell" bundle:nil] forCellWithReuseIdentifier:@"MainMapItemCell"];
        
        [arrColls addObject:coll];
         i++;
    }
    _myTabBar.items = arrTabbarItems;
 
    scroll.contentSize = CGSizeMake(arrItemConfig.count * kScreenSize.width, 0);
    
    if (![ZNSTools isiPhoneX]) {
        tabbar_h.constant = 49;
    }
    
    if (self.isOnline) {
        _myTabBar.selectedItem = arrTabbarItems[2];
    } 
    [self tabBar:_myTabBar didSelectItem:_myTabBar.selectedItem];
    
}

- (NSArray *)getCurrentPageSectionsWith:(NSInteger)idx{
    
    NSDictionary *d = arrItemConfig[idx];
    NSArray *items = d[@"items"];
    return items;
}

- (NSArray *)getCurrentPageItemsWithPage:(NSInteger)page section:(NSInteger)section{
 
    NSArray *items = [self getCurrentPageSectionsWith:page];
    return items[section];
}

- (void)loadNumberData{
    @weakify(self);
    [APIClient POST:@"common/verifynum" withParameters:@{} successWithBlcok:^(id response) {
        @strongify(self);
        self.offlineApproveCount = [response valueForKey:@"offline"];
        self.registerApproveCount = [response valueForKey:@"regapply"];
        self.tempApproveCount = [response valueForKey:@"temporary"];
        [self.operateCollectView reloadData];
    } errorWithBlock:^(ZNSError *error) {
        
    }];
}


- (void)playRingtone{
    AudioServicesPlaySystemSound(1104);
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
}


#pragma mark - Action

- (IBAction)showAlertMessageController:(id)sender{
    AlertMessageViewController *vc = [AlertMessageViewController create];
    vc.alert = [NSArray array];
    vc.alert = [vc.alert  arrayByAddingObjectsFromArray:self.alerts] ;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    self.alertCount = 0;
    [self.alerts removeAllObjects];
    [self.messageButton setTitle:[NSString stringWithFormat:@"警告消息（%@）",@(self.alertCount).stringValue] forState:UIControlStateNormal];
}


- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    
//    tabbarSelectedIdx = item.tag;
//    [self.operateCollectView reloadData];
    
    [scroll setContentOffset:CGPointMake(item.tag*kScreenSize.width, 0) animated:YES];
    
    BOOL isHidden = item.tag == 2;
    btnMy.hidden = !isHidden;
    btnScanCode.hidden = !isHidden;
    
}
 
- (IBAction)goUserProfileAction:(UIButton *)sender {
    
    [self.navigationController pushViewController:[MyController create] animated:YES];
    
}


- (IBAction)scanQRCodeAction:(UIButton *)sender {
    
    ScanQRCodeController *scanQRCode = [ScanQRCodeController create];
    @weakify(self);
    [scanQRCode setResultBlock:^(NSString *value) {
        @strongify(self);
        BuletoothUnlockController *bthUnlock = [BuletoothUnlockController create];
        bthUnlock.title = @"扫码开锁";
        bthUnlock.goScanCodeBlock = ^{
            [self scanQRCodeAction:nil];
        };
        bthUnlock.scanCodeResult = value;
        [self.navigationController pushViewController:bthUnlock animated:YES];
    }];
    
    [self.navigationController pushViewController:scanQRCode animated:YES];
}

#pragma mark - Scroll view delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    NSInteger idx = scrollView.contentOffset.x / kScreenSize.width;
    self.myTabBar.selectedItem = self.myTabBar.items[idx];
    
    BOOL isHidden = idx == 2;
    btnMy.hidden = !isHidden;
    btnScanCode.hidden = !isHidden;
    
}


#pragma mark - CollectView delegate && datasource


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{

    return [self getCurrentPageSectionsWith:collectionView.tag - 100].count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self getCurrentPageItemsWithPage:collectionView.tag - 100 section:section].count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *d = [self getCurrentPageItemsWithPage:collectionView.tag - 100 section:indexPath.section][indexPath.row];
    
    if (!d.count) { 
        MainMapItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MainMapItemCell" forIndexPath:indexPath];
        cell.sites =  mapSites;
        return cell;
        
    }else{
        
        MainItemCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MainItemCell" forIndexPath:indexPath];
        
        cell.operateLabel.text = d[@"itemTitle"];//arrOperate[indexPath.row];
        UIImage *image = [UIImage imageNamed:d[@"icon"]];//[UIImage imageNamed:arrOperate[indexPath.row]];
        if (!image) {
            image = [UIImage imageNamed:@"临时权限审批"];
        }
        
        if ([d[@"class"] isEqualToString:@"AlertController"]) {
            if (self.alertCount > 0) {
                cell.badgeLabel.text = @(self.alertCount).stringValue;
                cell.badgeLabel.hidden = NO;
            }else{
                cell.badgeLabel.hidden = YES;
                cell.badgeLabel.text = @"";
            }
        }else if ([d[@"class"] isEqualToString:@"RegisterUserApprovalViewController"]) {
            if (registerCount>0) {
                cell.badgeLabel.text = @(registerCount).stringValue;
                cell.badgeLabel.hidden = NO;
            }else{
                cell.badgeLabel.hidden = YES;
                cell.badgeLabel.text = @"";
            }
        }else if ([d[@"class"] isEqualToString:@"OfflineAuthenticationApprovalViewController"]) {
            if (offlineCount>0) {
                cell.badgeLabel.text = @(offlineCount).stringValue;
                cell.badgeLabel.hidden = NO;
            }else{
                cell.badgeLabel.hidden = YES;
                cell.badgeLabel.text = @"";
            }
        }else if ([d[@"class"] isEqualToString:@"TempPermissionApprovalViewController"]) {
            if (tempTaskCount>0) {
                cell.badgeLabel.text = @(tempTaskCount).stringValue;
                cell.badgeLabel.hidden = NO;
            }else{
                cell.badgeLabel.hidden = YES;
                cell.badgeLabel.text = @"";
            }
        }else if ([d[@"class"] isEqualToString:@"NoticeController"]) {
            cell.badgeLabel.hidden = !noticeIsNew;
            cell.badgeLabel.text = noticeIsNew?@"News":@"";
            
        }else if ([d[@"class"] isEqualToString:@"WorkBillApprovalController"]){
            
            if (workbillCount>0) {
                cell.badgeLabel.text = @(workbillCount).stringValue;
                cell.badgeLabel.hidden = NO;
            }else{
                cell.badgeLabel.hidden = YES;
                cell.badgeLabel.text = @"";
            }
            
        }else{
            cell.badgeLabel.hidden = YES;
            cell.badgeLabel.text = @"";
        }
        
        cell.operateImageView.image = image;
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *d = [self getCurrentPageItemsWithPage:collectionView.tag - 100 section:indexPath.section][indexPath.row];
    NSString *strClass = d[@"class"];
    if ([strClass length]) {
        
        UIViewController * vc;
        if ([strClass isEqualToString:@"    "]) { 
            vc = [ZNSExecuteUnlockHelper unlock];
        }else{
            vc   = [NSClassFromString(strClass) create];
            vc.title = d[@"itemTitle"];
        }
        
        if ([strClass isEqualToString:@"NoticeController"]){
            noticeIsNew = NO;
            [collectionView reloadItemsAtIndexPaths:@[indexPath]];
        }else if([strClass isEqualToString:@"RegisterUserApprovalViewController"]) {
            //        registerCount = 0;
            [collectionView reloadItemsAtIndexPaths:@[indexPath]];
        }else if([strClass isEqualToString:@"OfflineAuthenticationApprovalViewController"]) {
            //        offlineCount = 0;
            [collectionView reloadItemsAtIndexPaths:@[indexPath]];
        }else if([strClass isEqualToString:@"TempPermissionApprovalViewController"]) {
            //        tempTaskCount = 0;
            [collectionView reloadItemsAtIndexPaths:@[indexPath]];
        }else if ([d[@"class"] isEqualToString:@"WorkBillApprovalController"]){
            //        workbillCount = 0;
            [collectionView reloadItemsAtIndexPaths:@[indexPath]];
        }else if ([strClass isEqualToString:@"AlertController"]){
            AlertController *alert = (AlertController *)vc;
            alert.alts = self.alerts;
            self.alertCount = 0;
            [collectionView reloadItemsAtIndexPaths:@[indexPath]];
        }
        
        [self.navigationController pushViewController:vc animated:YES];
        [self.operateCollectView reloadData];
    }
    
    if (!self.isOnline && [strClass isEqualToString:@"OnlineAuthenticationSmartKeyViewController"]) {
        if (self.offline.beginAt.integerValue <= 0) {
            self.offline.beginAt = [NSString stringWithFormat:@"%lf",[NSDate date].timeIntervalSince1970];
            [ZNSCache cacheOffline:self.offline withUserName:[ZNSUser currentUser].username];
            [self.timer start];
            self.offline = [ZNSCache findOfflineDataOfUserName:[ZNSUser currentUser].username];
        }
    }
}

//定义每个Item 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
     NSDictionary *d = [self getCurrentPageItemsWithPage:collectionView.tag - 100 section:indexPath.section][indexPath.row];
    if (!d.count) {
        return CGSizeMake(kScreenSize.width, kScreenSize.height / 2 - 50);
    }else{
        return CGSizeMake(83, 112);
    }
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    NSDictionary *d = [self getCurrentPageItemsWithPage:collectionView.tag - 100 section:section].firstObject;
    if (!d.count) {
         return UIEdgeInsetsMake(0, 20, 0, 20);
    }else{
        return UIEdgeInsetsMake(34, 20, 0, 20);
    }
}

@end
