//
//  MainItemCell.h
//  ZhiNengSuo
//
//  Created by Chuenlu Kuo on 17/2/8.
//  Copyright © 2017年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainItemCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *operateImageView;
@property (strong, nonatomic) IBOutlet UILabel *operateLabel;
@property (strong, nonatomic) IBOutlet UILabel *badgeLabel;

@end
