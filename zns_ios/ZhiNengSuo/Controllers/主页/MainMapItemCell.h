//
//  MainMapItemCell.h
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/3.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainMapItemCell : UICollectionViewCell

@property (nonatomic,strong) NSArray *sites;

@end
