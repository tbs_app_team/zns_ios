//
//  MainItemCell.m
//  ZhiNengSuo
//
//  Created by Chuenlu Kuo on 17/2/8.
//  Copyright © 2017年 czwen. All rights reserved.
//

#import "MainItemCell.h"

@implementation MainItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.badgeLabel.layer.cornerRadius = 11;
    self.badgeLabel.layer.masksToBounds = YES;
    self.badgeLabel.hidden = YES;
}

@end
