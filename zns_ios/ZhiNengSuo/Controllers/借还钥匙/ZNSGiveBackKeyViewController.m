//
//  ZNSGiveBackKeyViewController.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 16/3/13.
//  Copyright © 2016 czwen. All rights reserved.
//

#import "ZNSGiveBackKeyViewController.h"
#import "ZNSBorrowSmartkey.h"
#import "ZNSBorrowSmartKeyTableViewCell.h"
#import "ZNSSmartKey.h"
#import "BlueToothViewController.h"
#import "JKAlertDialog.h"

@interface ZNSGiveBackKeyViewController ()<UITableViewDelegate,UITableViewDataSource>{
    JKAlertDialog  *_dialog;
}
@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSArray *datas;
@end

@implementation ZNSGiveBackKeyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[ZNSBorrowSmartKeyTableViewCell nib] forCellReuseIdentifier:[ZNSBorrowSmartKeyTableViewCell reuseIdentifier]];
    [self loadDatas];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"归还智能钥匙" style:UIBarButtonItemStylePlain target:self action:@selector(returnKey)];
}
- (void)returnKey{
    
    static BlueToothViewController *btVC;
    if (!btVC) {
        btVC = [BlueToothViewController create];
    }
    
    [btVC setConnectedBlock:^{
       [[GGBluetooth sharedManager] disconnectCurrentDevice];
    }];
    _dialog = [[JKAlertDialog alloc]initWithTitle:@"选择智能钥匙" message:@""];
    _dialog.contentView = btVC.view;
    
    [_dialog addButton:Button_CANCEL withTitle:@"取消" handler:^(JKAlertDialogItem *item) {
        NSLog(@"click %@",item.title);
    }];;
    
    @weakify(self);
    [btVC setConnectedSmartKeyBlock:^(ZNSSmartKey *smartKey){
       @strongify(self);
          [_dialog dismiss];
        [APIClient POST:[NSString stringWithFormat:@"smartkey/return/%@",smartKey.skid] withParameters:@{} successWithBlcok:^(id response) {
            
            [self loadDatas];
            //        [ZNSSmartKey getAllSmartKeySuccess:nil error:nil];
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:error.errorMessage];
        }];
        
    }];
    
    
    [_dialog show];
    [btVC setupBT];
    [btVC loadSmartKeys];
    
}
- (void)loadDatas{
    @weakify(self);
    [APIClient POST:API_SMART_KEY_BORROW withParameters:@{@"page":@-1,@"status":@"未归还"} successWithBlcok:^(id response) {
        @strongify(self);
        self.datas = [NSArray yy_modelArrayWithClass:[ZNSBorrowSmartkey class] json:[response valueForKey:@"items"]];
        [self.tableView reloadData];
    } errorWithBlock:^(ZNSError *error) {
        
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZNSBorrowSmartKeyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ZNSBorrowSmartKeyTableViewCell reuseIdentifier]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    [cell setModel:[self.datas objectAtIndex:indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    @weakify(self);
    return [tableView fd_heightForCellWithIdentifier:[ZNSBorrowSmartKeyTableViewCell reuseIdentifier] configuration:^(ZNSBorrowSmartKeyTableViewCell *cell) {
        @strongify(self);
        [cell setModel:[self.datas objectAtIndex:indexPath.row]];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
//    ZNSBorrowSmartkey *model = self.datas[indexPath.row];
//    @weakify(self);
//    [APIClient POST:[NSString stringWithFormat:@"smartkey/return/%@",model.sid] withParameters:@{} successWithBlcok:^(id response) {
//        @strongify(self);
//        NSMutableArray *tmp = [NSMutableArray arrayWithArray:self.datas];
//        [tmp removeObject:model];
//        self.datas = tmp;
//        [self.tableView reloadData];
//        [SVProgressHUD dismiss];
////        [self loadDatas];
////        [ZNSSmartKey getAllSmartKeySuccess:nil error:nil];
//    } errorWithBlock:^(ZNSError *error) {
//        [SVProgressHUD showErrorWithStatus:error.errorMessage];
//    }];
}
@end
