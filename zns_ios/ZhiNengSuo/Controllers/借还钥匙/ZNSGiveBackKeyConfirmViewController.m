//
//  ZNSGiveBackKeyConfirmViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/4/14.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "ZNSGiveBackKeyConfirmViewController.h"
#import "ZNSBorrowSmartKeyTableViewCell.h"

@interface ZNSGiveBackKeyConfirmViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSArray *datas;
@property (nonatomic,strong) NSNumber *page;

@end

@implementation ZNSGiveBackKeyConfirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView registerNib:[ZNSBorrowSmartKeyTableViewCell nib] forCellReuseIdentifier:[ZNSBorrowSmartKeyTableViewCell reuseIdentifier]];
    
//    @weakify(self);
//    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        @strongify(self);
//        [self loadDataWithPage:@0];
//    }];
//    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//        @strongify(self);
//        [self loadDataWithPage:@(self.page.integerValue+1)];
//    }];
//    [self.tableView.mj_header beginRefreshing];
    [self loadDatas];
    
}
- (void)loadDatas{
    @weakify(self);
    [APIClient POST:API_SMART_KEY_BORROW withParameters:@{@"page":@0,@"status":@"已归还"} successWithBlcok:^(id response) {
        @strongify(self);
        self.datas = [NSArray yy_modelArrayWithClass:[ZNSBorrowSmartkey class] json:[response valueForKey:@"items"]];
        [self.tableView reloadData];
        
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
        
    }];
    
}
- (void)loadDataWithPage:(NSNumber *)page{
    @weakify(self);
    [APIClient POST:API_SMART_KEY_BORROW withParameters:@{@"page":page,@"status":@"已归还"} successWithBlcok:^(id response) {
        @strongify(self);
        self.datas = [NSArray yy_modelArrayWithClass:[ZNSBorrowSmartkey class] json:[response valueForKey:@"items"]];
          [self.tableView reloadData];
        
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
      
        if ([[response valueForKey:@"page"] isEqual:[response valueForKey:@"pages"]]) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
        }
        
    } errorWithBlock:^(ZNSError *error) {
         [SVProgressHUD showErrorWithStatus:error.errorMessage];
         [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZNSBorrowSmartKeyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ZNSBorrowSmartKeyTableViewCell reuseIdentifier]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setModel:[self.datas objectAtIndex:indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    @weakify(self);
    return [tableView fd_heightForCellWithIdentifier:[ZNSBorrowSmartKeyTableViewCell reuseIdentifier] configuration:^(ZNSBorrowSmartKeyTableViewCell *cell) {
        @strongify(self);
        [cell setModel:[self.datas objectAtIndex:indexPath.row]];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
