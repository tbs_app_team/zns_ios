//
//  ZNSBorrowKeyConfirmViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/4/14.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "ZNSBorrowKeyConfirmViewController.h"
#import "ZNSBorrowSmartKeyTableViewCell.h"

@interface ZNSBorrowKeyConfirmViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSArray * datas;

@end

@implementation ZNSBorrowKeyConfirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tableView registerNib:[ZNSBorrowSmartKeyTableViewCell nib] forCellReuseIdentifier:[ZNSBorrowSmartKeyTableViewCell reuseIdentifier]];
    [self loadDatas];
}
- (void)loadDatas{
    @weakify(self);
    [APIClient POST:API_SMART_KEY_BORROW withParameters:@{@"page":@-1,@"status":@"未归还"} successWithBlcok:^(id response) {
        @strongify(self);
        self.datas = [NSArray yy_modelArrayWithClass:[ZNSBorrowSmartkey class] json:[response valueForKey:@"items"]];
        [self.tableView reloadData];
   
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
        
    }];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZNSBorrowSmartKeyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ZNSBorrowSmartKeyTableViewCell reuseIdentifier]];
    
     [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
     [cell setModel:[self.datas objectAtIndex:indexPath.row]];
     return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    @weakify(self);
    return [tableView fd_heightForCellWithIdentifier:[ZNSBorrowSmartKeyTableViewCell reuseIdentifier] configuration:^(ZNSBorrowSmartKeyTableViewCell *cell) {
        @strongify(self);
        [cell setModel:[self.datas objectAtIndex:indexPath.row]];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
