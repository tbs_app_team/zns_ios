//
//  ZNSBorrowKeyViewController.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 16/3/13.
//  Copyright © 2016 czwen. All rights reserved.
//

#import "ZNSBorrowKeyViewController.h"
#import "BlueToothViewController.h"
#import "JKAlertDialog.h"
#import "APIClient.h"
#import "ZNSBorrowSmartKeyTableViewCell.h"
#import "CommonTableViewController.h"

@interface ZNSBorrowKeyViewController ()<UITableViewDelegate,UITableViewDataSource>{
    JKAlertDialog *_dialog;
}
@property (nonatomic,weak) IBOutlet UILabel *nameLabel;
@property (nonatomic,weak) IBOutlet UILabel *selectedKeyLabel;
@property (weak, nonatomic) IBOutlet UIButton *borrowBtn;
@property (weak, nonatomic) IBOutlet UITableView *borrowKeyTableView;

// 用作获取蓝牙地址
@property (nonatomic,strong) NSMutableArray *bts;
@property (nonatomic,strong) NSArray *smartKeys;

- (IBAction)borrowBtnOn:(id)sender;

@property (nonatomic,strong) ZNSSmartKey *selectedKey;
@property  NSString *selectedKeyId;
@property (nonatomic,strong) NSArray *datas;

@end

@implementation ZNSBorrowKeyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.nameLabel setText:[NSString stringWithFormat:@"借用人：%@",[ZNSUser currentUser].name]];
    [self.selectedKeyLabel setText:[NSString stringWithFormat:@"借用钥匙：%@",STRING_OR_EMPTY(self.selectedKey.name)]];
    [self.borrowKeyTableView registerNib:[ZNSBorrowSmartKeyTableViewCell nib] forCellReuseIdentifier:[ZNSBorrowSmartKeyTableViewCell reuseIdentifier]];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"搜索智能钥匙" style:UIBarButtonItemStyleDone target:self action:@selector(showParseSmartKey)];
    [self loadBorrowKeys];
}

- (void)loadBorrowKeys{
    @weakify(self);
    [APIClient POST:API_SMART_KEY_BORROW withParameters:@{@"page":@0,@"username":[ZNSUser currentUser].username} successWithBlcok:^(id response) {
        @strongify(self);
        self.datas = [NSArray yy_modelArrayWithClass:[ZNSBorrowSmartkey class] json:[response valueForKey:@"items"]];
        [self.borrowKeyTableView reloadData];
    } errorWithBlock:^(ZNSError *error) {
        
    }];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)loadSmartKeys{
    
    @weakify(self);
    if (self.smartKeys.count<=0) {
        [SVProgressHUD showWithStatus:@"正在加载智能钥匙"];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [ZNSSmartKey getAllSmartKeySuccess:^(id response) {
            @strongify(self);
            self.smartKeys = response;
            [ZNSCache cacheSmartKeys:self.smartKeys withUserName:[ZNSUser currentUser].username];
            [SVProgressHUD dismiss];
        } error:^(ZNSError *error) {
            @strongify(self);
            NSArray *keys = [ZNSCache findSmartKeysCacheOfUserName:[ZNSUser currentUser].username];
            if ([keys isKindOfClass:[NSArray class]]) {
                [SVProgressHUD dismiss];
                self.smartKeys = keys;
            }else{
                [SVProgressHUD showErrorWithStatus:error.errorMessage];
            }
        }];
    });
}
- (void)showParseSmartKey{
    static BlueToothViewController *btVC;
    if (!btVC) {
        btVC = [BlueToothViewController create];
    }
    
    [btVC setConnectedBlock:^{
        [[GGBluetooth sharedManager] disconnectCurrentDevice];
    }];
    _dialog = [[JKAlertDialog alloc]initWithTitle:@"选择智能钥匙" message:@""];
    _dialog.contentView = btVC.view;
    
    [_dialog addButton:Button_CANCEL withTitle:@"取消" handler:^(JKAlertDialogItem *item) {
//        NSLog(@"click %@",item.title);
    }];;
    
    @weakify(self);
    [btVC setConnectedSmartKeyBlock:^(ZNSSmartKey *smartKey){
        @strongify(self);
        [_dialog dismiss];
        self.selectedKey = smartKey;
        self.selectedKeyId = smartKey.skid ;
        [self.selectedKeyLabel setText:[NSString stringWithFormat:@"借用钥匙：%@",STRING_OR_EMPTY(self.selectedKey.name)]];
    }];
    
    
    [_dialog show];
    [btVC setupBT];
    [btVC loadSmartKeys];
    
    
//    [self.view endEditing:YES];
//    [self.bts removeAllObjects];
//    [self loadSmartKeys];
//    if (!self.smartKeys) {
////        [SVProgressHUD showInfoWithStatus:@"无可借用的钥匙"];
//        return;
//    }
//    CommonTableViewController *vc= [[CommonTableViewController alloc]initWithStyle:UITableViewStylePlain];
//    
//    _dialog = [[JKAlertDialog alloc]initWithTitle:@"选择钥匙" message:@""];
//    _dialog.contentView = vc.view;
//    
//    
//    @weakify(self);
//    [[GGBluetooth sharedManager]setDiscoverPeripheralBlock:^(CBPeripheral *discoverPeripheral,NSDictionary *advertisementData,NSNumber *RSSI){
//        @strongify(self);
//        if (!self.bts) {
//            self.bts = [NSMutableArray array];
//        }
//        
//        if (![[self.bts valueForKey:@"identifier"] containsObject:discoverPeripheral.identifier]&& [[self.smartKeys valueForKeyPath:@"btAddr"]  containsObject:discoverPeripheral.macAddress]) {
//             ZNSSmartKey *smartKey  = [self.smartKeys objectAtIndex:[[self.smartKeys valueForKeyPath:@"btAddr"] indexOfObject:[discoverPeripheral.macAddress stringByReplacingOccurrencesOfString:@"-" withString:@""]]];
//            [self.bts addObject:discoverPeripheral];
//            [vc setDatas:self.bts];
//        }
//    }];
//    
//    [_dialog addButton:Button_CANCEL withTitle:@"取消" handler:^(JKAlertDialogItem *item) {
//        
//    }];
//    
//    [vc setSelectedBlock:^(CBPeripheral *discoverPeripheral){
//        @strongify(self);
//         ZNSSmartKey *smartKey  = [self.smartKeys objectAtIndex:[[self.smartKeys valueForKeyPath:@"btAddr"] indexOfObject:[discoverPeripheral.macAddress stringByReplacingOccurrencesOfString:@"-" withString:@""]]];
//        self.selectedKey = smartKey;
//        self.selectedKeyId = smartKey.skid ;
//        [self.selectedKeyLabel setText:[NSString stringWithFormat:@"借用钥匙：%@",STRING_OR_EMPTY(self.selectedKey.name)]];
//         [_dialog dismiss];
//    }];
//    
//    [_dialog show];
//    [[GGBluetooth sharedManager]startScan];
}

- (IBAction)borrowBtnOn:(id)sender {
    
    if (![self.selectedKeyId length]) {
        [SVProgressHUD showErrorWithStatus:@"请选择借用钥匙"];
        return;
    }
    
    @weakify(self);
    [APIClient POST:[NSString stringWithFormat:@"smartkey/borrow/%@",self.selectedKeyId] withParameters:@{} successWithBlcok:^(id response) {
        @strongify(self);
        [self.selectedKeyLabel  setText:@"借用钥匙："];
        [self loadBorrowKeys];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.datas.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ZNSBorrowSmartKeyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ZNSBorrowSmartKeyTableViewCell reuseIdentifier]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    [cell setModel:[self.datas objectAtIndex:indexPath.row]];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    @weakify(self);
    return [tableView fd_heightForCellWithIdentifier:[ZNSBorrowSmartKeyTableViewCell reuseIdentifier] configuration:^(ZNSBorrowSmartKeyTableViewCell *cell) {
        @strongify(self);
        [cell setModel:[self.datas objectAtIndex:indexPath.row]];
    }];
}
@end
