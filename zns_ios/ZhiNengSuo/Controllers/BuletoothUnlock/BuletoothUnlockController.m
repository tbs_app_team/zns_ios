//
//  BuletoothUnlockController.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/3.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import "BuletoothUnlockController.h"
#import "OnlineAuthenticationBtLockTableViewCell.h"
#import "HistoryTableViewCell.h"

@interface BuletoothUnlockController ()<UITableViewDelegate,UITableViewDataSource>{
    BOOL _startNewStage;
    
}

@property (nonatomic,weak) IBOutlet UITableView *tableView;
@property (nonatomic,weak) IBOutlet UITableView *historyTableView;
@property (nonatomic,strong) NSMutableArray *datas;
@property (nonatomic,strong) NSMutableArray *historyDatas;
@property (nonatomic,strong) NSMutableDictionary *tmpLocks;
@property (nonatomic,strong) NSArray *locks;
@property (nonatomic,assign) NSInteger loadLocksQueueCount;
@property (nonatomic,strong) ZNSOfflineBtOperation *offlineBtOperation;




@property (nonatomic,strong) UIBarButtonItem *smartKeyButtonItem;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressBarKnowWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressContainerViewHeight;
@property (nonatomic,weak) IBOutlet UIView *progressContainerView;
@property (nonatomic,weak) IBOutlet UIView *progressBarView;
@property (nonatomic,weak) IBOutlet UIView *progressBarKnobView;

@property (nonatomic,strong) NSString *operationID;
@property (nonatomic,strong) NSArray *datasWatingForSubmit;

@property (nonatomic,strong) NSTimer *timer;

@property (nonatomic,strong) NSMutableArray *history;

@end

@implementation BuletoothUnlockController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //按照规则截取二维码扫描出来的蓝牙地址
    if ([self.scanCodeResult length]) {
        NSString *subAtStr = @"app_download?id=";
        NSRange range = [self.scanCodeResult rangeOfString:subAtStr];
        if (range.length) {
            self.scanCodeResult = [self.scanCodeResult substringFromIndex:range.location+range.length];
        }
    }
    
    
    if ([ZNSUser currentUser].isOfflineLogin) {
        self.mode = AuthenticationControllerModeOffline;
    }
    //    if (self.mode != AuthenticationControllerModeOffline &&
    //        self.mode != AuthenticationControllerModeTemptask){
    //        self.mode = AuthenticationControllerModeNormal;
    //    }
    
     self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self configUI];
    [self loadData];
    [self setupBT];
    [self setupProgressBar];
    [self resetProgressViewForShow:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[GGBluetooth sharedManager]disconnectCurrentDevice];
}

- (void)configUI{
    self.datas = [NSMutableArray array];
    self.tmpLocks = [NSMutableDictionary dictionary];
    self.locks = [NSArray array];
    self.offlineBtOperation = [ZNSOfflineBtOperation new];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:[UITableViewCell reuseIdentifier]];
    [self.tableView registerNib:[OnlineAuthenticationBtLockTableViewCell nib] forCellReuseIdentifier:[OnlineAuthenticationBtLockTableViewCell reuseIdentifier]];
    //    @weakify(self);
    //    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
    //        @strongify(self);
    //        [[GGBluetooth sharedManager]disconnectCurrentDevice];
    //        [self loadData];
    //    }];
    
    BOOL isScanCode = [self.scanCodeResult length];
    
    UIBarButtonItem *scanButton = [[UIBarButtonItem alloc]initWithTitle:isScanCode?@"扫码":@"扫描" style:UIBarButtonItemStyleDone target:self action:@selector(rightBtnAction)];
    
    self.navigationItem.rightBarButtonItems = @[scanButton];
    
    self.historyTableView.dataSource = self;
    self.historyTableView.delegate = self;
    self.historyTableView.tag = 1;
    
    [self.historyTableView registerNib:[HistoryTableViewCell nib] forCellReuseIdentifier:[HistoryTableViewCell reuseIdentifier]];
    self.history = [NSMutableArray array];
}

- (void)popToMainPage{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)rightBtnAction{
    if ([self.scanCodeResult length]) { 
        [self.navigationController popViewControllerAnimated:NO];
        if (self.goScanCodeBlock) {
            self.goScanCodeBlock();
        }
    }else{
        [self scan];
    }
}

- (void)setupProgressBar{
    self.progressBarView.layer.cornerRadius = 12;
    self.progressBarView.layer.borderWidth = 1/[UIScreen mainScreen].scale;
    self.progressBarView.layer.borderColor = self.view.tintColor.CGColor;
    
    self.progressBarKnobView.layer.cornerRadius = 12;
    self.progressBarKnobView.backgroundColor = self.view.tintColor;
    self.progressBarKnowWidth.constant = 0.0*240;
    
}

- (void)setupBT{
    @weakify(self);
    //    __block NSArray *locksAddr = [[ZNSCache getAllLocksets]valueForKeyPath:@"btaddr"];
    [[GGBluetooth sharedManager] setDiscoverPeripheralBlock:^(CBPeripheral *discoverPeripheral,NSDictionary *advertisementData,NSNumber *RSSI){
        @strongify(self);
        [SVProgressHUD dismiss];
        if (self.locks.count == 0) {
            return ;
        }
        
        NSString *macAds = [discoverPeripheral.macAddress stringByReplacingOccurrencesOfString:@"-" withString:@""];
        
        //如果是扫码页面进入，则进行扫码结果和扫码附近蓝牙地址对比
        if ([self.scanCodeResult length]) { 
            BOOL match = [self.scanCodeResult compare:macAds
                                    options:NSCaseInsensitiveSearch | NSNumericSearch] == NSOrderedSame;
            if (!match) {
                return;
            }
        }
        //        BOOL enable = [[self.locks valueForKeyPath:@"btaddr"] containsObject:[discoverPeripheral.macAddress stringByReplacingOccurrencesOfString:@"-" withString:@""]];
        BOOL enable = NO;
        NSArray *btnAddrs = [self.locks valueForKeyPath:@"btaddr"];
       
        for (NSString *str in btnAddrs) {
            if ([str isEqualToString:macAds]) {
                enable = YES;
                break;
            }
        }
        
        if (!enable) {
            return;
        }
        
        //        if (![self.datas containsObject:discoverPeripheral] && [self.locks containsObject:discoverPeripheral.macAddress]) {
        
        if (![self.datas containsObject:discoverPeripheral]){
            
            ZNSLock *locker = [self.locks objectAtIndex:[[self.locks valueForKeyPath:@"btaddr"] indexOfObject:[discoverPeripheral.macAddress stringByReplacingOccurrencesOfString:@"-" withString:@""]]];
            
            [[GGBluetooth sharedManager] disconnectCurrentDevice];
            
            [[GGBluetooth sharedManager] connectPheripheral:discoverPeripheral withSuccessBlcok:^(BOOL success, CBPeripheral *connectedPeripheral, NSError *error) {
                
                [[GGBluetooth sharedManager] discoverCurrentPheripheralServices:^(NSArray *services) {
                    [[GGBluetooth sharedManager]discoverCharacteristicsFormService:services.firstObject withBlock:^(NSArray *characteristics, NSError *error) {
                        
                    }];
                }];
                [[GGBluetooth sharedManager] setCanWriteValueBlock:^{
                    
                    [[[GGBluetooth sharedManager] connectedPheripheral]writeValue:[ZNSBluetoothInstruction checkDoorStateInstruction].data forCharacteristic:[[GGBluetooth sharedManager] writeableCharacteristic] type: [[GGBluetooth sharedManager] writeableCharacteristic].properties == (CBCharacteristicPropertyRead+CBCharacteristicPropertyWriteWithoutResponse+CBCharacteristicPropertyNotify)? CBCharacteristicWriteWithoutResponse:CBCharacteristicWriteWithResponse] ;
                }];
                
                
                [[GGBluetooth sharedManager] setNotifyCharacteristicBlock:^(CBCharacteristic *characteristic,NSData *recvData,NSError *error){
                    
                    //如果发送查询信息有回应，则去掉提示框  2017-12-6--ojl
                    if (success) {
                        [SVProgressHUD dismiss];
                    }
                    ZNSBluetoothInstruction *b = [[ZNSBluetoothInstruction alloc]initWithByteData:characteristic.value];
                    
                    if (b.on) {
                        if(b.function == 0x02) {
                            locker.lockState = YES;
                            [[GGBluetooth sharedManager] disconnectCurrentDevice];
                            [self.datas addObject:discoverPeripheral];
                            [self.tableView reloadData];
                            [[GGBluetooth sharedManager] startScan];
                        }else if (b.function == 0x04){
                            locker.doorState = YES;
                            [[[GGBluetooth sharedManager] connectedPheripheral]writeValue:[ZNSBluetoothInstruction checkLockStateInstruction].data forCharacteristic:[[GGBluetooth sharedManager] writeableCharacteristic] type: [[GGBluetooth sharedManager] writeableCharacteristic].properties == (CBCharacteristicPropertyRead+CBCharacteristicPropertyWriteWithoutResponse+CBCharacteristicPropertyNotify)? CBCharacteristicWriteWithoutResponse:CBCharacteristicWriteWithResponse] ;
                        }
                    }else{
                        if (b.function == 0x02) {
                            
                            locker.lockState = NO;
                            [[GGBluetooth sharedManager] disconnectCurrentDevice];
                            [self.datas addObject:discoverPeripheral];
                            [self.tableView reloadData];
                            [[GGBluetooth sharedManager] startScan];
                        }else if (b.function == 0x04){
                            
                            locker.doorState = NO;
                            [[[GGBluetooth sharedManager] connectedPheripheral]writeValue:[ZNSBluetoothInstruction checkLockStateInstruction].data forCharacteristic:[[GGBluetooth sharedManager] writeableCharacteristic] type: [[GGBluetooth sharedManager] writeableCharacteristic].properties == (CBCharacteristicPropertyRead+CBCharacteristicPropertyWriteWithoutResponse+CBCharacteristicPropertyNotify)? CBCharacteristicWriteWithoutResponse:CBCharacteristicWriteWithResponse] ;
                        }
                    }
                }];
                
            }];
            
            
        }
    }];
    [[GGBluetooth sharedManager] setPeripheralDisconnectedBlock:^(CBPeripheral *connectedPeripheral, NSError *error){
        @strongify(self);
        [self resetProgressViewForShow:NO]; 
    }];
}

- (void)scan{
    [[GGBluetooth sharedManager] disconnectCurrentDevice];
    [self.datas removeAllObjects];
    [self.tableView reloadData];
    [[GGBluetooth sharedManager] startScan];
    [SVProgressHUD showWithStatus:@"正在扫描中，请稍后。。。"];
    @weakify(self);
    //    if (!self.timer) {
    self.timer = [NSTimer scheduledTimerWithTimeInterval:8.0f block:^(NSTimer *timer) {
        @strongify(self);
        [SVProgressHUD dismiss];
        if (self!=nil && self.datas.count<=0) {
            [SVProgressHUD showErrorWithStatus:@"没有搜索到锁具"];
        }
    } repeats:NO];
}

- (void)loadData{
    [SVProgressHUD showWithStatus:@"加载设备数据"];
    
    if (self.mode == AuthenticationControllerModeNormal) {
        @weakify(self);
        [APIClient POST:[NSString stringWithFormat:@"user/%@/object/",[ZNSUser currentUser].username] withParameters:@{@"page":@(-1)} successWithBlcok:^(id response) { 
            [SVProgressHUD dismiss];
            @strongify(self);
            self.locks = @[];
            [self.tmpLocks removeAllObjects];
            NSArray *objects = [NSArray yy_modelArrayWithClass:[ZNSObject class] json:[response valueForKey:@"items"]];
            
            [objects enumerateObjectsUsingBlock:^(ZNSObject *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                @strongify(self);
                self.locks = [self.locks arrayByAddingObjectsFromArray:obj.lockset];
            }];
            
            //            [ZNSCache cacheLockers:self.locks  withUserName:[ZNSUser currentUser].username];
            [self scan];
            [self.tableView.mj_header endRefreshing];
            
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:error.errorMessage];
            @strongify(self);
            [self.tableView.mj_header endRefreshing];
        }];
    }else{
        if (self.mode == AuthenticationControllerModeTemptask) {
            @weakify(self);
            [APIClient POST:[NSString stringWithFormat:@"object/%@/",self.optionObject.oid] withParameters:@{} successWithBlcok:^(id locks) {
                @strongify(self);
                self.locks = [self.locks arrayByAddingObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSLock class] json:[locks valueForKey:@"items"]]];
                if ([APIClient sharedClient].operationQueue.operationCount<=0) {
                    @strongify(self);
                    [SVProgressHUD dismiss];
                    [self scan];
                    [self.tableView.mj_header endRefreshing];
                }
            } errorWithBlock:^(ZNSError *error) {
                if ([APIClient sharedClient].operationQueue.operationCount<=0) {
                    [SVProgressHUD showErrorWithStatus:error.errorMessage];
                    @strongify(self);
                    [self.tableView.mj_header endRefreshing];
                }
            }];
        }else if(self.mode == AuthenticationControllerModeWorkBill){
            self.locks = self.workBillLocks;
            [SVProgressHUD dismiss];
            [self scan];
            [self.tableView.mj_header endRefreshing];
        }else if(self.mode == AuthenticationControllerModeWorkPatrol){
            self.locks = self.optionObject.lockset;
            [SVProgressHUD dismiss];
            [self scan];
            [self.tableView.mj_header endRefreshing];
        }else{
            self.locks = [ZNSCache findOfflineDataOfUserName:[ZNSUser currentUser].username].locks;
            [SVProgressHUD dismiss];
            [self scan];
            [self.tableView.mj_header endRefreshing];
        }
    }
}



#pragma mark - UITableView Delegate DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView.tag == 0) {
        return self.datas.count;
    }else{
        return self.history.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag == 1) {
        HistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[HistoryTableViewCell reuseIdentifier]];
        NSDictionary *dic = [self.history objectAtIndex:self.history.count-1- indexPath.row];
        NSString *unusualCase = [dic objectForKey:@"Unusal"];
        [cell.objectNameLabel setText:[[dic valueForKey:@"lock"] valueForKeyPath:@"name"]];
        [cell.userNameLabel setText:[NSString stringWithFormat:@"类型：%@",[[dic valueForKey:@"lock"] valueForKeyPath:@"type"]]];
        [cell.dateNameLabel setText:[dic valueForKey:@"time"]];
        [cell.resultLabel setText:[dic valueForKey:@"result"]];
        
        //异常显示红色 2017-12-6
        if ([unusualCase isEqualToString:@"正常"]) {
            [cell.resultLabel setTextColor:[UIColor greenColor]];
        }else{
            [cell.resultLabel setTextColor:[UIColor redColor]];
        }
        return cell;
    }
    
    CBPeripheral *p = [self.datas objectAtIndex:indexPath.row];
    
    
    BOOL enable = [[self.locks valueForKeyPath:@"btaddr"] containsObject:[p.macAddress stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    OnlineAuthenticationBtLockTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[OnlineAuthenticationBtLockTableViewCell reuseIdentifier]];
    {
        
        
        NSInteger index = [[self.locks valueForKeyPath:@"btaddr"] indexOfObject:[p.macAddress stringByReplacingOccurrencesOfString:@"-" withString:@""]];
        
        if (index!=NSNotFound) {
            //            ZNSLock *lock = [allLocks objectAtIndex:index];
            ZNSLock *lock = [self.locks objectAtIndex:index];
            cell.nameLabel.text = lock.name;
            
            if (lock.doorState == YES) {//open
                [cell.doorStateImageView setImage:[UIImage imageNamed:@"door_open"]];
                
            }else{
                [cell.doorStateImageView setImage:[UIImage imageNamed:@"door_close"]];
            }
            if (lock.lockState == YES) {
                [cell.lockStateImageView setImage:[UIImage imageNamed:@"lock_open"]];
            }else{
                [cell.lockStateImageView setImage:[UIImage imageNamed:@"lock_close"]];
            }
            
        }else{
            cell.nameLabel.text = @"未知设备";
        }
        
    }
    if ([cell.nameLabel.text isEqualToString:@"未知设备"]) {
        NSInteger index = [[self.locks valueForKeyPath:@"btaddr"] indexOfObject:[p.macAddress stringByReplacingOccurrencesOfString:@"-" withString:@""]];
        if (index!=NSNotFound) {
            ZNSLock *lock = [self.locks objectAtIndex:index];
            cell.nameLabel.text = lock.name;
        }
    }
    
    if ([[[GGBluetooth sharedManager] connectedPheripheral].identifier isEqual:p.identifier]) {
        [cell.connectButton setTitle:@"已连接" forState:UIControlStateNormal];
    }else{
        [cell.connectButton setTitle:@"连接" forState:UIControlStateNormal];
    }
    cell.statusLabel.text = @"";
    
    cell.nameLabel.textColor = enable?UIColorHex(0x000000):UIColorHex(0xa5a5a5);
    cell.statusLabel.textColor = enable?UIColorHex(0x000000):UIColorHex(0xa5a5a5);
    
    cell.unlockButton.enabled = enable;
    cell.connectButton.enabled = enable;
    
    if (enable) {
        [cell.connectButton bs_configureAsSuccessStyle];
        [cell.unlockButton bs_configureAsPrimaryStyle];
    }else{
        [cell.unlockButton bs_configureAsDefaultStyle];
        [cell.connectButton bs_configureAsDefaultStyle];
    }
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //    return [tableView fd_heightForCellWithIdentifier:[OnlineAuthenticationBtLockTableViewCell reuseIdentifier] configuration:^(OnlineAuthenticationBtLockTableViewCell *cell) {
    //        CBPeripheral *p = [self.datas objectAtIndex:indexPath.row];
    //        cell.nameLabel.text = p.name;
    //    }];
    if (tableView.tag == 1) {
        return 95;
    }
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag == 1) {
        return;
    }
    CBPeripheral *p = [self.datas objectAtIndex:indexPath.row];
    //    ZNSLock *lockInDatas = [self.datas objectAtIndex:indexPath.row];
    
    
    NSInteger index = [[self.locks valueForKeyPath:@"btaddr"] indexOfObject:[p.macAddress stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    ZNSLock *lock;
    if (index==NSNotFound) {
        return;
    }else{
        lock = [self.locks objectAtIndex:index];
    }
    
    @weakify(self);
    
    if ([[[GGBluetooth sharedManager] connectedPheripheral].identifier.UUIDString isEqualToString:p.identifier.UUIDString]) {
        if (self.mode != AuthenticationControllerModeOffline) {
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"netWork"] isEqualToString:@"OffNetWork"]) {
                [SVProgressHUD showErrorWithStatus:@"网络异常，禁止开锁操作"];
                return;
            }
        }
        if (self.mode == AuthenticationControllerModeTemptask) { //判断任务是否过期
            NSDateFormatter * df = [[NSDateFormatter alloc]init];
            df.dateFormat = @"yyyyMMddHHmmss";
            NSDate * endDate = [df dateFromString:self.tempTask.end_at];
            if ([endDate timeIntervalSinceDate:[NSDate date]] < 0) {
                [SVProgressHUD showErrorWithStatus:@"任务已经过期，请重新申请"];
                return;
            }
        }else if (self.mode == AuthenticationControllerModeOffline){
            ZNSOffline  *offline = [ZNSCache findOfflineDataOfUserName:[ZNSUser currentUser].username];
            if((offline.beginAt.integerValue+offline.duration.integerValue*60)<[NSDate date].timeIntervalSince1970){
                [SVProgressHUD showErrorWithStatus:@"离线任务已过期，禁止操作"];
                return;
            } 
        }
        [self unlockCurrentDevice];
    }else{
        if (self.mode != AuthenticationControllerModeOffline) {
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"netWork"] isEqualToString:@"OffNetWork"]) {
                [SVProgressHUD showErrorWithStatus:@"网络异常，禁止开锁操作"];
                return;
            }
        }
        if (self.mode == AuthenticationControllerModeTemptask) { //判断任务是否过期
            NSDateFormatter * df = [[NSDateFormatter alloc]init];
            df.dateFormat = @"yyyyMMddHHmmss";
            NSDate * endDate = [df dateFromString:self.tempTask.end_at];
            if ([endDate timeIntervalSinceDate:[NSDate date]] < 0) {
                [SVProgressHUD showErrorWithStatus:@"任务已经过期，请重新申请"];
                return;
            }
        }else if (self.mode == AuthenticationControllerModeOffline){
            ZNSOffline  *offline = [ZNSCache findOfflineDataOfUserName:[ZNSUser currentUser].username];
            if((offline.beginAt.integerValue+offline.duration.integerValue*60)<[NSDate date].timeIntervalSince1970){
                [SVProgressHUD showErrorWithStatus:@"离线任务已过期，禁止操作"];
                return;
            }
            
        }
        //        else{
        [UIAlertController showAlertInViewController:self.navigationController withTitle:@"开锁提示" message:[NSString stringWithFormat:@"是否对（%@）设备开锁？",lock.name] cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@[@"是"] tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
            if (buttonIndex == 2) {
                @strongify(self);
                [[GGBluetooth sharedManager]disconnectCurrentDevice];
                
                [self connectDeviceAtIndexPath:indexPath device:lock success:^{
                    @strongify(self);
                    [self unlockCurrentDevice];
                }];
            }
        }];
        //        }
    }
}

- (void)connectDeviceAtIndexPath:(NSIndexPath*)indexPath device:(ZNSLock*)lock  success:(voidBlock)successBlock{
    @weakify(self);
    [SVProgressHUD showWithStatus:@"蓝牙连接中"];
    
    [[GGBluetooth sharedManager]connectPheripheral:self.datas[indexPath.row] withSuccessBlcok:^(BOOL success, CBPeripheral *connectedPeripheral, NSError *error) {
        @strongify(self);
        if (success) {
            [SVProgressHUD showSuccessWithStatus:@"连接成功"];
            [self resetProgressViewForShow:YES];
        }else{
            [SVProgressHUD showErrorWithStatus:@"连接失败"];
            [self scan];
        }
        [[GGBluetooth sharedManager] discoverCurrentPheripheralServices:^(NSArray *services) {
            [[GGBluetooth sharedManager]discoverCharacteristicsFormService:services.firstObject withBlock:^(NSArray *characteristics, NSError *error) {
            }];
        }];
        
        [[GGBluetooth sharedManager] setNotifyCharacteristicBlock:^(CBCharacteristic *characteristic,NSData *recvData,NSError *error){
            ZNSBluetoothInstruction *b = [[ZNSBluetoothInstruction alloc]initWithByteData:characteristic.value];
            
            ZNSLock *lock = [self.locks objectAtIndex:[[self.locks valueForKeyPath:@"btaddr"] indexOfObject:[connectedPeripheral.macAddress stringByReplacingOccurrencesOfString:@"-" withString:@""]]];
            //异常处理 2017-12-6 --ojl
            NSDictionary *lastHistory = [self.history lastObject];
            ZNSLock *lockStatus = [lastHistory objectForKey:@"lock"];
            if (b.on) {
                //开锁
                if (self.mode == AuthenticationControllerModeTemptask) {
                    //                        [lock createUnlockHistorySuccess:YES verifyAt:nil verifyBy:nil action:@"临时申请开锁"];
                }else{
                    //                        [lock createUnlockHistorySuccess:YES verifyAt:nil verifyBy:nil action:@"鉴权开锁"];
                }
                //                if (b.control == 0x40) {
                if (b.function == 0x00){
                    [self operationOfResult:1 withLock:lock withUnusalCase:@"正常"];
                    //                    [self operationOfResult:1 withLock:lock];
                    //                    lock.lockState = YES;
                    //                    [self.tableView reloadData];
                }else if(b.function == 0x01||b.function == 0x02) {
                    if (!lockStatus.lockState) {
                        [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"已对（%@）设备开锁成功",lock.name]];
                        [self operationOfResult:2 withLock:lock withUnusalCase:@"正常"];
                    }else{
                        [SVProgressHUD showErrorWithStatus:@"操作异常"];
                        [self operationOfResult:2 withLock:lock withUnusalCase:@"异常"];
                    }
                    lock.lockState = YES;
                    [self.tableView reloadData];
                }else if (b.function == 0x03||b.function == 0x04){
                    if (lockStatus.lockState&&(!lockStatus.doorState)) {
                        [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"（%@）开门成功",lock.name]];
                        //                        [self operationOfResult:3 withLock:lock];
                        [self operationOfResult:3 withLock:lock withUnusalCase:@"正常"];
                    }else{
                        [SVProgressHUD showErrorWithStatus:@"操作异常"];
                        [self operationOfResult:3 withLock:lock withUnusalCase:@"异常"];
                    }
                    lock.doorState = YES;
                    [self.tableView reloadData];
                }
                //                }
            }else{
                //关锁
                //                if (b.control == 0x40) {
                if (b.function == 0x00){
                    //                    [self operationOfResult:0 withLock:lock];
                    //                    lock.lockState = NO;
                    //                    [self.tableView reloadData];
                    
                }else if (b.function == 0x01||b.function == 0x02) {
                    if (!lockStatus.doorState&&lockStatus.lockState) {
                        [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"（%@）设备已闭锁",lock.name]];
                        //                        [self operationOfResult:5 withLock:lock];
                        [self operationOfResult:5 withLock:lock withUnusalCase:@"正常"];
                    }else{
                        [SVProgressHUD showErrorWithStatus:@"操作异常"];
                        [self operationOfResult:5 withLock:lock withUnusalCase:@"异常"];
                    }
                    lock.lockState = NO;
                    [self.tableView reloadData];
                }else if (b.function == 0x03||b.function == 0x04){
                    if (lockStatus.lockState&&lockStatus.doorState) {
                        [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"（%@）已关门",lock.name]];
                        //                        [self operationOfResult:4 withLock:lock];
                        [self operationOfResult:4 withLock:lock withUnusalCase:@"正常"];
                    }else{
                        [SVProgressHUD showErrorWithStatus:@"操作异常"];
                        [self operationOfResult:4 withLock:lock withUnusalCase:@"异常"];
                    }
                    lock.doorState = NO;
                    [self.tableView reloadData];
                }
            }
        }];
        [[GGBluetooth sharedManager]setCanWriteValueBlock:^{
            // 开锁
            if (successBlock) {
                successBlock();
            }
        }];
    }];
}

- (void)unlockCurrentDevice{
    if (!_startNewStage) {
        _startNewStage = YES;
        ZNSLock *lock = [self.locks objectAtIndex:[[self.locks valueForKeyPath:@"btaddr"] indexOfObject:[[[GGBluetooth sharedManager] connectedPheripheral].macAddress stringByReplacingOccurrencesOfString:@"-" withString:@""]]];
    }
    
    [[[GGBluetooth sharedManager] connectedPheripheral]writeValue:[ZNSBluetoothInstruction unlockInstruction].data forCharacteristic:[[GGBluetooth sharedManager] writeableCharacteristic] type:[[GGBluetooth sharedManager] writeableCharacteristic].properties==(CBCharacteristicPropertyRead+CBCharacteristicPropertyWriteWithoutResponse+CBCharacteristicPropertyNotify)?CBCharacteristicWriteWithoutResponse:CBCharacteristicWriteWithResponse];
}

//  "result": 0|1|2|3|4|5|6|255 //开锁指令失败|开锁指令成功|开锁成功|开门成功|关门成功|闭锁成功|正常操作|越权未遂

- (void)operationOfResult:(NSInteger)result withLock:(ZNSLock*)lock withUnusalCase:(NSString*)value{
    
    NSString *resultString;
    
    switch (result) {
        case 1:
            resultString = @"开锁指令成功";
            self.progressBarKnowWidth.constant = 240 *0.2;
            break;
        case 2:
            resultString = @"开锁成功";
            self.progressBarKnowWidth.constant = 240 *0.4;
            break;
        case 3:
            resultString = @"开门成功";
            self.progressBarKnowWidth.constant = 240 *0.6;
            break;
        case 4:
            resultString = @"关门成功";
            self.progressBarKnowWidth.constant = 240 *0.8;
            break;
        case 5:
            resultString = @"闭锁成功";
            self.progressBarKnowWidth.constant = 240 *1;
            _startNewStage = NO;
            break;
        default:
            resultString = @"已闭锁";
            self.progressBarKnowWidth.constant = 240 *1;
            _startNewStage = NO;
            break;
    }
    
    [self.history addObject:@{@"lock":lock,@"result":resultString,@"time":[[NSDate date] stringWithFormat:@"yyyy-MM-dd hh:mm:ss"],@"Unusal":value}];
    
    
    [self.historyTableView reloadData];
    
    if (result>6) {
        return;
    }
    
    if (self.mode == AuthenticationControllerModeOffline) {
        
        [self initOfflineBtOperation:[lock.lid integerValue] deveceName:lock.name result:result operationTime:[ZNSDate getCurrentTime] isSeverError:NO];
    }else{
        [self makeOrderWithResult:result lock:lock];
    }
}

- (void)resetProgressViewForShow:(BOOL)show{
    if (show) {
        self.progressContainerViewHeight.constant = 100;
        self.progressBarKnowWidth.constant = 0;
    }else{
        self.progressContainerViewHeight.constant = 0;
    }
}

- (void)makeOrderWithResult:(NSInteger)result lock:(ZNSLock*)lock{
    
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if (self.mode == AuthenticationControllerModeTemptask) {
        [dic setValue:self.tempTask.tid forKey:@"tid"];
        [dic setValue:@"fixed" forKey:@"action"];
    }else if (self.mode == AuthenticationControllerModeWorkPatrol){
        [dic setValue:@"workbill" forKey:@"action"];
    }else{
        [dic setValue:@"fixed" forKey:@"action"];
    }
    [dic setValue:lock.lid forKey:@"lid"];
    
    //    if (result!=0 || result!=1) {
    //        [dic setValue:self.operationID forKey:@"opid"];
    //    }
    if ( result!=1) {
        [dic setValue:self.operationID forKey:@"opid"];
    }
    [dic setValue:@(result) forKey:@"result"];
    //    @weakify(lock);
    @weakify(self);
    [APIClient POST:API_OPERATION_DO withParameters:dic successWithBlcok:^(id response) {
        //        @strongify(lock);
        @strongify(self);
        NSLog(@"id:%@,tid:%@,%ld",self.operationID==nil?@"nil":self.operationID,self.tempTask==nil?@"nil":self.tempTask.tid,result);
        if (result==1) {
            self.operationID = [response valueForKey:@"opid"];
        }
        
        if (result == 5 || result==6) {
            self.operationID = nil;
        }
    } errorWithBlock:^(ZNSError *error) {
        if([error.errorMessage isEqualToString: @"服务器出错！"]){//服务器出错，先缓存操作记录
            
            [self initOfflineBtOperation:[lock.lid integerValue] deveceName:lock.name result:result operationTime:[ZNSDate getCurrentTime]isSeverError:YES];
            
        }
    }];
}


- (void)initOfflineBtOperation:(NSInteger)lid  deveceName:(NSString*)deveceName result:(NSInteger)result  operationTime:(NSString *)operationTime isSeverError:(BOOL)isSeverError{
    
    self.offlineBtOperation.lid = lid;
    self.offlineBtOperation.action = @"fixed";
    self.offlineBtOperation.deviceName = deveceName;
    
    switch (result) {
        case ZNSOfflineBtResultOrderFail://<开锁指令失败
            self.offlineBtOperation.directive_fault_at = operationTime;
            [self createOfflineBtUnlockSuccess:self.offlineBtOperation isSeverError:isSeverError];
            self.offlineBtOperation = [ZNSOfflineBtOperation new];
            break;
        case ZNSOfflineBtResultOrderSuccess://<开锁指令成功
            self.offlineBtOperation.directive_at = operationTime;
            break;
        case ZNSOfflineBtResultOrderUnlockSuccess://<开锁成功
            self.offlineBtOperation.unlock_at = operationTime;
            break;
        case ZNSOfflineBtResultOrderOpenDoorSuccess://<开门成功
            self.offlineBtOperation.opendoor_at = operationTime;
            break;
        case ZNSOfflineBtResultOrderCloseDoorSuccess://<关门成功
            self.offlineBtOperation.closedoor_at = operationTime;
            //            [self createOfflineBtUnlockSuccess:self.offlineBtOperation isSeverError:isSeverError];
            break;
        case ZNSOfflineBtResultOrderLockSuccess://<闭锁成功
            self.offlineBtOperation.lock_at = operationTime;
            [self createOfflineBtUnlockSuccess:self.offlineBtOperation isSeverError:isSeverError];
            self.offlineBtOperation = [ZNSOfflineBtOperation new];
            break;
        case ZNSOfflineBtResultOrderWarnning:
            self.offlineBtOperation.ultra_at = operationTime;
            [self createOfflineBtUnlockSuccess:self.offlineBtOperation isSeverError:isSeverError];
            self.offlineBtOperation = [ZNSOfflineBtOperation new];
            break;
        default:
            break;
    }
    
}
- (void)createOfflineBtUnlockSuccess:(ZNSOfflineBtOperation *)offlineBtOperation isSeverError:(BOOL)isSeverError{
    
    NSArray *offlineArr = [NSArray array];
    offlineArr = [ZNSCache  getOfflineBtOperation:[ZNSUser currentUser].username];
    
    
    if (offlineBtOperation.directive_fault_at == nil) {
        offlineBtOperation.directive_fault_at = @"";
    }
    if (offlineBtOperation.directive_at == nil) {
        offlineBtOperation.directive_at = @"";
    }
    if (offlineBtOperation.opendoor_at == nil) {
        offlineBtOperation.opendoor_at = @"";
    }
    if (offlineBtOperation.closedoor_at == nil) {
        offlineBtOperation.closedoor_at = @"";
    }
    if (offlineBtOperation.lock_at == nil) {
        offlineBtOperation.lock_at = @"";
    }
    if (offlineBtOperation.unlock_at == nil) {
        offlineBtOperation.unlock_at = @"";
    }
    if (offlineBtOperation.ultra_at == nil) {
        offlineBtOperation.ultra_at = @"";
    }
    
    offlineArr = [offlineArr arrayByAddingObject:offlineBtOperation];
    if (isSeverError) {
        [ZNSCache cacheOnlineBtOperation:offlineArr withUserName:[ZNSUser currentUser].username];
    }else{
        [ZNSCache cacheOfflineBtOperation:offlineArr withUserName:[ZNSUser currentUser].username];
    }
}


@end
