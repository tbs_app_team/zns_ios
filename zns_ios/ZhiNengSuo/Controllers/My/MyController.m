//
//  MyController.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/3.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import "MyController.h"
#import "AccountInfoController.h"
#import "ContactController.h"
#import "NoticeController.h"
#import "SystemSettingsViewController.h"
#import "SiteManagerController.h"
#import "SmartKeyManagerController.h"

@interface MyController ()

@end

@implementation MyController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"个人中心";
    
    [self initItem];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCellStyle)cellStyle{
    return UITableViewCellStyleValue1;
}

- (void)initItem{
    @weakify(self);
    ZNSUser *user = [ZNSUser currentUser];
    ZFSettingItem *accountItem = [ZFSettingItem itemWithIcon:nil title:user.name value:user.username type:ZFSettingItemTypeArrow];
    accountItem.operation = ^{
        @strongify(self);
        [self.navigationController pushViewController:[AccountInfoController create] animated:YES];
    };
    ZFSettingGroup *accountGroup = [[ZFSettingGroup alloc] init];
    accountGroup.items = @[accountItem];
    
    ZFSettingItem *siteInfoItem = [ZFSettingItem itemWithIcon:nil title:@"站点信息管理" type:ZFSettingItemTypeArrow];
    siteInfoItem.operation = ^{
         @strongify(self);
        SiteManagerController *siteMgr = [SiteManagerController create];
        [self.navigationController pushViewController:siteMgr animated:YES];
    };
    
    ZFSettingItem *smartKeyItem = [ZFSettingItem itemWithIcon:nil title:@"智能钥匙管理" type:ZFSettingItemTypeArrow];
    smartKeyItem.operation = ^{
        @strongify(self);
        SmartKeyManagerController *smartKeyMgr = [SmartKeyManagerController create];
        [self.navigationController pushViewController:smartKeyMgr animated:YES];
    };
    ZFSettingGroup *mgrGroup = [[ZFSettingGroup alloc] init];
    mgrGroup.items = @[siteInfoItem,smartKeyItem];
    
    ZFSettingItem *contactItem = [ZFSettingItem itemWithIcon:nil title:@"通讯录" type:ZFSettingItemTypeArrow];
    contactItem.operation = ^{
        @strongify(self);
        [self.navigationController pushViewController:[ContactController create] animated:YES];
    };
    ZFSettingItem *noticeItem = [ZFSettingItem itemWithIcon:nil title:@"公告" type:ZFSettingItemTypeArrow];
    noticeItem.operation = ^{
        @strongify(self);
        [self.navigationController pushViewController:[NoticeController create] animated:YES];
    };
    ZFSettingGroup *nolGroup = [[ZFSettingGroup alloc] init];
    nolGroup.items = @[contactItem,noticeItem];
    
    ZFSettingItem *settingItem = [ZFSettingItem itemWithIcon:nil title:@"设置" type:ZFSettingItemTypeArrow];
    settingItem.operation = ^{
        @strongify(self);
        [self.navigationController pushViewController:[SystemSettingsViewController create] animated:YES];
    };
    ZFSettingGroup *settingGroup = [[ZFSettingGroup alloc] init];
    settingGroup.items = @[settingItem];
    
    [_allGroups addObject:accountGroup];
    [_allGroups addObject:mgrGroup];
    [_allGroups addObject:nolGroup];
    [_allGroups addObject:settingGroup];
    
}

@end
