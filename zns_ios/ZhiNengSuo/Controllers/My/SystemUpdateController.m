//
//  SystemUpdateController.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/21.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import "SystemUpdateController.h"
#import "MKVersionCheckManager.h"
#import "UIAlertController+Blocks.h"

@interface SystemUpdateController (){
    
    IBOutlet UILabel *lblVersion;
    IBOutlet UILabel *lblUpdateTime;
    
}

@end

@implementation SystemUpdateController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"系统更新";
    
    NSString *lastUpdateTime = [[NSUserDefaults standardUserDefaults] objectForKey:@"VersionReleaseDate"];
    if ([lastUpdateTime length]) {
        lblUpdateTime.text = [NSString stringWithFormat:@"版本发布时间：%@",lastUpdateTime];
    }
    
     self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"更新" style:UIBarButtonItemStylePlain target:self action:@selector(doVersionCheck)];
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    lblVersion.text = [NSString stringWithFormat:@"当前版本：%@",app_Version];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)doVersionCheck{
    //版本检测
    [SVProgressHUD showWithStatus:@"正在获取版本信息..."];
    [[MKVersionCheckManager sharedInstance] doRequestVsersionInfoWithAppId:@"1100079619" sucess:^(NSString *downUrl, NSString *updateLog, NSString *versionNum, NSString *releaseDate) {
        [SVProgressHUD dismiss];
        [[NSUserDefaults standardUserDefaults] setObject:releaseDate forKey:@"VersionReleaseDate"];
        lblUpdateTime.text = [NSString stringWithFormat:@"版本发布时间：%@",releaseDate];
        
        [UIAlertController showAlertInViewController:self withTitle:@"升级提示" message:[NSString stringWithFormat:@"最新版本为%@,是否升级？",versionNum] cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@[@"升级"] tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
            if (buttonIndex == 1) {
                if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:downUrl]]) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:downUrl]];
                }
            }
        }];
        
    }faild:^(NSString *errorMsg) {
        
        [SVProgressHUD showImage:nil status:errorMsg];
    }];
}

@end
