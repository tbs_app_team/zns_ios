//
//  RoleChooseViewController.h
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/12/5.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoleChooseViewController : UIViewController

@property (nonatomic ,strong) ZNSUser * chooseUser;
@property (nonatomic ,strong) NSString * agreeMemo;
@property (nonatomic ,strong) arrayBlock changeBlock;

@end
