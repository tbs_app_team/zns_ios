//
//  LoginViewController.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/25/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "MainOnlineViewController.h"
#import "ZNSLabelTextFieldTableViewCell.h"
#import "NSData+Extensions.h"
#import <DTKeychain.h>
#import "SystemSettingsViewController.h"

@interface LoginViewController(){

    UIButton *btnCaptcha;
    IBOutlet UITextField *txtAccount;
    IBOutlet UITextField *txtPassword;
}
//@property (nonatomic,weak) IBOutlet UITableView *tableView;
//@property (nonatomic,strong) ZNSLabelTextFieldTableViewCell *accountCell;
//@property (nonatomic,strong) ZNSLabelTextFieldTableViewCell *passwordCell;
@property (nonatomic,strong) ZNSLabelTextFieldTableViewCell *verificationCodeCell;
@end


@implementation LoginViewController

- (void)doGetCaptcha{
      
//    NSString *urlWithServerParameter = [API_HOST stringByAppendingString:[NSString stringWithFormat:@"%@?s=%@&c=iOS&v=%@",API_COMMON_CAPTCHA,API_VERSION,APP_VERSION]];
//
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//
//    [manager GET:urlWithServerParameter parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
//
//    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        [btnCaptcha setBackgroundImage:[UIImage imageWithData:responseObject scale:1] forState:UIControlStateNormal];
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//
//    }];
    
    //TEST
//    self.accountCell.textField.text = @"13726291998";
//    self.passwordCell.textField.text = @"1";
//    [self.verificationCodeCell.textField becomeFirstResponder];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.title = @"登录";
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"注册" style:UIBarButtonItemStylePlain target:self action:@selector(registerAction)];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"设置" style:UIBarButtonItemStylePlain target:self action:@selector(setting)];
    
//    [self.tableView registerNib:[ZNSLabelTextFieldTableViewCell nib] forCellReuseIdentifier:[ZNSLabelTextFieldTableViewCell reuseIdentifier]];
    [self configCells];
    
    txtAccount.text= STRING_OR_EMPTY([[NSUserDefaults standardUserDefaults]valueForKey:@"lastLogin"]);
    
    if ([API_HOST length]) {
        [self doGetCaptcha];
    }else{
        [self setting];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)configCells{
//    self.accountCell = [ZNSLabelTextFieldTableViewCell loadFromNib];
//    [self.accountCell.label setText:@"账号"];
//    [self.accountCell.textField setPlaceholder:@"请输入账号"];
//    self.accountCell.textField.text = STRING_OR_EMPTY([[NSUserDefaults standardUserDefaults]valueForKey:@"lastLogin"]);
//    self.passwordCell = [ZNSLabelTextFieldTableViewCell loadFromNib];
//    [self.passwordCell.label setText:@"密码"];
//    [self.passwordCell.textField setPlaceholder:@"请输入密码"];
//    self.passwordCell.textField.text = @"";
//    self.passwordCell.textField.secureTextEntry = YES;
    
    self.verificationCodeCell = [ZNSLabelTextFieldTableViewCell loadFromNib];
    [self.verificationCodeCell.label setText:@"验证码"];
    [self.verificationCodeCell.textField setPlaceholder:@"请输入验证码"];
    
    UIView *vRight = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 30)];
    btnCaptcha = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCaptcha.frame = CGRectMake(0, 0, 60, 30);
    [btnCaptcha addTarget:self action:@selector(doGetCaptcha) forControlEvents:UIControlEventTouchUpInside];
    [vRight addSubview:btnCaptcha];
    [self.verificationCodeCell.textField setRightView:vRight];
    self.verificationCodeCell.textField.rightViewMode = UITextFieldViewModeAlways;
    
}


- (void)registerAction{
    [self.navigationController pushViewController:[RegisterViewController create] animated:YES];
}
- (void)setting{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"请设置服务器地址" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    __block UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alertController.textFields enumerateObjectsUsingBlock:^(UITextField * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [[NSNotificationCenter defaultCenter]removeObserver:obj];
        }];
        [alertController.view endEditing:YES];
        
        NSString *text = [alertController.textFields.firstObject valueForKey:@"text"];
        SAVE_SERVER_ADDRESS(text)
        NSLog(@"%@",text);
        
        NSString *alert_port = [alertController.textFields[1] valueForKey:@"text"];
        SAVE_ALERT_PORT(alert_port)
        
        NSLog(@"%@",alert_port);
        
//        if ([text length]) {
//            [self doGetCaptcha];
//        }
    }];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
        textField.placeholder = @"请输入服务器地址"; 
        textField.text = [API_HOST length]?API_HOST:@"http://112.91.145.58:31000";
        [[NSNotificationCenter defaultCenter]addObserverForName:UITextFieldTextDidChangeNotification object:textField queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
            action.enabled  = textField.text.length>0;
        }];
        action.enabled  = textField.text.length>0;
    }];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
        textField.placeholder = @"请输入推送端口号";
        textField.text = [API_ALERT_PORT length]?API_ALERT_PORT:@"31001";
        
        [[NSNotificationCenter defaultCenter]addObserverForName:UITextFieldTextDidChangeNotification object:textField queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
            action.enabled  = textField.text.length>0;
        }];
        action.enabled  = textField.text.length>0;
    }];
    
    [alertController addAction:action];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
//    SystemSettingsViewController *vc = [SystemSettingsViewController create];
//    vc.loginView = YES;
//    [self.navigationController  pushViewController:vc animated:YES];
}
- (IBAction)registerAction:(id)sender {
    
     [self.navigationController pushViewController:[RegisterViewController create] animated:YES];
}
- (IBAction)settingAction:(id)sender {
    
    [self setting];
}

- (IBAction)loginAction:(id)sender {
    if (txtAccount.text.length == 0 || txtPassword.text.length == 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入完整信息"];
//    }else if (![self.verificationCodeCell.textField.text length] && [[Reachability reachabilityForInternetConnection]currentReachabilityStatus] != NotReachable){
//        [SVProgressHUD showErrorWithStatus:@"请输入验证码"];
    }else if ([txtPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]].length>0) {
        [SVProgressHUD showErrorWithStatus:@"账号必须为数字"];
    }else{
        [SVProgressHUD show];
        NSString *username = txtAccount.text;
        NSString *password = [NSString getSha512String:txtPassword.text];
        
        if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable) {
            //离线
            if ([username isEqualToString:[[NSUserDefaults standardUserDefaults]valueForKey:@"lastLogin"]]) {
                if([password isEqualToString:[[NSUserDefaults standardUserDefaults]valueForKey:@"lastLoginPassWord"]]) {
                       [self loginOfflineWithUserName:username password:password];
                }else{
                    [SVProgressHUD showErrorWithStatus:@"离线登陆,密码错误"];                 }
                
            }else{
                [SVProgressHUD showErrorWithStatus:@"离线登陆，用户不存在"];
            }
            
        }else{
            //在线
            @weakify(self);
            [APIClient POST:API_USER_LOGIN withParameters:@{@"username": username,@"password": password,@"uniqueid":[self uniqueID],@"code":self.verificationCodeCell.textField.text} successWithBlcok:^(id response) {
                [[NSUserDefaults standardUserDefaults]setValue:username forKey:@"lastLogin"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [[NSUserDefaults standardUserDefaults]setValue:password forKey:@"lastLoginPassWord"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                [SVProgressHUD dismiss];
                ZNSUser *user = [ZNSUser yy_modelWithJSON:response];
                [user login];
                [ZNSUser cacheUsername:username andPassword:password];
                [ZNSUser cacheUserdata:response forUsername:username];
                
//                [self makeCache];
                
                [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_ACCOUNT_CHANGE object:nil userInfo:@{@"isOffline":@NO,@"login":@YES}];
            } errorWithBlock:^(ZNSError *error) {
                @strongify(self);
                if (error.errorCode == -1) {
                    [SVProgressHUD dismiss];
                    [UIAlertController showAlertInViewController:self.navigationController withTitle:@"访问服务器失败，请检查网络及服务器地址或是否尝试离线登录？" message:@"" cancelButtonTitle:@"否" destructiveButtonTitle:nil otherButtonTitles:@[@"是"] tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
                        if (buttonIndex == 2) {
                            
                            if ([username isEqualToString:[[NSUserDefaults standardUserDefaults]valueForKey:@"lastLogin"]]) {
                                if([password isEqualToString:[[NSUserDefaults standardUserDefaults]valueForKey:@"lastLoginPassWord"]]) {
                                    [self loginOfflineWithUserName:username password:password];
                                }else{
                                    [SVProgressHUD showErrorWithStatus:@"离线登陆,密码错误"];
                                     [self doGetCaptcha];
                                }
                                
                            }else{
                                [SVProgressHUD showErrorWithStatus:@"离线登陆，用户不存在"];
                                 [self doGetCaptcha];
                            }
                        }
                    }];
                }else{
                    [SVProgressHUD showInfoWithStatus:error.errorMessage];
                     [self doGetCaptcha];
                }
            }];
        }
    }
}

- (void)loginOfflineWithUserName:(NSString*)username password:(NSString *)password{
    ZNSOffline *offline = [ZNSCache findOfflineDataOfUserName:username];
    if (offline!=nil && (((offline.beginAt.integerValue+offline.duration.integerValue*60)>[NSDate date].timeIntervalSince1970)||offline.beginAt==0)) {
        [ZNSUser loginOfflineWithUsername:username passwor:password success:^(BOOL success) {
            [SVProgressHUD dismiss];
            if (success) {
                [self.navigationController pushViewController:[MainOnlineViewController create] animated:YES];
                [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_ACCOUNT_CHANGE object:nil userInfo:@{@"isOffline":@YES,@"login":@YES}];
            }else{
                [SVProgressHUD showInfoWithStatus:[ZNSError networkError].errorMessage];
            }
        }];
    }else{
        [SVProgressHUD showInfoWithStatus:@"你没有离线任务"];
    }
}
- (void)makeCache{
    
    [ZNSAPITool cacheOfflineDataSuccessWithBlcok:nil errorWithBlock:nil];
    
    // cache smart keys
    [ZNSSmartKey getAllSmartKeySuccess:^(id response) {
        [ZNSCache cacheSmartKeys:response withUserName:[ZNSUser currentUser].username];
    } error:nil];
    
    [APIClient POST:API_SECTION_OBJECT_WITH_SID([ZNSUser currentUser].sid) withParameters:@{@"page":@-1} successWithBlcok:^(id response) {
        [ZNSCache cacheObjects:[response objectForKey:@"items"]withUserName:[ZNSUser currentUser].username];
    } errorWithBlock:^(ZNSError *error) {
        
    }];
    
    [APIClient POST:API_OBJECT withParameters:@{@"page":@-1} successWithBlcok:^(id response) {
        [ZNSCache cacheAllObjects:[response objectForKey:@"items"]withUserName:[ZNSUser currentUser].username];
    } errorWithBlock:^(ZNSError *error) {
        //        [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
        
    }];
    
    [APIClient POST:API_USER withParameters:@{@"status":@"启用",@"page":@-1} successWithBlcok:^(id response) {
        [ZNSCache cacheUsers:[response objectForKey:@"items"] withUserName:[ZNSUser currentUser].username];
    } errorWithBlock:^(ZNSError *error) {
        //        [SVProgressHUD showErrorWithStatus:error.errorMessage maskType:SVProgressHUDMaskTypeBlack];
        
    }];
    
}

#pragma mark
#pragma mark - UITableView
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return 2;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.row ==0) {
//        return self.accountCell;
//    }else if(indexPath.row == 1){
//        return self.passwordCell;
//    }else if (indexPath.row == 2){ 
//        return self.verificationCodeCell;
//    }else{
//        return nil;
//    }
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 44;
//}

- (NSString *)uniqueID{

//   return @"uniqueID";
    DTKeychain *keychain = [DTKeychain sharedInstance];
    NSDictionary *query = [DTKeychainGenericPassword keychainItemQueryForService:@"com.contron.uniqueID" account:@"uuid"];
    
    // retrieve matching keychain items
    NSError *error;
    NSArray *items = [keychain keychainItemsMatchingQuery:query error:&error];
    
    if (items.count){
        DTKeychainGenericPassword *pass = items.firstObject;
        return pass.password;
    }else{
        DTKeychainGenericPassword *pass = [DTKeychainGenericPassword new];
        pass.account = @"uuid";
        pass.service = @"com.contron.uniqueID";
        pass.password = [NSString getSha512String:[NSUUID UUID].UUIDString];
        
        // write the new object to the keychain
        NSError *error;
        if (![keychain writeKeychainItem:pass error:&error])
        {
            NSLog(@"%@", [error localizedDescription]);
        }
        return pass.password;
    }

    
}
@end
