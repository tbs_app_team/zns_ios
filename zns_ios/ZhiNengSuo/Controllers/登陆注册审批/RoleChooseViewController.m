//
//  RoleChooseViewController.m
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/12/5.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import "RoleChooseViewController.h"
#import "ZNSRole.h"
#import "MJRefresh.h"

@interface RoleChooseViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *mainTableView;
@property (strong, nonatomic) NSMutableArray * arrData;
@property (strong, nonatomic) NSMutableArray * chooseRoles;
@property (assign, nonatomic) NSInteger page;
@end


@implementation RoleChooseViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initializeUI];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)initializeUI {
    self.title = @"角色选择";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"确定" style:UIBarButtonItemStylePlain target:self action:@selector(commit)];
    [self.mainTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    self.mainTableView.rowHeight = 45;
    self.mainTableView.editing = YES;
    
    if (!self.chooseUser.role.count) {
        self.chooseUser.role = [NSMutableArray array];
    }

    @weakify(self);
    self.mainTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        self.page = 0;
        [self doGetRole];
    }];
    
    self.mainTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self doGetRole];
    }];
    [self.mainTableView.mj_header beginRefreshing];
}

- (void)doGetRole{
    //     self.arrData = [NSArray arrayWithArray:self.chooseUser.role];
    
    [APIClient POST:API_ROLE withParameters:@{@"page":[NSNumber numberWithInteger:self.page]} successWithBlcok:^(id response) {
        
        [self.mainTableView.mj_header endRefreshing];
        [self.mainTableView.mj_footer endRefreshing];
        
        if (!self.arrData) {
            self.arrData = [NSMutableArray array];
        }
        
        if (self.page == 0){
            [self.arrData removeAllObjects];
            
        }
        [self.arrData addObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZNSRole class] json:response[@"items"]]];
        if ([response[@"page"]intValue] + 1 == [response[@"pages"]intValue]) {
            [self.mainTableView.mj_footer endRefreshingWithNoMoreData];
        }
        
        [self.mainTableView reloadData];
        for (int i = 0; i < self.arrData.count; i++) {
            for (ZNSRole * role in self.chooseUser.role) {
                if ([role.rid  isEqualToString:[self.arrData[i] valueForKey:@"rid"]]) {
                    [self.mainTableView scrollToRow:i inSection:0 atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.mainTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
                    });
                    break;
                }
            }
        }
        
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showSuccessWithStatus:error.errorMessage];
        [self.mainTableView.mj_header endRefreshing];
        [self.mainTableView.mj_footer endRefreshing];
        
    }];

    
}

#pragma mark - Action

- (void)commit {
//    NSArray * arrRid = [self.arrData valueForKeyPath:@"id"];
//    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
//    [APIClient POST:[NSString stringWithFormat:@"%@%@%@",API_USER,self.chooseUser.username,API_USER_ADDROLE] withParameters:@{@"rid":[NSString stringWithFormat:@"[%@]",[arrRid componentsJoinedByString:@","]]} successWithBlcok:^(id response) {
//        @weakify(self);
//        [SVProgressHUD show];
//        [APIClient POST:[NSString stringWithFormat:@"%@%@%@",API_USER,self.chooseUser.username,API_USER_VERIFY] withParameters:@{@"action": @"批准"} successWithBlcok:^(id response) {
//            @strongify(self);
//            [SVProgressHUD showSuccessWithStatus:@"审批成功"];
//            if (self.changeBlock) {
//                self.changeBlock();
//            }
//        } errorWithBlock:^(ZNSError *error) {
//            [SVProgressHUD showInfoWithStatus:error.errorMessage];
//        }];
//        [self.navigationController popViewControllerAnimated:YES];
//    } errorWithBlock:^(ZNSError *error) {
//        [SVProgressHUD showSuccessWithStatus:error.errorMessage];
//    }];
    if (self.changeBlock) {
        if (self.chooseUser.role.count) {
            self.changeBlock(self.chooseUser.role);
        }
        [self.navigationController popViewControllerAnimated:YES];
      }
    
}

#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
//    cell.textLabel.text = self.arrData[indexPath.row][@"name"];
     cell.textLabel.text = [self.arrData[indexPath.row] valueForKey:@"name"];
    return cell;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  UITableViewCellEditingStyleDelete|UITableViewCellEditingStyleInsert;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
//    [APIClient POST:[NSString stringWithFormat:@"%@%@%@%@/",API_USER,self.chooseUser.username,API_USER_ADDROLE,self.arrData[indexPath.row][@"id"]] withParameters:@{} successWithBlcok:^(id response) {
//        [SVProgressHUD dismiss];
//        if (self.changeBlock) {
//            self.changeBlock();
//        }
//    } errorWithBlock:^(ZNSError *error) {
//        [SVProgressHUD showSuccessWithStatus:error.errorMessage];
//    }];
    
    [self.chooseUser.role  addObject:[self.arrData objectAtIndex:indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
//    [APIClient POST:[NSString stringWithFormat:@"%@%@%@%@/",API_USER,self.chooseUser.username,API_USER_DELROLE,self.arrData[indexPath.row][@"id"]] withParameters:@{} successWithBlcok:^(id response) {
//        [SVProgressHUD dismiss];
//        if (self.changeBlock) {
//            self.changeBlock();
//        }
//    } errorWithBlock:^(ZNSError *error) {
//        [SVProgressHUD showSuccessWithStatus:error.errorMessage];
//    }];

    [self.chooseUser.role enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([[self.chooseUser.role[idx] valueForKey:@"rid"] isEqual:[[self.arrData objectAtIndex:indexPath.row] valueForKey:@"rid"]]) {
            [self.chooseUser.role removeObjectAtIndex:idx];
        }
    }];
   
}

@end
