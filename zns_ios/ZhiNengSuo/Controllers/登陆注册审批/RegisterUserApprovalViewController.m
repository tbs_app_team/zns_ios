//
//  RegisterUserApprovalViewController.m
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/28.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import "RegisterUserApprovalViewController.h"
#import "RegisterUserApprovalTableViewCell.h"
#import "MJRefresh.h"
#import "ZNSRole.h"
#import "RoleChooseViewController.h"

@interface RegisterUserApprovalViewController () <UITableViewDataSource, UITableViewDelegate> {
    IBOutlet UIButton *disagreeButton;
    IBOutlet UIButton *agreeButton;

}

@property (strong, nonatomic) IBOutlet UITableView *mainTableView;

@property (strong, nonatomic) NSMutableArray * arrData;
@property (assign, nonatomic) NSInteger page;

@end

@implementation RegisterUserApprovalViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.arrData = [@[] mutableCopy];
    [self initializeUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)initializeUI {
    
    [disagreeButton bs_configureAsDangerStyle];
    [agreeButton bs_configureAsSuccessStyle];
    self.mainTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 0;
        [self loadDataFromInternet];
    }];
    self.mainTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self loadDataFromInternet];
    }];
    [self.mainTableView.mj_header beginRefreshing];
}

- (void)loadDataFromInternet {
    //筛选未启用的账户(即申请但未通过的注册账号)
    @weakify(self);
    [APIClient POST:API_USER withParameters:@{@"status": @"待审批" ,@"page" : [NSString stringWithFormat:@"%ld",(long)self.page]} successWithBlcok:^(id response) {
        @strongify(self);
        
        NSArray *tem = [NSArray yy_modelArrayWithClass:[ZNSUser class] json:response[@"items"]];

        if (self.page == 0) {
            [self.arrData removeAllObjects];
            if (tem.count) {
                [self.arrData addObjectsFromArray:tem];
            }
//            self.arrData = [response[@"items"] mutableCopy];
            if (self.arrData.count == 0) {
                [SVProgressHUD showInfoWithStatus:@"暂时没有待审核的用户"];
            }
            [self.mainTableView.mj_header endRefreshing];
        } else {
//            [self.arrData addObjectsFromArray:response[@"items"]];
            [self.arrData addObjectsFromArray:tem];
            [self.mainTableView.mj_footer endRefreshing];
        }
        if ([response[@"page"]intValue] + 1 == [response[@"pages"]intValue]) {
            [self.mainTableView.mj_footer endRefreshingWithNoMoreData];
        } else {
            [self.mainTableView.mj_footer resetNoMoreData];
        }
        [self.mainTableView reloadData];
    } errorWithBlock:^(ZNSError *error) {
        [self.mainTableView.mj_header endRefreshing];
        [self.mainTableView.mj_footer endRefreshing];
        [SVProgressHUD showInfoWithStatus:error.errorMessage];
    }];
}

#pragma mark - Action
- (IBAction)batchApproval:(UIButton *)sender {
    if (sender.tag == 1) {
        [UIAlertController showAlertInViewController:self.navigationController withTitle:@"是否全部同意？" message:@"" cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@[@"是"] tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
            [SVProgressHUD show];
            @weakify(self);
            [ZNSAPITool batchHandel:API_USER_VERIFYALL agree:YES reason:@"" successWithBlcok:^(id response) {
                @strongify(self);
                [SVProgressHUD showSuccessWithStatus:@"已批量同意"];
                
                [self.mainTableView.mj_header beginRefreshing];
            } errorWithBlock:^(ZNSError *error) {
                [SVProgressHUD showErrorWithStatus:error.errorMessage];
            }];
        }];
    }else{
        UIAlertController * ac = [UIAlertController alertControllerWithTitle:@"拒绝这个用户的注册" message:nil preferredStyle:UIAlertControllerStyleAlert];
        [ac addTextFieldWithConfigurationHandler:^(UITextField *textField){
            textField.placeholder = @"请输入理由";
            
        }];
        [ac addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        }]];
        [ac addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            UITextField *tf = ac.textFields.firstObject;
            if (tf.text.length == 0) {
                [SVProgressHUD  showErrorWithStatus:@"输入内容不能为空"];
                return ;
            }
            if ([[NSString isEmpty:tf.text] isEqualToString:@"yes"]) {
                [SVProgressHUD  showErrorWithStatus:@"输入内容不能为空"];
                return ;
            }
            if (tf.text.length > 32) {
                [SVProgressHUD  showErrorWithStatus:@"输入长度应不超过32位"];
                return ;
            }
            
            [SVProgressHUD show];
            @weakify(self);
            [ZNSAPITool batchHandel:API_USER_VERIFYALL agree:NO reason:tf.text successWithBlcok:^(id response) {
                @strongify(self);
                [SVProgressHUD showSuccessWithStatus:@"已批量拒绝"];
                
                [self.mainTableView.mj_header beginRefreshing];
            } errorWithBlock:^(ZNSError *error) {
                [SVProgressHUD showErrorWithStatus:error.errorMessage];
            }];
        }]];
        [self presentViewController:ac animated:YES completion:nil];
    }
}

#pragma mark - UITableView

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [tableView fd_heightForCellWithIdentifier:@"RegisterUserApprovalCell" cacheByIndexPath:indexPath configuration:^(RegisterUserApprovalTableViewCell *cell) {
//        cell.user = [ZNSUser yy_modelWithDictionary:self.arrData[indexPath.row]];
        cell.user = [self.arrData objectAtIndex:indexPath.row];
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RegisterUserApprovalTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"RegisterUserApprovalCell" forIndexPath:indexPath];
    
    
//    cell.user = [ZNSUser yy_modelWithDictionary:self.arrData[indexPath.row]];
    cell.user = [self.arrData objectAtIndex:indexPath.row];
    
//    ZNSUser *chooseUser = [ZNSUser yy_modelWithDictionary:self.arrData[indexPath.row]];
    ZNSUser *chooseUser = [self.arrData objectAtIndex:indexPath.row];
    
    cell.indexBlock = ^(NSInteger index) {
        if (index == 0 ) {
//            [SVProgressHUD showInfoWithStatus:@"请先设置用户角色"];
            RoleChooseViewController * vc = [RoleChooseViewController create];
            vc.chooseUser = chooseUser;
            vc.changeBlock = ^(NSArray *arr){
                
                ZNSUser *user = [self.arrData objectAtIndex:indexPath.row];
//                [user.role removeAllObjects];
                user.role = [NSMutableArray array];
                if (arr.count) {
                    [user.role addObjectsFromArray:arr];
                }
                [self.arrData replaceObjectAtIndex:indexPath.row withObject:user];
//                NSDictionary *test = [NSDictionary yy_modelDictionaryWithClass:[ZNSUser class] json:[user yy_modelToJSONObject]];

                [self.mainTableView reloadData];
                //            [self.mainTableView.mj_header beginRefreshing];
            };
            [self.navigationController pushViewController:vc animated:YES];
            
        }else if (index == 2){
            ZNSUser *user = [self.arrData objectAtIndex:indexPath.row];
            
            if (!user.role.count) {
                
                [SVProgressHUD showInfoWithStatus:@"请先设置用户角色"];
                RoleChooseViewController * vc = [RoleChooseViewController create];
                vc.chooseUser = chooseUser;
                vc.changeBlock = ^(NSArray *arr){
                    
                    ZNSUser *user = [self.arrData objectAtIndex:indexPath.row];
//                    [user.role removeAllObjects];
                    user.role = [NSMutableArray array];
                    if (arr.count) {
                        [user.role addObjectsFromArray:arr];
                    }
                    [self.arrData replaceObjectAtIndex:indexPath.row withObject:user];
                    //                NSDictionary *test = [NSDictionary yy_modelDictionaryWithClass:[ZNSUser class] json:[user yy_modelToJSONObject]];
                    
                    [self.mainTableView reloadData];
                };
                [self.navigationController pushViewController:vc animated:YES];
          }else{
                @weakify(self);
                [SVProgressHUD show];
                NSArray *role = [self.arrData[indexPath.row] valueForKey:@"role"];
//                NSString *string = [[role valueForKeyPath:@"id"] componentsJoinedByString:@"、"];
               NSMutableArray  *ids = [NSMutableArray array];
                [role enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    [ids addObject:[role[idx] valueForKey:@"rid"]];
                }];
              
                [APIClient POST:[NSString stringWithFormat:@"%@%@%@",API_USER,[self.arrData[indexPath.row] valueForKey:@"username"],API_USER_VERIFY] withParameters:@{@"action":@"批准" ,@"memo" : @"", @"role":ids} successWithBlcok:^(id response) {
                    @strongify(self);
                        [SVProgressHUD dismiss];
                        [self.arrData removeAllObjects];
                        [self.mainTableView.mj_header beginRefreshing];
                } errorWithBlock:^(ZNSError *error) {
                        [SVProgressHUD showInfoWithStatus:error.errorMessage];
                }];
 
                
            }
  
        }else {
            UIAlertController * ac = [UIAlertController alertControllerWithTitle:index == 1?@"拒绝这个用户的注册请求":@"同意这个用户的注册请求" message:nil preferredStyle:UIAlertControllerStyleAlert];
            [ac addTextFieldWithConfigurationHandler:^(UITextField *textField){
                textField.placeholder = @"请输入理由";
                
            }];
            [ac addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            }]];
            [ac addAction:[UIAlertAction actionWithTitle:index == 1?@"确定":@"批准" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                UITextField *tf = ac.textFields.firstObject;
                
                if (index == 1 && tf.text.length == 0) {
                    [SVProgressHUD showInfoWithStatus:@"必须输入拒绝理由"];
                }else if (index == 1 && [[NSString isEmpty:tf.text] isEqualToString:@"yes"]) {
                    [SVProgressHUD showInfoWithStatus:@"必须输入拒绝理由"];
                }
                else if (index == 1 && tf.text.length > 32) {
                    [SVProgressHUD showInfoWithStatus:@"输入长度不能大于32位"];
                }
                else {
                    @weakify(self);
                    [SVProgressHUD show];
                    [APIClient POST:[NSString stringWithFormat:@"%@%@%@",API_USER,[self.arrData[indexPath.row] valueForKey:@"username"],API_USER_VERIFY] withParameters:@{@"action":@"拒绝" ,@"memo" : tf.text} successWithBlcok:^(id response) {
                        @strongify(self);
                        [SVProgressHUD dismiss];
                        [self.arrData removeAllObjects];
                        [self.mainTableView.mj_header beginRefreshing];
                    } errorWithBlock:^(ZNSError *error) {
                        [SVProgressHUD showInfoWithStatus:error.errorMessage];
                    }];

                }
            }]];
            [self presentViewController:ac animated:YES completion:nil];
        }
    };
    return cell;
}

@end
