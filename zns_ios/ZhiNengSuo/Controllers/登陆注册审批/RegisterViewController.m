//
//  RegisterViewController.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/25/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "RegisterViewController.h"
#import "ZNSLabelTextFieldTableViewCell.h"
#import "CRMediaPickerController.h"
#import <DTKeychain.h>
#import "ZNSSection.h"

@interface RegisterViewController ()<UITableViewDelegate,UITableViewDataSource,CRMediaPickerControllerDelegate>
@property (nonatomic,weak) IBOutlet UITableView *tableView;

@property (nonatomic,strong) ZNSLabelTextFieldTableViewCell *accountCell;
@property (nonatomic,strong) ZNSLabelTextFieldTableViewCell *passwordCell;
@property (nonatomic,strong) ZNSLabelTextFieldTableViewCell *confirmPasswordCell;
@property (nonatomic,strong) ZNSLabelTextFieldTableViewCell *nameCell;
@property (nonatomic,strong) ZNSLabelTextFieldTableViewCell *infoCell;
@property (nonatomic,strong) ZNSLabelTextFieldTableViewCell *sectionCell;        //区域
@property (nonatomic,strong) ZNSLabelTextFieldTableViewCell *companysCell;       //代维单位

@property (nonatomic,strong) NSArray *sections;                     //区域数组
//@property (nonatomic,strong) ZNSSection *selectedSection;           //选中的区域
@property (nonatomic,strong) NSString *section_id;                  //选中区域的id

@property (nonatomic,strong) NSArray *companys;                     //代维单位数组
@property (nonatomic,strong) NSString *company_id;                  //选中代维单位的id

@property (nonatomic,weak) IBOutlet UIImageView *selfImageView;
@property (nonatomic,weak) IBOutlet UIImageView *cardImageView;

@property (nonatomic,strong) UIImage *selfImage;
@property (nonatomic,strong) UIImage *cardImage;

@property (nonatomic,strong) CRMediaPickerController *imagePicker;
@property (nonatomic,assign) NSInteger currentPicking;//1,2
@end

@implementation RegisterViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"注册";
    
    [self.tableView registerNib:[ZNSLabelTextFieldTableViewCell nib] forCellReuseIdentifier:[ZNSLabelTextFieldTableViewCell reuseIdentifier]];
    [self configCells];
    
    self.cardImageView.clipsToBounds = YES;
    self.selfImageView.clipsToBounds = YES;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    @weakify(self);
//    [SVProgressHUD show];
//    [ZNSAPITool getAllSectionSuccessWithBlcok:^(id response) {
//        @strongify(self);
//        self.sections = response;
//        if ([[[self.sections objectAtIndex:0] valueForKey:@"name"] isEqual: @"0"]) {
//            self.sections = [self.sections subarrayWithRange:NSMakeRange(1, self.sections.count-1)];
//        }
//        [SVProgressHUD dismiss];
//    } errorWithBlock:^(ZNSError *error) {
//        @strongify(self);
//        [SVProgressHUD showErrorWithStatus:@"读取部门列表失败"];
//
//        [self.navigationController popViewControllerAnimated:YES];
//    }];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (void)configCells{
    self.accountCell = [ZNSLabelTextFieldTableViewCell loadFromNib];
    [self.accountCell.label setText:@"账号"];
    [self.accountCell.textField setPlaceholder:@"请输入账号"];
    self.passwordCell = [ZNSLabelTextFieldTableViewCell loadFromNib];
    [self.passwordCell.label setText:@"密码"];
    [self.passwordCell.textField setPlaceholder:@"请输入密码"];
    self.passwordCell.textField.secureTextEntry = YES;
    
    self.confirmPasswordCell = [ZNSLabelTextFieldTableViewCell loadFromNib];
    [self.confirmPasswordCell.label setText:@"确认密码"];
    [self.confirmPasswordCell.textField setPlaceholder:@"请再次输入密码"];
    self.confirmPasswordCell.textField.secureTextEntry = YES;
    
    self.nameCell = [ZNSLabelTextFieldTableViewCell loadFromNib];
    [self.nameCell.label setText:@"姓名"];
    [self.nameCell.textField setPlaceholder:@"请输入姓名"];
    
    self.infoCell = [ZNSLabelTextFieldTableViewCell loadFromNib];
    [self.infoCell.label setText:@"个人说明"];
    
    self.sectionCell = [ZNSLabelTextFieldTableViewCell loadFromNib];
    [self.sectionCell.label setText:@"区域"];
    [self.sectionCell.textField setPlaceholder:@"请选择区域"];
    self.sectionCell.textField.enabled = NO;
    
    self.companysCell = [ZNSLabelTextFieldTableViewCell loadFromNib];
    [self.companysCell.label setText:@"代维单位"];
    [self.companysCell.textField setPlaceholder:@"请选择代维单位"];
    self.companysCell.textField.enabled = NO;

    @weakify(self);
    [self.cardImageView setTapActionWithBlock:^{
        @strongify(self);
        self.currentPicking = 2;
        [self.imagePicker show];
    }];
    
    [self.selfImageView setTapActionWithBlock:^{
        @strongify(self);
        self.currentPicking = 1;
        [self.imagePicker show];
    }];
}

- (CRMediaPickerController *)imagePicker{
    if (!_imagePicker) {
        _imagePicker = [[CRMediaPickerController alloc]init];
        _imagePicker.delegate = self;
        _imagePicker.mediaType = CRMediaPickerControllerMediaTypeImage;
//        _imagePicker.sourceType = (CRMediaPickerControllerSourceTypePhotoLibrary|CRMediaPickerControllerSourceTypeCamera);
       _imagePicker.sourceType = CRMediaPickerControllerSourceTypeCamera;
    }
    return _imagePicker;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showSectionPicker{
    @weakify(self);
    if (!self.sections.count) {
        [SVProgressHUD showWithStatus:@"正在获取数据"];
        [ZNSAPITool getSectionWithUserName:@"0" success:^(id response) {
            @strongify(self);
            [SVProgressHUD dismiss];
            self.sections = response;
            [self showSectionPicker];
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:@"获取数据失败，请重新点击获取"];
        }];
        return;
    }
    [self.view endEditing:YES];
    [ActionSheetStringPicker showPickerWithTitle:@"选择区域" rows:[self.sections valueForKeyPath:@"name"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        [self.sectionCell.textField setText:selectedValue];
        ZNSSection *sec = [self.sections objectAtIndex:selectedIndex];
        self.section_id = sec.sid;
        self.companys = sec.companys;
        
        if ([self.company_id length]) {
            self.company_id = @"";
             [self.companysCell.textField setText:@""];
        }
         
    } cancelBlock:^(ActionSheetStringPicker *picker) {
    } origin:self.view];
}

- (void)showCompanysPicker{
    
    if (![self.section_id length]) {
        [SVProgressHUD showErrorWithStatus:@"请先选择区域"];
        return;
    }
    
    @weakify(self);
    [self.view endEditing:YES];
    [ActionSheetStringPicker showPickerWithTitle:@"选择代维单位" rows:[self.companys valueForKeyPath:@"name"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        @strongify(self);
        if (self.companys.count) {
            [self.companysCell.textField setText:selectedValue];
            self.company_id = [NSString stringWithFormat:@"%@",self.companys[selectedIndex][@"id"]];
        } 
    } cancelBlock:^(ActionSheetStringPicker *picker) {
    } origin:self.view];
}

#pragma mark
#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            return self.accountCell;
            break;
        case 1:
            return self.passwordCell;
            break;
        case 2:
            return self.confirmPasswordCell;
            break;
        case 3:
            return self.nameCell;
            break; 
        case 4:
            return self.sectionCell;
            break;
            
        case 5:
            return self.companysCell;
            break; 
        default:
            return self.infoCell;
            break;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
    if (indexPath.row == 4) {
        [self showSectionPicker];
    }else if (indexPath.row == 5){
        [self showCompanysPicker];
    }
}

- (IBAction)regist:(id)sender{
    if (self.accountCell.textField.text.length<=0) {
        [SVProgressHUD showErrorWithStatus:@"账号不能为空"];
        return;
    }
    if(self.passwordCell.textField.text.length<=0){
        
        [SVProgressHUD showErrorWithStatus:@"密码不能为空"];
        return;
    }
    if (self.confirmPasswordCell.textField.text.length<=0) {
        [SVProgressHUD showErrorWithStatus:@"确认密码不能为空"];
        return;
    }
    if (self.nameCell.textField.text.length<=0) {
        [SVProgressHUD showErrorWithStatus:@"姓名不能为空"];
        return;
    }
    if (![self.section_id length]) {
        [SVProgressHUD showErrorWithStatus:@"请选择区域"];
        return;
    }
    
    if (![self.company_id length]) {
        [SVProgressHUD showErrorWithStatus:@"请选择代维单位"];
        return;
    }
    
    
    if ([self.accountCell.textField.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]].length>0) {
        [SVProgressHUD showErrorWithStatus:@"账号必须为数字"];
        return;
    }
    
  
    if (self.accountCell.textField.text.length!=11) {
        [SVProgressHUD showErrorWithStatus:@"账号必须为11位"];
        return;
    }
    
    if (self.nameCell.textField.text.length>16) {
        [SVProgressHUD showErrorWithStatus:@"姓名最多为16位"];
        return;
    }
    
    NSString *str = [self.nameCell.textField.text  stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if ([str length] < self.nameCell.textField.text.length) {
        [SVProgressHUD showErrorWithStatus:@"姓名不能含有空格"];
        return;
    }
    
   NSString *str1 = [self.passwordCell.textField.text  stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (str1.length < self.passwordCell.textField.text.length) {
        [SVProgressHUD showErrorWithStatus:@"密码不能含有空格"];
        return;
    }
    
    if (self.selfImage==nil) {
        [SVProgressHUD showErrorWithStatus:@"请拍摄头像照片"];
        return;
    }
    if (self.cardImage==nil) {
        [SVProgressHUD showErrorWithStatus:@"请拍摄身份证照片"];
        return;
    }
    
    if (self.passwordCell.textField.text.length>30) {
        [SVProgressHUD showErrorWithStatus:@"密码最多为30位"];
        return;
    }
    
    
    if (![self.passwordCell.textField.text isEqualToString: self.confirmPasswordCell.textField.text]) {
        [SVProgressHUD showErrorWithStatus:@"确认密码不一致"];
        return;
    }
    
    
    if (self.infoCell.textField.text.length>32) {
        [SVProgressHUD showErrorWithStatus:@"个人说明不能超过32位"];
        return;
    }
    
    [SVProgressHUD show];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:@{@"username":self.accountCell.textField.text,@"password":[NSString getSha512String:self.passwordCell.textField.text],@"uniqueid":[self uniqueID],@"name":self.nameCell.textField.text,@"section":self.section_id,@"cid":self.company_id}];
    
    [parameters setValue:STRING_OR_EMPTY(self.infoCell.textField.text) forKey:@"memo"];
    
    @weakify(self);
    [APIClient POST:API_USER_REGISTER withParameters:parameters successWithBlcok:^(id response) {
        @strongify(self);
        [self uploadImageForUid:[response valueForKey:@"username"]];
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:error.errorMessage];
    }];

}
- (NSString *)uniqueID{
//    return @"uniqueID";
    DTKeychain *keychain = [DTKeychain sharedInstance];
    NSDictionary *query = [DTKeychainGenericPassword keychainItemQueryForService:@"com.contron.uniqueID" account:@"uuid"];
    
    // retrieve matching keychain items
    NSError *error;
    NSArray *items = [keychain keychainItemsMatchingQuery:query error:&error];
    
    if (items.count){
        DTKeychainGenericPassword *pass = items.firstObject;
        return pass.password;
    }else{
        DTKeychainGenericPassword *pass = [DTKeychainGenericPassword new];
        pass.account = @"uuid";
        pass.service = @"com.contron.uniqueID";
        pass.password = [NSString getSha512String:[NSUUID UUID].UUIDString];
        
        // write the new object to the keychain
        NSError *error;
        if (![keychain writeKeychainItem:pass error:&error])
        {
            NSLog(@"%@", [error localizedDescription]);
        }
        return pass.password;
    }

}
- (void)uploadImageForUid:(NSString *)uid{
    @weakify(self);
    [ZNSAPITool uploadImage:self.selfImage imageType:ZNSImageTypePortrait type:ZNSUploadTypeRegister forId:uid progressWithBlcok:^(CGFloat percentage) {
        [SVProgressHUD showProgress:percentage status:@"正在上传自拍头像"];
    } successWithBlcok:^(id response) {
        @strongify(self);
        
        [ZNSAPITool uploadImage:self.cardImage imageType:ZNSImageTypeIdCard type:ZNSUploadTypeRegister forId:uid progressWithBlcok:^(CGFloat percentage) {
            [SVProgressHUD showProgress:percentage status:@"正在上传身份证图片"];
        } successWithBlcok:^(id response) {
            @strongify(self);
            [SVProgressHUD showSuccessWithStatus:@"注册成功"];
            [self.navigationController popViewControllerAnimated:YES];
            
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:@"上传身份证图片失败"];
        }];
        
    } errorWithBlock:^(ZNSError *error) {
        [SVProgressHUD showErrorWithStatus:@"上传自拍头像失败"];
    }];
}

#pragma mark
#pragma mark - CRMediaPickerController
- (void)CRMediaPickerController:(CRMediaPickerController *)mediaPickerController didFinishPickingAsset:(ALAsset *)asset error:(NSError *)error{
    if (!error) {
        if (mediaPickerController.sourceType == CRMediaPickerControllerSourceTypeCamera) {
            ALAssetsLibrary * library = [[ALAssetsLibrary alloc] init];
            [library writeImageToSavedPhotosAlbum:asset.defaultRepresentation.fullResolutionImage orientation:asset.defaultRepresentation.orientation completionBlock:nil];
        }
        if (self.currentPicking == 1) {
            [self.selfImageView setImage:[UIImage imageWithCGImage:asset.aspectRatioThumbnail]];
            
            self.selfImage = [UIImage imageWithCGImage:asset.defaultRepresentation.fullResolutionImage scale:1 orientation:(UIImageOrientation)asset.defaultRepresentation.orientation];
        }else{
            [self.cardImageView setImage:[UIImage imageWithCGImage:asset.aspectRatioThumbnail]];
                
            self.cardImage = [UIImage imageWithCGImage:asset.defaultRepresentation.fullResolutionImage scale:1 orientation:(UIImageOrientation)asset.defaultRepresentation.orientation];
        }
    }
}
@end
