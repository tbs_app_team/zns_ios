//
//  ZNSFixedViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/11/7.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "ZNSFixedViewController.h"
#import "BlueToothViewController.h"
#import "ZNSTempTask.h"
#import "FixedPermissionTableViewCell.h"
#import "JKAlertDialog.h"
#import "ZNSTools.h"

@interface ZNSFixedViewController ()<UITableViewDelegate, UITableViewDataSource>{
     JKAlertDialog *dialog;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSMutableData *firstFrame;
@property (nonatomic,strong) NSMutableData *lockIdFrames;
@property (nonatomic,strong) NSMutableData *recordFrames;
@property (nonatomic,strong) NSArray * arrData;
@property (nonatomic,assign) CBCentralManagerState blueState;
@property (nonatomic,strong) NSString *tid;

@end

@implementation ZNSFixedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.arrData = [NSArray array];
    self.firstFrame = [NSMutableData data];
    self.lockIdFrames = [NSMutableData data];
    self.recordFrames = [NSMutableData data];
    
    [self.tableView registerNib:[FixedPermissionTableViewCell nib] forCellReuseIdentifier:@"fixedPermissonCell"];
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self loadData];
    }];
    
    [self.tableView.mj_header beginRefreshing];
    
    [[GGBluetooth sharedManager] setBluetoothStatusUpdateBlock:^(CBCentralManager *centeral){
        @strongify(self)
        self.blueState = (CBCentralManagerState)centeral.state;
    }];
}
#pragma tableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrData.count;
//    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FixedPermissionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"fixedPermissonCell" forIndexPath:indexPath];
    [cell setMyTask:[self.arrData objectAtIndex:indexPath.row]];
    @weakify(self);
    cell.downBlock = ^{
        @strongify(self);
        if (self.blueState == CBCentralManagerStatePoweredOff) {
            [SVProgressHUD showErrorWithStatus:@"该设备尚未打开蓝牙,请在设置中打开"];
        }else{
            [[GGBluetooth sharedManager] disconnectCurrentDevice];
            [self separateFrame:[self.arrData objectAtIndex:indexPath.row]];
        }
    };
    cell.returnBlock=^{
        @strongify(self);
        if (self.blueState == CBCentralManagerStatePoweredOff) {
            [SVProgressHUD showErrorWithStatus:@"该设备尚未打开蓝牙,请在设置中打开"];
        }else{
            [[GGBluetooth sharedManager] disconnectCurrentDevice];
            [self returnData:[self.arrData objectAtIndex:indexPath.row]];
//            [ZNSTools returnData:[self.arrData objectAtIndex:indexPath.row] withTaskFlag:TransFixed taskStr:@"TransFixed"];
        }
    };
    if ([ZNSTools isSCLock]) {
        [cell setBtnHidden:YES];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    
////    [tableView reloadRowAtIndexPath:indexPath withRowAnimation:UITableViewRowAnimationNone];
//    
//}
#pragma mark -- Private

- (void)loadData{
    
    @weakify(self)
    [APIClient POST:API_USER_FIXED_TASK([ZNSUser currentUser].username) withParameters:@{@"status":@"待下载",@"page":@-1} successWithBlcok:^(id response) {
        @strongify(self)
        self.arrData = [NSArray yy_modelArrayWithClass:[ZNSTempTask class] json:response[@"items"]];
        [self.tableView.mj_header endRefreshing];
        //        [self.tableView.mj_footer endRefreshingWithNoMoreData];
        [self.tableView reloadData];
    } errorWithBlock:^(ZNSError *error) {
        [self.tableView.mj_header endRefreshing];
        NSLog(@"%@",error.errorMessage);
    }];
}
- (void)separateFrame:(ZNSTempTask *)task{
    
    [self.firstFrame resetBytesInRange:NSMakeRange(0, [self.firstFrame length])];
    [self.firstFrame setLength:0];
    
    [self.lockIdFrames resetBytesInRange:NSMakeRange(0, [self.lockIdFrames length])];
    [self.lockIdFrames setLength:0];
    
    NSInteger guid = task.tid.integerValue;
    
    //guid 16byte
    NSData *taskId = [ZNSTools getTaskIdDataFromInt:guid taskFlag:TransFixed];
    [self.firstFrame appendData:taskId];
    
    //name 16byte
    NSData * tem = [NSData data];
    tem = [ZNSTools getNameDataFromString:task.name nameLength:16];
    [self.firstFrame appendData:tem];
    
    
    //lock count
    __block NSUInteger temCount = 0;
    __block NSMutableArray *locks=[NSMutableArray array];
    [task.objects enumerateObjectsUsingBlock:^(ZNSObject *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        temCount += obj.lockset.count;
        [locks addObjectsFromArray:obj.lockset];
    }];
    tem = [NSData dataWithBytes:&temCount length:4];
    [self.firstFrame appendData:tem];
    
    //lockIds
    __block NSMutableData *temIdData = [NSMutableData data];
    [locks enumerateObjectsUsingBlock:^(ZNSLock* obj, NSUInteger idx, BOOL * _Nonnull stop) {
        int16_t i1 = [obj.lid integerValue];
        NSData *tem1 = [NSData dataWithBytes:&i1 length:2];
        [temIdData appendData:tem1];
        
        //        i1 = [obj.stationId integerValue];
        i1 = 1;
        tem1 = [NSData dataWithBytes:&i1 length:2];
        [temIdData appendData:tem1];
        
    }];
    [self.lockIdFrames appendData:temIdData];
    
    //user count
    guid = task.users.count;
    tem = [NSData dataWithBytes:&guid length:2];
    [self.firstFrame appendData:tem];
    
    
    //users id
    temIdData = [NSMutableData data];
    [task.users enumerateObjectsUsingBlock:^(ZNSUser *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        temCount = obj.idid.integerValue;
        [temIdData appendBytes:&temCount length:2];
    }];
    [self.firstFrame appendData:temIdData];
    
    //task limited
//    guid = task.limited.integerValue;
//    tem = [NSData dataWithBytes:&guid length:4];
//    [self.firstFrame appendData:tem];
    task.begin_at = [NSString formatTime:[NSDate date] withFormat:@"yyyyMMddHHmmss"];
    task.end_at = [NSString formatTime:[[NSDate date]dateByAddingMinutes:task.limited.integerValue] withFormat:@"yyyyMMddHHmmss"];
    tem  =  [ZNSTools getFinalDateString:[NSData dataWithHexString:[NSString dateHexStringFromDigitalString:task.begin_at]]];
    [self.firstFrame appendData:tem];
    
    tem  = [ZNSTools getFinalDateString:[NSData dataWithHexString:[NSString dateHexStringFromDigitalString:task.end_at]]];
    [self.firstFrame appendData:tem];
    
    [UIAlertController showAlertInViewController:self.parentViewController withTitle:@"选择钥匙" message:@"数据准备完毕，是否下载到钥匙" cancelButtonTitle:@"否" destructiveButtonTitle:nil otherButtonTitles:@[@"是"] tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
        if(buttonIndex == 2){
            [self selectKey];
            _tid = task.tid;
        }
    }];
    NSLog(@"%@",self.firstFrame);
    NSLog(@"%@",self.lockIdFrames);
}

- (void)selectKey{
    static BlueToothViewController *btVC;
    
    if (!btVC) {
        btVC = [BlueToothViewController create];
    }
    
    @weakify(self)
    [btVC setConnectedBlock:^{
        @strongify(self)
        __block NSUInteger i=0;
        
        [[GGBluetooth sharedManager] setNotifyCharacteristicBlock:^(CBCharacteristic *characteristic,NSData *recvData,NSError *error){
            @strongify(self)
            ZNSBluetoothInstruction *b = [[ZNSBluetoothInstruction alloc]initWithByteData:characteristic.value];
            
            if (b.function == Func_Code_00 ) {//firstFrame
                if (b.isRight == 0x80) {
                    if ([self.lockIdFrames length]) {
                        NSUInteger framesCount = [self.lockIdFrames length]%256==0? [self.lockIdFrames length]/256 : ([self.lockIdFrames length]/256 + 1);
                        NSData *framesData = [NSData dataWithBytes:&framesCount length:2];
                        ZNSBluetoothInstruction *d=[[ZNSBluetoothInstruction alloc] initWithMoreData:Cmd_Code_03 function:Func_Code_01 body:nil rfid:framesData];
                        [[GGBluetooth sharedManager] writeValueData:d.data];
                        
                    }else{
//                        [self updateStatusDownloaded];
                        [SVProgressHUD showSuccessWithStatus:@"任务下载成功"];
                        [[GGBluetooth sharedManager] disconnectCurrentDevice];
                    }
                    
                }else{
                    [SVProgressHUD showErrorWithStatus:@"任务下载失败，请重试"];
                    [[GGBluetooth sharedManager] disconnectCurrentDevice];
                }
                
            }
            
            if (b.function == Func_Code_01 || b.function == Func_Code_02 ) {//lockid frames
                if (b.isRight == 0x80) {
                    if(i!=([self.lockIdFrames length]%256==0? [self.lockIdFrames length]/256 : ([self.lockIdFrames length]/256 + 1))) {
                        [[GGBluetooth sharedManager] sendFrame:(i+1) numberOfFramesPer:[self.lockIdFrames length]%256 == 0 ? 256/4: ([self.lockIdFrames length]%256)/4 frameLen:256  cmd:Cmd_Code_03 fun:Func_Code_02 data:self.lockIdFrames];
                        i++;
                        NSLog(@"lock frame ：%ld",i);
                    }else{
                        i = 0;
//                        [self updateStatusDownloaded];
                        [SVProgressHUD showSuccessWithStatus:@"任务下载成功"];
                        [[GGBluetooth sharedManager] disconnectCurrentDevice];
                    }
                    
                    
                    
                }else{
                    i = 0;
                    [SVProgressHUD showErrorWithStatus:@"任务下载失败，请重试"];
                    [[GGBluetooth sharedManager] disconnectCurrentDevice];
                }
                
            }
            
        }];
        
        
        [[GGBluetooth sharedManager] setSendFailureBlock:^{
            [SVProgressHUD showErrorWithStatus:@"钥匙无回应，请重试"];
            [[GGBluetooth sharedManager] disconnectCurrentDevice];
        }];
        [SVProgressHUD showWithStatus:@"正在下载任务到钥匙"];
        [self packFixedPession];
        
        
    }];
    dialog = [[JKAlertDialog alloc]initWithTitle:@"选择智能钥匙" message:@""];
    dialog.contentView = btVC.view;
    
    [dialog addButton:Button_CANCEL withTitle:@"取消" handler:^(JKAlertDialogItem *item) {
        
    }];;
    
    [btVC setConnectedSmartKeyBlock:^(ZNSSmartKey *smartKey){
        [dialog dismiss];
        
    }];
    
    [dialog show];
    [btVC setupBT];
    [btVC loadSmartKeys];
}

- (void)updateStatusDownloaded{
    
//    @weakify(self)
    [APIClient POST:API_USER_FIXED_STATUS(_tid) withParameters:@{@"status":@"已下载"} successWithBlcok:^(id response) {
//        @strongify(self)
        NSLog(@"response=%@", response);
    } errorWithBlock:^(ZNSError *error) {
        NSLog(@"%@",error.errorMessage);
    }];
}

- (void)deletedTask:(ZNSTempTask *)task{
    
    NSData *tem = [ZNSTools getTaskIdDataFromInt:task.tid.integerValue taskFlag:TransFixed];
    
    //删除任务
    ZNSBluetoothInstruction *da =[[ZNSBluetoothInstruction alloc] initWithInstruction:Cmd_Code_06 function:Func_Code_00 body:nil rfid:tem];
         [[GGBluetooth sharedManager] writeValueData:da.data];
//    //固定任务日志删除，钥匙没回复
//    for (int j=0; j<=([da.data length]/20); j++) {
//        [[[GGBluetooth sharedManager] connectedPheripheral]  writeValue:[da.data subdataWithRange:NSMakeRange(0+j*20, j==[da.data length]/20?([da.data length]-20*j):20)] forCharacteristic:[[GGBluetooth sharedManager] writeableCharacteristic] type:[[GGBluetooth sharedManager] writeableCharacteristic].properties==(CBCharacteristicPropertyRead+CBCharacteristicPropertyWriteWithoutResponse+CBCharacteristicPropertyNotify)?CBCharacteristicWriteWithoutResponse:CBCharacteristicWriteWithResponse];
//    }
    
}
- (void)packFixedPession{
    
    [[GGBluetooth sharedManager] setCanWriteValueBlock:^{
        
        [[GGBluetooth sharedManager] writeValueData:[[ZNSBluetoothInstruction alloc] initWithMoreData:Cmd_Code_03 function:Func_Code_00 body:nil rfid:self.firstFrame].data];
    }];
    
}

- (void)returnData:(ZNSTempTask *)task{
    
    static BlueToothViewController *btVC;
    if (!btVC) {
        btVC = [BlueToothViewController create];
    }
    
    @weakify(self)
    [btVC setConnectedBlock:^{
        
        __block int16_t frameCount = 0;
        __block int16_t user_id = 0;
        __block int16_t j = 0;
        [[GGBluetooth sharedManager] setNotifyCharacteristicBlock:^(CBCharacteristic *characteristic,NSData *recvData,NSError *error){
            @strongify(self)
            ZNSBluetoothInstruction *b = [[ZNSBluetoothInstruction alloc]initWithByteData:recvData];
            NSLog(@"%@",recvData);
            if(b.function == Func_Code_00 && b.cmd == Cmd_Code_05){
                int taskCount=0 ;
                
                [[b.bodyData subdataWithRange:NSMakeRange(0, 2)] getBytes:&taskCount length:2];
                if (taskCount == 0) {
                    [SVProgressHUD showInfoWithStatus:@"钥匙中没有可回传的任务"];
                    [[GGBluetooth sharedManager] disconnectCurrentDevice];
                }else{
                    
                    NSData *tem = [ZNSTools getTaskIdDataFromInt:task.tid.integerValue taskFlag:TransFixed];
                    int   i=0;
                    BOOL hasReturnTicket = NO;
                    for ( i=0; i<taskCount; i++){
                        
                        if ([tem isEqualToData:[b.bodyData subdataWithRange:NSMakeRange(2+i*16, 16)]]) {
                            //任务回传
                            hasReturnTicket = YES;
                            ZNSBluetoothInstruction *da =[[ZNSBluetoothInstruction alloc] initWithInstruction:Cmd_Code_04 function:Func_Code_00 body:nil rfid:tem];
                            //                        NSLog(@"%@",da.data);
                            [[GGBluetooth sharedManager] writeValueData:da.data];
                            
                        }
                    }
                    if (!hasReturnTicket) { //钥匙中要回传的任务不在列表中
                        [SVProgressHUD showInfoWithStatus:@"钥匙中需要回传的任务不在列表中"];
                        [[GGBluetooth sharedManager] disconnectCurrentDevice];
                    }
                }
            }
            
            if (b.function == Func_Code_00 && b.cmd == Cmd_Code_04) {//回传总帧数
                
                [[b.bodyData subdataWithRange:NSMakeRange(0, 2)]  getBytes:&frameCount length:2];
                [[b.bodyData subdataWithRange:NSMakeRange(2, 2)] getBytes:&user_id length:2];
                if(j!=frameCount) {
                    j++;
                    NSData *frame = [NSData dataWithBytes:&j length:2];
                    [[GGBluetooth sharedManager] writeValueData:[[ZNSBluetoothInstruction alloc] initWithMoreData:Cmd_Code_04 function:Func_Code_01 body:nil rfid:frame].data];
                    
                    
                }else{
                    [SVProgressHUD showInfoWithStatus:@"钥匙中无当前任务操作记录"];
//                    [[GGBluetooth sharedManager] disconnectCurrentDevice];
                    [self deletedTask:task];
                }
            }
            if (b.function == Func_Code_01 && b.cmd == Cmd_Code_04) {
                
                [self.recordFrames appendData:[b.bodyData subdataWithRange:NSMakeRange(4, [b.bodyData length]-4)]];
                
                if(j!=frameCount) {
                    j++;
                    NSData *frame = [NSData dataWithBytes:&j length:2];
                    [[GGBluetooth sharedManager] writeValueData:[[ZNSBluetoothInstruction alloc] initWithMoreData:Cmd_Code_04 function:Func_Code_01 body:nil rfid:frame].data];
                }else{
                    j = 0;
                    frameCount = 0;
                    NSData * test = [NSData dataWithData:self.recordFrames];
                    //                        NSLog(@"%@",test);//test
                    NSMutableArray *logs = [NSMutableArray array];
                    NSData *bit0;
                    for (int i=0; i<([test length]/17); i++) {
                        
                        NSMutableData *tmp=[NSMutableData data];
                        bit0 = [test subdataWithRange:NSMakeRange(1+i*17, 1)];
                        [tmp appendData:bit0];
                        bit0 = [test subdataWithRange:NSMakeRange(0+i*17, 1)];
                        [tmp appendData:bit0];
                        
                        if (![[tmp hexString] isEqualToString:@"FFFF"]) {
                            [logs addObject:@{
                                              
                                              @"lid": [NSString stringWithFormat:@"%ld",strtoul([[tmp hexString] UTF8String], 0, 16)],
                                              @"unlock_at": [NSString timeStringFormHexString:[[test subdataWithRange:NSMakeRange(9+i*17, 7)] hexString]],
                                              @"result": [NSString stringWithFormat:@"%ld",strtoul([[[test subdataWithRange:NSMakeRange(16+i*17, 1)] hexString] UTF8String], 0, 16)],
                                              }];
                        }
                        
                    }
                    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{@"tid":task.tid,
                                                                                               @"action":@"TransFixed",
                                                                                               @"uid":[NSString stringWithFormat:@"%i",user_id]}];
                    [dic setObject:logs forKey:@"taskHistory"];
                    [APIClient POST:API_TASK_HISTORY withParameters:dic successWithBlcok:^(id response) {
                        @strongify(self)
//                        [self updateStatusDownloaded];
                        //固定任务删除操作记录
                        [self deletedTask:task];
                        [self.recordFrames resetBytesInRange:NSMakeRange(0, [self.recordFrames length])];
                        [self.recordFrames setLength:0];
                        [SVProgressHUD showSuccessWithStatus:@"回传成功"];
//                        [[GGBluetooth sharedManager] disconnectCurrentDevice];
                        
                    } errorWithBlock:^(ZNSError *error) {
                        [self.recordFrames resetBytesInRange:NSMakeRange(0, [self.recordFrames length])];
                        [self.recordFrames setLength:0];
                        [SVProgressHUD showErrorWithStatus:error.errorMessage];
                        [[GGBluetooth sharedManager] disconnectCurrentDevice];
                    }];
                    
                }
                
            }
            if (b.cmd == Cmd_Code_06 && b.function == Func_Code_00) {
                
                NSLog(@"任务删除成功");
                [[GGBluetooth sharedManager] disconnectCurrentDevice];
            }
        }];
        [[GGBluetooth sharedManager] setSendFailureBlock:^{
            [SVProgressHUD showErrorWithStatus:@"钥匙无回应，请重试"];
            [[GGBluetooth sharedManager] disconnectCurrentDevice];
        }];
        [[GGBluetooth sharedManager] setCanWriteValueBlock:^{//任务查询
            [[GGBluetooth sharedManager] writeValueData:[[ZNSBluetoothInstruction alloc] initWithMoreData:Cmd_Code_05 function:Func_Code_00 body:nil rfid:nil].data];
        }];
        
    }];
    dialog = [[JKAlertDialog alloc]initWithTitle:@"选择智能钥匙" message:@""];
    dialog.contentView = btVC.view;
    
    [dialog addButton:Button_CANCEL withTitle:@"取消" handler:^(JKAlertDialogItem *item) {
        
    }];;
    
    //    @weakify(self);
    [btVC setConnectedSmartKeyBlock:^(ZNSSmartKey *smartKey){
        //        @strongify(self);
        [dialog dismiss];
        
    }];
    
    [dialog show];
    [btVC setupBT];
    [btVC loadSmartKeys];
    
}


@end
