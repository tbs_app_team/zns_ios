//
//  ZNSInitialViewController.m
//  ZhiNengSuo
//
//  Created by ekey on 16/6/14.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "ZNSInitialViewController.h"
#import "BlueToothViewController.h"
#import "ZNSTools.h"
#import "SCBluetoothReturn.h"
#import "SCBluetoothInstruction.h"

@interface ZNSInitialViewController ()<UITableViewDelegate,UITableViewDataSource>{
    __block NSMutableData *userData;
    __block NSMutableData *lockNameData;
    __block NSMutableData *lockIdData;
    __block NSMutableData *lockRfidData;
    __block NSMutableData *lockTypeData;
    __block NSMutableData *lockLogicData;
    __block NSMutableData *lockConfigData;
    
    
    __weak IBOutlet UISegmentedControl *segmentControl;
    __weak IBOutlet UILabel *lblInfo;
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *packetButton;
@property (nonatomic,strong) NSArray *data;
@property (nonatomic,strong) NSMutableArray *selectedData;
@property (nonatomic,strong) JKAlertDialog *dialog;
@property (nonatomic,strong) NSArray *locks;

- (IBAction)packBtnOnClick:(id)sender;

@end

@implementation ZNSInitialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    if ([ZNSTools isSCLock]) {
        [self configBT];
        [_packetButton setTitle:@"初始化锁具" forState:UIControlStateNormal];
        segmentControl.hidden = YES;
        lblInfo.hidden = YES;
        _tableView.hidden = YES;
    }
    else {
        self.data=@[@"配置信息",
                    @"锁信息",
                    @"锁名称",
                    @"验电逻辑",
                    @"用户数据"];
        self.selectedData = [NSMutableArray array];
        [self.packetButton bs_configureAsPrimaryStyle];
        self.locks = [NSArray array];
        lockNameData = [NSMutableData data];
        userData = [NSMutableData data];
        lockIdData = [NSMutableData data];
        lockRfidData = [NSMutableData data];
        lockTypeData = [NSMutableData data];
        lockLogicData = [NSMutableData data];
        lockConfigData = [NSMutableData data];
        
        _tableView.editing = YES;
        [_tableView.tableHeaderView setHidden:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if ([ZNSTools isSCLock]) {
        [[GGBluetooth sharedManager]disconnectCurrentDevice];
        [_dialog dismiss];
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if ([ZNSTools isSCLock]) {
        [self initBlueTooth];
    }
}

#pragma mark -- segmentDelegate

- (IBAction)segmentValueChanged:(UISegmentedControl *)sender {
    NSLog(@"%ld",sender.selectedSegmentIndex);
    if (sender.selectedSegmentIndex == 1) {
        self.data=@[@"配置信息",
                    @"锁信息",
                    @"锁名称",
                    @"用户数据"];
        [self.tableView setHeight:176.0];

    }else{
        self.data=@[@"配置信息",
                    @"锁信息",
                    @"锁名称",
                    @"验电逻辑",
                    @"用户数据"];
        [self.tableView setHeight:220.0];
    }
    self.selectedData =[@[] mutableCopy];
    [self.tableView reloadData];
    
    
}

#pragma mark -- tableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.data.count;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  UITableViewCellEditingStyleDelete|UITableViewCellEditingStyleInsert;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
//    if ([self.selectedData containsObject:[self.data objectAtIndex:indexPath.row]]) {
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//    }else{
//        cell.accessoryType = UITableViewCellAccessoryNone;
//    }
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.textLabel.text = [self.data objectAtIndex:indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
     [self.selectedData addObject:[self.data objectAtIndex:indexPath.row]];

//    [self.tableView reloadData];
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([self.selectedData containsObject:[self.data objectAtIndex:indexPath.row]]) {
        [self.selectedData removeObject:[self.data objectAtIndex:indexPath.row]];
    }
}


#pragma mark - Other

- (BOOL)checkSelectedItemIsOk{
    if (![self.selectedData containsObject:@"锁名称"]) {
        return YES;
    }
    
    if (![self.selectedData containsObject:@"配置信息"]) {
        return NO;
    }
    if (![self.selectedData containsObject:@"锁信息"]) {
        return NO;
    }
//    if (![self.selectedData containsObject:@"验电逻辑"]) {
//        return NO;
//    }
//    if (![self.selectedData containsObject:@"用户数据"]) {
//        return NO;
//    }
    return YES;
}

- (IBAction)packBtnOnClick:(id)sender {
    
    if ([ZNSTools isSCLock]) {
        [self initBlueTooth];
        return;
    }
    
    if (![self checkSelectedItemIsOk]) {
        [SVProgressHUD showErrorWithStatus:@"如果已选择打包锁名称，则必须同时选择配置信息和锁信息"];
        return;
    }
    
    //下载数据，下载成功后，弹出对话框，选择钥匙
    [SVProgressHUD showWithStatus:@"正在下载数据。。。" ];
    if ([self.selectedData containsObject:@"配置信息"] ||
        [self.selectedData containsObject:@"锁信息"] ||
        [self.selectedData containsObject:@"锁名称"] ||
        [self.selectedData containsObject:@"验电逻辑"] ) {
        
        [lockRfidData resetBytesInRange:NSMakeRange(0, lockRfidData.length)];
        [lockRfidData setLength:0];  ;
        [lockLogicData resetBytesInRange:NSMakeRange(0, lockLogicData.length)];
        [lockLogicData setLength:0];  ;
        [lockTypeData resetBytesInRange:NSMakeRange(0, lockTypeData.length)];
        [lockTypeData setLength:0];  ;
        [lockIdData resetBytesInRange:NSMakeRange(0, lockIdData.length)];
        [lockIdData setLength:0];  ;
        [lockNameData resetBytesInRange:NSMakeRange(0, lockNameData.length)];
        [lockNameData setLength:0];
        [userData resetBytesInRange:NSMakeRange(0, userData.length)];
        [userData setLength:0];
        [lockConfigData resetBytesInRange:NSMakeRange(0, lockConfigData.length)];
        [lockConfigData setLength:0];
        self.locks = [NSArray array];
        
        
        @weakify(self)
      [APIClient POST:@"section/0/object/"  withParameters:@{@"page":@-1} successWithBlcok:^(id response) {
            @strongify(self)
            NSArray *objects = [NSArray yy_modelArrayWithClass:[ZNSObject class] json:[response valueForKey:@"items"]];

            [objects enumerateObjectsUsingBlock:^(ZNSObject *obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    @strongify(self);
                
                NSArray *types = [obj.lockset valueForKey:@"type"];
                if ([types containsObject:@"验电锁"] || [types containsObject:@"无源验电锁"]) {
                    NSUInteger i;
                    NSString *lockId;
                    if ([types containsObject:@"验电锁"]) {
                        i = [types indexOfObject:@"验电锁"];
                        ZNSLock *tmp = [obj.lockset objectAtIndex:i];
                        tmp.logic = @"ffffffff";
                        tmp.type = @"254";
                        lockId = tmp.lid;
                    }else{
                        i = [types indexOfObject:@"无源验电锁"];
                        
                        ZNSLock *tmp = [obj.lockset objectAtIndex:i];
                        tmp.logic = @"ffffffff";
                        tmp.type = @"251";
                        lockId = tmp.lid;
                        
                        if ([tmp.rfid length] == 10) {
                            NSString *rfid = tmp.rfid;
                            tmp.rfid = [rfid stringByReplacingCharactersInRange:NSMakeRange(9, 1) withString:@"0"];
                        }  
                    }
                    [obj.lockset enumerateObjectsUsingBlock:^(ZNSLock *lock, NSUInteger idx, BOOL * _Nonnull stop) {
                        if(idx != i){
                          //低字节在前高再后
                          lock.logic = lockId;
                          lock.type = @"253";
                        }
                    }];
                    
                }else{
                    [obj.lockset enumerateObjectsUsingBlock:^(ZNSLock *lock, NSUInteger idx, BOOL * _Nonnull stop) {
                           lock.logic = @"ffffffff";
                           lock.type = @"1";
                    }];
                }
                
//                for (ZNSLock *lock in obj.lockset) {
//                    if ([lock.type isEqualToString:@"验电锁"]) {
//                        lock.logic = @"ffffffff";
//                        lock.type = @"254";
//                    }else if ([lock.type isEqualToString:@"无源验电锁"]){
//                        lock.logic = @"ffffffff";
//                        lock.type = @"251";
//                    }else{
//
//                    }
//                }
                

//                NSString *yds_id = @"";//判断是否有验电锁
//                for (NSInteger j = 0; j < obj.lockset.count; j++) {
//                    ZNSLock *loc = obj.lockset[j];
//                    if ([loc.type containsString:@"验电锁"]) {
//                        yds_id = loc.lid;
//                    }
//                    loc.logic = @"ffffffff";
//                    loc.type = @"1";
//
//                }
//
//                if ([yds_id length]) {
//                    for (NSInteger k = 0; k < obj.lockset.count; k++) {
//                        ZNSLock *lk = obj.lockset[k];
//                        if ([yds_id isEqualToString:lk.lid]) {
//                            lk.logic = @"ffffffff";
//                            lk.type = @"254";
//                        }else{
//                            lk.logic = [NSString stringWithFormat:@"%@",yds_id];
//                            lk.type = @"253";
//                        }
//                    }
//                }
 
//                    if([[obj.lockset valueForKey:@"type"] containsObject:@"验电锁"]){
//                        NSUInteger i;
//                        i=[[obj.lockset valueForKey:@"type"]indexOfObject:@"验电锁"];
//                        ZNSLock *tmp = [obj.lockset objectAtIndex:i];
//                        tmp.logic = @"ffffffff";
//                        tmp.type = @"254";
//
//                        [obj.lockset enumerateObjectsUsingBlock:^(ZNSLock *lock, NSUInteger idx, BOOL * _Nonnull stop) {
//                            if(idx != i){
////                                lock.logic = [NSString stringWithFormat:@"%@%@",tmp.lid,tmp.stationId];
//                                //低字节在前高再后
//                                lock.logic = [NSString stringWithFormat:@"%@",tmp.lid];
//                                lock.type = @"253";
//                            }
//                        }];
//                    }else{
//                        [obj.lockset enumerateObjectsUsingBlock:^(ZNSLock *lock, NSUInteger idx, BOOL * _Nonnull stop) {
//
//                            lock.logic = @"ffffffff";
//                            lock.type = @"1";
//
//                        }];
//
//                    }
                     self.locks = [self.locks arrayByAddingObjectsFromArray:obj.lockset];


            }];

           [self.locks enumerateObjectsUsingBlock:^(ZNSLock* obj, NSUInteger idx, BOOL * _Nonnull stop) {
               
//               NSData *temName =  [obj.name  dataUsingEncoding:gb_2312Encoding];
//               if ([temName length] <= 60) {
//
//                   [lockNameData  appendBytes:[temName bytes] length:[temName length]];
//                    Byte add[60]= {0};
//                   [lockNameData appendBytes:&add length:60-[temName length]];
//               }else{
//                   
//                    [lockNameData appendBytes:[temName bytes] length:60];
//               }
               NSData *temName = [ZNSTools getNameDataFromString:obj.name nameLength:60];
               [lockNameData appendData:temName];

               int16_t i1 = [obj.lid integerValue];
//               int16_t i2 = [obj.stationId integerValue];
               int16_t i2 = 1 ;
               NSData *tem1 = [NSData dataWithBytes:&i1 length:2];
               NSData *tem2 = [NSData dataWithBytes:&i2 length:2];
               [lockIdData appendData:tem1];
               [lockIdData appendData:tem2];
               
               i1 = [obj.type integerValue];
               Byte type = i1;
               [lockTypeData appendBytes:&type length:sizeof(type)];
               
               //注意rfid的高低字节顺序
               tem1 = [NSData dataWithHexString:obj.rfid];
               if (tem1==nil) {
                   Byte address[] = {0x00,0x00,0x00,0x00,0x00};
                   [lockRfidData appendBytes:&address length:sizeof(address)];
               }else{
                   [lockRfidData appendData:tem1];
               }
//               tem1 = [NSData dataWithHexString:obj.logic];
               if ([obj.logic isEqualToString:@"ffffffff"]) {
                    tem1 = [NSData dataWithHexString:obj.logic];
               }else{
                   int16_t loc16 = [obj.logic integerValue];
                   Byte bbbbb = loc16;
                   NSData *locData = [NSData dataWithBytes:&bbbbb length:sizeof(bbbbb)];

                   Byte station_bye[] = {0x00,0x01,0x00};
                   NSMutableData *ddddd = [NSMutableData dataWithData:locData];
                   [ddddd appendBytes:station_bye length:sizeof(station_bye)];
                   tem1 = [NSData dataWithData:ddddd];
               }
//               Byte moni_bye[] = {0xff,0xff,0xff,0xff,0xc0,0x00,0x01,0x00};
              [lockLogicData appendData:tem1];
//               lockLogicData = [NSMutableData dataWithBytes:moni_bye length:sizeof(moni_bye)];
           }];
     
            [APIClient POST:API_USER  withParameters:@{@"page":@-1} successWithBlcok:^(id response) {
               
               NSArray *users = [NSArray yy_modelArrayWithClass:[ZNSUser class] json:[response valueForKey:@"items"]];
                
               [users enumerateObjectsUsingBlock:^(ZNSUser* obj, NSUInteger idx, BOOL * _Nonnull stop) {
                   NSUInteger i = [obj.idid integerValue];
                   NSData *temp = [NSData dataWithBytes:&i length: 2];
                   [userData appendData:temp];
                   
                   temp = [ZNSTools getNameDataFromString:obj.username nameLength:19];
                   [userData appendData:temp];
//                   temp = [obj.username dataUsingEncoding:gb_2312Encoding];
//                   if ([temp length]<19) {
//                       [userData  appendBytes:[temp bytes] length:[temp length]];
//                       Byte add[19]= {0};
//                       [userData appendBytes:&add length:19-[temp length]];
//                   }else{
//                       [userData  appendBytes:[temp bytes] length:19];
//                   }
                   
                   temp = [ZNSTools getNameDataFromString:obj.password nameLength:6];
                   [userData appendData:temp];
//                   temp = [obj.password dataUsingEncoding:gb_2312Encoding];
//                   if (temp==nil) {
//                       Byte add[]={0x00,0x00,0x00,0x00,0x00,0x00};
//                       [userData appendBytes:&add length:sizeof(add)];
//                   }else{
//
//                     [userData appendBytes:[temp bytes] length:6];
//                   }
                   
                   temp = [NSData dataWithHexString:obj.rfid];
                   if (temp==nil) {
                       Byte add[]={0x00,0x00,0x00,0x00,0x00};
                       [userData appendBytes:&add length:sizeof(add)];
                   }else{
                        [userData appendBytes:[temp bytes] length:5];
                   }
                  
                   
               }];
                
                [SVProgressHUD dismiss];
                
                if (([self.selectedData containsObject:@"锁名称"] || [self.selectedData containsObject:@"验电逻辑"] || [self.selectedData containsObject:@"锁信息"]) && !self.locks.count) {
                    
                    [UIAlertController showAlertInViewController:self.parentViewController withTitle:@"提示" message:@"锁具数据为空，无法打包" cancelButtonTitle:@"否" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
                        
                    }];
                    return;
                }
                
                [self isDownload];
           } errorWithBlock:^(ZNSError *error) {
               [SVProgressHUD showErrorWithStatus:error.errorMessage];
           }];

        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:error.errorMessage];
        }];
    
    }else if ([self.selectedData containsObject:@"用户数据"]){
        @weakify(self)
        [APIClient POST:API_USER  withParameters:@{@"page":@-1} successWithBlcok:^(id response) {
            
            @strongify(self)
            userData = [NSMutableData data] ;
            NSArray *users = [NSArray yy_modelArrayWithClass:[ZNSUser class] json:[response valueForKey:@"items"]];
            
            [users enumerateObjectsUsingBlock:^(ZNSUser* obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSUInteger i = [obj.sid integerValue];
                NSData *temp = [NSData dataWithBytes:&i length: 2];
               [userData appendData:temp];
                
                temp = [ZNSTools getNameDataFromString:obj.username nameLength:19];
                [userData appendData:temp];
               
                temp = [ZNSTools getNameDataFromString:obj.password nameLength:6];
                [userData appendData:temp];
                
                temp = [NSData dataWithHexString:obj.rfid];
                if (temp==nil) {
                    Byte add[5]={0x00};
                    [userData appendBytes:&add length:5];
                }else{
                    [userData appendBytes:[temp bytes] length:5];
                }
                
            }];
            [SVProgressHUD dismiss];
            [self isDownload];
            
        } errorWithBlock:^(ZNSError *error) {
            [SVProgressHUD showErrorWithStatus:error.errorMessage];
        }];
    } else{
     [SVProgressHUD showInfoWithStatus:@"请选择需要打包的数据"];
    }
}


#pragma mark -- Private

- (void)isDownload{
    [UIAlertController showAlertInViewController:self.parentViewController withTitle:@"提示" message:@"数据下载完毕，是否下载到钥匙" cancelButtonTitle:@"否" destructiveButtonTitle:nil otherButtonTitles:@[@"是"] tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
        if (buttonIndex==2) {
            [self selectKey];
        }
        
        
    }];
}
- (void)selectKey{
    static BlueToothViewController *btVC;
    if (!btVC) {
        btVC = [BlueToothViewController create];
    }
    [btVC setSmartKeyType:(segmentControl.selectedSegmentIndex == 1 ? @"E402" : @"E401")];
    
    @weakify(self)
    [btVC setConnectedBlock:^{
        @strongify(self)
    __block NSUInteger i=0;
    [[GGBluetooth sharedManager] setSendFailureBlock:^{
        [SVProgressHUD showErrorWithStatus:@"初始化失败，请重试"];
        [[GGBluetooth sharedManager] disconnectCurrentDevice];
    }];
    [[GGBluetooth sharedManager] setNotifyCharacteristicBlock:^(CBCharacteristic *characteristic,NSData *recvData,NSError *error){
        @strongify(self)
         [SVProgressHUD show];
        ZNSBluetoothInstruction *b = [[ZNSBluetoothInstruction alloc]initWithByteData:characteristic.value];
        if (b.cmd == 0x01 && b.function == 0x00) {
            if ( [self.selectedData containsObject:@"配置信息"] ) {
                [self packLockConfig];
            }else if ( [self.selectedData containsObject:@"锁信息"] ) {
                [self packLockId];
            }else if ([self.selectedData containsObject:@"锁名称"]) {
                [self packLockNames];
            }else if ([self.selectedData containsObject:@"验电逻辑"]) {
                [self packLockLogic];
            }else if ([self.selectedData containsObject:@"用户数据"]){
                [self packUserCmd];
            }
        }
        if (b.cmd == 0x02 && b.function == 0x00) {//lock config  4+2+16+16+6＋6+11 bytes
            if (b.isRight == 0x80) {
                i = 0;
                if ( [self.selectedData containsObject:@"锁信息"] ) {
                    [self packLockId];
                }else if ([self.selectedData containsObject:@"锁名称"]) {
                    [self packLockNames];
                }else if ([self.selectedData containsObject:@"验电逻辑"]) {
                    [self packLockLogic];
                }else if ([self.selectedData containsObject:@"用户数据"]){
                    [self packUserCmd];
                }else{
                    [SVProgressHUD showSuccessWithStatus:@"初始化成功"];
                    [[GGBluetooth sharedManager] disconnectCurrentDevice];
                }
               
            }else{
                i = 0;
                [SVProgressHUD showErrorWithStatus:@"初始化lockConfig失败，请重试"];
                [[GGBluetooth sharedManager] disconnectCurrentDevice];
            }
            
        }
        if (b.function == 0x01 || b.function == 0x02) {//lock id 4bytes
            if (b.isRight == 0x80) {
                if(i!=([lockIdData length]%FRAME_LENGTH==0 ? [lockIdData length]/FRAME_LENGTH : ([lockIdData length]/FRAME_LENGTH + 1))) {
                    
                    [[GGBluetooth sharedManager] sendFrame:(i+1) numberOfFramesPer:[lockIdData length]%FRAME_LENGTH == 0 ? FRAME_LENGTH/4: ([lockIdData length]%FRAME_LENGTH)/4 frameLen:FRAME_LENGTH  cmd:0x02 fun:0x02 data:lockIdData];
                    i++;
                     NSLog(@"lock id ：%ld",i);
                }else{
                    i = 0;
                    [self packLockRfid];
                }
                
                
            }else{
                i = 0;
                [SVProgressHUD showErrorWithStatus:@"初始化lockId失败，请重试"];
                [[GGBluetooth sharedManager] disconnectCurrentDevice];
            }
            
        }

        if (b.function == 0x03 || b.function == 0x04) {//lock rfid 5bytes
            if (b.isRight == 0x80) {
                if(i!=([lockRfidData length]%255==0 ? [lockRfidData length]/255 : ([lockRfidData length]/255 + 1))) {
                    
                    [[GGBluetooth sharedManager] sendFrame:(i+1) numberOfFramesPer:[lockRfidData length]%255 == 0 ? 255/5: ([lockRfidData length]%255)/5 frameLen:255  cmd:0x02 fun:0x04 data:lockRfidData];
                    i++;
                     NSLog(@"lock rfid ：%ld",i);
                }else{
                    i = 0;
                    [self packLockType];
                }
                
                
            }else{
                i = 0;
                [SVProgressHUD showErrorWithStatus:@"初始化失败，请重试"];
                [[GGBluetooth sharedManager] disconnectCurrentDevice];
            }
            
        }
        if (b.function == 0x07 || b.function == 0x08) {//lock type 1byte
            if (b.isRight == 0x80) {
                if(i!=([lockTypeData length]%FRAME_LENGTH==0 ? [lockTypeData length]/FRAME_LENGTH : ([lockTypeData length]/FRAME_LENGTH + 1))) {
                    
                    [[GGBluetooth sharedManager] sendFrame:(i+1) numberOfFramesPer:[lockTypeData length]%FRAME_LENGTH == 0 ? FRAME_LENGTH: ([lockTypeData length]%FRAME_LENGTH) frameLen:FRAME_LENGTH  cmd:0x02 fun:0x08 data:lockTypeData];
                    i++;
                     NSLog(@"lock type ：%ld",i);
                }else{
                    i = 0;
                    if ([self.selectedData containsObject:@"锁名称"]) {
                        [self packLockNames];
                    }else if ([self.selectedData containsObject:@"验电逻辑"]) {
                        [self packLockLogic];
                    }else if ([self.selectedData containsObject:@"用户数据"]){
                        [self packUserCmd];
                    }else{
                        [SVProgressHUD showSuccessWithStatus:@"初始化成功"];
                        [[GGBluetooth sharedManager] disconnectCurrentDevice];
                    }
                    
                }
                
            }else{
                i = 0;
                [SVProgressHUD showErrorWithStatus:@"初始化失败，请重试"];
                [[GGBluetooth sharedManager] disconnectCurrentDevice];
            }
        }
        
         if (b.function == 0x05 || b.function == 0x06) {//lock name 60byte
            if (b.isRight == 0x80) {
                if(i!=([lockNameData length]%240==0 ? [lockNameData length]/240 : ([lockNameData length]/240 + 1))) {
                    
                    [[GGBluetooth sharedManager] sendFrame:(i+1) numberOfFramesPer:[lockNameData length]%240 == 0 ? 240/60: ([lockNameData length]%240)/60 frameLen:240  cmd:0x02 fun:0x06 data:lockNameData];
                    i++;
                    NSLog(@"lock name ：%ld",i);
                }else{
                    i = 0;
                    if ([self.selectedData containsObject:@"验电逻辑"]) {
                        [self packLockLogic];
                    }else if ([self.selectedData containsObject:@"用户数据"]){
                        [self packUserCmd];
                    }else{
                        [SVProgressHUD showSuccessWithStatus:@"初始化成功"];
                        [[GGBluetooth sharedManager] disconnectCurrentDevice];
                    }

                }
          
            }else{
                i = 0;
                [SVProgressHUD showErrorWithStatus:@"初始化失败，请重试"];
                [[GGBluetooth sharedManager] disconnectCurrentDevice];
            }
        }
        
   
        if (b.function == 0x0b || b.function == 0x0c) {//lock logic 4byte
            if (b.isRight == 0x80) {
                if(i!=([lockLogicData length]%FRAME_LENGTH==0 ? [lockLogicData length]/FRAME_LENGTH : ([lockLogicData length]/FRAME_LENGTH + 1))) {
                    
                     [[GGBluetooth sharedManager] sendFrame:(i+1) numberOfFramesPer:[lockLogicData length]%FRAME_LENGTH == 0 ? FRAME_LENGTH/4: ([lockLogicData length]%FRAME_LENGTH)/4 frameLen:FRAME_LENGTH  cmd:0x02 fun:0x0c data:lockLogicData];
                    i++;
                    NSLog(@"lock logic ：%ld",i);
                }else{
                    i = 0;
                    if ([self.selectedData containsObject:@"用户数据"]){
                        [self packUserCmd];
                    }else{
                        [SVProgressHUD showSuccessWithStatus:@"初始化成功"];
                        [[GGBluetooth sharedManager] disconnectCurrentDevice];
                    }
                }
          
            }else{
                i = 0;
                [SVProgressHUD showErrorWithStatus:@"初始化失败，请重试"];
                [[GGBluetooth sharedManager] disconnectCurrentDevice];
            }

        }
        if (b.function == 0x0d || b.function == 0x0e) {//user 32bytes
            if (b.isRight == 0x80) {
                if(i!=([userData length]%FRAME_LENGTH==0 ? [userData length]/FRAME_LENGTH : ([userData length]/FRAME_LENGTH + 1))) {
                    [[GGBluetooth sharedManager] sendFrame:(i+1) numberOfFramesPer:[userData length]%FRAME_LENGTH == 0 ? FRAME_LENGTH/32: ([userData length]%FRAME_LENGTH)/32 frameLen:FRAME_LENGTH  cmd:0x02 fun:0x0e data:userData];
                    i++;
                    NSLog(@"user ：%ld",i);
                }else{
                    i = 0;
                    [SVProgressHUD showSuccessWithStatus:@"初始化成功"];
                    [[GGBluetooth sharedManager] disconnectCurrentDevice];
                }
                
                
            }else{
                i = 0;
                [SVProgressHUD showInfoWithStatus:@"初始化失败，请重试"];
                [[GGBluetooth sharedManager] disconnectCurrentDevice];
            }
            
        }
        
    }];
        
        [SVProgressHUD showWithStatus:@"正在初始化"];
        [self proofingTime];
    }];
    _dialog = [[JKAlertDialog alloc]initWithTitle:@"选择智能钥匙" message:@""];
    _dialog.contentView = btVC.view;
    
    [_dialog addButton:Button_CANCEL withTitle:@"取消" handler:^(JKAlertDialogItem *item) {
        
    }];;
    
    [btVC setConnectedSmartKeyBlock:^(ZNSSmartKey *smartKey){
        [_dialog dismiss];

        NSUInteger num = self.locks.count ;
        [lockConfigData appendBytes:&num length:4];
        num = [userData length]/32; //user count
        [lockConfigData appendBytes:&num length:2];
        
         NSData *tmData = [ZNSTools getNameDataFromString:smartKey.name nameLength:16];
        [lockConfigData appendData:tmData];
       
        [lockConfigData appendBytes:&num length:16];
        
//        tmData = [@"400030" dataUsingEncoding:gb_2312Encoding];
//        [lockConfigData appendData:tmData];
        tmData = [ZNSTools getNameDataFromString:@"400030" nameLength:6];
        [lockConfigData appendData:tmData];
        
        tmData = [NSData dataWithHexString:smartKey.btAddr];
        [lockConfigData appendData:tmData];
        
        
        if (smartKey.phone == nil) {
//            tmData = [@"00000000000" dataUsingEncoding:gb_2312Encoding];
            tmData = [ZNSTools getNameDataFromString:@"00000000000" nameLength:11];
            [lockConfigData appendData:tmData];
        }else{
//            tmData = [smartKey.phone dataUsingEncoding:gb_2312Encoding];
             tmData = [ZNSTools getNameDataFromString:smartKey.phone nameLength:11];
            [lockConfigData appendData:tmData];
        }
        NSLog(@"lockConfigData = %@",lockConfigData);
        
 
     
 
    }];

    [_dialog show];
    [btVC setupBT];
    [btVC loadSmartKeys];
}

//年份是低字节在前
-(void)proofingTime{
    [[GGBluetooth sharedManager] setCanWriteValueBlock:^{
        
        NSString * timeString = [NSString dateHexStringFromDigitalString:[ZNSDate getCurrentTime]];
        NSData *timeData = [NSData dataWithHexString:timeString];
        
        NSMutableData *finalData = [NSMutableData data];
        NSData *tmp = [NSData data];
        tmp = [timeData subdataWithRange:NSMakeRange(1, 1)];
        [finalData appendData:tmp];
        tmp = [timeData subdataWithRange:NSMakeRange(0, 1)];
        [finalData appendData:tmp];
        tmp = [timeData subdataWithRange:NSMakeRange(2, timeData.length-2)];
        [finalData appendData:tmp];
        
        ZNSBluetoothInstruction *da =[[ZNSBluetoothInstruction alloc] initWithInstruction:0x01 function:0x00 body:nil rfid:finalData];
        
        [[GGBluetooth sharedManager] writeValueData:da.data];

    }];
}
- (void)packLockConfig{
    
    ZNSBluetoothInstruction *da =[[ZNSBluetoothInstruction alloc] initWithInstruction:0x02 function:0x00 body:nil rfid:lockConfigData];

    [[GGBluetooth sharedManager] writeValueData:da.data];

}

- (void)packLockNames{
    
    if (self.locks.count) {

          NSUInteger length =[lockNameData length]%240==0? [lockNameData length]/240 : ([lockNameData length]/240 + 1);
        NSData *tmp = [NSData dataWithBytes:&length length:2];
        
        [[GGBluetooth sharedManager] writeValueData:[[ZNSBluetoothInstruction alloc] initWithInstruction:0x02 function:0x05 body:nil rfid:tmp].data];

    }else{
        if ([self.selectedData containsObject:@"用户数据"]){
            [self packUserCmd];
        }else{
            [SVProgressHUD showSuccessWithStatus:@"初始化成功"];
            [[GGBluetooth sharedManager] disconnectCurrentDevice];
        }
    }
    
}

- (void)packUserCmd{
    
    if ([userData length]) {
        NSUInteger length =[userData length]%256==0? [userData length]/256 : ([userData length]/256 + 1);
        NSData *tmp = [NSData dataWithBytes:&length length:2];
        
        [[GGBluetooth sharedManager] writeValueData:[[ZNSBluetoothInstruction alloc] initWithInstruction:0x02 function:0x0d body:nil rfid:tmp].data];

    }

}

- (void)packLockId{
   
    if (self.locks.count) {
        
        NSUInteger length =[lockIdData length]%FRAME_LENGTH==0? [lockIdData length]/FRAME_LENGTH : ([lockIdData length]/FRAME_LENGTH + 1);
        NSData *tmp = [NSData dataWithBytes:&length length:2];
        [[GGBluetooth sharedManager]writeValueData:[[ZNSBluetoothInstruction alloc] initWithInstruction:0x02 function:0x01 body:nil rfid:tmp].data ];
    }else{
        if ([self.selectedData containsObject:@"用户数据"]){
                        [self packUserCmd];
        }else{
            [SVProgressHUD showSuccessWithStatus:@"初始化成功"];
            [[GGBluetooth sharedManager] disconnectCurrentDevice];
        }
    }

}

- (void)packLockRfid{
    
    NSUInteger length =[lockRfidData length]%255==0? [lockRfidData length]/255 : ([lockRfidData length]/255 + 1);
    NSData *tmp = [NSData dataWithBytes:&length length:2];
    [[GGBluetooth sharedManager] writeValueData:[[ZNSBluetoothInstruction alloc] initWithInstruction:0x02 function:0x03 body:nil rfid:tmp].data ];

}

- (void)packLockType{
    
    NSUInteger length =[lockTypeData length]%256==0? [lockTypeData length]/256 : ([lockTypeData length]/256 + 1);
    NSData *tmp = [NSData dataWithBytes:&length length:2];
    [[GGBluetooth sharedManager] writeValueData:[[ZNSBluetoothInstruction alloc] initWithInstruction:0x02 function:0x07 body:nil rfid:tmp].data];
}

- (void)packLockLogic{
    
    if (self.locks.count) {

        NSUInteger length =[lockLogicData length]%256==0? [lockLogicData length]/256 : ([lockLogicData length]/256 + 1);
        NSData *tmp = [NSData dataWithBytes:&length length:2];
        NSData *data = [[ZNSBluetoothInstruction alloc] initWithInstruction:0x02 function:0x0b body:nil rfid:tmp].data;
        [[GGBluetooth sharedManager] writeValueData:data];

    }else{
        if ([self.selectedData containsObject:@"用户数据"]){
            [self packUserCmd];
        }else{
            [SVProgressHUD showSuccessWithStatus:@"初始化成功"];
            [[GGBluetooth sharedManager] disconnectCurrentDevice];
        }
    }
}

-(void)initBlueTooth
{
    if (![[GGBluetooth sharedManager] connectedPheripheral]) {
        static BlueToothViewController *btVC;
        if (!btVC) {
            btVC = [BlueToothViewController create];
        }
        [btVC setConnectedBlock:^{
            [_dialog dismiss];
            
            [[GGBluetooth sharedManager] setCanWriteValueBlock:^{
                
                [[GGBluetooth sharedManager] writeValueDataSC:[SCBluetoothInstruction superClearInstruction].data];
            }];
            
            [[GGBluetooth sharedManager] setSendFailureBlock:^{
                [SVProgressHUD showErrorWithStatus:@"初始化失败，请重试"];
            }];
        }];
        
        
        btVC.timeOutBlock = ^{
            [_dialog dismiss];
            [SVProgressHUD dismiss];
        };
        
        _dialog = [[JKAlertDialog alloc]initWithTitle:@"选择智能钥匙" message:@""];
        _dialog.contentView = btVC.view;
        
        [_dialog addButton:Button_CANCEL withTitle:@"取消" handler:^(JKAlertDialogItem *item) {
            NSLog(@"click %@",item.title);
        }];;
        
        [_dialog show];
        [btVC setupBT];
        [btVC loadSmartKeys];
    }
}

-(void)configBT
{
    [[GGBluetooth sharedManager] setNotifyCharacteristicBlock:^(CBCharacteristic *characteristic,NSData *recvData,NSError *error){
        if ([ZNSTools isSCLock]) {
            
            SCBluetoothReturn *bluetoothReturn = [[SCBluetoothReturn alloc]initWithByteData:recvData];
            
            switch ([bluetoothReturn bluetoothReturnType]) {
                case ReturnTypeInit:
                {
                    SCInitResult *result = [bluetoothReturn parseInitInstruction];
                    if (result.mState == 1) {
                        [SVProgressHUD showSuccessWithStatus:@"初始化成功"];
                    }
                    else {
                        [SVProgressHUD showErrorWithStatus:@"初始化失败"];
                    }
                }
                    break;
                case ReturnTypeLockInfo:
                {
                    SCLockInfoResult *result = [bluetoothReturn parseLockInfoInstruction];
                    
                    if (result.mState == 1) {
                        if (result.mLockState == 1) {
                            [SVProgressHUD showSuccessWithStatus:@"锁已开"];
                            return ;
                        }
                        if (result.mLockInstallState == 0) {
                            [[GGBluetooth sharedManager] writeValueDataSC:[SCBluetoothInstruction clearInstruction].data];
                            return ;
                        }
                        if (result.mLockState == 2 && result.mLockInstallState == 1) {
                            [SVProgressHUD showErrorWithStatus:@"锁具已安装"];
                        }
                    }
                    
                }
                    break;
                case ReturnTypeClear:
                {
                    SCClearResult *result = [bluetoothReturn parseClearInstruction];
                    if (result.mState == 1) {
                        [[GGBluetooth sharedManager] writeValueDataSC:[SCBluetoothInstruction initInstruction].data];
                    }
                    else {
                        [SVProgressHUD showErrorWithStatus:@"清空失败"];
                    }
                }
                    break;
                case ReturnTypeSuperClear:
                {
                    SCSuperClearResult *result = [bluetoothReturn parseSuperClearInstruction];
                    if (result.mState == 1) {
                        [[GGBluetooth sharedManager] writeValueDataSC:[SCBluetoothInstruction initInstruction].data];
                    }
                    else {
                        [SVProgressHUD showErrorWithStatus:@"超级清空失败"];
                    }
                }
                    break;
                default:
                    break;
            }
        }
    }];
}

@end
