//
//  main.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/22/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, @"ELCUIApplication", NSStringFromClass([AppDelegate class]));
    }
}
