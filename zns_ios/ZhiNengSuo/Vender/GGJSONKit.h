//
//  GGJSONKit.h
//  GGCommonKit
//
//  Created by Ron on 14-3-22.
//  Copyright (c) 2014年 HGG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(GGJSONKit)

- (id)gg_objectFromJSONString;
- (NSData *)gg_JSONData;

@end

@interface NSData(GGJSONKit)

- (id)gg_objectFromJSONData;

@end

@interface NSArray(GGJSONKit)

- (NSData *)gg_JSONData;
- (NSString *)gg_JSONString;

@end

@interface NSDictionary(GGJSONKit)

- (NSData *)gg_JSONData;
- (NSString *)gg_JSONString;

@end