//
//  ZFSettingValueForImageCell.h
//  cv_ios
//
//  Created by Chuenlu Kuo on 2018/7/17.
//  Copyright © 2018年 dbjtech. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZFSettingItem;

@interface ZFSettingValueForImageCell : UITableViewCell

@property (nonatomic, strong) ZFSettingItem *item;

+ (instancetype)settingCellWithTableView:(UITableView *)tableView;

@end
