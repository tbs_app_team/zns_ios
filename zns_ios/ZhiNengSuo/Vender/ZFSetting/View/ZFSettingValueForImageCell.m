//
//  ZFSettingValueForImageCell.m
//  cv_ios
//
//  Created by Chuenlu Kuo on 2018/7/17.
//  Copyright © 2018年 dbjtech. All rights reserved.
//

#import "ZFSettingValueForImageCell.h"
#import "ZFSettingItem.h"

@interface ZFSettingValueForImageCell(){
    
    IBOutlet UIImageView *valueImage;
    
}

@end

@implementation ZFSettingValueForImageCell

+ (instancetype)settingCellWithTableView:(UITableView *)tableView{
    // 0.用static修饰的局部变量，只会初始化一次
    static NSString *ID = @"ValueImageCell";
    
    // 1.拿到一个标识先去缓存池中查找对应的Cell
    ZFSettingValueForImageCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    
    // 2.如果缓存池中没有，才需要传入一个标识创建新的Cell
    if (!cell) {
        cell = [[ZFSettingValueForImageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self = [[[NSBundle mainBundle] loadNibNamed:@"ZFSettingValueForImageCell" owner:self options:nil] lastObject];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}
 

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setItem:(ZFSettingItem *)item {
    _item = item;
    // 设置数据
    self.imageView.image = item.icon.length ? [UIImage imageNamed:item.icon] : nil;
    self.textLabel.text = item.title;
    valueImage.image = item.image;
    
    if (item.type == ZFSettingItemTypeArrow) {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        // 用默认的选中样式
        self.selectionStyle = UITableViewCellSelectionStyleBlue;
    } else if (item.type == ZFSettingItemTypeSwitch) {
        // 禁止选中
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    } else {
        // 什么也没有，清空右边显示的view
        self.accessoryView = nil;
        // 用默认的选中样式
        self.selectionStyle = UITableViewCellSelectionStyleBlue;
    }
}

@end
