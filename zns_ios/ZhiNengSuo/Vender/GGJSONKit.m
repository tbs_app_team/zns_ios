//
//  GGJSONKit.m
//  hotelbrt
//
//  Created by Ron on 9/26/14.
//  Copyright (c) 2014 HGG. All rights reserved.
//

#import "GGJSONKit.h"

@implementation NSString(GGJSONKit)

- (id)gg_objectFromJSONString{
    return self.gg_JSONData.gg_objectFromJSONData;
}

- (NSData *)gg_JSONData{
    return [self dataUsingEncoding:NSUTF8StringEncoding];
}

@end

@implementation NSData(GGJSONKit)

- (id)gg_objectFromJSONData{
    return [NSJSONSerialization JSONObjectWithData:self options:NSJSONReadingAllowFragments error:nil];
}

@end

@implementation NSArray(GGJSONKit)

- (NSData *)gg_JSONData{
    NSData *data = nil;
    if ([NSJSONSerialization isValidJSONObject:self]) {
        data = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:nil];
    }else{
        NSLog(@"--->>object %@ not a json object",self);
    }
    return data;
}

- (NSString *)gg_JSONString{
    NSString *string = nil;
    NSData *data = self.gg_JSONData;
    if (data) {
        string = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    }
    return string;
}

@end

@implementation NSDictionary(GGJSONKit)

- (NSData *)gg_JSONData{
    NSData *data = nil;
    if ([NSJSONSerialization isValidJSONObject:self]) {
        data = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:nil];
    }else{
        NSLog(@"--->>object %@ not a json object",self);
    }
    return data;
}

- (NSString *)gg_JSONString{
    NSString *string = nil;
    NSData *data = self.gg_JSONData;
    if (data) {
        string = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    }
    return string;
}

@end