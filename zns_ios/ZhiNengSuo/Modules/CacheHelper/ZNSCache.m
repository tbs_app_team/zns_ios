//
//  ZNSCache.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/16/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "ZNSCache.h"

@implementation ZNSCache
+ (YYDiskCache*)offlineDataCache{
    static YYDiskCache *cache;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:@"offlineData"];
        cache = [[YYDiskCache alloc]initWithPath:filePath];
    });
    return cache;
}

+ (YYDiskCache*)smartKeyDataCache{
    static YYDiskCache *cache;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:@"smartKey"];
        cache = [[YYDiskCache alloc]initWithPath:filePath];
    });
    return cache;
}

+ (YYDiskCache*)objectsDataCache{
    static YYDiskCache *cache;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:@"object"];
        cache = [[YYDiskCache alloc]initWithPath:filePath];
    });
    return cache;
}
+ (YYDiskCache*)allObjectsDataCache{
    static YYDiskCache *cache;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:@"allObject"];
        cache = [[YYDiskCache alloc]initWithPath:filePath];
    });
    return cache;
}
+ (YYDiskCache*)userDataCache{
    static YYDiskCache *cache;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:@"user"];
        cache = [[YYDiskCache alloc]initWithPath:filePath];
    });
    return cache;
}
+ (YYDiskCache*) offlineKeyDataCache{
    static YYDiskCache *cache;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:@"offlineKey"];
        cache = [[YYDiskCache alloc]initWithPath:filePath];
    });
    return cache;
}
+ (YYDiskCache*) onlineKeyDataCache{
    static YYDiskCache *cache;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:@"onlineKey"];
        cache = [[YYDiskCache alloc]initWithPath:filePath];
    });
    return cache;
}
+ (YYDiskCache*) offlineBtDataCache{
    static YYDiskCache *cache;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:@"offlineBt"];
        cache = [[YYDiskCache alloc]initWithPath:filePath];
    });
    return cache;
}

+ (YYDiskCache*) onlineBtDataCache{
    static YYDiskCache *cache;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:@"onlineBt"];
        cache = [[YYDiskCache alloc]initWithPath:filePath];
    });
    return cache;
}

+ (ZNSOffline*)findOfflineDataOfUserName:(NSString *)userName{
    return [ZNSOffline yy_modelWithDictionary:(NSDictionary*)[[self offlineDataCache]objectForKey:userName]];
}

+ (void)cacheOffline:(ZNSOffline*)offline withUserName:(NSString *)userName{
    if (offline) {
        [[self offlineDataCache] setObject:[offline yy_modelToJSONObject] forKey:userName];
    }else{
        [[self offlineDataCache] removeObjectForKey:userName];
    }
}

+ (NSArray*)findSmartKeysCacheOfUserName:(NSString *)userName{
    return [NSArray yy_modelArrayWithClass:[ZNSSmartKey class] json:[[self smartKeyDataCache]objectForKey:userName]];
}

+ (void)cacheSmartKeys:(NSArray*)keys withUserName:(NSString *)userName{
    [[self smartKeyDataCache] setObject:[keys yy_modelToJSONObject] forKey:userName];
}

+ (void)cacheObjects:(NSDictionary *)dic withUserName:(NSString *)userName{
    [[self objectsDataCache] setObject:dic forKey:userName];
}

+ (void)cacheAllObjects:(NSDictionary *)dic withUserName:(NSString *)userName{
    [[self allObjectsDataCache] setObject:dic forKey:userName];
}

+ (void)cacheUsers:(NSArray *)dic withUserName:(NSString *)userName{
    [[self userDataCache] setObject:[dic yy_modelToJSONObject] forKey:userName];
    
}
+ (NSArray *)getUsers:(NSString *)userName{
    return [NSArray yy_modelArrayWithClass:[ZNSUser class] json:[[self userDataCache]objectForKey:userName]];

}
+ (void)cacheOfflineKeyOperation:(NSArray *)offlines withUserName:(NSString *)userName{
    if (offlines) {
      [[self offlineKeyDataCache] setObject:[offlines yy_modelToJSONObject] forKey:userName];
    }else{
        [[self offlineKeyDataCache] removeObjectForKey:userName];
    }

    
}
+ (NSArray *) getOfflineKeyOperation:(NSString *)userName{
    NSArray *tem = [NSArray yy_modelArrayWithClass:[ZNSOfflineKeyOperation class] json:[[self offlineKeyDataCache]objectForKey:userName]];
    
    if (tem == nil) {
        return [NSArray array];
    }
    return tem;
    
}
+ (void)cacheOfflineBtOperation:(NSArray *)offlines withUserName:(NSString *)userName{
    if (offlines) {
        [[self offlineBtDataCache] setObject:[offlines yy_modelToJSONObject]   forKey:userName];
    }else{
        [[self offlineBtDataCache] removeObjectForKey:userName];
    }
    
    
}
+ (NSArray *) getOfflineBtOperation:(NSString *)userName{
    NSArray *tem = [NSArray yy_modelArrayWithClass:[ZNSOfflineBtOperation class] json:[[self offlineBtDataCache]objectForKey:userName]];
    
    if (tem == nil) {
        return [NSArray array];
    }
    return tem;
}
+ (void)cacheOnlineKeyOperation:(NSArray *)offlines withUserName:(NSString *)userName{
    if (offlines) {
        [[self onlineKeyDataCache] setObject:[offlines yy_modelToJSONObject] forKey:userName];
    }else{
        [[self onlineKeyDataCache] removeObjectForKey:userName];
    }
    
    
}
+ (NSArray *) getOnlineKeyOperation:(NSString *)userName{
    
    NSArray *tem = [NSArray yy_modelArrayWithClass:[ZNSOfflineKeyOperation class] json:[[self onlineKeyDataCache]objectForKey:userName]];
    
    if (tem == nil) {
        return [NSArray array];
    }
    return tem;
    
}
+ (void)cacheOnlineBtOperation:(NSArray *)offlines withUserName:(NSString *)userName{
    if (offlines) {
        [[self onlineBtDataCache] setObject:[offlines yy_modelToJSONObject]   forKey:userName];
    }else{
        [[self onlineBtDataCache] removeObjectForKey:userName];
    }
    
    
}
+ (NSArray *) getOnlineBtOperation:(NSString *)userName{
    NSArray *tem = [NSArray yy_modelArrayWithClass:[ZNSOfflineBtOperation class] json:[[self onlineBtDataCache]objectForKey:userName]];
    
    if (tem == nil) {
        return [NSArray array];
    }
    return tem;
}

+ (NSArray *)getAllLocksets{
    
    NSArray *objects = [NSArray yy_modelArrayWithClass:[ZNSObject class] json:[[self objectsDataCache]objectForKey:[ZNSUser currentUser].username]];
    __block NSArray *locks = @[];
    [objects enumerateObjectsUsingBlock:^(ZNSObject *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        locks = [locks arrayByAddingObjectsFromArray:obj.lockset];
    }];
    return locks;
}




+ (ZNSObject*)getCacheOfOid:(NSString *)oid{
    NSArray *objects = [NSArray yy_modelArrayWithClass:[ZNSObject class] json:[[self objectsDataCache]objectForKey:@"userName"]];
    ZNSObject *obj = [objects objectAtIndex:[[objects valueForKey:@"oid"] indexOfObject:oid]];
    return obj;
}
+ (NSArray *)getAllObjects:(NSString*)username{
    NSArray *objects = [NSArray yy_modelArrayWithClass:[ZNSObject class] json:[[self allObjectsDataCache]objectForKey:username]];
    return objects;
}
+ (NSArray *)getAllLocksetsFromAllObjects{
    
    NSArray *objects = [NSArray yy_modelArrayWithClass:[ZNSObject class] json:[[self allObjectsDataCache]objectForKey:[ZNSUser currentUser].username]];
    __block NSArray *locks = @[];
    [objects enumerateObjectsUsingBlock:^(ZNSObject *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        locks = [locks arrayByAddingObjectsFromArray:obj.lockset];
    }];
    return locks;
}


@end
