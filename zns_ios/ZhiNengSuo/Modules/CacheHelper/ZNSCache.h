//
//  ZNSCache.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/16/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZNSOffline.h"
#import "ZNSSmartKey.h"
#import "ZNSObject.h"
#import "ZNSLock.h"
#import "ZNSOfflineKeyOperation.h"
#import "ZNSOfflineBtOperation.h"

@interface ZNSCache : NSObject

+ (ZNSOffline*)findOfflineDataOfUserName:(NSString *)userName;

+ (void)cacheOffline:(ZNSOffline*)offline withUserName:(NSString *)userName;

+ (NSArray*)findSmartKeysCacheOfUserName:(NSString *)userName;

+ (void)cacheSmartKeys:(NSArray*)keys withUserName:(NSString *)userName;

+ (void)cacheObjects:(NSDictionary *)dic withUserName:(NSString *)userName;
+ (NSArray *)getAllLocksets;
+ (ZNSObject*)getCacheOfOid:(NSString *)oid;

//+ (void)cacheLockers:(NSArray *)dic withUserName:(NSString *)userName;
//+ (NSArray*)findLockersCacheOfUserName:(NSString *)userName;
+ (void)cacheUsers:(NSArray *)dic withUserName:(NSString *)userName;
+ (NSArray *)getUsers:(NSString *)userName;

+ (NSArray *) getOfflineKeyOperation:(NSString *)userName;
+ (void)cacheOfflineKeyOperation:(NSArray *)offlines withUserName:(NSString *)userName;

+ (NSArray *) getOfflineBtOperation:(NSString *)userName;
+ (void)cacheOfflineBtOperation:(NSArray *)offlines withUserName:(NSString *)userName;

+ (NSArray *) getOnlineKeyOperation:(NSString *)userName;
+ (void)cacheOnlineKeyOperation:(NSArray *)offlines withUserName:(NSString *)userName;

+ (NSArray *) getOnlineBtOperation:(NSString *)userName;
+ (void)cacheOnlineBtOperation:(NSArray *)offlines withUserName:(NSString *)userName;


+ (void)cacheAllObjects:(NSDictionary *)dic withUserName:(NSString *)userName;
+ (NSArray *)getAllObjects:(NSString*)username;
+ (NSArray *)getAllLocksetsFromAllObjects;

@end
