//
//  ZNSTimeTools.h
//  ZhiNengSuo
//
//  Created by Chuenlu Kuo on 17/1/11.
//  Copyright © 2017年 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZNSTimeTools : NSObject

+ (ZNSTimeTools *)shared;

- (NSString *)timeFomrmatterWith:(NSString *)str;

@end
