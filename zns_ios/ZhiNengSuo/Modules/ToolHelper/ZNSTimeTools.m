//
//  ZNSTimeTools.m
//  ZhiNengSuo
//
//  Created by Chuenlu Kuo on 17/1/11.
//  Copyright © 2017年 czwen. All rights reserved.
//

#import "ZNSTimeTools.h"

@interface ZNSTimeTools(){

    NSDateFormatter *inputFormatter;
    NSDateFormatter *df2;
}

@end

@implementation ZNSTimeTools

+ (ZNSTimeTools *)shared{
    
    static ZNSTimeTools *sharedInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}


- (id)init{
    self = [super init];
    if (self) {
         inputFormatter = [[NSDateFormatter alloc] init] ;
        [inputFormatter setDateFormat: @"yyyyMMddHHmmss"];
        [inputFormatter setLocale:[[NSLocale alloc]initWithLocaleIdentifier:@"zh_CN"]];
        
        df2 = [[NSDateFormatter alloc]init];
        [df2 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }
    return self;
}

- (NSString *)timeFomrmatterWith:(NSString *)str{

    NSDate *inputDate =[[NSDate alloc]init];
    inputDate = [inputFormatter dateFromString:str];
    return [df2  stringFromDate:inputDate];
}

@end
