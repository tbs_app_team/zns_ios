//
//  ZNSTools.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/21.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "ZNSTools.h"
#import "BlueToothViewController.h"
#import "ZNSTempTask.h"
#import "sys/utsname.h"


@interface ZNSTools(){
//    NSMutableData *recordFrames;
}
//@property (nonatomic, strong) NSMutableData *recordFrames;
@end

@implementation ZNSTools


+ (NSData *)getTaskIdDataFromInt:(NSInteger)guid taskFlag:(Byte)taskFlag{
    
    NSMutableData *tem = [NSMutableData data];
//    Byte taskFlag[1] = {0xD0};
    [tem appendBytes:&taskFlag length:1];
    
    Byte add0[11]={0};
    [tem appendBytes:add0 length:11];
    
    Byte taskId[4] = {0};
    taskId[3] = (guid & 0xFF);
    taskId[2] = ((guid >> 8) & 0xFF);
    taskId[1] = ((guid >> 16) & 0xFF);
    taskId[0] = ((guid >> 24) & 0xFF);
    [tem appendBytes:taskId length:4];
    return tem;
}

+ (NSData *)getNameDataFromString:(NSString *)nameStr nameLength:(NSInteger)length{
    
    NSData * tem = [nameStr dataUsingEncoding:CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000)];
    if (tem == nil) {
        Byte nameByte[60]={0x00};
        return [NSData dataWithBytes:&nameByte length:length];
    }
    NSMutableData *nameData = [NSMutableData data];
    if ([tem length] < length) {
        [nameData appendBytes:[tem bytes] length:[tem length]];
        Byte add[60]={0};
        [nameData appendBytes:add length:length-[tem length]];
    }else{
        [nameData appendBytes:[tem bytes] length:length];
    }
    return nameData;
}

//年份是低字节在前
+ (NSData*)getFinalDateString:(NSData *)dateString{
    
    NSMutableData *finalData = [NSMutableData data];
    NSData *tmp = [NSData data];
    tmp = [dateString subdataWithRange:NSMakeRange(1, 1)];
    [finalData appendData:tmp];
    tmp = [dateString subdataWithRange:NSMakeRange(0, 1)];
    [finalData appendData:tmp];
    tmp = [dateString subdataWithRange:NSMakeRange(2, dateString.length-2)];
    [finalData appendData:tmp];
    return [NSData dataWithData:finalData];
}

+ (void)returnData:(ZNSTempTask *)task withTaskFlag:(Byte)taskFlag taskStr:(NSString*)taskStr{
    
    static BlueToothViewController *btVC;
    if (!btVC) {
        btVC = [BlueToothViewController create];
    }
    NSMutableData *recordFrames = [NSMutableData data];
    
    @weakify(self)
    [btVC setConnectedBlock:^{
        //            @strongify(self)
        
        __block int16_t frameCount = 0;
        __block int16_t user_id = 0;
        __block int16_t j = 0;
        [[GGBluetooth sharedManager] setNotifyCharacteristicBlock:^(CBCharacteristic *characteristic,NSData *recvData,NSError *error){
//            @strongify(self)
            ZNSBluetoothInstruction *b = [[ZNSBluetoothInstruction alloc]initWithByteData:recvData];
            
            if(b.function == Func_Code_00 && b.cmd == Cmd_Code_05){
                if (b.isRight == isAckRight) {
                    int taskCount ;
                    
                    [[b.bodyData subdataWithRange:NSMakeRange(0, 2)] getBytes:&taskCount length:2];
                    if (taskCount == 0) {
                        [SVProgressHUD showInfoWithStatus:@"钥匙中没有可回传的任务"];
                        [[GGBluetooth sharedManager] disconnectCurrentDevice];
                    }else{
                        
                        NSData *tem = [ZNSTools getTaskIdDataFromInt:task.tid.integerValue taskFlag:taskFlag];
                        int i;
                        BOOL hasReturnTicket = NO;
                        for (i=0; i<taskCount; i++){
                            
                            if ([tem isEqualToData:[b.bodyData subdataWithRange:NSMakeRange(2+i*16, 16)]]) {
                                //任务回传
                                hasReturnTicket = YES;
                                ZNSBluetoothInstruction *da =[[ZNSBluetoothInstruction alloc] initWithInstruction:Cmd_Code_04 function:Func_Code_00 body:nil rfid:tem];
                                NSLog(@"%@",da.data);
                                [[GGBluetooth sharedManager] writeValueData:da.data];
                            }
                        }
                        if (!hasReturnTicket) { //钥匙中要回传的任务不在列表中
                            [SVProgressHUD showInfoWithStatus:@"钥匙中需要回传的任务不在列表中"];
                            [[GGBluetooth sharedManager] disconnectCurrentDevice];
                        }
                    }

                }
            }
            
            if (b.function == Func_Code_00 && b.cmd == Cmd_Code_04) {//回传总帧数
                if (b.isRight == isAckRight) {
                    [[b.bodyData subdataWithRange:NSMakeRange(0, 2)]  getBytes:&frameCount length:2];
                    [[b.bodyData subdataWithRange:NSMakeRange(2, 2)] getBytes:&user_id length:2];
                    if(j!=frameCount) {
                        j++;
                        NSData *frame = [NSData dataWithBytes:&j length:2];
                        
                        [[GGBluetooth sharedManager] writeValueData:[[ZNSBluetoothInstruction alloc] initWithMoreData:Cmd_Code_04 function:Func_Code_01 body:nil rfid:frame].data];
                        
                        
                    }else{
                        [SVProgressHUD showInfoWithStatus:@"钥匙中无当前任务操作记录"];
                        [[GGBluetooth sharedManager] disconnectCurrentDevice];
                    }
                }
            }
            if (b.function == Func_Code_01 && b.cmd == Cmd_Code_04) {
                
                [recordFrames appendData:[b.bodyData subdataWithRange:NSMakeRange(4, [b.bodyData length]-4)]];
                
                if(j!=frameCount) {
                    j++;
                    NSData *frame = [NSData dataWithBytes:&j length:2];
                    [[GGBluetooth sharedManager] writeValueData:[[ZNSBluetoothInstruction alloc] initWithMoreData:Cmd_Code_04 function:Func_Code_01 body:nil rfid:frame].data];
                    
                    
                }else{
                    j = 0;
                    frameCount = 0;
                    NSData * test = [NSData dataWithData:recordFrames];
                    //                        NSLog(@"%@",test);//test
                    NSMutableArray *logs = [NSMutableArray array];
                    NSData *bit0;
                    for (int i=0; i<([test length]/17); i++) {
                        
                        NSMutableData *tmp=[NSMutableData data];
                        bit0 = [test subdataWithRange:NSMakeRange(1+i*17, 1)];
                        [tmp appendData:bit0];
                        bit0 = [test subdataWithRange:NSMakeRange(0+i*17, 1)];
                        [tmp appendData:bit0];
                        
                        if (![[tmp hexString] isEqualToString:@"FFFF"]) {
                            [logs addObject:@{
                                              
                                              @"lid": [NSString stringWithFormat:@"%ld",strtoul([[tmp hexString] UTF8String], 0, 16)],
                                              @"unlock_at": [NSString timeStringFormHexString:[[test subdataWithRange:NSMakeRange(9+i*17, 7)] hexString]],
                                              @"result": [NSString stringWithFormat:@"%ld",strtoul([[[test subdataWithRange:NSMakeRange(16+i*17, 1)] hexString] UTF8String], 0, 16)],
                                              }];
                        }
                        
                    }
                    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{@"tid":task.tid,
                                                                                               @"action":taskStr,
                                                                                               @"uid":[ZNSUser currentUser].idid}];
                    [dic setObject:logs forKey:@"taskHistory"];
                    [APIClient POST:API_TASK_HISTORY withParameters:dic successWithBlcok:^(id response) {
                        @strongify(self)
                        
                        [recordFrames resetBytesInRange:NSMakeRange(0, [recordFrames length])];
                        [recordFrames setLength:0];
                        [SVProgressHUD showSuccessWithStatus:@"回传成功"];
                        [self deletedTask:task withTaskFlag:taskFlag];
                        
                    } errorWithBlock:^(ZNSError *error) {
                        [recordFrames resetBytesInRange:NSMakeRange(0, [recordFrames length])];
                        [recordFrames setLength:0];
                        [SVProgressHUD showErrorWithStatus:error.errorMessage];
                        [[GGBluetooth sharedManager] disconnectCurrentDevice];
                    }];
                    
                }
                
            }
            if (b.cmd == Cmd_Code_06 && b.function == Func_Code_00) {
                if (b.isRight == isAckRight) {
                    NSLog(@"任务删除成功");
                    [[GGBluetooth sharedManager] disconnectCurrentDevice];
                }
            }
        }];
        [[GGBluetooth sharedManager] setSendFailureBlock:^{
            [SVProgressHUD showErrorWithStatus:@"钥匙无回应，请重试"];
            [[GGBluetooth sharedManager] disconnectCurrentDevice];
        }];
        [[GGBluetooth sharedManager] setCanWriteValueBlock:^{//任务查询
            [[GGBluetooth sharedManager] writeValueData:[[ZNSBluetoothInstruction alloc] initWithMoreData:Cmd_Code_05 function:Func_Code_00 body:nil rfid:nil].data];
            
        }];
        
    }];
    JKAlertDialog *dialog = [[JKAlertDialog alloc]initWithTitle:@"选择智能钥匙" message:@""];
    dialog.contentView = btVC.view;
    
    [dialog addButton:Button_CANCEL withTitle:@"取消" handler:^(JKAlertDialogItem *item) {
        
    }];;
    
    [btVC setConnectedSmartKeyBlock:^(ZNSSmartKey *smartKey){
        [dialog dismiss];
        
    }];
    
    [dialog show];
    [btVC setupBT];
    [btVC loadSmartKeys];
    
}

+ (BOOL)isSCLock{
    id lockType = [[NSUserDefaults standardUserDefaults]objectForKey:LOCK_TYPE];
    return [lockType isEqual:@2];
}

- (void)deletedTask:(ZNSTempTask *)task withTaskFlag:(Byte)taskFlag{
    
    NSData *tem = [ZNSTools getTaskIdDataFromInt:task.tid.integerValue taskFlag:taskFlag];
    
    //删除任务
    ZNSBluetoothInstruction *da =[[ZNSBluetoothInstruction alloc] initWithInstruction:Cmd_Code_06 function:Func_Code_00 body:nil rfid:tem];
    [[GGBluetooth sharedManager] writeValueData:da.data];
    //    //固定任务日志删除，钥匙没回复
    //    for (int j=0; j<=([da.data length]/20); j++) {
    //        [[[GGBluetooth sharedManager] connectedPheripheral]  writeValue:[da.data subdataWithRange:NSMakeRange(0+j*20, j==[da.data length]/20?([da.data length]-20*j):20)] forCharacteristic:[[GGBluetooth sharedManager] writeableCharacteristic] type:[[GGBluetooth sharedManager] writeableCharacteristic].properties==(CBCharacteristicPropertyRead+CBCharacteristicPropertyWriteWithoutResponse+CBCharacteristicPropertyNotify)?CBCharacteristicWriteWithoutResponse:CBCharacteristicWriteWithResponse];
    //    }
    
}


+ (BOOL)isiPhoneX{

    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *deviceString = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    return ([deviceString isEqualToString:@"iPhone10,3"] || [deviceString isEqualToString:@"iPhone10,6"]);
}
 

@end
