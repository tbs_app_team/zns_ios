//
//  ZNSTools.h
//  ZhiNengSuo
//
//  Created by ekey on 16/9/21.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ZNSTempTask ;

static const Byte isAckRight = 0x80;

@interface ZNSTools : NSObject

typedef enum : NSInteger{
    TransAllUnlock       = 0x00,  //全站解锁
    TransTemp            = 0x80,  //临时任务
    TransGuardTour       = 0x90,  //电子巡更
    TransSectionalLock   = 0xA0,  //部门锁上锁任务
    TransSectionalUnlock = 0xB0,  //部门锁解锁任务
    TransFixed           = 0xC0,  //固定任务
    TransPermanent       = 0xD0,  //预置任务
    TransCollectRFID     = 0xE0,  //采码任务
    TransTask            = 0xF0   //任务授权
}TaskType;

typedef enum : NSInteger{
    Func_Code_00 = 0x00,//计划任务、固定任务
    Func_Code_01 = 0x01,
    Func_Code_02 = 0x02,
    Func_Code_03 = 0x03,
    Func_Code_04 = 0x04,
    Func_Code_05 = 0x05,
    Func_Code_06 = 0x06, //预置任务
    Func_Code_07 = 0x07,
    Func_Code_08 = 0x08,
    Func_Code_09 = 0x09,
    Func_Code_0A = 0x0A,
    Func_Code_0B = 0x0B,
    Func_Code_0C = 0x0C,
    Func_Code_0D = 0x0D,
    Func_Code_0E = 0x0E,
    Func_Code_0F = 0x0F,
}FuncCodeNum;

typedef enum : NSInteger{
    Cmd_Code_03 = 0x03,//下载任务
    Cmd_Code_04 = 0x04,//任务回传
    Cmd_Code_05 = 0x05,//任务查询
    Cmd_Code_06 = 0x06,//删除任务
}CmdCodeNum;



+ (NSData *)getTaskIdDataFromInt:(NSInteger)guid taskFlag:(Byte)taskFlag;
+ (NSData *)getNameDataFromString:(NSString *)nameStr nameLength:(NSInteger)length;
+ (NSData*)getFinalDateString:(NSData *)dateString;
+ (void)returnData:(ZNSTempTask *)task withTaskFlag:(Byte)taskFlag taskStr:(NSString*)taskStr;

+ (BOOL)isSCLock;

+ (BOOL)isiPhoneX;

@end
