//
//  MKVersionCheck.m
//  acbclient
//
//  Created by Chuenlu Kuo on 16/6/17.
//  Copyright © 2016年 dbjtech. All rights reserved.
//

#import "MKVersionCheckManager.h"

@implementation MKVersionCheckManager

+ (MKVersionCheckManager *)sharedInstance{
    
    static MKVersionCheckManager *sharedAccountManagerInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedAccountManagerInstance = [[self alloc] init];
    });
    return sharedAccountManagerInstance;
}

- (void)doRequestVsersionInfoWithAppId:(NSString *)appId sucess:(void(^)(NSString *downUrl,NSString *updateLog,NSString *versionNum,NSString *releaseDate))sucess faild:(void(^)(NSString *errorMsg))faild{

    NSURL *nsurl = [NSURL URLWithString:[NSString stringWithFormat:@"https://itunes.apple.com/lookup?id=%@",appId]];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionTask *task = [session dataTaskWithURL:nsurl completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (error == nil) {
            NSDictionary *root = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            
            NSArray *results = root[@"results"];
            if (results && [results isKindOfClass:[NSArray class]]) {
                if (results.count) {
                    NSDictionary *d = results.firstObject;
                    if (d && [d isKindOfClass:[NSDictionary class]]) {
                        
                        NSString *lastVersion = d[@"version"];
                        NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
                        NSString *currentVserion = [infoDict objectForKey:@"CFBundleShortVersionString"];
                        
                        NSString *resDate = d[@"currentVersionReleaseDate"];
                        
                        if ([lastVersion compare:currentVserion options:NSNumericSearch] == NSOrderedDescending){
                            sucess(d[@"trackViewUrl"],d[@"releaseNotes"],d[@"version"],resDate);
                        }else{
                            faild(@"当前版本是最新版本");
                        }
                    }
                }
            }
        }else{
            
            NSLog(@"version check error is%@",error);
            faild(error.description);
        }
    }];
    
    
    [task resume];
    
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
//    [request setURL:nsurl];
//    [request setHTTPMethod:@"GET"];
//    [request setTimeoutInterval:15];
//
//    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
//         if (connectionError == nil) { //请求成功
////             NSLog(@"%@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
//
//             NSDictionary *root = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
//
//             NSArray *results = root[@"results"];
//             if (results && [results isKindOfClass:[NSArray class]]) {
//                 if (results.count) {
//                     NSDictionary *d = results.firstObject;
//                     if (d && [d isKindOfClass:[NSDictionary class]]) {
//
//                         NSString *lastVersion = d[@"version"];
//                         NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
//                         NSString *currentVserion = [infoDict objectForKey:@"CFBundleShortVersionString"];
//
//                         if ([lastVersion compare:currentVserion options:NSNumericSearch] == NSOrderedDescending){
//                              sucess(d[@"trackViewUrl"],d[@"releaseNotes"],d[@"version"]);
//                         }
//                     }
//                 }
//             }
//         }else{
//             NSLog(@"version check error is%@",connectionError);
//             faild(connectionError.description);
//         }
//    }];
}

@end
