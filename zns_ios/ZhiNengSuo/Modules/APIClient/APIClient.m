//
//  APIClient.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/23/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "APIClient.h"

@implementation APIClient

+ (instancetype)sharedClient {
    static APIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
//        NSString *apihost = API_HOST;
        _sharedClient = [APIClient manager];//[[APIClient alloc] initWithBaseURL:[NSURL URLWithString:apihost]];
        _sharedClient.requestSerializer = [AFJSONRequestSerializer serializer];
        _sharedClient.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", @"image/png",nil];
    });
    return _sharedClient;
}

+ (NSURLSessionDataTask*)POST:(NSString *)url withParameters:(NSDictionary *)p successWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock{
    NSString *urlWithServerParameter = [url stringByAppendingString:[NSString stringWithFormat:@"?s=%@&c=iOS&v=%@",API_VERSION,APP_VERSION]];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:p];
    ZNSUser *currentUser = [ZNSUser currentUser];
    if (currentUser) {
        [parameters setValue:currentUser.token forKey:@"k"];
    }

    NSString *api = [NSString stringWithFormat:@"%@/%@",API_HOST,urlWithServerParameter];
    return [[APIClient sharedClient] POST:api parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject valueForKey:@"error"]) {
            if ([[responseObject valueForKey:@"error"] isEqualToString: @"登陆已超时，请重新登陆！"]&&[ZNSUser currentUser]) {
                [[ZNSUser currentUser]logout];
                [SVProgressHUD showErrorWithStatus:@"登陆已超时，请重新登陆！" maskType:SVProgressHUDMaskTypeBlack];
            }else if (errorBlock) {
                errorBlock([ZNSError yy_modelWithDictionary:responseObject]);
            }
        }else{
            if (succesBlock) {
                succesBlock(responseObject);
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (errorBlock) {
            if ([error.domain isEqualToString: NSURLErrorDomain]) {
                errorBlock([ZNSError serverError]);
                
            }else{
                errorBlock([ZNSError networkError]);
            }
        }
        
    }];
}

+ (NSURLSessionDataTask*)upload:(NSString *)url image:(UIImage *)image name:(NSString *)name parameters:(NSDictionary *)p progressWithBlcok:(requestProgressBlock)progressBlock successWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock{
    
    NSString *urlWithServerParameter = [url stringByAppendingString:[NSString stringWithFormat:@"?s=%@&c=iOS&v=%@",API_VERSION,APP_VERSION]];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:p];
    ZNSUser *currentUser = [ZNSUser currentUser];
    if (currentUser) {
        [parameters setValue:currentUser.token forKey:@"k"];
    }
    //    AFHTTPRequestOperation *operation = [[APIClient sharedClient]POST:urlWithServerParameter parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
    //        NSData *data = nil;
    //        if ([image isKindOfClass:[UIImage class]]) {
    //            data = UIImageJPEGRepresentation(image, 0.4);
    //        }
    //        [formData appendPartWithFileData:data name:name fileName:name mimeType:@"multipart/form-data"];
    //    } success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
    //        if ([responseObject valueForKey:@"error"]) {
    //            if ([[responseObject valueForKey:@"error"] isEqualToString: @"登陆已超时，请重新登陆！"]&&[ZNSUser currentUser]) {
    //                [[ZNSUser currentUser]logout];
    //                [SVProgressHUD showErrorWithStatus:@"登陆已超时，请重新登陆！" maskType:SVProgressHUDMaskTypeBlack];
    //            }else if (errorBlock) {
    //                errorBlock([ZNSError yy_modelWithDictionary:responseObject]);
    //            }
    //        }else{
    //            if (succesBlock) {
    //                succesBlock(responseObject);
    //            }
    //        }
    //    } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
    //        if (errorBlock) {
    //            if ([error.domain isEqualToString: NSURLErrorDomain]) {
    //                errorBlock([ZNSError serverError]);
    //            }else{
    //                errorBlock([ZNSError networkError]);
    //            }
    //        }
    //    }];
    //
    //    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
    //        double percentDone = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
    //        if (progressBlock) {
    //            progressBlock(percentDone);
    //        }
    //    }];
    //    [operation start];
    //
    //    return operation;
    
    NSString *api = [NSString stringWithFormat:@"%@/%@",API_HOST,urlWithServerParameter];
    return [[APIClient sharedClient] POST:api parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSData *data = nil;
        if ([image isKindOfClass:[UIImage class]]) {
            data = UIImageJPEGRepresentation(image, 0.4);
        }
        [formData appendPartWithFileData:data name:name fileName:name mimeType:@"multipart/form-data"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject valueForKey:@"error"]) {
            if ([[responseObject valueForKey:@"error"] isEqualToString: @"登陆已超时，请重新登陆！"]&&[ZNSUser currentUser]) {
                [[ZNSUser currentUser]logout];
                [SVProgressHUD showErrorWithStatus:@"登陆已超时，请重新登陆！" maskType:SVProgressHUDMaskTypeBlack];
            }else if (errorBlock) {
                errorBlock([ZNSError yy_modelWithDictionary:responseObject]);
            }
        }else{
            if (succesBlock) {
                succesBlock(responseObject);
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (errorBlock) {
            if ([error.domain isEqualToString: NSURLErrorDomain]) {
                errorBlock([ZNSError serverError]);
            }else{
                errorBlock([ZNSError networkError]);
            }
        }
        
    }];
} 
@end
