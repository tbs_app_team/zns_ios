//
//  APIAddress.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/23/15.
//  Copyright © 2015 czwen. All rights reserved.
//


//#define STAGING @"我是正式api和测试api的开关宏定义，把我注释掉就是正式环境"

//#ifndef STAGING
#define API_HOST [[NSUserDefaults standardUserDefaults]valueForKey:@"SERVER_HOST"]
//#else
//#define API_HOST @"http://10.7.4.174:8080"
//#endif
#define API_ALERT_PORT [[NSUserDefaults standardUserDefaults]valueForKey:@"ALERT_PORT"]

#define API_VERSION     @1
#define APP_VERSION     @1

#define SAVE_SERVER_ADDRESS(x) [[NSUserDefaults standardUserDefaults]setValue:x forKey:@"SERVER_HOST"];[[NSUserDefaults standardUserDefaults]synchronize];

#define SAVE_ALERT_PORT(x)  [[NSUserDefaults standardUserDefaults]setValue:x forKey:@"ALERT_PORT"];[[NSUserDefaults standardUserDefaults]synchronize];

#define API_USER                @"/user/"
#define API_USER_LOGIN          @"/user/login"
#define API_USER_LOGOUT         @"/user/logout"
#define API_USER_REGISTER       @"/user/reg"
#define API_USER_PASSWORD       @"/password"
#define API_USER_VERIFY         @"/verify"
#define API_USER_ADDROLE        @"/role/add/"
#define API_USER_DELROLE        @"/role/del/"
#define API_USER_LASTOFFLINE    @"/last_offline/"
#define API_USER_NEWOFFLINE     @"/offline/new"
#define API_USER_NEWTEMPTASK    @"/temp_task/new"
#define API_USER_PRESET_TASK(x) [NSString stringWithFormat:@"/preset/task/execute/%@/get",x]
#define API_USER_FIXED_TASK(x)  [NSString stringWithFormat:@"/fixation/task/execute/%@/get",x]
#define API_TASK_HISTORY        @"/history/task/log/new"

#define API_TEMP_TASK_WITH_USERNAME(x)  [NSString stringWithFormat:@"user/%@/temp_task/",x] //[用户名]

#define API_APPLYOFFLINE        @"/offline/apply/"
#define API_OFFLINE_VERIFY_LIST      @"/offline/verify/"
#define API_APPLYTEMPTASK       @"/temp_task/apply/"
#define API_TEMPTASK_VERIFY_LIST      @"/temp_task/verify/"
#define API_TEMPTASK_VERIFY_WITH_TID(x)  [NSString stringWithFormat:@"temp_task/%@/verify",x] //[tid]
#define API_VERIFT_OFFLINE_WITH_OID(x)  [NSString stringWithFormat:@"offline/%@/verify/",x] //[oid]
#define API_TEMPTASK_DETAIT_WITH_TID(x) [NSString stringWithFormat:@"temp_task/%@",x] //[sid]

#define API_ROLE                @"/role/"
#define API_ZONE                @"/zone/"
#define API_ROLE_ZONE_WITH_RID(x)  [NSString stringWithFormat:@"role/%@/zone/",x] //[rid]

#define API_LOCKSET             @"lockset/"
#define API_OBJECT             @"object/"
 

#define API_SECTION_OBJECT_WITH_SID(x)          [NSString stringWithFormat:@"section/%@/object/",x] //[sid]
#define API_SECTION_SMART_KEY_WITH_SID(x)       [NSString stringWithFormat:@"section/%@/smartkey/",x] //[sid]

#define API_LOCKSET_NEW_WITH_OID(x)          [NSString stringWithFormat:@"object/%@/lockset/new/",x] //[sid]
#define API_LOCKSET_MODIFY_WITH_OID(x)        [NSString stringWithFormat:@"lockset/%@/update/",x] //[sid]
#define API_LOCKSET_DEL_WITH_LID(x)          [NSString stringWithFormat:@"lockset/%@/del/",x] //[sid]

#define API_TEMP_TASK_VERIFYALL     @"temp_task/verifyall"
#define API_OFFLINE_VERIFYALL       @"offline/verifyall"
#define API_USER_VERIFYALL          @"user/verifyall"
#define API_USER_NAME_OBJECT        @"user/%@/object/" //用户权限范围内的设备

//@"lockset/[lid]/update"
//@"lockset/[lid]/del"
//@"lockset/[lid]/code/[代码]"

#define API_OPERATION @"operation/"
#define API_OPERATION_DO @"operation/do"

#define API_HISTORY @"history/"
#define API_ALERT @"alert/"

#define API_SMART_KEY_BORROW @"smartkey/borrow/" //列出智能钥匙借还记录

//新增

#define API_DICT_LIST           @"/dict/"

//运维
#define API_WORKBILL_APPLY         @"workbill/apply"    //进站申请
#define API_WORKBILL_GET           @"/workbill/"
#define API_WORKBILL_NEW           @"/workbill/new"
#define API_WORKBILL_UPDATE(x)     [NSString stringWithFormat:@"workbill/%@/accept/",x]
#define API_WORKBILL_RETURN(x)     [NSString stringWithFormat:@"workbill/%@/return/",x]

#define API_WORKPATROL_GET           @"/workpatrol/"
#define API_WORKPATROL_NEW           @"/workpatrol/new/"

#define API_WORKBILL_INTIME          @"/workbill/intime"
#define API_WORKBILL_INTIME_DETAIL   @"/workbill/intime/detail"

#define API_WORKPATROL_FINISH        @"/workpatrol/execute/"
#define API_WORKPATROL_FINISH_DETAIL(x) [NSString stringWithFormat:@"/workpatrol/execute/%@/detail",x]

#define API_WORKBILL_VERIFY(x) [NSString stringWithFormat:@"workbill/%@/verify",x]

//获取验证码
#define API_COMMON_CAPTCHA @"/common/captcha"

//获取公告列表
#define API_NOTICE @"/notice/"
//获取公告详情
#define API_NOTICE_DETAIL(x) [NSString stringWithFormat:@"/notice/%@",x]

//获取通讯录
#define API_CONTACT @"/contact/"

//获取默认解锁顺序
#define API_UNLOCK_ORDER @"/dict/unlock_order/"

//开锁类型
#define API_UNLOCK_PROTOCOL          @"/dict/unlock_protocol/dictObject"

#define API_USER_FIXED_STATUS(x)  [NSString stringWithFormat:@"/fixation/task/%@/updatestatus",x]
#define API_USER_PRESET_STATUS(x)  [NSString stringWithFormat:@"/preset/task/%@/updatestatus",x]
#define API_USER_TEMP_TASK_STATUS(x)  [NSString stringWithFormat:@"/temp_task/%@/updatestatus",x]
