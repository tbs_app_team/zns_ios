//
//  ZNSAPITool.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/2/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZNSAPITool : NSObject
/**
 *  获取区域，以及区域下的代维单位。2018年8月30日更新，v3.1版本的需求
 *
 */
+ (void)getSectionWithUserName:(NSString *)username success:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock;
/**
 *  获取门编号，2018年9月04日更新，v3.1版本的需求
 *
 */
+ (void)getDoorcodeSuccessWithBlock:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock;
/**
 *  获取所有单位
 *
 */
+ (void)getAllSectionSuccessWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock;

/**
 *  获取代维单位
 *
 */
+ (void)getAgentSectionSuccessWithSID:(NSString *)sid succesBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock;

/**
 *  获取所有单位
 *
 */
+ (void)getAllAreaSuccessWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock;

/**
 *  获取所有运营商
 ＊
 */
+ (void)getAllCarrierSuccessWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock;
+ (void)getAllLockTypeSuccessWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock;
+ (void)getAllObjectTypeSuccessWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock;
+ (void)getAllZonesSuccessWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock;

/**
 *  获取所有的门类型
 ＊
 */
+ (void)getAllDoorTypeSuccessWithBlock:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock;

/**
 *  获取控制器的工作模式
 ＊
 */
+ (void)getControllerWorkModeSuccessWithBlock:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock;

/**
 *  获取控制器的类型
 ＊
 */
+ (void)getControllerTypeSuccessWithBlock:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock;

/**
 *  获取智能钥匙的类型
 ＊
 */
+ (void)getSmartKeyTypeSuccessWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock;

/**
 *  批量 批准／拒绝
 *
 */
+ (void)batchHandel:(NSString *)api agree:(BOOL)agree reason:(NSString *)reason successWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock;

+ (void)cacheOfflineDataSuccessWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock;

/**
 *  上传图片方法
 *
 */
+ (void)uploadImage:(UIImage *)image imageType:(ZNSImageType)imageType type:(ZNSUploadType)requestType forId:(NSString *)idString progressWithBlcok:(requestProgressBlock)progressBlock successWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock;

/**
 *  获取图片url
 *
 */
+ (NSURL*)imageUrlOfImageType:(ZNSImageType)imageType type:(ZNSUploadType)requestType forId:(NSString *)idString username:(NSString *)username;
@end
