//
//  ZNSAPITool.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/2/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "ZNSAPITool.h"
#import "ZNSSection.h"
@implementation ZNSAPITool

//获取区域，以及区域下的代维单位。2018年8月30日更新，v3.1版本的需求
+ (void)getSectionWithUserName:(NSString *)username success:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock{
    
    [APIClient POST:[NSString stringWithFormat:@"section/%@/getsec",username] withParameters:nil successWithBlcok:^(id response) { 
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZNSSection class] json:[response valueForKey:@"items"]];
        if (succesBlock) {
            succesBlock(array);
        }
    } errorWithBlock:errorBlock];
}

//获取门编号，2018年9月04日更新，v3.1版本的需求
+ (void)getDoorcodeSuccessWithBlock:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock{
    
    [APIClient POST:@"dict/doorcode/" withParameters:nil successWithBlcok:^(id response) {
//        NSArray *array = [NSArray yy_modelArrayWithClass:[ZNSSection class] json:[response valueForKey:@"items"]];
        if (succesBlock) {
            succesBlock(response[@"items"]);
        }
    } errorWithBlock:errorBlock];
}


+ (void)getAllSectionSuccessWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock{
    [APIClient POST:@"section/" withParameters:@{@"type":@"机构"} successWithBlcok:^(id response) {
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZNSSection class] json:[response valueForKey:@"items"]];
        if (succesBlock) {
            succesBlock(array);
        }
    } errorWithBlock:errorBlock];
}

+ (void)getAllAreaSuccessWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock{
    [APIClient POST:@"section/" withParameters:@{@"type":@"区域"} successWithBlcok:^(id response) {
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZNSSection class] json:[response valueForKey:@"items"]];
        if (succesBlock) {
            succesBlock(array);
        }
    } errorWithBlock:errorBlock];
}

+ (void)getAgentSectionSuccessWithSID:(NSString *)sid succesBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock{
    [APIClient POST:@"section/companys" withParameters:@{@"id":sid} successWithBlcok:^(id response) {
        NSArray *array = [response valueForKey:@"items"];
        if (succesBlock) {
            succesBlock(array);
        }
    } errorWithBlock:errorBlock];
}


+ (void)getAllCarrierSuccessWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock{
    [APIClient POST:@"dict/carrier/" withParameters:@{} successWithBlcok:^(id response) {
        NSArray *array = [response valueForKey:@"items"];
        if (succesBlock) {
            succesBlock(array);
        }
    } errorWithBlock:errorBlock];
}

+ (void)getAllLockTypeSuccessWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock{
    [APIClient POST:@"dict/lock_type/" withParameters:@{} successWithBlcok:^(id response) {
        NSArray *array = [response valueForKey:@"items"];
        if (succesBlock) {
            succesBlock(array);
        }
    } errorWithBlock:errorBlock];
}

+ (void)getAllObjectTypeSuccessWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock{
    [APIClient POST:@"dict/obj_type/" withParameters:@{} successWithBlcok:^(id response) {
        NSArray *array = [response valueForKey:@"items"];
        if (succesBlock) {
            succesBlock(array);
        }
    } errorWithBlock:errorBlock];
}

+ (void)getAllDoorTypeSuccessWithBlock:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock{
    [APIClient POST:@"dict/door_type/" withParameters:@{} successWithBlcok:^(id response) {
        NSArray *array = [response valueForKey:@"items"];
        if (succesBlock) {
            succesBlock(array);
        }
    } errorWithBlock:errorBlock];
}

+ (void)getControllerTypeSuccessWithBlock:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock{
    [APIClient POST:@"dict/controller_type/" withParameters:@{} successWithBlcok:^(id response) {
        NSArray *array = [response valueForKey:@"items"];
        if (succesBlock) {
            succesBlock(array);
        }
    } errorWithBlock:errorBlock];
}

+ (void)getControllerWorkModeSuccessWithBlock:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock{
    [APIClient POST:@"dict/work_mode/" withParameters:@{} successWithBlcok:^(id response) {
        NSArray *array = [response valueForKey:@"items"];
        if (succesBlock) {
            succesBlock(array);
        }
    } errorWithBlock:errorBlock];
}

+ (void)getAllZonesSuccessWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock{
    [APIClient POST:[NSString stringWithFormat:@"zone/%@/",@-1] withParameters:@{@"page":@-1} successWithBlcok:^(id response) {
        NSArray *array = [response valueForKey:@"items"];
        if (succesBlock) {
            succesBlock(array);
        }
    } errorWithBlock:errorBlock];
}

+ (void)getSmartKeyTypeSuccessWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock{
    [APIClient POST:@"dict/sm_type/" withParameters:nil successWithBlcok:^(id response) {
        NSArray *array = [response valueForKey:@"items"];
        if (succesBlock) {
            succesBlock(array);
        }
    } errorWithBlock:errorBlock];
}

+ (void)batchHandel:(NSString *)api agree:(BOOL)agree reason:(NSString *)reason successWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock{
    [APIClient POST:api withParameters:@{@"action":agree?@"批准":@"拒绝",@"memo":STRING_OR_EMPTY(reason)} successWithBlcok:succesBlock errorWithBlock:errorBlock];
}

+ (void)cacheOfflineDataSuccessWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock{
    [APIClient POST:[NSString stringWithFormat:@"user/%@/offline/",[ZNSUser currentUser].username] withParameters:@{} successWithBlcok:^(id response) {
        [ZNSCache cacheOffline:[ZNSOffline yy_modelWithDictionary:response] withUserName:[ZNSUser currentUser].username];
        if (succesBlock) {
            succesBlock(response);
        }
    } errorWithBlock:^(ZNSError *error) {
        if (errorBlock) {
            errorBlock(error);
        }
    }];
}

+ (void)uploadImage:(UIImage *)image imageType:(ZNSImageType)imageType type:(ZNSUploadType)requestType forId:(NSString *)idString progressWithBlcok:(requestProgressBlock)progressBlock successWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock{
    NSString *url;
    NSString *fileName;
    
    switch (imageType) {
        case ZNSImageTypeIdCard:
            fileName = @"idcard";
            break;
        default:
            fileName = @"portrait";
            break;
    }
    
    switch (requestType) {
        case ZNSUploadTypeRegister:
            url = [NSString stringWithFormat:@"user/%@/%@",idString,fileName];
            break;
        case ZNSUploadTypeOfflineApply:
            url = [NSString stringWithFormat:@"offline/%@/%@",idString,fileName];
            break;
        default:
            url = [NSString stringWithFormat:@"temp_task/%@/%@",idString,fileName];
            break;
    }
    
    [APIClient upload:url image:image name:fileName parameters:@{} progressWithBlcok:progressBlock successWithBlcok:succesBlock errorWithBlock:errorBlock];
}

+ (NSURL*)imageUrlOfImageType:(ZNSImageType)imageType type:(ZNSUploadType)requestType forId:(NSString *)idString username:(NSString *)username{
    NSString *url;

    switch (requestType) {
        case ZNSUploadTypeRegister:
            switch (imageType) {
                case ZNSImageTypeIdCard:
                    url = [NSString stringWithFormat:@"/public/%@/idcard.jpg",username];
                    break;
                default:
                    url = [NSString stringWithFormat:@"/public/%@/portrait.jpg",username];
                    break;
            }
            break;
        case ZNSUploadTypeOfflineApply:
            switch (imageType) {
                case ZNSImageTypeIdCard:
                    url = [NSString stringWithFormat:@"/public/%@/idcard-offline-%@.jpg",username,idString];
                    break;
                default:
                    url = [NSString stringWithFormat:@"/public/%@/portrait-offline-%@.jpg",username,idString];
                    break;
            }
            break;
        default:
            switch (imageType) {
                case ZNSImageTypeIdCard:
                    url = [NSString stringWithFormat:@"/public/%@/idcard-temp-%@.jpg",username,idString];
                    break;
                default:
                    url = [NSString stringWithFormat:@"/public/%@/portrait-temp-%@.jpg",username,idString];
                    break;
            }
            break;
    }

    return [NSURL URLWithString:[API_HOST stringByAppendingPathComponent:url]];
}

@end
