//
//  APIClient.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/23/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import "ZNSError.h"
#import "APIAddress.h"

@interface APIClient : AFHTTPSessionManager
+ (instancetype)sharedClient;

+ (NSURLSessionDataTask*)POST:(NSString *)url withParameters:(NSDictionary *)p successWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock;

+ (NSURLSessionDataTask*)upload:(NSString *)url image:(UIImage *)image name:(NSString *)name parameters:(NSDictionary *)p progressWithBlcok:(requestProgressBlock)progressBlock successWithBlcok:(requestSuccessBlock)succesBlock errorWithBlock:(requestErrorBlock)errorBlock;
 
@end
