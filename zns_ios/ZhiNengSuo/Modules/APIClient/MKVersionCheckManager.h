//
//  MKVersionCheck.h
//  acbclient
//
//  Created by Chuenlu Kuo on 16/6/17.
//  Copyright © 2016年 dbjtech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MKVersionCheckManager : NSObject

+ (MKVersionCheckManager *)sharedInstance;

- (void)doRequestVsersionInfoWithAppId:(NSString *)appId sucess:(void(^)(NSString *downUrl,NSString *updateLog,NSString *versionNum,NSString *releaseDate))sucess faild:(void(^)(NSString *errorMsg))faild;

@end
