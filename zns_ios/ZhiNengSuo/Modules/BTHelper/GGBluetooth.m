//
//  GGBluetooth.m
//  BluetoothTesting
//
//  Created by ChenZhiWen on 12/25/14.
//  Copyright (c) 2014 Ganguo Technology Co.,Ltd. All rights reserved.
//

#define SERVICE_UUID                    @"0000ffe0-0000-1000-8000-00805f9b34fb"
#define SERVICE_UUID_SC                 @"0000fff0-0000-1000-8000-00805f9b34fb"

#import "GGBluetooth.h"
#import "NSData+Extensions.h"
#import "ZNSTools.h"
@interface GGBluetooth()<CBCentralManagerDelegate,CBPeripheralDelegate>{
    
    NSTimer *timer;
}
@property (nonatomic,strong) CBCentralManager *centerManager;
@property (nonatomic,strong) CBPeripheral *currentPheripheral;
@property (nonatomic, copy) ConnectBlock connectBlock;
@property (nonatomic, copy) DisconnectBlock disconnectBlock;
@property (nonatomic, copy) DiscoverServicesBlock discoverServicesBlock;
@property (nonatomic, copy) DiscoverCharacteristicBlock discoverCharacteristicBlock;
@property (nonatomic, copy) WriteValueBlock writeValueBlock;
@property (nonatomic) dispatch_queue_t bluetoothQueue;
@property (nonatomic,assign) BOOL  isConnect;
@property (nonatomic,assign) BOOL  isSendOk;
@property (nonatomic,strong) NSMutableData * recvData;
@end
@implementation GGBluetooth
+ (id)sharedManager {
    static GGBluetooth *staticInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        staticInstance = [[GGBluetooth alloc] init];
        staticInstance.centerManager = [[CBCentralManager alloc]initWithDelegate:staticInstance queue:staticInstance.bluetoothQueue];
    });
    return staticInstance;
}

#pragma mark
#pragma mark - GGBluetooth Public Method
- (void)startScan{
    if ([ZNSTools isSCLock]) {
        CBUUID *serviceUUID = [CBUUID UUIDWithString:SERVICE_UUID_SC];
        [self.centerManager scanForPeripheralsWithServices:@[serviceUUID] options:nil];
    }
    else {
        [self.centerManager scanForPeripheralsWithServices:nil options:nil];
    }
}
- (void)stopScan{
    [self.centerManager stopScan];
}

- (void)connectPheripheral:(CBPeripheral*)peripheral withSuccessBlcok:(ConnectBlock)connectedBlock{
    self.connectBlock = connectedBlock;
    self.isConnect = NO;
    self.recvData = [NSMutableData data];
    [self.centerManager connectPeripheral:peripheral options:nil];
    //连接超时判断
    [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(connectTimeout) userInfo:nil repeats:NO];
    
}
- (void)connectTimeout{
    if (self.isConnect) {
        //        self.isConnect = NO;
        return;
    }
    if (self.connectBlock) {
        self.connectBlock(NO,nil,nil);
    }
}

- (void)disconnectPheripheral:(CBPeripheral*)peripheral{
    if (peripheral) {
        [self.centerManager cancelPeripheralConnection:peripheral];
    }
}

- (void)discoverCurrentPheripheralServices:(DiscoverServicesBlock)block{
    self.discoverServicesBlock = block;
}

- (void)discoverCharacteristicsFormService:(CBService*)service withBlock:(DiscoverCharacteristicBlock)block{
    [service.peripheral discoverCharacteristics:nil forService:service];
    self.discoverCharacteristicBlock = block;
}

- (void)readCharacteristicValue:(CBCharacteristic*)characteristic{
    [characteristic.service.peripheral readValueForCharacteristic:characteristic];
}

- (void)writeValue:(NSData*)data forCharacteristic:(CBCharacteristic*)characteristic withBlock:(WriteValueBlock)block{
    self.writeValueBlock = block;
    [self.currentPheripheral writeValue:data forCharacteristic:self.writeableCharacteristic type:self.writeableCharacteristic.properties==(CBCharacteristicPropertyRead+CBCharacteristicPropertyWriteWithoutResponse+CBCharacteristicPropertyNotify)?CBCharacteristicWriteWithoutResponse:CBCharacteristicWriteWithResponse];
    //发送超时判断
     [NSTimer scheduledTimerWithTimeInterval:8.0f target:self selector:@selector(sendTimeout) userInfo:nil repeats:NO];
    self.isSendOk = NO;
}

- (void)writeValueData:(NSData*)data {
    NSLog(@"writeValueData =%@",data);
    
    if ([data length]>20) {
        
        for (int j=0; j<=([data length]/20); j++) {
           [self.currentPheripheral writeValue:[data subdataWithRange:NSMakeRange(0+j*20, j==[data length]/20?([data length]-20*j):20)] forCharacteristic:self.writeableCharacteristic type:self.writeableCharacteristic.properties==(CBCharacteristicPropertyRead+CBCharacteristicPropertyWriteWithoutResponse+CBCharacteristicPropertyNotify)?CBCharacteristicWriteWithoutResponse:CBCharacteristicWriteWithResponse];
        }
    }else{
          [self.currentPheripheral writeValue:data forCharacteristic:self.writeableCharacteristic type:self.writeableCharacteristic.properties==(CBCharacteristicPropertyRead+CBCharacteristicPropertyWriteWithoutResponse+CBCharacteristicPropertyNotify)?CBCharacteristicWriteWithoutResponse:CBCharacteristicWriteWithResponse];
    }
    
    //发送超时判断
     timer =  [NSTimer scheduledTimerWithTimeInterval:12.0f target:self selector:@selector(sendTimeout) userInfo:nil repeats:NO];
    self.isSendOk = NO;
}

- (void)writeValueDataSC:(NSData*)data {
    unsigned long count = 1;
    
    NSMutableData *sendData = [NSMutableData data];
    
    if (data.length > 19) {
        count = (data.length / 19) + 1;
    }
    for (int i = 0; i < count; i++) {
        
        NSMutableData *tempData = [NSMutableData data];
        unsigned long length = 19;
        if (count - i == 1) {
            length = data.length - (i * 19);
        }
        NSInteger instructionIndex = (i + 1);
        NSInteger zero = 0;
        [tempData appendBytes:&instructionIndex length:1];
        [tempData appendData:[data subdataWithRange:NSMakeRange(i * 19, length)]];
        for (int i = (int)length; i < 19; i++) {
            [tempData appendBytes:&zero length:1];
        }
        [sendData appendData:tempData];
    }
    
    NSLog(@"write data = %@", sendData);
    
    [self.currentPheripheral writeValue:sendData forCharacteristic:self.writeableCharacteristic type:self.writeableCharacteristic.properties==(CBCharacteristicPropertyRead+CBCharacteristicPropertyWriteWithoutResponse+CBCharacteristicPropertyNotify)?CBCharacteristicWriteWithoutResponse:CBCharacteristicWriteWithResponse];
    
    //发送超时判断
    [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(sendTimeout) userInfo:nil repeats:NO];
    self.isSendOk = NO;
}

- (void)sendTimeout{
    if (!self.isSendOk) {
        if (self.sendFailureBlock) {
            self.sendFailureBlock();
        }
    }
}
- (void)disconnectCurrentDevice{
    if (self.connectedPheripheral) {
        [self disconnectPheripheral:self.connectedPheripheral];
    }
}
- (void) sendFrame:(NSUInteger)frameCount  numberOfFramesPer:(NSUInteger)numberOfFramesPer

          frameLen:(NSUInteger)frameLen cmd:(Byte)cmd fun:(Byte)fun data:(NSData*)data{
    
    NSData *test = [NSData dataWithBytes: &frameCount length: 2];
    NSMutableData * sum = [NSMutableData dataWithData:test];
    test = [NSData dataWithBytes:&numberOfFramesPer length:2];
    [sum appendData:test];
    
    ZNSBluetoothInstruction *d=[[ZNSBluetoothInstruction alloc] initWithMoreData:cmd function:fun body:sum rfid:[data subdataWithRange:NSMakeRange(0+(frameCount-1)*frameLen, (frameCount-1)==([data length]/frameLen)?[data length]%frameLen:frameLen)]];
//    [self writeValueData:d.data];
    
    for (int j=0; j<=([d.data length]/20); j++) {

        [self.currentPheripheral writeValue:[d.data subdataWithRange:NSMakeRange(0+j*20, j==[d.data length]/20?([d.data length]-20*j):20)] forCharacteristic:self.writeableCharacteristic type:self.writeableCharacteristic.properties==(CBCharacteristicPropertyRead+CBCharacteristicPropertyWriteWithoutResponse+CBCharacteristicPropertyNotify)?CBCharacteristicWriteWithoutResponse:CBCharacteristicWriteWithResponse];

    }
    
    
}


#pragma mark
#pragma mark - GGBluetooth Public Property

- (CBPeripheral*)connectedPheripheral{
    return self.currentPheripheral;
}

- (CBCentralManagerState)bluetoothState{
    return (CBCentralManagerState)self.centerManager.state;
}

- (BOOL)isScanning{
    return self.centerManager.isScanning;
}
#pragma mark
#pragma mark - CBCentralManagerDelegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central{
    if (self.bluetoothStatusUpdateBlock) {
        self.bluetoothStatusUpdateBlock(central);
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI{
    if (self.discoverPeripheralBlock) {
        if ([self describeDictonary:advertisementData]) {
            peripheral.macAddress = [self macAddressFromAdvertisementData:advertisementData];
            self.discoverPeripheralBlock(peripheral,advertisementData,RSSI);
        }
    }
}

- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals{
    
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral{
    self.currentPheripheral = peripheral;
    self.currentPheripheral.delegate = self;
    self.isConnect = YES;
    if (self.connectBlock) {
        self.connectBlock(YES,peripheral,nil);
    }
    
    [self.currentPheripheral discoverServices:nil];
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    self.isConnect = NO;
    if (self.connectBlock) {
        self.connectBlock(NO,nil,error);
    }
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    self.currentPheripheral = nil;
    if (self.disconnectBlock) {
        self.disconnectBlock(peripheral,error);
    }
    if (self.peripheralDisconnectedBlock) {
        self.peripheralDisconnectedBlock(peripheral,error);
    }
}
#pragma mark
#pragma mark - CBPeripheralDelegate
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error{
    if (self.discoverServicesBlock) {
        self.discoverServicesBlock(peripheral.services);
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error{
    if (self.discoverCharacteristicBlock) {
        self.discoverCharacteristicBlock(service.characteristics,error);
    }
    for (CBCharacteristic *c in service.characteristics) {
        CBCharacteristicProperties properties = c.properties;
        if (properties & CBCharacteristicPropertyBroadcast) {
            NSLog(@"CBCharacteristicPropertyBroadcast");
        }
        if (properties & CBCharacteristicPropertyRead) {
            NSLog(@"CBCharacteristicPropertyRead");
        }
        if (properties & CBCharacteristicPropertyWriteWithoutResponse) {
            NSLog(@"CBCharacteristicPropertyWriteWithoutResponse");
        }
        if (properties & CBCharacteristicPropertyWrite) {
            NSLog(@"CBCharacteristicPropertyWrite");
        }
        if (properties & CBCharacteristicPropertyNotify) {
            NSLog(@"CBCharacteristicPropertyNotify");
        }
        if (properties & CBCharacteristicPropertyIndicate) {
            NSLog(@"CBCharacteristicPropertyIndicate");
        }
        if (properties & CBCharacteristicPropertyAuthenticatedSignedWrites) {
            NSLog(@"CBCharacteristicPropertyAuthenticatedSignedWrites");
        }
        if (properties & CBCharacteristicPropertyExtendedProperties) {
            NSLog(@"CBCharacteristicPropertyExtendedProperties");
        }
        if (properties & CBCharacteristicPropertyNotifyEncryptionRequired) {
            NSLog(@"CBCharacteristicPropertyNotifyEncryptionRequired");
        }
        if (properties & CBCharacteristicPropertyIndicateEncryptionRequired) {
            NSLog(@"CBCharacteristicPropertyIndicateEncryptionRequired");
        }
        if (c.properties & CBCharacteristicPropertyNotify) {
            [self.currentPheripheral setNotifyValue:YES forCharacteristic:c];
            self.notifyCharacteristic = c;
        }
        if (c.properties == CBCharacteristicPropertyRead) {
            [self.currentPheripheral setNotifyValue:YES forCharacteristic:c];
            self.notifyCharacteristic = c;
        }
        //SC
        if ([ZNSTools isSCLock]) {
            if (c.properties & CBCharacteristicPropertyRead) {
                [self.currentPheripheral readValueForCharacteristic:c];
            }
        }
       
        // 特征值的特性是CBCharacteristicPropertyWrite+CBCharacteristicPropertyWriteWithoutResponse
        if (c.properties & (CBCharacteristicPropertyWrite+CBCharacteristicPropertyWriteWithoutResponse)) {
            self.writeableCharacteristic = c;
            if (self.canWriteValueBlock) {
                self.canWriteValueBlock();
            }
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
     self.isSendOk = YES;
    if (self.notifyCharacteristicBlock) {
//         self.notifyCharacteristicBlock(characteristic,characteristic.value,error);
        if ([ZNSTools isSCLock]) {
            if (characteristic.value.length < 7) {
                return ;
            }
            long count = characteristic.value.length / 20;
            for (int i = 0; i < count; i++) {
                if (i == 0) {
                    [self.recvData appendData:[characteristic.value subdataWithRange:NSMakeRange(0, 20)]];
                }
                else {
                    [self.recvData appendData:[characteristic.value subdataWithRange:NSMakeRange(i * 20 + 1, 19)]];
                }
            }
            if ([self scEqualsCheckCRC16:self.recvData]) {
                self.notifyCharacteristicBlock(characteristic,self.recvData,error);
                self.recvData  = [NSMutableData data]; 
            }
        }else {
            [self.recvData appendData:characteristic.value];
            if ([self equalsCheckCRC16:self.recvData]) {
                self.notifyCharacteristicBlock(characteristic,self.recvData,error);
                self.recvData  = [NSMutableData data];
                
                if ([timer isValid]) {
                    [timer invalidate];
                    timer = nil;
                }
            }
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    NSLog(@"didWriteValueForCharacteristic = %@",characteristic);
    if (self.writeValueBlock) {
        self.writeValueBlock(characteristic,error);
    }
}

- (BOOL) equalsCheckCRC16:(NSData*)data{
    NSUInteger len = [data length];
    if (len <7) {
        return NO;
    }
    //取出需要计算校验的内容部分，报文长度开始至动态部分结束 ＝ 报文 － 4同步头－2个校验码
    NSData  *dataBody = [data subdataWithRange:NSMakeRange(4, len-6)];
    uint16_t crc16Sum = [dataBody crc16Modbus];
    Byte crc16Sumb1 = (crc16Sum >> 8) & 0xFF;
    Byte crc16Sumb2 = crc16Sum & 0xFF;
    NSMutableData  *newCheck = [NSMutableData dataWithBytes:&crc16Sumb1 length:1];
    [newCheck  appendBytes:&crc16Sumb2 length:1];
    NSData  *oldCheck = [data subdataWithRange:NSMakeRange(len-2, 2)];
    if ([newCheck isEqualToData:oldCheck]) {
        return YES;
    }
    return NO;
    
}

- (BOOL)scEqualsCheckCRC16:(NSData*)data {
    NSInteger bodyLength = 0;
    NSData *bodyLengthData = [data subdataWithRange:NSMakeRange(3, 1)];
    [bodyLengthData getBytes:&bodyLength length:1];
    
    NSUInteger len = [data length];
    if ((len - 1) <= bodyLength) {
        return NO;
    }
    return YES;
}

/**
 *  智能锁设备
 */
- (Boolean)describeDictonary: (NSDictionary *) dict
{
    NSArray *keys;
    id key;
    keys = [dict allKeys];
    for(int i = 0; i < [keys count]; i++)
    {
        key = [keys objectAtIndex:i];
        if([key isEqualToString:@"kCBAdvDataManufacturerData"])
        {
            NSData *tempValue = [dict objectForKey:key];
            const Byte *tempByte = [tempValue bytes];
            if([tempValue length] == 8 && tempByte[0] == 0x48 && tempByte[1] == 0x4D)
            {
                if(tempByte[2] == 0x00 && tempByte[3] == 0x0E && tempByte[4] == 0x0B)
                { //is dual mode HM-12 HM-13 HM-14
                    return true;
                }else //is HM-10, HM-11, HM-15, HM-16, HM-17
                    return true;
            }
        }else if([key isEqualToString:@"kCBAdvDataLocalName"])
        {
            //there is name
            NSString *szName = [dict objectForKey: key];
            if ([szName hasPrefix:@"HM"]) {
                return true;
            }
            if ([szName hasPrefix:@"LanQian"]) {
                return true;
            }
            if ([szName isEqualToString:@"LanQianTech"]) {
                return true;
            }
            if ([szName isEqualToString:@"LanQian Tech"]) {
                return true;
            }
            if ([szName isEqualToString:@"Lan Qian Tech"]) {
                return true;
            }
        }
    }
    return true;
}
- (NSString *)macAddressFromAdvertisementData:(NSDictionary *)advertisementData{
    if ([advertisementData valueForKey:@"kCBAdvDataManufacturerData"]) {
        NSData *data = [advertisementData valueForKey:@"kCBAdvDataManufacturerData"];
        if (7<[data length]) {
            NSData *mac = [data subdataWithRange:NSMakeRange(2, 6)];
            NSString *macAddressString = [NSString hexadecimalString:mac];
            NSLog(@"mac address ===================== %@", macAddressString);
            return macAddressString;
        }else{
          return @"";
        }
    }else{
        return @"";
    }
}
@end
