//
//  GGBluetooth.h
//  BluetoothTesting
//
//  Created by ChenZhiWen on 12/25/14.
//  Copyright (c) 2014 Ganguo Technology Co.,Ltd. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
typedef void (^DiscoverPeripheralBlock)(CBPeripheral *discoverPeripheral,NSDictionary *advertisementData,NSNumber *RSSI);
typedef void (^ConnectBlock)(BOOL success,CBPeripheral *connectedPeripheral, NSError *error);
typedef void (^DisconnectBlock)(CBPeripheral *connectedPeripheral, NSError *error);
typedef void (^DiscoverServicesBlock)(NSArray *services);
typedef void (^DiscoverCharacteristicBlock)(NSArray *characteristics,NSError *error);
typedef void (^NotifyCharacteristicBlock)(CBCharacteristic *characteristic,NSData *recvData,NSError *error);
typedef void (^WriteValueBlock)(CBCharacteristic *characteristic,NSError *error);
typedef void (^CenteralStatusUpdateBlcok)(CBCentralManager *centeral);
typedef void (^CanWriteValueBlock)(void);
typedef void (^SendFailureBlock)(void);

@interface GGBluetooth : NSObject

/**
 *  Centeral状态改变的block
 */
@property (nonatomic, copy) CenteralStatusUpdateBlcok bluetoothStatusUpdateBlock;


/**
 *  发现周边的block
 */
@property (nonatomic, copy) DiscoverPeripheralBlock discoverPeripheralBlock;

/**
 *  设置特征值改变的通知block
 */
@property (nonatomic, copy) NotifyCharacteristicBlock notifyCharacteristicBlock;

/**
 *  设置特征值改变的通知block
 */
@property (nonatomic, copy) DisconnectBlock peripheralDisconnectedBlock;
/**
 *  发现可写的值block
 */
@property (nonatomic, copy) CanWriteValueBlock canWriteValueBlock;
/**
 *  设置发送失败的block
 */
@property (nonatomic, copy) SendFailureBlock sendFailureBlock;

/**
 *  可写的特性  烤箱only
 */
@property (nonatomic, strong) CBCharacteristic *writeableCharacteristic;
/**
 *  notify特性  烤箱only
 */
@property (nonatomic, strong) CBCharacteristic *notifyCharacteristic;
+ (id)sharedManager;
/**
 *  开始扫描
 */
- (void)startScan;
/**
 *  停止扫描
 */
- (void)stopScan;
/**
 *  连接周边设备
 */
- (void)connectPheripheral:(CBPeripheral*)peripheral withSuccessBlcok:(ConnectBlock)connectedBlock;
/**
 *  断开周边设备
 */
- (void)disconnectPheripheral:(CBPeripheral*)peripheral;
/**
 *  断开当前连接
 */
- (void)disconnectCurrentDevice;
/**
 *  查找当前设备的服务
 */
- (void)discoverCurrentPheripheralServices:(DiscoverServicesBlock)block;
/**
 *  查找服务的特征
 */
- (void)discoverCharacteristicsFormService:(CBService*)service withBlock:(DiscoverCharacteristicBlock)block;
/**
 *  读取特征值
 */
- (void)readCharacteristicValue:(CBCharacteristic*)characteristic;
/**
 *  写入值
 */
- (void)writeValue:(NSData*)data forCharacteristic:(CBCharacteristic*)characteristic withBlock:(WriteValueBlock)block;

- (void)writeValueData:(NSData*)data;

- (void)writeValueDataSC:(NSData*)data;

- (CBPeripheral*)connectedPheripheral;

- (CBCentralManagerState)bluetoothState;

- (BOOL)isScanning;

- (void) sendFrame:(NSUInteger)frameCount  numberOfFramesPer:(NSUInteger)numberOfFramesPer

          frameLen:(NSUInteger)frameLen cmd:(Byte)cmd fun:(Byte)fun data:(NSData*)data;

/**
 *  智能锁项目
 */
- (NSString *)macAddressFromAdvertisementData:(NSDictionary *)advertisementData;
@end
