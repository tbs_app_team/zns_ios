//
//  ZNSHistory.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 16/1/19.
//  Copyright © 2016 czwen. All rights reserved.
//

#import "ZNSHistory.h"

@implementation ZNSHistory
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{
             @"hid":@"id",
             };
}

- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    NSString *unlock_at_dateString = dic[@"unlock_at"];
    if (![unlock_at_dateString isKindOfClass:[NSString class]]) return NO;
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyyMMddHHmmss"];
    
    NSDate *unlock_at = [formatter dateFromString:unlock_at_dateString];
    self.unlock_at = [unlock_at stringWithFormat:@"yyyy年MM月dd日 HH:mm:ss"];
    return YES;
}

@end
