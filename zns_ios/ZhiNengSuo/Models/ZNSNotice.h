//
//  ZNSNotice.h
//  ZhiNengSuo
//
//  Created by Chuenlu Kuo on 16/12/28.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZNSNotice : NSObject

@property (nonatomic,assign) NSInteger nid;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *content;
@property (nonatomic,strong) NSString *createBy;
@property (nonatomic,strong) NSString *createAt;

- (void)setupDataWith:(NSDictionary *)json;

@end
