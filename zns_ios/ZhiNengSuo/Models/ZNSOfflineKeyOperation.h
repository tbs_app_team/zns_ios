//
//  ZNSOfflineKeyOperation.h
//  ZhiNengSuo
//
//  Created by ekey on 16/4/8.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZNSCache.h"
#import "ZNSUser.h"

@interface ZNSOfflineKeyOperation : NSObject

typedef enum : NSUInteger {
    ZNSOfflineKeyResultOrderNormal=6,//<正常操作
    ZNSOfflineKeyResultOrderWarnning=255,//<越权未遂
} ZNSOfflineKeyResult;

@property NSString *action;// 操作类型fixed、temp
@property NSInteger result;// 操作结果
@property NSInteger lid;// 锁ID
@property NSString *unlock_at;//操作时间

//+ (void)createOfflineUnlockSuccess:(ZNSOfflineKeyOperation *)offlineKeyOperation;

@end
