//
//  ZNSError.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/24/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "ZNSError.h"

@implementation ZNSError
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{
             @"errorMessage":@"error",
             };
}

+ (ZNSError *)networkError{
    static ZNSError *error;
    if (!error) {
        error = [ZNSError new];
        error.errorMessage = @"网络状态欠佳";
    }
    return error;
}

+ (ZNSError *)serverError{
    static ZNSError *error;
    if (!error) {
        error = [ZNSError new];
        error.errorMessage = @"服务器出错！";
        error.errorCode = -1;
    }
    return error;
}

@end
