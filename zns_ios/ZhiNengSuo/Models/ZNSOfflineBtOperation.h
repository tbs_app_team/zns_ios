//
//  ZNSOfflineBtOperation.h
//  ZhiNengSuo
//
//  Created by ekey on 16/4/8.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZNSOfflineBtOperation : NSObject

typedef enum : NSUInteger {
    ZNSOfflineBtResultOrderFail=0,//<开锁指令失败
    ZNSOfflineBtResultOrderSuccess=1,//<开锁指令成功
    ZNSOfflineBtResultOrderUnlockSuccess=2,//<开锁成功
    ZNSOfflineBtResultOrderOpenDoorSuccess=3,//<开门成功
    ZNSOfflineBtResultOrderCloseDoorSuccess=4,//<关门成功
    ZNSOfflineBtResultOrderLockSuccess=5,//<闭锁成功
    ZNSOfflineBtResultOrderNormal=6,//<正常操作
    ZNSOfflineBtResultOrderWarnning=255,//<越权未遂
} ZNSOfflineBtOperationResult;

@property NSString* deviceName;// 设备名称
@property NSInteger lid;// 锁ID
@property NSString *updateDateTime;// 更新时间

@property NSString *action;// "fixed|temp", //鉴权开锁|临时申请开锁

@property NSInteger tid;// 临时申请开锁时必带此项，鉴权开锁没有此项

//@property  BOOL endSign;// 这条记录结束标记true 结束、false 未结束

@property NSString* directive_fault_at ;// 开锁指令状态失败

@property NSString* directive_at ;// 开锁指令成功时间

@property NSString* unlock_at ;// 开锁时间

@property NSString* opendoor_at;// 开门时间

@property NSString* closedoor_at ;// 关门时间

@property NSString* lock_at;// 闭锁时间

@property NSString* ultra_at;// 越权时间

@property NSInteger result;// 0|1|2|3|4|5|6|255
// /开锁指令失败|开锁指令成功|开锁成功|开门成功|关门成功|闭锁成功|正常操作|越权未遂


@end
