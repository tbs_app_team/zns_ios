//
//  ZNSWorkBill.h
//  ZhiNengSuo
//
//  Created by ekey on 16/9/19.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZNSObject.h"

@interface ZNSWorkBill : NSObject

@property NSString *skid;//任务id
@property NSString *billno;//工单编号
@property NSString *worktype;//工单类型
@property NSString *worktypeValue;
@property NSString *workdesc;//工单内容
@property NSString *oid;//基站［设备］
@property NSString *oname;//基站［设备］
@property NSString *sid;//执行单位
@property NSString *sname;//执行单位
@property NSString *limited;//进站超时时间
@property NSString *limitedValue;
@property NSString *memo;//备注
@property NSString *status;//工单状态
@property NSString *create_by;//派工人
@property NSString *create_username;//派工人手机号
@property NSString *createtime;//派工时间
@property NSString *rec_by;//接单ren
@property NSString *rec_username;//接单人手机号
@property NSString *rec_at;//接单时间
@property NSString *rectime;
@property NSString *intime;//进站时间（第一次开锁时间）
@property NSString *backtime;//回单时间
@property NSArray *lockset;
@property BOOL inflag;//超时，标示为1，否则为0
@property BOOL backflag;//回单是否异常，默认0，（正常）1（异常）

@property NSString *apply_at;  //申请时间
@property NSString *apply_by;  //申请人姓名
@property NSString *apply_username;  //申请人账号
@property NSString *applyin_at;  //申请进站时间
@property NSString *applyleave_at;  //申请离站时间
@property NSString *in_by;  //进站人姓名
@property NSString *in_at;  //实际进站时间
@property NSString *back_at;  //回单时间
@property NSString *mobile;  //
@property NSString *need_accompany;  //是否需要随工
@property NSString *update_at;  //记录产生时间
@property NSString *validate_lng_lat;  //是否需要判断经纬度
@property ZNSObject *object;


@end
