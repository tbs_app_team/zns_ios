//
//  ZNSWorkBill.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/19.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "ZNSWorkBill.h"

@implementation ZNSWorkBill


+ (NSDictionary *)modelCustomPropertyMapper{
    return @{
             @"skid":@"id",
            };
}
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             @"object":[ZNSObject class],
             };
}
@end
