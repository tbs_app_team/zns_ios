//
//  ZNSWorkBillTimeliness.h
//  ZhiNengSuo
//
//  Created by ekey on 16/9/22.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZNSWorkBillTimeliness : NSObject

@property NSString *queryobj;//基站（设备）
@property NSString *inrate;//进站及时率
@property NSString *plancount;//应进站数
@property NSString *realcount;//实际进站数
@property NSString *intimecount;//及时进站数
@property NSString *uncount;//未进站数

@property NSString *name;//
@property NSString *income;//
@property NSString *incomeRate;//
@property NSString *should;//
@property NSString *unfinished;//
@property NSArray *details;
@property NSString *actual;

@end
