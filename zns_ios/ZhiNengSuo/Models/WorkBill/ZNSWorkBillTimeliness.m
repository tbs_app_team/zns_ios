//
//  ZNSWorkBillTimeliness.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/22.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "ZNSWorkBillTimeliness.h"
#import "ZNSWorkBill.h"

@implementation ZNSWorkBillTimeliness

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             @"details":[ZNSWorkBill class],
             };
}

@end
