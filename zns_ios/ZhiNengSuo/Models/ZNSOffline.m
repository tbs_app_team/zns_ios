//
//  ZNSOffline.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/7/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "ZNSOffline.h"

@implementation ZNSOffline

+ (NSDictionary *)modelCustomPropertyMapper{
    return @{
             @"oid":@"id",
             @"locks":@"items",
             };
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             @"apply_at":[ZNSDate class],
             @"locks":[ZNSLock class],
             };
}
@end
