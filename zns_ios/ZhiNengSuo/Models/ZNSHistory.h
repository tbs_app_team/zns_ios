//
//  ZNSHistory.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 16/1/19.
//  Copyright © 2016 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    ZNSHistoryResultOrderFail=0,//<开锁指令失败
    ZNSHistoryResultOrderSuccess=1,//<开锁指令成功
    ZNSHistoryResultOrderUnlockSuccess=2,//<开锁成功
    ZNSHistoryResultOrderOpenDoorSuccess=3,//<开门成功
    ZNSHistoryResultOrderCloseDoorSuccess=4,//<关门成功
    ZNSHistoryResultOrderLockSuccess=5,//<闭锁成功
    ZNSHistoryResultOrderNormal=6,//<正常操作
    ZNSHistoryResultOrderWarnning=255,//<越权未遂
} ZNSHistoryResult;

@interface ZNSHistory : NSObject
@property NSNumber *hid;
@property NSNumber *oid;
@property NSString *oname;
@property NSString *ocarrier;
@property NSString *oarea;
@property NSString *otype;
@property NSNumber *osid;
@property NSString *osection;
@property NSNumber *lid;
@property NSString *ltype;
@property NSString *action;
@property NSString *directive_at;
@property NSString *unlock_at;
@property NSString *opendoor_at;
@property NSString *closedoor_at;
@property NSString *lock_at;
@property NSString *unlock_id;
@property NSString *unlock_by;
@property NSString *urole;
@property NSString *usid;
@property NSString *usection;
@property NSString *verify_by;
@property NSString *verify_at;
@property NSString *status;
@property NSString *result;
//@property ZNSHistoryResult result;
@end
