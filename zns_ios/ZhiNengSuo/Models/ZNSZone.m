//
//  ZNSZone.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/10/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "ZNSZone.h"

@implementation ZNSZone

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             @"object":[ZNSObject class],
             };
}
- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    NSString *zid = [dic valueForKey:@"zid"];
    if (zid) {
        _zid = zid;
    }else{
        _zid = [dic valueForKey:@"id"];
    }
    NSString *updatetime = dic[@"updatetime"];
    if (!updatetime) return NO;
    _updateTime = [[NSDate dateWithString:updatetime format:@"YYYYMMddHHmmss"]dateByAddingSeconds:[NSTimeZone localTimeZone].secondsFromGMT];
    return YES;
}

- (BOOL)modelCustomTransformToDictionary:(NSMutableDictionary *)dic {
    if (!_updateTime) return NO;
    dic[@"updatetime"] = [[_updateTime dateByAddingSeconds:-[NSTimeZone localTimeZone].secondsFromGMT] stringWithFormat:@"YYYYMMddHHmmss"];
    return YES;
}
@end
