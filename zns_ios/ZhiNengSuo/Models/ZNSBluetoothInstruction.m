//
//  ZNSBluetoothInstruction.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/30/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "ZNSBluetoothInstruction.h"
#import "NSData+Extensions.h"
@interface ZNSBluetoothInstruction()
@property (nonatomic, assign) Byte body;
@property (nonatomic, assign) Byte control;
@property (nonatomic, assign) Byte cmd;
@property (nonatomic, assign) Byte function;
@property (nonatomic, assign) Byte ack;// 1 or 2
@property (nonatomic, assign) Byte isRight;//0x80 or..
@property (nonatomic, strong) NSData *data;
@property (nonatomic, strong) NSString *rfid;
@property (nonatomic, strong) NSData *rfidData;
@property (nonatomic, strong) NSData *bodyData;
@end

@implementation ZNSBluetoothInstruction
static const Byte SYNC_HEAD[] = {0xa5,0x5a,0xa5,0x5a};
- (instancetype)initWithInstruction:(Byte)instruction function:(Byte)function body:(Byte)body rfid:(NSData*)rfid{
    self = [super init];
    
    self.body = body;
    
    NSMutableData *allByte = [NSMutableData dataWithBytes:&SYNC_HEAD length:sizeof(SYNC_HEAD)];
    
    //地址
    Byte address[] = {0x00,0x00,0x00,0x00};
    //控制字
    Byte control ;
    if (rfid) {
        control = 0x10;
    }else{
        control = 0x10;
    }
    //动态部分＝命令码＋功能码＋数据正文
    
//    Byte dynamicPart[] = {instruction,function,body};
    NSMutableData *dynamicPart = [[NSMutableData alloc] init];
    [dynamicPart appendBytes:&instruction length:sizeof(instruction)];
    [dynamicPart appendBytes:&function length:sizeof(function)];
    
    if (rfid) {
        [dynamicPart appendData:rfid];
    }
    if (body) {
        [dynamicPart appendBytes:&body length:sizeof(body)];
    }

    //报文长度
//    Byte lenth =  dynamicPart.length;
    NSUInteger i = dynamicPart.length;
    NSData *test = [NSData dataWithBytes: &i length: 2];
    
    
    NSMutableData *crc16CheckData = [NSMutableData data];
    [crc16CheckData appendData:test];
    [crc16CheckData appendBytes:&address length:sizeof(address)];
    [crc16CheckData appendBytes:&control length:sizeof(control)];
    [crc16CheckData appendData:dynamicPart];
    
    unsigned short crc16Sum = [crc16CheckData crc16Modbus];
    Byte crc16Sumb1 = (crc16Sum >> 8) & 0xFF;
    Byte crc16Sumb2 = crc16Sum & 0xFF;

//    [allByte appendBytes:&lenth length:sizeof(lenth)];
    [allByte appendData:test];
    [allByte appendBytes:&address length:sizeof(address)];
    [allByte appendBytes:&control length:sizeof(control)];
    [allByte appendData:dynamicPart];
    [allByte appendBytes:&crc16Sumb1 length:sizeof(crc16Sumb1)];
    [allByte appendBytes:&crc16Sumb2 length:sizeof(crc16Sumb2)];
    
    self.data = allByte;
    return self;
}

- (instancetype)initWithMoreData:(Byte)instruction function:(Byte)function body:(NSData*)body rfid:(NSData*)rfid{
    self = [super init];
    
//    self.body = body;
    
    NSMutableData *allByte = [NSMutableData dataWithBytes:&SYNC_HEAD length:sizeof(SYNC_HEAD)];
    
    //地址
    Byte address[] = {0x00,0x00,0x00,0x00};
    //控制字
    Byte control ;
    
    control = 0x10;

    //动态部分＝命令码＋功能码＋数据正文
    
    //    Byte dynamicPart[] = {instruction,function,body};
    NSMutableData *dynamicPart = [[NSMutableData alloc] init];
    [dynamicPart appendBytes:&instruction length:sizeof(instruction)];
    [dynamicPart appendBytes:&function length:sizeof(function)];
    
    if (body) {
//        NSData *test = [NSData dataWithBytes: &body length: 2];
        [dynamicPart appendData:body];

    }
    if (rfid) {
        [dynamicPart appendData:rfid];
    }
    
    
    //报文长度
    //    Byte lenth =  dynamicPart.length;
    NSUInteger i = dynamicPart.length;
    NSData *test = [NSData dataWithBytes: &i length: 2];
    
    
    NSMutableData *crc16CheckData = [NSMutableData data];
    [crc16CheckData appendData:test];
    [crc16CheckData appendBytes:&address length:sizeof(address)];
    [crc16CheckData appendBytes:&control length:sizeof(control)];
    [crc16CheckData appendData:dynamicPart];
    
    unsigned short crc16Sum = [crc16CheckData crc16Modbus];
    Byte crc16Sumb1 = (crc16Sum >> 8) & 0xFF;
    Byte crc16Sumb2 = crc16Sum & 0xFF;
    
    //    [allByte appendBytes:&lenth length:sizeof(lenth)];
    [allByte appendData:test];
    [allByte appendBytes:&address length:sizeof(address)];
    [allByte appendBytes:&control length:sizeof(control)];
    [allByte appendData:dynamicPart];
    [allByte appendBytes:&crc16Sumb1 length:sizeof(crc16Sumb1)];
    [allByte appendBytes:&crc16Sumb2 length:sizeof(crc16Sumb2)];
    
    self.data = allByte;
    return self;
}


- (instancetype)initWithByteData:(NSData*)data{
    self = [super init];
    
    // 同步头
    NSString *head = [NSString hexadecimalString:[data subdataWithRange:NSMakeRange(0, 4)]];
    if (![head isEqualToString:[NSString hexadecimalString:[NSData dataWithBytes:&SYNC_HEAD length:sizeof(SYNC_HEAD)]]]) {
        return nil;
    }
    
//    int bodyLengthChar;
//    [data getBytes:&bodyLengthChar range:NSMakeRange(4, 2)];
//    NSInteger bodyLength = [NSNumber numberWithUnsignedChar:bodyLengthChar].integerValue;
    
    NSInteger bodyLength = 0;
    [[data subdataWithRange:NSMakeRange(4, 2)]  getBytes:&bodyLength length:2];
    
    
//    NSString *address = [NSString hexadecimalString:[data subdataWithRange:NSMakeRange(5, 4)]];
    
    unsigned char control;
    [data getBytes:&control range:NSMakeRange(10, 1)];
    self.control = control;
    printf("\ncontrol:%02x",control);

    unsigned char order;
    [data getBytes:&order range:NSMakeRange(11, 1)];
    printf("cmd:%02x",order);
    self.cmd = order;

    unsigned char func;
    [data getBytes:&func range:NSMakeRange(12, 1)];
    printf("func:%02x",func);
    self.function = func;
    
    unsigned char body;
    [data getBytes:&body range:NSMakeRange(13, 1)];
    self.on = [NSNumber numberWithUnsignedChar:body].boolValue;
    
   self.bodyData = [data subdataWithRange:NSMakeRange(13, bodyLength-2)];
    
//    if (order == 0x07) {
//        self.battery = [NSNumber numberWithUnsignedShort:0x2a09].integerValue;
//    }
    if (bodyLength-2 == 5) {
        self.rfid = [NSString hexadecimalString:[data subdataWithRange:NSMakeRange(13, bodyLength-2)]];
        self.rfidData = [data subdataWithRange:NSMakeRange(13, bodyLength-2)];
    }
    unsigned char tmp;
    if (bodyLength-2 == 2) {//ack
        
        [data getBytes:&tmp range:NSMakeRange(13, 1)];
        self.ack = tmp;
        [data getBytes:&tmp range:NSMakeRange(14, 1)];
        self.isRight = tmp;
    }

    NSLog(@"body:%@\n",self.bodyData);
    
    return self;
}

+ (instancetype)unlockInstruction{
    ZNSBluetoothInstruction *instruction = [[ZNSBluetoothInstruction alloc]initWithInstruction:LOCK_INSTRUCTION_CODE function:FUNCTION_UNLOCK_CODE body:0x01 rfid:nil];
    return instruction;
}

+ (instancetype)checkDoorStateInstruction{
ZNSBluetoothInstruction *instruction = [[ZNSBluetoothInstruction alloc]initWithInstruction:LOCK_INSTRUCTION_CODE function:FUNCTION_DOOR_STATUS_CHECK_CODE body:nil rfid:nil];
return instruction;
}
+ (instancetype)checkLockStateInstruction{
    ZNSBluetoothInstruction *instruction = [[ZNSBluetoothInstruction alloc]initWithInstruction:LOCK_INSTRUCTION_CODE function:FUNCTION_LOCK_STATUS_CHECK_CODE body:nil rfid:nil];
    return instruction;
}

+ (instancetype)unlockInstructionWithRFID:(NSData *)rfid{
    
    ZNSBluetoothInstruction *instruction = [[ZNSBluetoothInstruction alloc]initWithInstruction:SMART_KEY_INSTRUCTION_CODE function:FUNCTION_UNLOCK_CODE body:0x01 rfid:rfid];
    return instruction;
}

+ (instancetype)batteryInstruction{
    
    ZNSBluetoothInstruction *instruction = [[ZNSBluetoothInstruction alloc]initWithInstruction:0x07 function:0x00 body:nil rfid:nil];
    return instruction;
}

- (Byte)control{
    return _control;
}

- (Byte)function{
    return _function;
}
@end
