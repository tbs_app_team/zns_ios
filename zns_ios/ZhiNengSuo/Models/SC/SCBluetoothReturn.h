//
//  SCBluetoothReturn.h
//  TTZNMJ
//
//  Created by isprint on 2017/10/31.
//  Copyright © 2017年 contron. All rights reserved.
//

typedef enum {
    ReturnTypeLockInfo = 1,
    ReturnTypeInit = 2,
    ReturnTypeSwitch = 3,
    ReturnTypeLog = 4,
    ReturnTypeBattery = 5,
    ReturnTypeLockDetail = 6,
    ReturnTypeResetSecret = 7, //重置秘钥
    ReturnTypeClear = 8,
    ReturnTypeSuperClear = 9,
    ReturnTypeLockType = 10,
    ReturnTypeResetLockId = 11 //修改识别号
}ReturnType;

@interface SCBaseResult : NSObject

@property (nonatomic, assign) NSInteger mState; //指令状态 1: 成功

@end

@interface SCSwitchResult: SCBaseResult

@property (nonatomic, assign) NSInteger mLockState;

@end

@interface SCResetKeySecretResult: SCBaseResult

@end

@interface SCInitResult: SCBaseResult

@end

@interface SCLockInfoResult: SCBaseResult

@property (nonatomic, assign) NSInteger mLockState; //0:未知 1:开 2:关
@property (nonatomic, assign) NSInteger mLockInstallState; //0:未安装 1:已安装
@property (nonatomic, strong) NSString *mLockId;

@end

@interface SCLockTypeResult: SCBaseResult

@property (nonatomic, strong) NSString *mLockType;

@end

@interface SCBatteryResult: SCBaseResult

@property (nonatomic, assign) long mKeyEnergy;
@property (nonatomic, assign) long mKeyType;
@property (nonatomic, assign) long mKeyMan;

@end

@interface SCLockDetailResult: SCBaseResult

@property (nonatomic, strong) NSString *mLockId;
@property (nonatomic, assign) long mCorpId;
@property (nonatomic, assign) long mHardId;
@property (nonatomic, assign) long mSoftId;

@end

@interface SCLockLogResult: SCBaseResult

@property (nonatomic, strong) NSString *mDate;
@property (nonatomic, strong) NSString *mUID;
@property (nonatomic, assign) int mType;
@property (nonatomic, assign) int mResult;

@end

@interface SCClearResult: SCBaseResult

@end

@interface SCSuperClearResult: SCBaseResult

@end

@interface SCResetLockIdResult: SCBaseResult

@end

#import <Foundation/Foundation.h>

@interface SCBluetoothReturn : NSObject

- (instancetype)initWithByteData:(NSData*)data;

- (SCSwitchResult*)parseSwitchInstruction;

- (SCBaseResult*)parseResetKeySecretInstruction;

- (SCInitResult*)parseInitInstruction;

- (SCLockInfoResult*)parseLockInfoInstruction;

- (SCBaseResult*)parseLockTypeInstruction;

- (SCBatteryResult*)parseBatteryInstruction;

- (SCBaseResult*)parseLockDetailInstruction;

- (SCBaseResult*)parseLogInstruction;

- (SCClearResult*)parseClearInstruction;

- (SCSuperClearResult*)parseSuperClearInstruction;

- (SCBaseResult*)parseResetLockIdInstruction;

- (ReturnType)bluetoothReturnType;

@end


