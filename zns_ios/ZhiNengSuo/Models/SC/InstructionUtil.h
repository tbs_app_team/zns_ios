//
//  InstructionUtil.h
//  TTZNMJ
//
//  Created by peng on 17/11/4.
//  Copyright © 2017年 contron. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstructionUtil : NSObject

+ (id)shareInstance;

@property (nonatomic, assign) NSInteger mLockState;
@property (nonatomic, strong) NSData *mLockId;
@property (nonatomic, strong) NSData *mInitKey;
@property (nonatomic, strong) NSData *mGroupId;
@property (nonatomic, strong) NSData *mGroupSecret;
@property (nonatomic, strong) NSData *mSessionSecret;

@property (nonatomic, strong) NSMutableArray *listLog;

- (void)initData;

- (NSString *)lockId;
- (NSData *)mGroupSecret8;

//- (void)appendLog:(NSString *)dateString uId:(NSString *)uId type:(int)aType result:(int)aResult;
//- (NSString *)showLog;

@end
