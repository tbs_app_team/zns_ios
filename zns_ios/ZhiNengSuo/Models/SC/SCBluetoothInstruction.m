//
//  SCBluetoothInstruction.m
//  TTZNMJ
//
//  Created by isprint on 2017/10/31.
//  Copyright © 2017年 contron. All rights reserved.
//

#import "SCBluetoothInstruction.h"
#import "Rc4.h"
#import "InstructionUtil.h"
#import "NSString+Additions.h"
#import "NSData+YYAdd.h"
#import "ZNSUser.h"
#import "InstructionUtil.h"
@interface SCBluetoothInstruction ()

// 发送指令
@property (nonatomic, strong) NSData *data;

@end

@implementation SCBluetoothInstruction

- (instancetype)initWithInstruction:(Byte)instruction {
    self = [super init];
    
    Byte frame[] = {instruction};
    NSMutableData *allByte = [NSMutableData dataWithBytes:&frame length:sizeof(frame)];
    self.data = allByte;
    
    return self;
}

- (instancetype)initWithInstruction:(Byte)instruction data:(NSData *)aData key:(NSData *)rcKey {
    self = [super init];
    
    int dataLength = (int)aData.length;
    int keyLength = (int)rcKey.length;
    
    NSLog(@"data rc4 before = %@, key = %@", aData, rcKey);
    
    UInt8 *cipher = malloc(dataLength);
    [Rc4 encrypt:cipher data:(UInt8 *)[aData bytes] dataLength:dataLength key:(UInt8 *)[rcKey bytes] keyLength:keyLength];
    
    Byte c = 0;
    for (int i = 0; i < dataLength; i++) {
        c ^= cipher[i];
    }
    
    Byte instructionHead[] = {instruction, 0x00, dataLength};
    NSMutableData *instructionData = [NSMutableData dataWithBytes:instructionHead length:3];
    
    [instructionData appendBytes:cipher length:dataLength];
    [instructionData appendBytes:&c length:1];

    NSLog(@"data rc4 = %@", instructionData);
    
    self.data = instructionData;
    
    return self;
}

+ (instancetype)lockInfoInstruction
{
    SCBluetoothInstruction *instruction = [[SCBluetoothInstruction alloc]initWithInstruction:0x01];
    return instruction;
}

+ (instancetype)initInstruction
{
    NSMutableData *data = [NSMutableData data];
    
    NSData *mInitKey = [[InstructionUtil shareInstance] mInitKey];
    NSData *mGroupId = [[InstructionUtil shareInstance] mGroupId];
    NSData *mGroupSecret = [[InstructionUtil shareInstance] mGroupSecret];
    
    [data appendData:mInitKey];
    [data appendData:mGroupId];
    [data appendData:mGroupSecret];

    NSData *sessionSecret = [[InstructionUtil shareInstance] mSessionSecret];
    
    SCBluetoothInstruction *instruction = [[SCBluetoothInstruction alloc]initWithInstruction:0x02 data:data key:sessionSecret];
    
    return instruction;
}

+ (instancetype)switchLockInstruction:(int)switchType
{
    NSMutableData *data = [NSMutableData data];
    NSMutableData *auth = [NSMutableData data];
    
    NSData *uIdData = [self uIdData];
    
    [auth appendData:uIdData];
    [auth appendData:[[InstructionUtil shareInstance] mLockId]];
    
    NSData *mInitKey = [[InstructionUtil shareInstance] mInitKey];
    
    int dataLength = (int)auth.length;
    int keyLength = (int)mInitKey.length;
    
    UInt8 *cipher = malloc(dataLength);
    
    [Rc4 encrypt:cipher data:(UInt8 *)[auth bytes] dataLength:dataLength key:(UInt8 *)[mInitKey bytes] keyLength:keyLength];
    [data appendBytes:cipher length:dataLength];
    
    NSString *dateString = [NSString formatTimeWithFormat:[NSDate date] format:@"yyyyMMddhhmmss"];
    NSData *dateData = [dateString dataUsingEncoding:NSUTF8StringEncoding];
    [data appendData:dateData];
    
    [data appendData:uIdData];
    
    [data appendBytes:&switchType length:1];
    
    NSData *sessionSecret = [[InstructionUtil shareInstance] mSessionSecret];
    
    SCBluetoothInstruction *instruction = [[SCBluetoothInstruction alloc]initWithInstruction:0x03 data:data key:sessionSecret];
    
    return instruction;
    
}

+ (instancetype)batteryInstruction{
    
    SCBluetoothInstruction *instruction = [[SCBluetoothInstruction alloc]initWithInstruction:0x05];
    return instruction;
}

+ (instancetype)lockTypeInstruction{
    
    SCBluetoothInstruction *instruction = [[SCBluetoothInstruction alloc]initWithInstruction:0x0A];
    return instruction;
}

+ (instancetype)lockDetailInstruction{
    
    SCBluetoothInstruction *instruction = [[SCBluetoothInstruction alloc]initWithInstruction:0x06];
    return instruction;
}

+ (instancetype)resetKeySecretInstruction
{
    NSMutableData *data = [NSMutableData data];
    
    NSData *mInitKey = [[InstructionUtil shareInstance] mInitKey];
    NSData *mGroupSecret = [[InstructionUtil shareInstance] mGroupSecret];
    
    [data appendData:mGroupSecret];
    [data appendData:mInitKey];
    
    SCBluetoothInstruction *instruction = [[SCBluetoothInstruction alloc]initWithInstruction:0x07 data:data key:mGroupSecret];
    
    return instruction;
}

+ (instancetype)clearInstruction
{
    NSData *groupSecretData = [[InstructionUtil shareInstance] mGroupSecret];
    NSData *keyData = [[InstructionUtil shareInstance] mGroupSecret8];
    
    SCBluetoothInstruction *instruction = [[SCBluetoothInstruction alloc]initWithInstruction:0x08 data:groupSecretData key:keyData];
    
    return instruction;
}

+ (instancetype)superClearInstruction
{
    NSData *mInitData = [NSMutableData dataWithLength:16]; // [[InstructionUtil shareInstance] mInitKey];
    
    NSData *keyData = [[InstructionUtil shareInstance] mSessionSecret];
    
    Byte instructionByte = {0x09};
    
    SCBluetoothInstruction *instruction = [[SCBluetoothInstruction alloc]initWithInstruction:instructionByte data:mInitData key:keyData];
    
    return instruction;
}

+ (instancetype)resetLockIdInstruction
{
    Byte instructionByte[] = {0x0B, 0x00, 0x10};
    
    NSMutableData *data = [NSMutableData data];
    NSMutableData *temp = [NSMutableData data];
    
    int x = arc4random() % 255;
    
    for (int i = 0; i < 4; i++) {
        [temp appendBytes:&x length:1];
    }
    
    NSData *mLockIdData = [[InstructionUtil shareInstance] mLockId];
    [temp appendData: [mLockIdData subdataWithRange:NSMakeRange(4, 12)]];
    
    Byte *dateByte = (Byte*) [temp bytes];
    
    Byte c = 0;
    for (int i = 0; i < temp.length; i++) {
        c ^= dateByte[i];
    }
    
    [data appendBytes:instructionByte length:3];
    [data appendData:temp];
    [data appendBytes:&c length:1];
    
    SCBluetoothInstruction *instruction = [[SCBluetoothInstruction alloc]init];
    instruction.data = data;
    
    return instruction;
}

+ (NSData *)uIdData
{
    int uId = [[ZNSUser currentUser].idid intValue];
    uId = ntohl(uId);
    NSData *uIdData = [NSData dataWithBytes: &uId length: sizeof(uId)];
    NSMutableData *data = [NSMutableData dataWithLength:16 - uIdData.length];
    [data appendData:uIdData];
    
    return data;
}

@end
