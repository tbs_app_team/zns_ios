//
//  SCBluetoothInstruction.h
//  TTZNMJ
//
//  Created by isprint on 2017/10/31.
//  Copyright © 2017年 contron. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCBluetoothInstruction : NSObject

+ (instancetype)lockInfoInstruction;

+ (instancetype)initInstruction;

+ (instancetype)switchLockInstruction:(int)switchType;

+ (instancetype)batteryInstruction;

+ (instancetype)lockTypeInstruction;

+ (instancetype)lockDetailInstruction;

+ (instancetype)resetKeySecretInstruction;

+ (instancetype)clearInstruction;

+ (instancetype)superClearInstruction;

+ (instancetype)resetLockIdInstruction;

- (NSData*)data;

@end
