//
//  InstructionUtil.m
//  TTZNMJ
//
//  Created by peng on 17/11/4.
//  Copyright © 2017年 contron. All rights reserved.
//

#import "InstructionUtil.h"
#import "NSString+Additions.h"
#import "NSNumber+YYAdd.h"
@interface InstructionUtil ()

@end

@implementation InstructionUtil

+ (id)shareInstance{
    
    static InstructionUtil *sharedInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedInstance = [[self alloc] init];
        
        [sharedInstance initData];
        
    });
    return sharedInstance;
}

- (void)initData
{
    Byte groupIdByte[] = {0, 0, 0, 1};
    Byte keyInitByte[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    Byte groupSecretByte[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    Byte sessionSecretByte[] = {0, 1, 2, 3, 4, 5, 6, 7};
    
    self.mGroupId = [NSData dataWithBytes:groupIdByte length:4];
    self.mGroupSecret = [NSData dataWithBytes:groupSecretByte length:16];
    self.mInitKey = [NSData dataWithBytes:keyInitByte length:16];
    self.mSessionSecret = [NSData dataWithBytes:sessionSecretByte length:8];
}

- (NSString *)lockId
{
    if (self.mLockId && self.mLockId != NULL && self.mLockId != nil && self.mLockId.length > 0) {
        return [NSString hexadecimalString:self.mLockId];
    }
    else {
        return @"1111111111111111";
    }
}

- (NSData *)mGroupSecret8
{
    return [[[InstructionUtil shareInstance] mGroupSecret] subdataWithRange:NSMakeRange(0, 8)];
}

//- (void)appendLog:(NSString *)dateString uId:(NSString *)uId type:(int)aType result:(int)aResult
//{
//    if (_listLog == nil) {
//        _listLog = [NSMutableArray array];
//    }
//
//    NSString *formatTime = [NSString formatTimeString:dateString withFormat:@"yyyyMMddHHmmss"];
//
//
//    NSString *result = [NSString stringWithFormat:@"操作时间:%@, 操作类型:%@, 操作结果:%@, 用户编号: %@", formatTime, (aType == 1 ? @"成功" : @"失败"), (aResult == 1 ? @"成功" : @"失败"), [NSNumber numberWithString:uId]];
//
//    [_listLog addObject:result];
//}
//
//- (NSString *)showLog
//{
//    if (_listLog && _listLog.count > 0) {
//        NSMutableString *result = [NSMutableString string];
//        for (NSString *log in _listLog) {
//            [result appendFormat:@"%@\n", log];
//        }
//        return result;
//    }
//    return nil;
//}

@end
