//
//  SCBluetoothReturn.m
//  TTZNMJ
//
//  Created by isprint on 2017/10/31.
//  Copyright © 2017年 contron. All rights reserved.
//

#import "SCBluetoothReturn.h"
#import "NSString+Additions.h"
#import "InstructionUtil.h"
#import "Rc4.h"
#import "NSNumber+YYAdd.h"
@interface SCBluetoothReturn ()

@property (nonatomic, assign) NSUInteger bodyLength;
// 返回结果
@property (nonatomic, strong) NSData *bodyData;
@property (nonatomic, assign) ReturnType returnType;
@end

@implementation SCBluetoothReturn

- (instancetype)initWithByteData:(NSData*)data{
    self = [super init];
    
//    NSLog(@"data:%@\n",data);
    
    NSInteger returnType = 0;
    NSData *typeData = [data subdataWithRange:NSMakeRange(1, 1)];
    [typeData getBytes:&returnType length:1];
    
    self.returnType = (ReturnType) returnType;
    
    NSData *bodyLengthData = [data subdataWithRange:NSMakeRange(3, 1)];
    [bodyLengthData getBytes:&_bodyLength length:1];
    
    self.bodyData = [data subdataWithRange:NSMakeRange(4,data.length - 4)];
    
//    NSLog(@"body:%@\n",self.bodyData);
    
    return self;
}

- (SCBaseResult*)parseSwitchInstruction
{
    SCSwitchResult *result = [SCSwitchResult new];
    
    if (self.returnType == ReturnTypeSwitch) {
        
        int state = 0;
        
        [[self.bodyData subdataWithRange:NSMakeRange(0, 1)] getBytes:&state length:1];
        
        NSInteger mLockState = [[InstructionUtil shareInstance] mLockState];
        
        if (state == 1) {
            mLockState = (mLockState == 1 ? 2 : 1);
            [[InstructionUtil shareInstance] setMLockState:mLockState];
        }
        
        [result setMState:state];
        [result setMLockState:mLockState];
    }
    
    return result;
}

- (SCBaseResult*)parseResetKeySecretInstruction
{
    SCResetKeySecretResult *result = [SCResetKeySecretResult new];
    
    if (self.returnType == ReturnTypeResetSecret) {
        
        NSData *resultData = [self.bodyData subdataWithRange:NSMakeRange(0, 1)];
        
        NSData *groupSecretData = [[InstructionUtil shareInstance] mGroupSecret];
        
        int dataLength = (int)resultData.length;
        int keyLength = (int)groupSecretData.length;
        
        UInt8 *decipher = malloc(dataLength);
        
        [Rc4 decrypt:decipher cipher:(UInt8 *)[resultData bytes] cipherLength:dataLength key:(UInt8 *)[groupSecretData bytes] keyLength:keyLength];
        
        result.mState = decipher[0];
    }
    
    return result;
}

- (SCInitResult*)parseInitInstruction
{
    SCInitResult *result = [SCInitResult new];
    
    if (self.returnType == ReturnTypeInit) {
        
        int state = 0;
        
        [[self.bodyData subdataWithRange:NSMakeRange(0, 1)] getBytes:&state length:1];
        
        result.mState = state;
        
        if (state == 1) {
            [[InstructionUtil shareInstance]initData];
        }
    }
    
    return result;
}

- (SCLockInfoResult*)parseLockInfoInstruction{
    
    SCLockInfoResult *result = [SCLockInfoResult new];
    
    if (self.returnType == ReturnTypeLockInfo) {
        
        if (_bodyLength == 30) {
            NSData *lockIdData = [self.bodyData subdataWithRange:NSMakeRange(0, 16)];
            
            long lockState = 0;
            long lockInstallState = 0;
            
            NSData *groupIdData = [self.bodyData subdataWithRange:NSMakeRange(17, 4)];
            NSData *sessionSecret = [self.bodyData subdataWithRange:NSMakeRange(21, 8)];
            
            [[self.bodyData subdataWithRange:NSMakeRange(16, 1)] getBytes:&lockState length:1];
            [[self.bodyData subdataWithRange:NSMakeRange(29, 1)] getBytes:&lockInstallState length:1];
            
            lockState = lockState >> 4;
            
            result.mLockState = lockState;
            result.mLockInstallState = lockInstallState;
            result.mLockId = [NSString hexadecimalString:lockIdData];
            result.mState = 1;
            
            [[InstructionUtil shareInstance] setMLockState:lockState];
            [[InstructionUtil shareInstance] setMLockId:lockIdData];
            [[InstructionUtil shareInstance] setMGroupId:groupIdData];
            [[InstructionUtil shareInstance] setMSessionSecret:sessionSecret];
        }
        
    }
    
    return result;
}

- (SCBaseResult*)parseLockTypeInstruction{
    
    SCLockTypeResult *result = [SCLockTypeResult new];
    
    if (self.returnType == ReturnTypeLockType) {
        
        result.mLockType = [[NSString alloc]initWithData:[self.bodyData subdataWithRange:NSMakeRange(0, _bodyLength)] encoding:NSUTF8StringEncoding];
    }
    
    return result;
}

- (SCBatteryResult*)parseBatteryInstruction{
    
    SCBatteryResult *result = [SCBatteryResult new];
    
    if (self.returnType == ReturnTypeBattery) {
        
        if (_bodyLength == 4) {
            long keyEnergy = 0;
            long keyType = 0;
            long keyMan = 0;
            [[self.bodyData subdataWithRange:NSMakeRange(0, 1)] getBytes:&keyEnergy length:1];
            [[self.bodyData subdataWithRange:NSMakeRange(1, 1)] getBytes:&keyType length:1];
            
            keyMan = [NSNumber numberWithString:[NSString hexadecimalString:[self.bodyData subdataWithRange:NSMakeRange(2, 2)]]].longValue;

            result.mState = 1;
            result.mKeyEnergy = keyEnergy;
            result.mKeyType = keyType;
            result.mKeyMan = keyMan;
        }
    }
    
    return result;
}

- (SCBaseResult*)parseLockDetailInstruction{
    
    SCLockDetailResult *result = [SCLockDetailResult new];
    
    if (self.returnType == ReturnTypeLockDetail) {
        
        long corpId = 0;
        long hardId = 0;
        long softId = 0;
        
        NSData *lockIdData = [self.bodyData subdataWithRange:NSMakeRange(0, 16)];
        
        [[self.bodyData subdataWithRange:NSMakeRange(16, 1)] getBytes:&corpId length:1];
        
        hardId = [NSNumber numberWithString:[NSString hexadecimalString:[self.bodyData subdataWithRange:NSMakeRange(17, 2)]]].longValue;
        softId = [NSNumber numberWithString:[NSString hexadecimalString:[self.bodyData subdataWithRange:NSMakeRange(19, 2)]]].longValue;
        
        result.mLockId = [NSString hexadecimalString:lockIdData];
        result.mCorpId = corpId;
        result.mHardId = hardId;
        result.mSoftId = softId;
    }
    
    return result;
}

- (SCBaseResult*)parseLogInstruction{
    
    SCLockLogResult *result = [SCLockLogResult new];
    
    if (self.returnType == ReturnTypeLog) {
        
        NSData *resultData = [self.bodyData subdataWithRange:NSMakeRange(0, 32)];
        
        NSData *keyData = [[InstructionUtil shareInstance] mSessionSecret];
        
        int dataLength = (int)resultData.length;
        int keyLength = (int)keyData.length;
        
        UInt8 *decipher = malloc(dataLength);
        
        [Rc4 decrypt:decipher cipher:(UInt8 *)[resultData bytes] cipherLength:dataLength key:(UInt8 *)[keyData bytes] keyLength:keyLength];
        
        NSMutableString *dateString = [NSMutableString string];
        NSMutableString *uIdString = [NSMutableString string];
        
        for (int i = 0; i < 14; i++) {
            [dateString appendFormat:@"%c", (unsigned char)decipher[i]];
        }
        
        for (int i = 16; i < 32; i++) {
            [uIdString appendFormat:@"%02x", decipher[i]];
        }
        
        int aType = decipher[14];
        int aResult = decipher[15];
        
        result.mDate = dateString;
        result.mUID = uIdString;
        result.mType = aType;
        result.mResult = aResult;
    }
    
    return result;
    
}

- (SCClearResult*)parseClearInstruction{
    
    SCClearResult *result = [SCClearResult new];
    
    if (self.returnType == ReturnTypeClear) {
        
        NSData *resultData = [self.bodyData subdataWithRange:NSMakeRange(0, 1)];
        
        NSData *groupSecretData = [[InstructionUtil shareInstance] mGroupSecret8];
        
        int dataLength = (int)resultData.length;
        int keyLength = (int)groupSecretData.length;
        
        UInt8 *decipher = malloc(dataLength);
        
        [Rc4 decrypt:decipher cipher:(UInt8 *)[resultData bytes] cipherLength:dataLength key:(UInt8 *)[groupSecretData bytes] keyLength:keyLength];
        
        result.mState = decipher[0];
    }
    
    return result;
}

- (SCSuperClearResult*)parseSuperClearInstruction{
    
    SCSuperClearResult *result = [SCSuperClearResult new];
    
    if (self.returnType == ReturnTypeSuperClear) {
        
        NSData *resultData = [self.bodyData subdataWithRange:NSMakeRange(0, 1)];
        
        NSData *keyData = [[InstructionUtil shareInstance] mSessionSecret];
        
        int dataLength = (int)resultData.length;
        int keyLength = (int)keyData.length;
        
        UInt8 *decipher = malloc(dataLength);
        
        [Rc4 decrypt:decipher cipher:(UInt8 *)[resultData bytes] cipherLength:dataLength key:(UInt8 *)[keyData bytes] keyLength:keyLength];
        
        result.mState = decipher[0];
    }
    
    return result;
}

- (SCBaseResult*)parseResetLockIdInstruction
{
    SCResetLockIdResult *result = [SCResetLockIdResult new];
    
    if (self.returnType == ReturnTypeResetLockId) {
        
        int state = 0;
        
        [[self.bodyData subdataWithRange:NSMakeRange(0, 1)] getBytes:&state length:1];
        
        result.mState = state;
    }
    
    return result;
}

- (ReturnType)bluetoothReturnType
{
    return _returnType;
}

@end

@implementation SCBaseResult

@end

@implementation SCSwitchResult

- (NSString *) description
{
    if (self.mState == 1) {
        return self.mLockState == 1 ? @"关锁成功" : @"开锁成功";
    }
    else {
        return self.mLockState == 1 ? @"关锁失败" : @"开锁失败";
    }
}

@end

@implementation SCResetKeySecretResult

- (NSString *) description
{
    return self.mState == 1 ? @"重置密钥成功" : @"重置密钥失败";
}

@end

@implementation SCInitResult

- (NSString *) description
{
    return self.mState == 1 ? @"初始化成功" : @"锁具已安装";
}

@end

@implementation SCLockInfoResult

- (NSString *) description
{
    NSMutableString *result = [NSMutableString string];
    
    NSString *lockStateString = @"未知";
    
    switch (self.mLockState) {
        case 0:
            lockStateString = @"未知";
            break;
        case 1:
            lockStateString = @"开启";
            break;
        case 2:
            lockStateString = @"关闭";
            break;
            
        default:
            break;
    }
    
    [result appendFormat:@"门锁ID：%@\n", self.mLockId];
    [result appendFormat:@"门锁状态：%@\n", lockStateString];
    [result appendFormat:@"安装状态：%@", self.mLockInstallState == 0 ? @"未安装" : @"已安装"];
    
    return result;
}

@end

@implementation SCLockTypeResult

- (NSString *) description
{
    return self.mLockType;
}

@end

@implementation SCBatteryResult

- (NSString *) description
{
    NSMutableString *result = [NSMutableString string];
    
    [result appendFormat:@"电量：%ld\n", self.mKeyEnergy];
    [result appendFormat:@"类型：%ld\n", self.mKeyType];
    [result appendFormat:@"厂商：%ld", self.mKeyMan];
    
    return result;
}

@end

@implementation SCLockDetailResult

- (NSString *) description
{
    NSMutableString *result = [NSMutableString string];
    
    [result appendFormat:@"锁具ID：%@\n", self.mLockId];
    [result appendFormat:@"厂家编号：%ld\n", self.mCorpId];
    [result appendFormat:@"硬件版本号：%ld\n", self.mHardId];
    [result appendFormat:@"软件版本号：%ld", self.mSoftId];
    
    return result;
}

@end

@implementation SCLockLogResult

- (NSString *) description
{
    NSString *formatTime = [NSString formatTimeString:self.mDate withFormat:@"yyyyMMddHHmmss"];

    NSString *result = [NSString stringWithFormat:@"操作时间:%@, 操作类型:%@, 操作结果:%@, 用户编号: %@", formatTime, (self.mType == 1 ? @"成功" : @"失败"), (self.mResult == 1 ? @"成功" : @"失败"), [NSNumber numberWithString:self.mUID]];
    
    return result;
}

@end

@implementation SCClearResult

- (NSString *) description
{
    return self.mState == 1 ? @"清空成功" : @"清空失败";
}

@end

@implementation SCSuperClearResult

- (NSString *) description
{
    return self.mState == 1 ? @"超级清空成功" : @"超级清空失败";
}

@end

@implementation SCResetLockIdResult

- (NSString *) description
{
    return self.mState == 1 ? @"修改锁识别码成功" : @"修改锁识别码失败";
}

@end
