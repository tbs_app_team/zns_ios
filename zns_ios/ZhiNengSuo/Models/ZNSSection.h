//
//  ZNSSection.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/3/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZNSSection : NSObject
@property NSString *sid;
@property NSString *intro;
@property NSString *name;
@property NSString *pid;
@property NSString *type;
@property NSString *parent;
@property (nonatomic,strong) NSArray *companys;   //代维单位数组
@end
