//
//  ZNSZone.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/10/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZNSObject.h"
@interface ZNSZone : NSObject
@property NSString *zid;
@property NSString *memo;
@property NSString *name;
@property NSArray *object;
@property NSString *section;
@property NSString *sid;
@property NSString *type;
@property NSDate *updateTime;
@end
//id = 2;
//memo = "\U8fd9\U662f\U4e00\U4e2aB\U533a\U57df";
//name = "B\U533a\U57df";
//object =             (
//                      {
//                          area = "B\U533a\U57df";
//                          carrier = "\U7535\U4fe1";
//                          id = 3;
//                          name = "\U8bbe\U59073";
//                          section = "xx\U90e8\U95e8";
//                          type = "\U57fa\U7ad9\U95e8";
//                      }
//                      );
//section = "xx\U90e8\U95e8";
//sid = 2;
//type = "";
//updatetime = 20151209233035;