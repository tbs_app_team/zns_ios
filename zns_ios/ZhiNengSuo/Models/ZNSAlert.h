//
//  ZNSAlert.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 16/1/24.
//  Copyright © 2016 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZNSDate.h"

@interface ZNSAlert : NSObject
//"id": "id",
//"msg": "消息内容",
//"create_at": "消息时间"
@property NSString *title;
@property NSString *message;
//@property ZNSDate *time;
@property NSString *time;

@property (nonatomic,strong) NSString *aid;
@property (nonatomic,strong) NSString *msg;
@property (nonatomic,strong) NSString *create_at;
@end
