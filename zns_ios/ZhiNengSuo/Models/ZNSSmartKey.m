//
//  ZNSSmartKey.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/11/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "ZNSSmartKey.h"

@implementation ZNSSmartKey
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{
             @"skid":@"id",
             @"btAddr":@"btaddr",
             };
}

- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    NSString *btUUID = dic[@"btaddr"];
    if (![btUUID isKindOfClass:[NSString class]]) return NO;
    _btAddr = [[btUUID stringByReplacingOccurrencesOfString:@"-" withString:@""]lowercaseString];
    return YES;
}

+ (void)getAllSmartKeySuccess:(requestSuccessBlock)success error:(requestErrorBlock)error{
    [APIClient POST:[NSString stringWithFormat:@"user/%@/smartkey/",[ZNSUser currentUser].username] withParameters:@{} successWithBlcok:^(id response) {
        NSArray *keys = [NSArray yy_modelArrayWithClass:[ZNSSmartKey class] json:[response valueForKey:@"items"]];

        if (success) {
            success(keys);
        }
        [ZNSCache cacheSmartKeys:keys withUserName:[ZNSUser currentUser].username];

    } errorWithBlock:error];
}

- (NSDictionary *)generteSmartKeyParametersWith:(BOOL)isAdd{
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{@"name":self.name,
                          @"type":self.type,
                          @"section":self.section,
                          @"btaddr":self.btAddr,
                          @"user":self.user?:@""
                          }];
    if (!isAdd) {
        [dic addEntriesFromDictionary:@{@"sid":self.sid}];
    }
    return dic;
}
@end
