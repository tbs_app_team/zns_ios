//
//  ZNSTempTask.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/7/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZNSObject.h"
@interface ZNSTempTask : NSObject

@property NSString *tid;//	自增量，主键
@property NSString *name;//	填入事项
@property NSString *apply_by;
@property NSString *apply_at;
@property NSString *begin_at;
@property NSString *end_at;
@property NSString *verify_by;
@property NSString *verify_at;
@property NSString *status;
@property NSArray  *objects;
@property NSString *apply_id;
@property NSString *apply_username;
@property NSString *limited;


@property NSArray  *users;
//users =             (
//{
//    id = 2;
//    username = 18665089527;
//}
//);

@property NSString *verify_username;

+ (void)getCurrentUserTempTasksWithPage:(NSNumber *)page success:(requestSuccessBlock)success error:(requestErrorBlock)error;

+ (void)getVerifyTempTasksWithPage:(NSNumber *)page success:(requestSuccessBlock)success error:(requestErrorBlock)error;

- (void)applyTempTaskWithObjects:(NSArray *)arr success:(requestSuccessBlock)success error:(requestErrorBlock)error;

- (void)getObjectsSuccess:(requestSuccessBlock)success error:(requestErrorBlock)error;
@end
