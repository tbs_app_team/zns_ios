//
//  ZNSWorkPatrolTimelinessDetail.m
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 17/1/11.
//  Copyright © 2017年 czwen. All rights reserved.
//

#import "ZNSWorkPatrolTimelinessDetail.h"

@implementation ZNSWorkPatrolTimelinessDetail

+ (NSDictionary *)modelCustomPropertyMapper{
    return @{
             @"pdid":@"id",
             };
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             @"objects":[ZNSObject class],
             };
}

@end
