//
//  ZNSWorkPatrol.h
//  ZhiNengSuo
//
//  Created by ekey on 16/9/20.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZNSWorkPatrol : NSObject

@property NSString *skid;//任务id

@property NSString *sid;//执行单位
@property NSString *sname;//执行单位

@property NSString *content;//巡检内容
@property NSString *demand;//巡检要求
@property NSString *cycle;//巡检周期
@property NSString *objectCount;//巡检基站数
@property NSString *memo;//备注

@property NSString  *oname; //基站

@property NSString *create_by;//派工人
@property NSString *create_username;//派工人手机号
@property NSString *createtime;//派工时间

@property NSArray  *objects;

@property NSString *begin_at;//
@property NSString *end_at;//
@property NSString *create_at;//
@property NSString *patrol_content;//
@property NSString *patrol_cycle;//
@property NSString *patrol_demand;//
@property NSString *patrol_item;//
@property NSString *patrol_object_count;//

@end
