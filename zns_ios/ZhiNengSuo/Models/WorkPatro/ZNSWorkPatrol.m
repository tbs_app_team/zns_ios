//
//  ZNSWorkPatrol.m
//  ZhiNengSuo
//
//  Created by ekey on 16/9/20.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "ZNSWorkPatrol.h"

@implementation ZNSWorkPatrol

+ (NSDictionary *)modelCustomPropertyMapper{
    return @{
             @"skid":@"id",
             };
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             @"objects":[ZNSObject class],
             };
}

@end
