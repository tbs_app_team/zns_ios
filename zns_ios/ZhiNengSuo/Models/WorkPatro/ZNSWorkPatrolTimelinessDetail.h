//
//  ZNSWorkPatrolTimelinessDetail.h
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 17/1/11.
//  Copyright © 2017年 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZNSObject.h"

@interface ZNSWorkPatrolTimelinessDetail : NSObject

@property NSString *eid;//
@property NSString *pdid;//
@property ZNSObject *object;//
@property NSString *oid;//
@property NSString *patrol_at;//
@property NSString *patrol_by;// 

@end
