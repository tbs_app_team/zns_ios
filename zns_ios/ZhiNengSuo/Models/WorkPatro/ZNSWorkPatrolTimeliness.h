//
//  ZNSWorkPatrolTimeliness.h
//  ZhiNengSuo
//
//  Created by ekey on 16/9/22.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZNSWorkPatrolTimeliness : NSObject

@property NSString *pid;//巡检id
@property NSString *patrolcontent; //巡检内容
@property NSString *patrolcycle;//巡检周期
@property NSString *patrolcyclenum;//周期数
@property NSString *patrolrate;//巡检完成率
@property NSString *plancount;//应巡站数
@property NSString *realcount;//实巡站数
@property NSString *uncount;//落巡站数
@property NSString *sname;//代维公司

@property NSString *oname;//基站
@property NSString *patrolyear;//年份
@property NSString *patroltime;//创建时间
@property NSString *patrolby;//创建人

@property NSString *create_at;//
@property NSString *create_by;//
@property NSString *patrol_content;//
@property NSString *patrol_cycle;//
@property NSString *patrol_exec_count;//
@property NSString *patrol_object_count;//
@property NSString *patrol_rate;//
@property NSString *time;//
@property NSString *unfinished;//

@end
