//
//  ZNSNotice.m
//  ZhiNengSuo
//
//  Created by Chuenlu Kuo on 16/12/28.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "ZNSNotice.h"

#import "ZNSTimeTools.h"

@implementation ZNSNotice

- (void)setupDataWith:(NSDictionary *)json{

    if (json && [json isKindOfClass:[NSDictionary class]]) {
        self.nid = [json[@"id"] integerValue];
        self.title = json[@"title"];
        self.content = json[@"content"];
        self.createBy = json[@"create_by"];
        self.createAt = [[ZNSTimeTools shared] timeFomrmatterWith:json[@"create_at"]];
    }
}

@end
