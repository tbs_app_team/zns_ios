//
//  ZNSDate.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/7/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZNSDate : NSObject
@property NSNumber *date;
@property NSNumber *day;
@property NSNumber *hours;
@property NSNumber *minutes;
@property NSNumber *month;
@property NSNumber *nanos;
@property NSNumber *seconds;
@property NSNumber *time;
@property NSNumber *timezoneOffset;
@property NSNumber *year;
+ (NSString *)getCurrentTime;
@end
