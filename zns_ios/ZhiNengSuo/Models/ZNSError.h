//
//  ZNSError.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/24/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZNSError : NSObject
@property NSInteger errorCode;
@property NSString  *errorMessage;
+ (ZNSError*)networkError;
+ (ZNSError*)serverError;
@end
