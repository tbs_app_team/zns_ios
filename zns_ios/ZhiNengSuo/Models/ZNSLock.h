//
//  ZNSLock.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/3/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZNSObject.h"
@interface ZNSLock : NSObject
@property NSString *name;
@property NSString *btaddr;
@property NSString *btName;
@property NSString *rfid;
@property NSString *oid;
@property NSString *lid;//id
@property NSString *type;
@property NSString *doortype;
@property NSString *doorcode;
@property BOOL lockState;//yes kai /no guan
@property BOOL doorState;

@property NSString  *stationId;
@property NSString  *logic;

@property NSString *gate_status;//远程解锁的门状态，0：关闭，1:开启，v3.1版本新增加,2018.12.5新增
@property NSString *lock_status;//远程解锁的开关状态，0：关闭，1:开启，v3.1版本新增加,2018.08.23新增
@property NSString *lock_status_desc;//远程解锁的开关状态文字描述，本地逻辑处理。v3.1版本新增加,2018.08.23新增


/**
 *  显示用，和接口无关
 */
@property (nonatomic,strong) ZNSObject *object;
+ (void)getLocksetWithPage:(NSNumber *)page success:(requestSuccessBlock)success error:(requestErrorBlock)error;

- (void)createUnlockHistorySuccess:(BOOL)susccess verifyAt:(NSString *)verifyAt verifyBy:(NSString *)verifyBy action:(NSString *)action;

@end
