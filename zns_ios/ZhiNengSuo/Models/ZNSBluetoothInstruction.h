//
//  ZNSBluetoothInstruction.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/30/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#define LOCK_INSTRUCTION_CODE 0x21
#define SMART_KEY_INSTRUCTION_CODE 0x20
/**
 *  解锁
 */
#define FUNCTION_UNLOCK_CODE 0x00
/**
 *  锁状态汇报 发给手机
 */
#define FUNCTION_LOCK_STATUS_REPORTER_CODE 0x01
/**
 *  锁状态汇报 发给智能锁
 */
#define FUNCTION_LOCK_STATUS_CHECK_CODE 0x02
/**
 *  门状态汇报 发给手机
 */
#define FUNCTION_DOOR_STATUS_REPORTER_CODE 0x03
/**
 *  门状态汇报 发给智能锁
 */
#define FUNCTION_DOOR_STATUS_CHECK_CODE 0x04

#import <Foundation/Foundation.h>

@interface ZNSBluetoothInstruction : NSObject
@property (nonatomic,assign) BOOL on;
@property (nonatomic,assign) NSInteger battery;
//- (instancetype)initWithInstruction:(Byte)instruction function:(Byte)function body:(Byte)body;
- (instancetype)initWithInstruction:(Byte)instruction function:(Byte)function body:(Byte)body rfid:(NSData*)rfid;
- (instancetype)initWithMoreData:(Byte)instruction function:(Byte)function body:(NSData*)body rfid:(NSData*)rfid;
- (instancetype)initWithByteData:(NSData*)data;
- (NSData*)data;
- (Byte)control;
- (Byte)function;
- (Byte)body;
- (Byte) cmd;
- (Byte) ack;// 1 or 2
- (Byte) isRight;//0x80 or..
- (NSString *)rfid;
- (NSData *)rfidData;
- (NSData *)bodyData;
+ (instancetype)unlockInstruction;

+ (instancetype)batteryInstruction;

+ (instancetype)unlockInstructionWithRFID:(NSData *)rfid;

+ (instancetype)checkDoorStateInstruction;
+ (instancetype)checkLockStateInstruction;


@end
