//
//  ZNSTempTask.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/7/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "ZNSTempTask.h"

@interface ZNSTempTask (){
    
}

@end
@implementation ZNSTempTask
@synthesize tid;

+ (NSDictionary *)modelCustomPropertyMapper{
    return @{
             @"tid":@"id",
             @"objects":@"object",
             };
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             @"objects":[ZNSObject class],
             @"users":[ZNSUser class],
             };
}

+ (void)getCurrentUserTempTasksWithPage:(NSNumber *)page success:(requestSuccessBlock)success error:(requestErrorBlock)error {
    //exceedflag 0表示过期，1表示未过期
    [APIClient POST:API_TEMP_TASK_WITH_USERNAME([ZNSUser currentUser].username) withParameters:@{@"status":@[@"待下载",@"已下载",@"已执行"],@"page":page,@"exceedflag":@1} successWithBlcok:success errorWithBlock:error];
}

+ (void)getVerifyTempTasksWithPage:(NSNumber *)page success:(requestSuccessBlock)success error:(requestErrorBlock)error {
    [APIClient POST:API_TEMPTASK_VERIFY_LIST withParameters:@{@"status":@[@"待审批"],@"page":page} successWithBlcok:success errorWithBlock:error];
}

- (void)applyTempTaskWithObjects:(NSArray *)arr success:(requestSuccessBlock)success error:(requestErrorBlock)error{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic = [self yy_modelToJSONObject];
    [dic setValue:arr forKey:@"object"];
    [APIClient POST:API_APPLYTEMPTASK withParameters:dic successWithBlcok:success errorWithBlock:error];
}

- (void)getObjectsSuccess:(requestSuccessBlock)success error:(requestErrorBlock)error{
//    @weakify(self);
    [APIClient POST:API_TEMPTASK_DETAIT_WITH_TID(self.tid) withParameters:@{} successWithBlcok:^(id response) {
        if (success) {
            success([ZNSTempTask yy_modelWithDictionary:response]);
        }
    } errorWithBlock:error];
}

+ (void) deleteSelf:(NSString*)tid{
    
    uint16_t guid = tid.integerValue;
    NSMutableData * tem = [NSMutableData dataWithBytes:&guid length:sizeof(guid)];
    if ([tem length]<16) {
        Byte add[16]={0};
        [tem appendBytes:add length:16-[tem length]];
    }
    
    //删除任务
    ZNSBluetoothInstruction *da =[[ZNSBluetoothInstruction alloc] initWithInstruction:0x06 function:0x00 body:nil rfid:tem];
    //    NSLog(@"%@",da.data);
    for (int j=0; j<=([da.data length]/20); j++) {
        
        [[[GGBluetooth sharedManager]connectedPheripheral] writeValue:[da.data subdataWithRange:NSMakeRange(0+j*20, j==[da.data length]/20?([da.data length]-20*j):20)] forCharacteristic:[[GGBluetooth sharedManager] writeableCharacteristic] type:[[GGBluetooth sharedManager] writeableCharacteristic].properties==(CBCharacteristicPropertyRead+CBCharacteristicPropertyWriteWithoutResponse+CBCharacteristicPropertyNotify)?CBCharacteristicWriteWithoutResponse:CBCharacteristicWriteWithResponse];
        
    }
}
@end
