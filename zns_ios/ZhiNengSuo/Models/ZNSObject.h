//
//  ZNSObject.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/7/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZNSController.h"

@interface ZNSObject : NSObject

@property  NSString *area;
@property  NSString *carrier;
@property  NSString *oid;
@property  NSString *name;
@property  NSString *section;
@property  NSString *type;
@property  NSArray *lockset;
@property  NSString *lat;
@property  NSString *lng;
// 开锁范围id
@property  NSNumber *zid;
// 开锁范围名
@property  NSString *zname;

@property  NSString *address;
@property  NSString *code;
@property  NSString *districts;
@property  NSString *fsustatus;
@property  NSInteger lockCount;
@property  NSString *operationid;
@property  NSString *resourceid;
@property  NSString *overscene;
@property  NSInteger sid;
@property  NSString *source;
@property  NSString *svrlevel;
@property  NSString *agent_section;  //废弃，使用company_name替代
@property  NSString *agent_sid;      //废弃，使用companyid替代
@property  NSString *sitecode;
@property  NSString *company_name;  //代维单位，v3.1版本新增字段
@property  NSString *companyid;     //代维单位id，v3.1版本新增字段

@property NSArray *controllerlog;      //锁状态和门信息

@property (nonatomic,strong) NSArray *controller;


+ (void)getAllDevicesWithPage:(NSNumber *)page search:(NSString *)search isAgt:(BOOL)isAgt successWithBlock:(requestSuccessBlock)blokc errorWithBlock:(requestErrorBlock)error;

- (NSDictionary *)generteNewObjectParameters;
- (NSDictionary *)modifyObjectParameters;
@end
