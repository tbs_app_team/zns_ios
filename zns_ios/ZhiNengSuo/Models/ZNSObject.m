//
//  ZNSObject.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/7/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "ZNSObject.h"
#import <objc/runtime.h>

@implementation ZNSObject
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{
             @"oid":@"id",
//             @"locks":@"lockset",
             };
}
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             @"lockset":[ZNSLock class],
             @"controller":[ZNSController class]};
}
+ (void)getAllDevicesWithPage:(NSNumber *)page search:(NSString *)search isAgt:(BOOL)isAgt successWithBlock:(requestSuccessBlock)blokc errorWithBlock:(requestErrorBlock)error{
    
    NSString *api = isAgt?[NSString stringWithFormat:@"section/%@/agent/object/downward",@(0).stringValue]:API_SECTION_OBJECT_WITH_SID(@(0).stringValue);
    
    [APIClient POST:api withParameters:@{@"page":page,@"search":search} successWithBlcok:blokc errorWithBlock:error];
}

- (NSDictionary *)generteNewObjectParameters{
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{@"name":self.name,
                                                                               @"companyid": [self.companyid length]?self.companyid:@"",//代维单位
                                                                               @"area": self.area,
                                                                               @"zid":@[self.zid],
                                                                               @"type":self.type,
                                                                               @"controller":[self getControllers],
                                                                               }];
    if ([self.lat length] && [self.lng length]) {
        [dic addEntriesFromDictionary:@{@"lat":self.lat,@"lng":self.lng}];
    }
   
    __block NSMutableArray *lockset = [NSMutableArray array];
    [self.lockset enumerateObjectsUsingBlock:^(ZNSLock *lock, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [lockset addObject:@{
                             @"name": lock.name,
                             @"type": lock.type,
                             @"doortype": lock.doortype,
                             @"doorcode": lock.doorcode?:@"",
                             @"rfid": lock.rfid == nil? @"":lock.rfid,
                             @"btaddr": lock.btaddr == nil ? @"":lock.btaddr,
                             }];
    }];
    [dic setValue:lockset forKey:@"lockset"];
    return dic;
}
- (NSDictionary *)modifyObjectParameters{
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{@"name":self.name,
                                                                               @"type":self.type,
                                                                               @"companyid":[self.companyid length]?self.companyid:@"",
                                                                               @"zid":@[self.zid],
                                                                               @"controller":[self getControllers],
                                                                               @"lat":self.lat==nil?@"":self.lat,
                                                                               @"lng":self.lng==nil?@"":self.lng}];
    __block NSMutableArray *lockset = [NSMutableArray array];
    [self.lockset enumerateObjectsUsingBlock:^(ZNSLock *lock, NSUInteger idx, BOOL * _Nonnull stop) {
        
        NSMutableDictionary *lockPar = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                       @"name": lock.name,
                                                                                       @"type": lock.type,
                                                                                       @"doortype": lock.doortype,
                                                                                       @"doorcode": lock.doorcode?:@"",
                                                                                       @"rfid": lock.rfid == nil? @"":lock.rfid,
                                                                                       @"btaddr": lock.btaddr == nil ? @"":lock.btaddr,
                                                                                       }];
        
        if ([lock.lid integerValue] != 0) {
            [lockPar setObject:lock.lid forKey:@"id"];
        }
        [lockset addObject:lockPar];
    }];
    [dic setValue:lockset forKey:@"lockset"];
    return dic;
}

- (NSArray *)getControllers{
    if (!self.controller.count) {
        return @[];
    }
    ZNSController *zc = self.controller.firstObject;
    if (!zc) {
        return @[];
    }
    NSDictionary *con = @{@"code":[NSNumber numberWithInteger:zc.code],
                                 @"type":zc.type?:@"",
                                 @"work_mode":zc.work_mode?:@"",
                                 @"remark":zc.remark?:@""};
    return @[con];
}

- (id)copyWithZone:(NSZone *)zone {
    
    id obj = [[[self class] allocWithZone:zone] init];
    Class class = [self class];
    while (class != [NSObject class]) {
        unsigned int count;
        Ivar *ivar = class_copyIvarList(class, &count);
        for (int i = 0; i < count; i++) {
            Ivar iv = ivar[i];
            const char *name = ivar_getName(iv);
            NSString *strName = [NSString stringWithUTF8String:name];
            //利用KVC取值
            id value = [[self valueForKey:strName] copy];//如果还套了模型也要copy呢
            [obj setValue:value forKey:strName];
        }
        free(ivar);
        
        class = class_getSuperclass(class);//记住还要遍历父类的属性呢
    }
    return obj;
}

@end
