//
//  ZNSExecuteUnlockHelper.m
//  ZhiNengSuo
//
//  Created by Chuenlu Kuo on 17/2/22.
//  Copyright © 2017年 czwen. All rights reserved.
//

#import "ZNSExecuteUnlockHelper.h"
#import <UIKit/UIKit.h>
#import "OnlineAuthenticationOpenBtLockViewController.h"
#import "OnlineAuthenticationSmartKeyViewController.h"
#import "ZNSTools.h"
@implementation ZNSExecuteUnlockHelper

+ (UIViewController *)unlock{
    UIViewController *vc = [OnlineAuthenticationOpenBtLockViewController create];
    vc.title = @"在线鉴权开锁";
    NSArray *unlocks = [[NSUserDefaults standardUserDefaults] objectForKey:FIRST_UNLOCK];
    if (unlocks && [unlocks isKindOfClass:[NSArray class]] && unlocks.count) {
        NSInteger btLockIndex = [unlocks indexOfObject:@"蓝牙"];
        NSInteger keyIndex = [unlocks indexOfObject:@"智能锁"];
        if (btLockIndex > keyIndex) {
            vc = [OnlineAuthenticationSmartKeyViewController create];
            vc.title = @"智能钥匙开锁";
        }
    }
    return vc;
}

@end
