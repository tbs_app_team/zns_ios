//
//  ZNSFixedTask.h
//  ZhiNengSuo
//
//  Created by ekey on 16/6/22.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZNSFixedTask : NSObject
@property NSString *tid;//	自增量，主键
@property NSString *name;
@property NSString *apply_by;
@property NSString *apply_at;
@property NSString *apply_username;
@property NSString *begin_at;
@property NSString *end_at;

@property NSString *status;
@property NSArray  *objects;
@property NSArray  *users;
//users =             (
//{
//    id = 2;
//    username = 18665089527;
//}
//);
@property NSString *verify_by;
@property NSString *verify_at;
@property NSString *verify_username;
@end
