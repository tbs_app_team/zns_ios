//
//  ZNSBorrowSmartkey.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 16/3/20.
//  Copyright © 2016 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZNSBorrowSmartkey : NSObject
@property NSString *borrow_by;
@property NSString *borrow_username;
@property NSString *btaddr;
@property NSString *create_at;
@property NSString *create_by;
@property NSString *create_username;
//@property NSString *id;
@property NSString *return_at;
@property NSString *sid;
@property NSString *sname;
@property NSString *ssection;
@property NSString *stype;
@end
