//
//  ZNSRole.m
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/12/5.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import "ZNSRole.h"

@implementation ZNSRole

+ (NSDictionary *)modelCustomPropertyMapper{
    return @{
             @"rid":@"id",
             };
}

- (void)getAllZoneSuccess:(requestSuccessBlock)success error:(requestErrorBlock)error{
    [APIClient POST:API_ROLE_ZONE_WITH_RID(self.rid) withParameters:@{} successWithBlcok:^(id response) {
        
    } errorWithBlock:error];
}

@end
