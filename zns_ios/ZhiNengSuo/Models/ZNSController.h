//
//  ZNSController.h
//  ZhiNengSuo
//
//  Created by Chanlu.Kuo on 2018/8/7.
//  Copyright © 2018年 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZNSController : NSObject

@property (nonatomic,assign) NSInteger code;
@property (nonatomic,assign) NSInteger cid;
@property (nonatomic,assign) NSInteger oid;
@property (nonatomic,strong) NSString *remark;
@property (nonatomic,strong) NSString *type;
@property (nonatomic,strong) NSString *work_mode;

@end
