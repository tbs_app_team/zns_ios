//
//  ZNSFixedTask.m
//  ZhiNengSuo
//
//  Created by ekey on 16/6/22.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "ZNSFixedTask.h"

@implementation ZNSFixedTask

+ (NSDictionary *)modelCustomPropertyMapper{
    return @{
             @"tid":@"id",
             @"objects":@"object",
             };
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             @"objects":[ZNSObject class],
             @"users":[ZNSUser class],
             };
}

@end
