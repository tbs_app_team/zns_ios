//
//  ZNSUser.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/24/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "ZNSUser.h"
#import "AppDelegate.h"
@implementation ZNSUser
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{
             @"token":@"k",
             @"verifyAt":@"verify_at",
             @"idid":@"id",
             };
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             @"role":[ZNSRole class],
             };
}
- (BOOL)modelCustomTransformToDictionary:(NSMutableDictionary *)dic {
    
    return YES;
}

- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    //    NSString *btUUID = dic[@"updateDateTime"];
    //    if (![btUUID isKindOfClass:[NSString class]]) return NO;
    //    updateDateTime = [[btUUID stringByReplacingOccurrencesOfString:@"-" withString:@""]lowercaseString];
    return YES;
}

+ (instancetype)currentUser{
    AppDelegate *delegate =  (AppDelegate *)[UIApplication sharedApplication].delegate;
    return delegate.currentUser;
}

- (void)login{
    AppDelegate *delegate =  (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegate.currentUser = self;
}

- (void)logout{
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    delegate.currentUser = nil;
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_ACCOUNT_CHANGE object:nil userInfo:@{@"login":@NO}];
}

+ (void)cacheUsername:(NSString *)username andPassword:(NSString *)password{
    [[self offlineAccountPassword] setObject:password forKey:[NSString getSha512String:username]];
}

+ (void)cacheUserdata:(id)data forUsername:(NSString *)username{
    [[self offlineAccountCache] setObject:data forKey:username];
}

+ (void)loginOfflineWithUsername:(NSString *)username passwor:(NSString *)password success:(void (^)(BOOL))success{
    NSString *cachePassword = (NSString *)[[self offlineAccountPassword] objectForKey:[NSString getSha512String:username]];
    if ([cachePassword isEqualToString:password]) {
        id data = [[self offlineAccountCache]objectForKey:username];
        if (data) {
            ZNSUser *user = [ZNSUser yy_modelWithJSON:data];
            user.isOfflineLogin = YES;
            [user login];
            if (success) {
                success(YES);
            }
        }else{
            if (success) {
                success(NO);
            }
        }
    }else{
        if (success) {
            success(NO);
        }
    }
}

+ (YYDiskCache*)offlineAccountCache{
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:@"offlineAccount"];
    YYDiskCache *cache = [[YYDiskCache alloc]initWithPath:filePath];
    return cache;
}

+ (YYDiskCache*)offlineAccountPassword{
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:@"offlineAP"];
    YYDiskCache *cache = [[YYDiskCache alloc]initWithPath:filePath];
    return cache;
}
@end
