//
//  ZNSRole.h
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/12/5.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZNSRole : NSObject

@property NSString * rid;
@property NSString * name;
@property NSString * status;
@property NSString * intro;
@property NSArray  * permission;


- (void)getAllZoneSuccess:(requestSuccessBlock)success error:(requestErrorBlock)error;
@end
