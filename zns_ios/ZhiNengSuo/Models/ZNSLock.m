//
//  ZNSLock.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/3/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "ZNSLock.h"

@implementation ZNSLock
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{
             @"btName":@"btname",
             @"lid":@"id",
             };
}

+ (void)getLocksetWithPage:(NSNumber *)page success:(requestSuccessBlock)success error:(requestErrorBlock)error{
    [APIClient POST:API_LOCKSET withParameters:@{@"page":page} successWithBlcok:^(id response) {
        if (success) {
            success(response);
        }
    } errorWithBlock:error];
}
- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    NSString *btUUID = dic[@"btaddr"];
    if (![btUUID isKindOfClass:[NSString class]]) return NO;
    _btaddr = [btUUID stringByReplacingOccurrencesOfString:@":" withString:@""];
    _btaddr = [_btaddr lowercaseString];
    if (_btaddr.length<1) {
        _btaddr = @"";
    }
    _rfid = [[dic valueForKey:@"rfid"]lowercaseString];
    return YES;
}


- (void)createUnlockHistorySuccess:(BOOL)susccess verifyAt:(NSString *)verifyAt verifyBy:(NSString *)verifyBy action:(NSString *)action{
    
    NSDictionary *dic = @{
                          @"lid":self.lid,
                          @"result":susccess?@6:@255,
                          @"action":action,
                          };
    [APIClient POST:@"history/new" withParameters:dic
   successWithBlcok:^(id response) {
       
       
   } errorWithBlock:^(ZNSError *error) {
       
       
   }];
}

@end
