//
//  ZNSOffline.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/7/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZNSDate.h"
#import "ZNSLock.h"
@interface ZNSOffline : NSObject
@property NSString *action_at;
@property NSString *apply_by;
@property NSString *apply_username;
@property NSString *apply_at;
@property NSString *duration;
@property NSString *oid;
@property NSString *status;
@property NSString *verify_at;
@property NSString *zid;
@property NSString *verify_by;
//@property NSTimeInterval beginAt;
@property NSString *beginAt;
@property NSArray *locks;
@property NSString *apply_id;
//actionAt = 00000000000000;
//applyAt =             {
//    date = 7;
//    day = 1;
//    hours = 22;
//    minutes = 22;
//    month = 11;
//    nanos = 0;
//    seconds = 41;
//    time = 1449498161000;
//    timezoneOffset = "-480";
//    year = 115;
//};
//applyBy = 001;
//duration = 2;
//id = 2;
//status = "\U5f85\U5ba1\U6279";
//verifyAt = 00000000000000;
@end
