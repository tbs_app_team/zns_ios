//
//  ZNSUser.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/24/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZNSRole.h"

@interface ZNSUser : NSObject

@property NSString *idid;
@property NSString *name;
@property NSString *token;
@property NSString *memo;
@property NSString *sid;
@property NSString *section;
@property NSArray *permission;
@property NSMutableArray *role;
@property NSString *username;
@property NSString *verifyAt;
@property NSString *status;
@property NSNumber *s;
@property NSString *lastlogin_at;
@property NSString *reg_at;
@property NSString *verify_by;

//钥匙打包
@property NSString *password;
@property NSString *rfid;


@property BOOL isOfflineLogin;
//@property NSNumber *uid;
//@property NSString *intro;
//@property NSString *status;
//@property NSString *name;

+ (instancetype)currentUser;

- (void)login;
- (void)logout;

+ (void)cacheUsername:(NSString *)username andPassword:(NSString *)password;
+ (void)cacheUserdata:(id)data forUsername:(NSString *)username;
+ (void)loginOfflineWithUsername:(NSString *)username passwor:(NSString *)password success:(void(^)(BOOL success))success;
@end