//
//  ZNSContact.h
//  ZhiNengSuo
//
//  Created by Chuenlu Kuo on 17/1/24.
//  Copyright © 2017年 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZNSContact : NSObject

@property (nonatomic,assign) NSInteger cid;
@property (nonatomic,strong) NSString *memo;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *phone;
@property (nonatomic,strong) NSString *section;

@end
