//
//  ZNSDict.h
//  ZhiNengSuo
//
//  Created by ekey on 16/9/20.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZNSDict : NSObject

@property (nonatomic, strong) NSString* type;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* value;

@end
