//
//  ZNSDate.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/7/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "ZNSDate.h"

@implementation ZNSDate

+ (NSString *)getCurrentTime{
    
    NSDateFormatter *formatter =[[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:@"yyyyMMddHHmmss"];
    return [formatter stringFromDate:[NSDate date]];
}
@end
