//
//  AppConfig.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/28/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#ifndef AppConfig_h
#define AppConfig_h

#define NOTIFICATION_ACCOUNT_CHANGE @"NOTIFICATION_ACCOUNT_CHANGE"

//自动注销时长
#define AUTO_LOGOUT_TIME @"AUTO_LOGOUT_TIME"
//优先解锁方式
#define FIRST_UNLOCK @"FIRST_UNLOCK"

//锁类型，两种，标准锁+四川锁
#define LOCK_TYPE @"LOCK_TYPE"

#endif /* AppConfig_h */
