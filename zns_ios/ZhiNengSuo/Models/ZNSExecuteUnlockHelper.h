//
//  ZNSExecuteUnlockHelper.h
//  ZhiNengSuo
//
//  Created by Chuenlu Kuo on 17/2/22.
//  Copyright © 2017年 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZNSExecuteUnlockHelper : NSObject

+ (UIViewController *)unlock;

@end
