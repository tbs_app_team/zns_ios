//
//  ZNSOfflineKeyOperation.m
//  ZhiNengSuo
//
//  Created by ekey on 16/4/8.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "ZNSOfflineKeyOperation.h"

@implementation ZNSOfflineKeyOperation

// 当 JSON 转为 Model 完成后，该方法会被调用。
// 你可以在这里对数据进行校验，如果校验不通过，可以返回 NO，则该 Model 会被忽略。
// 你也可以在这里做一些自动转换不能完成的工作。

- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
//    NSString *btUUID = dic[@"action"];
//    if (![btUUID isKindOfClass:[NSString class]]) return NO;
//    _action = btUUID ;
    return YES;
}

// 当 Model 转为 JSON 完成后，该方法会被调用。
// 你可以在这里对数据进行校验，如果校验不通过，可以返回 NO，则该 Model 会被忽略。
// 你也可以在这里做一些自动转换不能完成的工作。
- (BOOL)modelCustomTransformToDictionary:(NSMutableDictionary *)dic {

    return YES;
}



@end
