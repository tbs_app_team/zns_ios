//
//  ZNSSmartKey.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/11/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZNSSmartKey : NSObject
@property NSString *skid;
@property NSString *name;
@property NSString *btAddr;
@property NSString *type;//E401=大钥匙（有显示屏的）,E402=小钥匙
@property NSString *sid;
@property NSString *section;
@property NSString *user;

@property NSString *phone;

+ (void)getAllSmartKeySuccess:(requestSuccessBlock)success error:(requestErrorBlock)error;

- (NSDictionary *)generteSmartKeyParametersWith:(BOOL)isAdd;
@end
