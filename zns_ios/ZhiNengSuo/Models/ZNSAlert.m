//
//  ZNSAlert.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 16/1/24.
//  Copyright © 2016 czwen. All rights reserved.
//

#import "ZNSAlert.h"

@implementation ZNSAlert
+ (NSDictionary *)modelCustomPropertyMapper{
    return @{
             @"aid":@"id",
             };
}
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             @"create_at":[ZNSDate class],
             };
}
@end
