//
//  NSString+Additions.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/27/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)
+ (NSString*) getSha512String:(NSString*)srcString;
+ (NSString *)hexadecimalString:(NSData*)data;
+ (NSString *)formatTimeString:(NSString *)srcTime;
+ (NSString *)formatTime:(NSDate*)date;
+ (NSString *)formatTime:(NSDate*)date withFormat:(NSString*)format;
+ (NSString *)isEmpty:(NSString *)str;
+ (NSString *)hexStringFromString:(NSString *)string;

+ (NSString *)dateHexStringFromDigitalString:(NSString *)string;
+ (NSString*)timeStringFormHexString:(NSString *)timeString;

+ (NSString *)formatTimeWithFormat:(NSDate*)date format:(NSString*)format;
+ (NSString *)formatTimeString:(NSString *)srcTime withFormat:(NSString*)format;
@end
