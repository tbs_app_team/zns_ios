//
//  UIViewController+Additions.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/25/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Additions)
+ (id)create;

+ (id)createFromStoryboardName:(NSString *)name withIdentifier:(NSString *)identifier;
@end
