//
//  UINavigationItem+CustomBackButton.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/14/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "UINavigationItem+CustomBackButton.h"

@implementation UINavigationItem (CustomBackButton)
-(UIBarButtonItem *)backBarButtonItem{
    return [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:nil action:NULL];
}
@end
