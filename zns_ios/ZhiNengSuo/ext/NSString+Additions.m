//
//  NSString+Additions.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/27/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "NSString+Additions.h"
#import <CommonCrypto/CommonDigest.h>
@implementation NSString (Additions)
+ (NSString*) getSha512String:(NSString*)srcString {
    const char *cstr = [srcString cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:srcString.length];
    uint8_t digest[CC_SHA512_DIGEST_LENGTH];
    
    CC_SHA512(data.bytes, data.length, digest);
    
    NSMutableString* result = [NSMutableString stringWithCapacity:CC_SHA512_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_SHA512_DIGEST_LENGTH; i++)
        [result appendFormat:@"%02x", digest[i]];
    return result;
}

+ (NSString *)hexadecimalString:(NSData*)data
{
    /* Returns hexadecimal string of NSData. Empty string if data is empty.   */
    const unsigned char *dataBuffer = (const unsigned char *)[data bytes];
    if (!dataBuffer){
        return [NSString string];
    }
    NSUInteger          dataLength  = [data length];
    NSMutableString     *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
    for (int i = 0; i < dataLength; ++i){
        [hexString appendFormat:@"%02x", (unsigned int)dataBuffer[i]];
    }
    return [NSString stringWithString:hexString];
}

+ (NSString *)formatTimeString:(NSString *)srcTime {
    NSDateFormatter * df = [[NSDateFormatter alloc]init];
    df.dateFormat = @"yyyyMMddHHmmss";
    NSDate * apply_atDate = [df dateFromString:srcTime];
    df.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    return [df stringFromDate:apply_atDate];
}

+ (NSString *)formatTime:(NSDate*)date{
    NSDateFormatter * format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [format stringFromDate:date];
}

+ (NSString *)formatTime:(NSDate*)date withFormat:(NSString*)format{
    NSDateFormatter * form = [[NSDateFormatter alloc] init];
    [form setDateFormat:format];
    return [form stringFromDate:date];
}

+ (NSString*)isHexString:(NSString*)str{
    
    int i;
    for (i=0; i<[str length]; i++) {
        if (([str characterAtIndex:i]>='0' && [str characterAtIndex:i]<='9')||
            ([str characterAtIndex:i]>='a' && [str characterAtIndex:i]<='f') ||
            ([str characterAtIndex:i]>='A' && [str characterAtIndex:i]<='F' ) ){
//            return YES;
        }else{
            return @"no";
        }
      
    }
    return @"yes";
    
}
+ (NSString *)isEmpty:(NSString *)str {
    
    if (!str) {
        return @"yes";
    } else {
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        
        //Returns a new string made by removing from both ends of the receiver characters contained in a given character set.
        NSString *trimedString = [str stringByTrimmingCharactersInSet:set];
        
        if ([trimedString length] == 0) {
            return @"yes";
        } else {
            return @"no";
        }
    }
}

//"20160630060708"  "1410061e060708"
+ (NSString *)hexStringFromString:(NSString *)string{
    NSData *myD = [string dataUsingEncoding:NSUTF8StringEncoding];
    const unsigned char *dataBuffer = (const unsigned char *)[myD bytes];
   if (!dataBuffer){
        return [NSString string];
    }
    NSUInteger          dataLength  = [myD length];
    NSMutableString     *hexString  = [NSMutableString stringWithCapacity:(dataLength/2)];
    for (int i = 0; i < dataLength; ){
        [hexString appendFormat:@"%02x", (dataBuffer[i]-0x30)*10 +(dataBuffer[i+1]-0x30)];
        i+=2;
    }
   return [NSString stringWithString:hexString];
}


+ (NSString *)dateHexStringFromDigitalString:(NSString *)string{
    NSData *myD = [string dataUsingEncoding:NSUTF8StringEncoding];
    const unsigned char *dataBuffer = (const unsigned char *)[myD bytes];
    if (!dataBuffer){
        return [NSString string];
    }
    NSUInteger          dataLength  = [myD length];
    NSMutableString     *hexString  = [NSMutableString stringWithCapacity:(dataLength/2)];
    [hexString appendFormat:@"%04x",(dataBuffer[0]-0x30)*1000 + (dataBuffer[1]-0x30)*100 +(dataBuffer[2]-0x30)*10 + (dataBuffer[3]-0x30)];
    for (int i = 4; i < dataLength; ){
        [hexString appendFormat:@"%02x", (dataBuffer[i]-0x30)*10 +(dataBuffer[i+1]-0x30)];
        i+=2;
    }
    return [NSString stringWithString:hexString];
}

+ (NSString *)formatTimeString:(NSString *)srcTime withFormat:(NSString*)format {
    NSDateFormatter * df = [[NSDateFormatter alloc]init];
    df.dateFormat = format;
    NSDate * apply_atDate = [df dateFromString:srcTime];
    df.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    return [df stringFromDate:apply_atDate];
}
 

+ (NSString *)formatTimeWithFormat:(NSDate*)date format:(NSString*)format{
    NSDateFormatter * mat = [[NSDateFormatter alloc] init];
    [mat setDateFormat:format];
    return [mat stringFromDate:date];
}

+ (NSString*)timeStringFormHexString:(NSString *)timeString{
    NSMutableString *stringBuf = [NSMutableString string];
    NSString *str=[timeString substringWithRange:NSMakeRange(2, 2)];
    [stringBuf appendString:str];
    str=[timeString substringWithRange:NSMakeRange(0, 2)];
    [stringBuf appendString:str];
    long    year = strtoul([stringBuf UTF8String],0,16);
    long    mon  = strtoul([[timeString substringWithRange:NSMakeRange(4, 2)] UTF8String],0,16);
    long    day  = strtoul([[timeString substringWithRange:NSMakeRange(6, 2)] UTF8String],0,16);
    long    hour  = strtoul([[timeString substringWithRange:NSMakeRange(8, 2)] UTF8String],0,16);
    long    min  = strtoul([[timeString substringWithRange:NSMakeRange(10, 2)] UTF8String],0,16);
    long    sec  = strtoul([[timeString substringWithRange:NSMakeRange(12, 2)] UTF8String],0,16);
    
    return  [NSString stringWithFormat:@"%04d%02d%02d%02d%02d%02d",year,mon,day,hour,min,sec];
}
//+ (NSString*)UTF8_To_GB2312:(NSString*)utf8string
//{
//    NSStringEncoding encoding =CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
//    NSData* gb2312data = [utf8string dataUsingEncoding:encoding];
//    return [[NSString alloc] initWithData:gb2312data encoding:encoding];
//}
@end
