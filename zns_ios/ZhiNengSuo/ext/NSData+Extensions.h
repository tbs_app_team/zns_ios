//
//  NSData+Extensions.h
//  ;
//
//  Created by ChenZhiWen on 11/29/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Extensions)
- (unsigned short)crc16Modbus;
@end
