//
//  CBPeripheral+Additions.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/28/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <CoreBluetooth/CoreBluetooth.h>

@interface CBPeripheral (Additions)
@property (nonatomic, strong) NSString *macAddress;
@end
