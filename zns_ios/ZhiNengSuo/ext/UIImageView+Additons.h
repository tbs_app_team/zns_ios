//
//  UIView+Additons.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/25/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Additons)

/**
 *	@brief	浏览头像
 *
 *	@param 	oldImageView 	头像所在的imageView
 */
- (void)showImage;

@end
