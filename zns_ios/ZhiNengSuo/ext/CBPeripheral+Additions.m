//
//  CBPeripheral+Additions.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/28/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "CBPeripheral+Additions.h"
#import <objc/runtime.h>

static const void *MacAddressKey = &MacAddressKey;

@implementation CBPeripheral (Additions)
@dynamic macAddress;

- (NSString *)macAddress {
    return objc_getAssociatedObject(self, MacAddressKey);
}

- (void)setMacAddress:(NSString *)macAddress{
    objc_setAssociatedObject(self, MacAddressKey, macAddress, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
