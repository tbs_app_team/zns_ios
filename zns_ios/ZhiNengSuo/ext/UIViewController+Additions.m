//
//  UIViewController+Additions.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/25/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "UIViewController+Additions.h"

@implementation UIViewController (Additions)
+ (id)create{
    NSString *className = NSStringFromClass([self class]);
    id newObj = [[UIStoryboard storyboardWithName:className bundle:[NSBundle mainBundle]] instantiateInitialViewController];
    return newObj;
}

+ (id)createFromStoryboardName:(NSString *)name withIdentifier:(NSString *)identifier{
    if (name && identifier) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:name bundle:[NSBundle mainBundle]];
        
        return [storyboard instantiateViewControllerWithIdentifier:identifier];
        
    }
    return nil;
}

@end
