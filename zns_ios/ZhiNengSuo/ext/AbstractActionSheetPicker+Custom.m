//
//  AbstractActionSheetPicker+Custom.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/14/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "AbstractActionSheetPicker+Custom.h"

@implementation AbstractActionSheetPicker (Custom)
- (UIBarButtonItem *)createButtonWithType:(UIBarButtonSystemItem)type target:(id)target action:(SEL)buttonAction {
    if (type == UIBarButtonSystemItemDone) {
        return [[UIBarButtonItem alloc]initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:target action:buttonAction];
    }else if (type == UIBarButtonSystemItemCancel){
        return [[UIBarButtonItem alloc]initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:target action:buttonAction];
    }else{
        return [[UIBarButtonItem alloc]initWithBarButtonSystemItem:type target:target action:buttonAction];
    }
}
@end
