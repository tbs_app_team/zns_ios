//
//  YYMacros.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/24/15.
//  Copyright © 2015 czwen. All rights reserved.
//
#import "ZNSError.h"

#ifndef YYMacros_h
#define YYMacros_h

typedef void(^voidBlock)();
typedef void(^stringBlock)(NSString *result);
typedef void(^boolBlock)(BOOL boolen);
typedef void(^indexBlock)(NSInteger index);
typedef void(^errorBlock)(NSError *error);
typedef void(^numberBlock)(NSNumber *result);
typedef void(^arrayWithErrorBlock)(NSArray *results,NSError *error);
typedef void(^arrayBlock)(NSArray *results);
typedef void(^idBlock)(id model);
typedef UITableViewCell*(^cellBlock)(UITableViewCell *results ,id model);
typedef BOOL(^boolReturnBlock)();
typedef void(^dicBlock)(NSDictionary *dic);


typedef void(^requestErrorBlock)(ZNSError *error);
typedef void(^requestSuccessBlock)(id response);
typedef void(^requestProgressBlock)(CGFloat percentage);


#define STRING_OR_EMPTY(A)  ({ __typeof__(A) __a = (A); __a ? __a : @""; })

typedef enum : NSUInteger {
    AuthenticationControllerModeNormal,
    AuthenticationControllerModeOffline,
    AuthenticationControllerModeTemptask,
    AuthenticationControllerModeWorkBill,
    AuthenticationControllerModeWorkPatrol,
} AuthenticationControllerMode;

typedef enum :NSUInteger {
    SystemSettingMode,
    AddLockMode,
}GetDeveceLocationMode;

typedef enum : NSUInteger {
    ZNSUploadTypeRegister,
    ZNSUploadTypeOfflineApply,
    ZNSUploadTypeTempTaskApply,
} ZNSUploadType;

typedef enum : NSUInteger {
    ZNSImageTypePortrait,
    ZNSImageTypeIdCard,
} ZNSImageType;

typedef enum:NSInteger{
    WorkQueryModeBill,
    WorkQueryModePatrol,
}WorkQueryMode;

#endif /* YYMacros_h */
