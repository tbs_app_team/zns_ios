//
//  CollectLockTableViewCell.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/5/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "CollectLockTableViewCell.h"
@interface CollectLockTableViewCell()
@property (nonatomic,weak) IBOutlet UIButton *addButton;
@property (nonatomic,weak) IBOutlet UILabel *lockNameLabel;
@end

@implementation CollectLockTableViewCell

- (void)awakeFromNib
{
     [super awakeFromNib];
    // Initialization code
    [self.addButton bs_configureAsSuccessStyle];
}

- (void)setLock:(ZNSLock *)lock{
    _lock = lock;
    [self.lockNameLabel setText:lock.name];
}

- (IBAction)add:(id)sender{
    if (self.addBlock) {
        self.addBlock();
    }
}

@end
