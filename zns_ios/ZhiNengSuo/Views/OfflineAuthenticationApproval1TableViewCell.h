//
//  OfflineAuthenticationApproval1TableViewCell.h
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/28.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSOffline.h"
@interface OfflineAuthenticationApproval1TableViewCell : UITableViewCell
@property (nonatomic,strong) ZNSOffline *offline;
@property (nonatomic,copy) indexBlock actionBlock;
@end
