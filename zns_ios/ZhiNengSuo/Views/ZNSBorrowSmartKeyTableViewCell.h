//
//  ZNSBorrowSmartKeyTableViewCell.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 16/3/20.
//  Copyright © 2016 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSBorrowSmartkey.h"
@interface ZNSBorrowSmartKeyTableViewCell : UITableViewCell

@property (nonatomic,strong) ZNSBorrowSmartkey *model;
@end
