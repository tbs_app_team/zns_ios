//
//  OnlineAuthenticationBtLockTableViewCell.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/11/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "OnlineAuthenticationBtLockTableViewCell.h"
@interface OnlineAuthenticationBtLockTableViewCell()
@end

@implementation OnlineAuthenticationBtLockTableViewCell

- (void)awakeFromNib
{
     [super awakeFromNib];
    // Initialization code
    [self.connectButton bs_configureAsSuccessStyle];
    [self.unlockButton bs_configureAsPrimaryStyle];
}

- (IBAction)unlock:(id)sender{
    if (self.unlockBlock) {
        self.unlockBlock();
    }
}

- (IBAction)connect:(id)sender{
    if (self.connectBlock) {
        self.connectBlock();
    }
}
@end
