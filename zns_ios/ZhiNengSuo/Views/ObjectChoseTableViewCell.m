//
//  ObjectChoseTableViewCell.m
//  ZhiNengSuo
//
//  Created by ekey on 16/11/17.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "ObjectChoseTableViewCell.h"

@implementation ObjectChoseTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code 
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)seleBtnOnClick:(UIButton *)sender {
    if (self.seleBlock) {
        self.seleBlock();
    }
}

@end
