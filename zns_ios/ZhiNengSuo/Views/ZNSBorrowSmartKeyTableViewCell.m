//
//  ZNSBorrowSmartKeyTableViewCell.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 16/3/20.
//  Copyright © 2016 czwen. All rights reserved.
//

#import "ZNSBorrowSmartKeyTableViewCell.h"
@interface ZNSBorrowSmartKeyTableViewCell()
@property (nonatomic,weak) IBOutlet UILabel *label1;
@property (nonatomic,weak) IBOutlet UILabel *label2;
@property (nonatomic,weak) IBOutlet UILabel *label3;
@property (nonatomic,weak) IBOutlet UILabel *label4;
@property (nonatomic,weak) IBOutlet UILabel *label5;
@end

@implementation ZNSBorrowSmartKeyTableViewCell

- (void)awakeFromNib
{
     [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(ZNSBorrowSmartkey *)model{
    _model = model;
    [self.label1 setText:[NSString stringWithFormat:@"钥匙名称：%@",model.sname]];
    [self.label2 setText:[NSString stringWithFormat:@"借用人：%@",model.borrow_by]];
    [self.label3 setText:[NSString stringWithFormat:@"借用时间：%@",model.create_at]];
    [self.label4 setText:[NSString stringWithFormat:@"归还登记人：%@",STRING_OR_EMPTY(model.create_username)]];
    [self.label5 setText:[NSString stringWithFormat:@"归还时间：%@",STRING_OR_EMPTY(model.return_at)]];
}
@end
