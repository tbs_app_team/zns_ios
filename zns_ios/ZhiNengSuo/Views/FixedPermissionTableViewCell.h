//
//  FixedPermissionTableViewCell.h
//  ZhiNengSuo
//
//  Created by ekey on 16/6/22.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSTempTask.h"
@interface FixedPermissionTableViewCell : UITableViewCell


@property (nonatomic,strong) ZNSTempTask *myTask;
@property (nonatomic,copy) voidBlock downBlock;
@property (nonatomic,copy) voidBlock returnBlock;

- (void)setBtnHidden:(BOOL)hidden;

@end
