//
//  TempPermissionApprovalTableViewCell.h
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/28.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSTempTask.h"

@interface TempPermissionApprovalTableViewCell : UITableViewCell

@property (nonatomic ,strong) ZNSTempTask * tempTask;
@property (nonatomic ,strong) indexBlock indexBlock;

@end
