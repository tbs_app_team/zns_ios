//
//  GetLocationTableViewCell.h
//  ZhiNengSuo
//
//  Created by ekey on 16/4/28.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSObject.h"
@interface GetLocationTableViewCell : UITableViewCell

@property (nonatomic,copy) voidBlock getBlock;
@property (nonatomic,copy) voidBlock upBlock;
@property (nonatomic,strong)  ZNSObject *object;

@end
