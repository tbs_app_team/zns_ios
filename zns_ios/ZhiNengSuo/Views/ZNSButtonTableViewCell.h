//
//  ZNSButtonTableViewCell.h
//  ZhiNengSuo
//
//  Created by ekey on 16/11/17.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZNSButtonTableViewCell : UITableViewCell

@property (nonatomic, copy) voidBlock saveBlock;
@property (nonatomic, copy) voidBlock deleBlock;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;

@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

@end
