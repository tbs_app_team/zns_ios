//
//  ZNSButtonTableViewCell.m
//  ZhiNengSuo
//
//  Created by ekey on 16/11/17.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "ZNSButtonTableViewCell.h"

@implementation ZNSButtonTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)saveBtnOnClick:(id)sender {
    if (self.saveBlock) {
        self.saveBlock();
    }
}

- (IBAction)deleBtnOnClick:(id)sender {
    if (self.deleBlock) {
        self.deleBlock();
    }
}

@end
