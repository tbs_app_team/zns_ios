//
//  OperationCollectionViewCell.m
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/27.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import "OperationCollectionViewCell.h"

@implementation OperationCollectionViewCell
- (void)awakeFromNib{
    [super awakeFromNib];
    self.badgeLabel.layer.cornerRadius = 11;
    self.badgeLabel.layer.masksToBounds = YES;
    self.badgeLabel.hidden = YES;
}
@end
