//
//  TempPermissionApprovalHistoryCell.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/29/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "TempPermissionApprovalHistoryCell.h"

@implementation TempPermissionApprovalHistoryCell{
    
    IBOutlet UILabel *tempInfoLabel;
    IBOutlet UILabel *nameLabel;
    
    IBOutlet UILabel *beginAtLabel;
    IBOutlet UILabel *endAtLabel;
    IBOutlet UILabel *objectsLabel;
    IBOutlet UILabel *statusLabel;
    IBOutlet UILabel *verifyByLabel;
}

- (void)awakeFromNib {
    // Initialization code
     [super awakeFromNib];
}

- (void)setTempTask:(ZNSTempTask *)tempTask {
    tempInfoLabel.text = tempTask.name;
    nameLabel.text = tempTask.apply_by;
    beginAtLabel.text = [NSString formatTimeString:tempTask.begin_at];
    endAtLabel.text = [NSString formatTimeString:tempTask.end_at];
    [objectsLabel setText:[[tempTask.objects valueForKey:@"name"] componentsJoinedByString:@","]];
    [statusLabel setText:tempTask.status];
    [verifyByLabel setText:tempTask.verify_by];
}
@end
