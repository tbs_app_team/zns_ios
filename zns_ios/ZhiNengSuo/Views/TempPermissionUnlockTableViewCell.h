//
//  TempPermissionUnlockTableViewCell.h
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/29.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSTempTask.h"

@interface TempPermissionUnlockTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnDownload;
@property (weak, nonatomic) IBOutlet UIButton *btnReturn;

@property (nonatomic ,strong) ZNSTempTask * tempTask;
@property (nonatomic, copy) voidBlock getLocationBlock;
@property (nonatomic, copy) voidBlock downBlock;
@property (nonatomic, copy) voidBlock returnBlock;
@end
