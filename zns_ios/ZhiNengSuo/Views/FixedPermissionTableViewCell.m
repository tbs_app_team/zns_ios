//
//  FixedPermissionTableViewCell.m
//  ZhiNengSuo
//
//  Created by ekey on 16/6/22.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "FixedPermissionTableViewCell.h"


@implementation FixedPermissionTableViewCell{
    
    __weak IBOutlet UILabel *missionNameLabel;
    
    __weak IBOutlet UILabel *operatingBeginTimeLabel;
    __weak IBOutlet UILabel *operatingEndTimeLabel;
    __weak IBOutlet UIButton *downloadBtn;
    __weak IBOutlet UIButton *returnBtn;
}
- (IBAction)downBtnClick:(id)sender {
    if (self.downBlock) {
        self.downBlock();
    }
}
- (IBAction)returnBtnClick:(id)sender {
    if (self.returnBlock) {
        self.returnBlock();
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setMyTask:(ZNSTempTask *)myTask{
    [missionNameLabel setText:myTask.name];
    [operatingBeginTimeLabel setText:[NSString stringWithFormat:@"%@分钟",myTask.limited]];
    [operatingEndTimeLabel setText:myTask.apply_username];
    [downloadBtn bs_configureAsPrimaryStyle];
    [returnBtn bs_configureAsPrimaryStyle];
}

- (void)setBtnHidden:(BOOL)hidden
{
    [downloadBtn setHidden:hidden];
    [returnBtn setHidden:hidden];
}

@end
