//
//  HistoryTableViewCell.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 16/1/19.
//  Copyright © 2016 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSHistory.h"
@interface HistoryTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UILabel *objectNameLabel;
@property (nonatomic,weak) IBOutlet UILabel *userNameLabel;
@property (nonatomic,weak) IBOutlet UILabel *dateNameLabel;
@property (nonatomic,weak) IBOutlet UILabel *resultLabel;
@property (nonatomic,strong) ZNSHistory *history;

@end