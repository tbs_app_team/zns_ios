//
//  TempPermissionApprovalTableViewCell.m
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/28.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import "TempPermissionApprovalTableViewCell.h"

@interface TempPermissionApprovalTableViewCell ()

@property (strong , nonatomic) IBOutlet UIImageView *photoImageView;
@property (strong , nonatomic) IBOutlet UIImageView *IdCardImageView;

@end

@implementation TempPermissionApprovalTableViewCell {
    
    IBOutlet UILabel *tempInfoLabel;
    IBOutlet UILabel *nameLabel;
    IBOutlet UIButton *disagreeButton;
    IBOutlet UIButton *agreeButton;
    
    IBOutlet UILabel *beginAtLabel;
    IBOutlet UILabel *endAtLabel;
    IBOutlet UILabel *objectsLabel;
}

- (void)awakeFromNib {
     [super awakeFromNib];
    // Initialization code
    [disagreeButton bs_configureAsDangerStyle];
    [agreeButton bs_configureAsSuccessStyle];
    @weakify(self);
    [self.photoImageView setTapActionWithBlock:^{
        @strongify(self);
        [self.photoImageView showImage];
    }];
    
    [self.IdCardImageView setTapActionWithBlock:^{
        @strongify(self);
        [self.IdCardImageView showImage];
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setTempTask:(ZNSTempTask *)tempTask {
    tempInfoLabel.text = tempTask.name;
    nameLabel.text = tempTask.apply_by;
    NSURL * imgUrl = [ZNSAPITool imageUrlOfImageType:ZNSImageTypePortrait type:ZNSUploadTypeTempTaskApply forId:tempTask.tid username:tempTask.apply_username];
    [self.photoImageView yy_setImageWithURL:imgUrl placeholder:[UIImage imageNamed:@"iconfont-tupian"]options:YYWebImageOptionIgnoreDiskCache completion:nil];
    imgUrl = [ZNSAPITool imageUrlOfImageType:ZNSImageTypeIdCard type:ZNSUploadTypeTempTaskApply forId:tempTask.tid username:tempTask.apply_username];
    [self.IdCardImageView yy_setImageWithURL:imgUrl placeholder:[UIImage imageNamed:@"iconfont-tupian"]options:YYWebImageOptionIgnoreDiskCache completion:nil];
    
    beginAtLabel.text = [NSString formatTimeString:tempTask.begin_at];
    endAtLabel.text = [NSString formatTimeString:tempTask.end_at];
    [objectsLabel setText:[[tempTask.objects valueForKey:@"name"] componentsJoinedByString:@","]];

}

- (IBAction)didTapButtonAction:(UIButton *)sender {
    if (self.indexBlock) {
        self.indexBlock(sender.tag);
    }
}

@end
