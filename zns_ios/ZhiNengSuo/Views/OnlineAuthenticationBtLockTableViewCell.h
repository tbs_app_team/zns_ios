//
//  OnlineAuthenticationBtLockTableViewCell.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/11/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnlineAuthenticationBtLockTableViewCell : UITableViewCell
@property (nonatomic,assign) BOOL valid;

@property (nonatomic,weak) IBOutlet UILabel *nameLabel;
@property (nonatomic,weak) IBOutlet UILabel *statusLabel;
@property (nonatomic,copy) voidBlock connectBlock;
@property (nonatomic,copy) voidBlock unlockBlock;
@property (nonatomic,weak) IBOutlet UIButton *connectButton;
@property (weak, nonatomic) IBOutlet UIImageView *doorStateImageView;
@property (weak, nonatomic) IBOutlet UIImageView *lockStateImageView;
@property (nonatomic,weak) IBOutlet UIButton *unlockButton;
@end
