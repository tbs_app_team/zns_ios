//
//  OfflineAuthenticationApproval2TableViewCell.h
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/28.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSOffline.h"
@interface OfflineAuthenticationApproval2TableViewCell : UITableViewCell
@property (nonatomic,strong) ZNSOffline *offline;

@end
