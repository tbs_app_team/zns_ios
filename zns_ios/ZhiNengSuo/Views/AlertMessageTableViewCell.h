//
//  AlertMessageTableViewCell.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 16/1/24.
//  Copyright © 2016 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertMessageTableViewCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UILabel *dateLabel;
@property (nonatomic,weak) IBOutlet UILabel *messageLabel;

@end
