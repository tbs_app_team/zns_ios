//
//  OfflineAuthenticationApproval1TableViewCell.m
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/28.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import "OfflineAuthenticationApproval1TableViewCell.h"

@interface OfflineAuthenticationApproval1TableViewCell ()

@property (strong , nonatomic) IBOutlet UIImageView *photoImageView;
@property (strong , nonatomic) IBOutlet UIImageView *IdCardImageView;

@end

@implementation OfflineAuthenticationApproval1TableViewCell {
    
    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *taskNameLaber;
    IBOutlet UILabel *taskTimeLabel;
    IBOutlet UIButton *disagreeButton;
    IBOutlet UIButton *agreeButton;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [disagreeButton bs_configureAsDangerStyle];
    [agreeButton bs_configureAsSuccessStyle];
    @weakify(self);
    [self.photoImageView setTapActionWithBlock:^{
        @strongify(self);
        [self.photoImageView showImage];
    }];
    
    [self.IdCardImageView setTapActionWithBlock:^{
        @strongify(self);
        [self.IdCardImageView showImage];
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)didTapButtonAction:(UIButton *)sender {
    NSLog(@"didTapTag:%ld",(long)sender.tag);
    if (self.actionBlock) {
        self.actionBlock(sender.tag);
    }
}

- (void)setOffline:(ZNSOffline *)offline{
    _offline = offline;
    [nameLabel setText:offline.apply_by];
    taskNameLaber.text = @"日常巡逻";
    taskTimeLabel.text = [NSString stringWithFormat:@"%@分钟",offline.duration];
    NSURL * imgUrl = [ZNSAPITool imageUrlOfImageType:ZNSImageTypePortrait type:ZNSUploadTypeOfflineApply forId:offline.oid username:offline.apply_username];
    [self.photoImageView yy_setImageWithURL:imgUrl placeholder:[UIImage imageNamed:@"iconfont-tupian"]options:YYWebImageOptionIgnoreDiskCache completion:nil];
    imgUrl = [ZNSAPITool imageUrlOfImageType:ZNSImageTypeIdCard type:ZNSUploadTypeOfflineApply forId:offline.oid username:offline.apply_username];
    [self.IdCardImageView yy_setImageWithURL:imgUrl placeholder:[UIImage imageNamed:@"iconfont-tupian"]options:YYWebImageOptionIgnoreDiskCache completion:nil];
}


@end
