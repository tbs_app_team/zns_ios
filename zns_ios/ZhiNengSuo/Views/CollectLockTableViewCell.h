//
//  CollectLockTableViewCell.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/5/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSLock.h"
@interface CollectLockTableViewCell : UITableViewCell
@property (nonatomic,strong) ZNSLock *lock;
@property (nonatomic,copy) voidBlock addBlock;
@end
