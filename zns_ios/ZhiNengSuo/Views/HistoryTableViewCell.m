//
//  HistoryTableViewCell.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 16/1/19.
//  Copyright © 2016 czwen. All rights reserved.
//

#import "HistoryTableViewCell.h"
@interface HistoryTableViewCell()

@end

@implementation HistoryTableViewCell

- (void)awakeFromNib
{
     [super awakeFromNib];
    // Initialization code
}

- (void)setHistory:(ZNSHistory *)history{
    _history = history;
    [self.objectNameLabel setText:history.oname];
    [self.userNameLabel setText:[NSString stringWithFormat:@"操作人：%@",STRING_OR_EMPTY(history.unlock_by)]];
    [self.dateNameLabel setText:[NSString stringWithFormat:@"操作日期：%@",STRING_OR_EMPTY(history.unlock_at)]];
    
    if ([history.result isEqualToString:@"开锁指令失败"]||[history.result isEqualToString:@"越权未遂"]) {
        [self.resultLabel setTextColor:[UIColor redColor]];
    }else{
        [self.resultLabel setTextColor:[UIColor greenColor]];
    }
    [self.resultLabel setText:history.result];
}
@end
