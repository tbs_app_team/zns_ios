//
//  RegisterUserApprovalTableViewCell.m
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/28.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import "RegisterUserApprovalTableViewCell.h"

@interface RegisterUserApprovalTableViewCell ()

@property (strong , nonatomic) IBOutlet UIImageView *photoImageView;
@property (strong , nonatomic) IBOutlet UIImageView *IdCardImageView;

@end

@implementation RegisterUserApprovalTableViewCell {
    
    IBOutlet UILabel *proposerLabel;
    IBOutlet UILabel *departmentLabel;
    IBOutlet UILabel *phoneLabel;
    IBOutlet UILabel *ownerInfoLabel;
    IBOutlet UILabel *registerTimeLabel;
    IBOutlet UILabel *roleLabel;
    IBOutlet UIButton *setRoleButton;
    IBOutlet UIButton *agreeButton;
    IBOutlet UIButton *disAgreeButton;
}

- (void)awakeFromNib {
     [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [setRoleButton bs_configureAsPrimaryStyle];
    [agreeButton bs_configureAsSuccessStyle];
    [disAgreeButton bs_configureAsDangerStyle];
    @weakify(self);
    [self.photoImageView setTapActionWithBlock:^{
        @strongify(self);
        [self.photoImageView showImage];
    }];
    
    [self.IdCardImageView setTapActionWithBlock:^{
        @strongify(self);
        [self.IdCardImageView showImage];
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setUser:(ZNSUser *)user {
    proposerLabel.text = user.name;
    departmentLabel.text = user.section;
    phoneLabel.text = user.username;
    ownerInfoLabel.text = user.memo.length>0?user.memo:@" ";
    registerTimeLabel.text = [NSString formatTimeString:user.reg_at];
    
    roleLabel.text = [[user.role valueForKeyPath:@"name"] componentsJoinedByString:@"，"];
    if (roleLabel.text.length == 0) {
        roleLabel.text = @" ";
    }
    
    NSURL * imgUrl = [ZNSAPITool imageUrlOfImageType:ZNSImageTypePortrait type:ZNSUploadTypeRegister forId:nil username:user.username];
    [self.photoImageView yy_setImageWithURL:imgUrl placeholder:[UIImage imageNamed:@"iconfont-tupian"]];
    imgUrl = [ZNSAPITool imageUrlOfImageType:ZNSImageTypeIdCard type:ZNSUploadTypeRegister forId:nil username:user.username];
    [self.IdCardImageView yy_setImageWithURL:imgUrl placeholder:[UIImage imageNamed:@"iconfont-tupian"]];
}

- (IBAction)didTapButtonAction:(UIButton *)sender {
    if (self.indexBlock) {
        self.indexBlock(sender.tag);
    }
}

@end
