//
//  ZNSLabelTextFieldTableViewCell.m
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/25/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import "ZNSLabelTextFieldTableViewCell.h"

@implementation ZNSLabelTextFieldTableViewCell

- (void)awakeFromNib
{
     [super awakeFromNib];
    // Initialization code
    self.textField.delegate = self;
    
    if (self.showToolBar) {
        [self setupToolBar];
    }

}
- (IBAction)fieldDidEndOnExit:(UITextField *)sender {
    [sender resignFirstResponder];
}




- (void)setShowToolBar:(BOOL)showToolBar{
    _showToolBar = showToolBar;
    if (showToolBar) {
        [self setupToolBar];
    }else{
        self.textField.inputAccessoryView = nil;
    }
}

- (void)setupToolBar{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"采码" style:UIBarButtonItemStyleDone target:self action:@selector(toolBarAction)],
                           nil];
    [numberToolbar sizeToFit];
    self.textField.inputAccessoryView = numberToolbar;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (self.textBlcok) {
        self.textBlcok([textField.text stringByReplacingCharactersInRange:range withString:string]);
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (self.textBlcok) {
        self.textBlcok(textField.text);
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (self.canEditBlock) {
        return self.canEditBlock();
    }
    return YES;
}

- (void)toolBarAction{
    if (self.toolBarActionBlock) {
        self.toolBarActionBlock();
    }
}

@end
