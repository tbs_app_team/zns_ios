//
//  GetLocationTableViewCell.m
//  ZhiNengSuo
//
//  Created by ekey on 16/4/28.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import "GetLocationTableViewCell.h"

@interface GetLocationTableViewCell(){
    
    __weak IBOutlet UILabel *nameLabel;
    __weak IBOutlet UILabel *lngLable;
    __weak IBOutlet UILabel *locationLabel;
}
- (IBAction)getLocation:(id)sender;


@end

@implementation GetLocationTableViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setObject:(ZNSObject *)object{
    [nameLabel setText:object.name];
    
    [nameLabel setLineBreakMode: NSLineBreakByCharWrapping];
    nameLabel.numberOfLines = 0;
    [nameLabel sizeToFit];
//    [self sizeToFit];
    [locationLabel setText:object.lat];
    [lngLable setText:object.lng];

}
- (IBAction)getLocation:(id)sender {
    if (self.getBlock) {
        self.getBlock();
    }
}


@end
