//
//  TempPermissionApprovalHistoryCell.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 12/29/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZNSTempTask.h"
@interface TempPermissionApprovalHistoryCell : UITableViewCell
@property (nonatomic ,strong) ZNSTempTask * tempTask;

@end
