//
//  OfflineAuthenticationApproval2TableViewCell.m
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/28.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import "OfflineAuthenticationApproval2TableViewCell.h"

@implementation OfflineAuthenticationApproval2TableViewCell {
    
    IBOutlet UILabel *taskNameLabel;
    IBOutlet UILabel *applyInfoLabel;
    IBOutlet UILabel *approvalLabel;
    IBOutlet UILabel *durationLabel;
}

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setOffline:(ZNSOffline *)offline{
    _offline = offline;
    applyInfoLabel.text = [NSString stringWithFormat:@"%@ %@",offline.apply_by,[NSString formatTimeString:offline.apply_at]];
//    taskNameLabel.text = @"日常巡逻";
    approvalLabel.text = [NSString stringWithFormat:@"%@ %@",offline.status,STRING_OR_EMPTY([NSString formatTimeString:offline.action_at])];
    durationLabel.text = [NSString stringWithFormat:@"%@小时",@(offline.duration.integerValue/60)];

}

@end
