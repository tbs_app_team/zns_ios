//
//  OperationCollectionViewCell.h
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/27.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OperationCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *operateImageView;
@property (strong, nonatomic) IBOutlet UILabel *operateLabel;
@property (strong, nonatomic) IBOutlet UILabel *badgeLabel;

@end
