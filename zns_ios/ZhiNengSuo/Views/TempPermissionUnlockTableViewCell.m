//
//  TempPermissionUnlockTableViewCell.m
//  ZhiNengSuo
//
//  Created by 郑思越 on 15/11/29.
//  Copyright © 2015年 czwen. All rights reserved.
//

#import "TempPermissionUnlockTableViewCell.h"

@implementation TempPermissionUnlockTableViewCell {
    
    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *timeRangeLabel;
    IBOutlet UILabel *approveNameLabel;
    IBOutlet UILabel *approveTimeLabel;
    IBOutlet UIButton *locationBtn;
}
- (IBAction)getLocation:(id)sender {
    if (self.getLocationBlock) {
        self.getLocationBlock();
    }
}

- (void)awakeFromNib {
    // Initialization code
     [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setTempTask:(ZNSTempTask *)tempTask {
    nameLabel.text = tempTask.name.length == 0?@" ":tempTask.name;
    timeRangeLabel.text = [NSString stringWithFormat:@"%@至%@",[NSString formatTimeString:tempTask.begin_at],[NSString formatTimeString:tempTask.end_at]];
    approveTimeLabel.text = [NSString formatTimeString:tempTask.verify_at].length == 0?@" ":[NSString formatTimeString:tempTask.verify_at];
    approveNameLabel.text = tempTask.verify_by.length == 0?@" ":tempTask.verify_by;
//    [locationBtn setHidden:YES];
}
- (IBAction)onReturnBtnClicked:(id)sender {
    if (self.returnBlock) {
        self.returnBlock();
    }
   
}
- (IBAction)onDownBtnClicked:(id)sender {
    if (self.downBlock) {
        self.downBlock();
    }
}


@end
