//
//  ObjectChoseTableViewCell.h
//  ZhiNengSuo
//
//  Created by ekey on 16/11/17.
//  Copyright © 2016年 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ObjectChoseTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIButton *selectedBtn;
@property (nonatomic, copy) voidBlock seleBlock;
@end
