//
//  ZNSLabelTextFieldTableViewCell.h
//  ZhiNengSuo
//
//  Created by ChenZhiWen on 11/25/15.
//  Copyright © 2015 czwen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZNSLabelTextFieldTableViewCell : UITableViewCell<UITextFieldDelegate>

@property (nonatomic,weak) IBOutlet UILabel *label;
@property (nonatomic,weak) IBOutlet UITextField *textField;
@property (nonatomic,copy) stringBlock textBlcok;
@property (nonatomic,copy) boolReturnBlock canEditBlock;
@property (nonatomic,copy) voidBlock toolBarActionBlock;
@property (nonatomic,assign) BOOL showToolBar;
@end
